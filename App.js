import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {Provider} from 'react-native-paper';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import API from './Constant/API';
import FlashPg from './Components/Authen/FlashPg';
import MainStack from './MainStack';
import StackNavigator from './StackNavigator';
import AsyncStorage from '@react-native-async-storage/async-storage';
import PushNotification from 'react-native-push-notification';
import messaging from '@react-native-firebase/messaging';
import {Root} from 'native-base';
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {isLoading: true, check_email: ''};
  }
  performTimeConsumingTask = async () => {
    return new Promise((resolve) =>
      setTimeout(() => {
        resolve('result');
      }, 1000),
    );
  };

  async checkUser() {
    try {
      const emailAndId = await AsyncStorage.getItem('check_user');
      const parse = JSON.parse(emailAndId);
      this.setState({check_email: parse.email});
      console.log(parse, 'chkkkk parse');
      try {
        const apptoken = await AsyncStorage.getItem('mobile_token');
        // console.log(
        //   apptoken,
        //   '---------------------------------<<>><<<>><<<<<<<>>><<><<><',
        // );
        const res = await this.api.App_token_submit(parse.token, apptoken);
        console.log(res, 'TOKEN SAVED OR -----------------');
      } catch (er) {
        console.log(er, 'error feting mobile token');
      }
    } catch (error) {
      this.setState({check_email: ''});
      console.log(error, 'ERR');
    }
  }
  async componentDidMount() {
    console.log('<=======COMPONENT MOUNTED=======>');

    // Push Notification Code Background notif k lie
    PushNotification.configure({
      onRegister: async function (token) {
        console.log('TOKEN for App_id:', token);
        try {
          await AsyncStorage.setItem('mobile_token', token.token);
        } catch (err) {
          console.log(err, 'token not saved');
        }
      },
      onNotification: function (notification) {
        console.log('NOTIFICATION:', notification);
        messaging().setBackgroundMessageHandler(async (remoteMessage) => {
          console.log('Message handled in the background!', remoteMessage);
        });
      },
      onAction: function (notification) {
        messaging().setBackgroundMessageHandler(async (remoteMessage) => {
          console.log('Message handled in the background!', remoteMessage);
        });
      },
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      popInitialNotification: true,
      requestPermissions: true,
    });

    // chanal for local
    PushNotification.createChannel(
      {
        channelId: '1',
        channelName: 'Notification',
        channelDescription: 'A channel',
        playSound: true,
        soundName: 'default',
        importance: 4,
        vibrate: true,
      },
      (created) => {
        console.log('channel created', created);
      },
    );

    // for Foreground Notif
    messaging().onMessage((message) => {
      this.test(message.notification);
      console.log(message.notification, 'Recieve in forgorund');
      // PushNotification.getChannels((e) => {
      //   console.log(
      //     e,
      //     '---------------EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE',
      //   );
      //   e.forEach((element) => {
      //     if (element == '1') {
      //       PushNotification.localNotification({
      //         channelId: element,
      //         title: message.notification.title,
      //         message: message.notification.message,
      //         visibility: 'public',
      //       });
      //     }
      //   });
      // });
    });

    // for testing Local notif
    // this.test();
    this.checkUser();
    const data = await this.performTimeConsumingTask();

    if (data !== null) {
      this.setState({isLoading: false});
    }
  }
  test = (params) => {
    PushNotification.getChannels((e) => {
      e.forEach((element) => {
        if (element == '1') {
          PushNotification.localNotification({
            channelId: element,
            title: params.title,
            message: params.body,
          });
        }
      });
    });
  };
  render() {
    console.log(this.state.check_email, '---------emal');
    if (this.state.isLoading) {
      return <FlashPg />;
    } else {
      return (
        <Root>
          <Provider>
            <StackNavigator check={this.state.check_email} />
          </Provider>
        </Root>
      );
    }
  }
}
