import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/Entypo';
import {
  View,
  TextInput,
  Button,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import {Container, Content} from 'native-base';
import * as Animatable from 'react-native-animatable';
export default class FlashPg extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#15b7da',
        }}>
        <Animatable.Image
          animation="zoomIn"
          duration={3000}
          source={require('../../assets/logo.png')}
          style={{width: 200, height: 200}}
          resizeMode="stretch"
        />
      </View>
      // <View style={styles.viewStyle}>
      //   <View style={styles.txt}>
      //     <Text style={{fontSize: 23, fontWeight: 'bold'}}>Wellcome to</Text>
      //     <Text
      //       style={{
      //         fontSize: 20,
      //         color: 'cadetblue',
      //         fontWeight: 'bold',
      //         marginTop: '3%',
      //         marginRight: '2%',
      //       }}>
      //       <Icon name="location-pin" color={'cadetblue'} size={29} />
      //       iProperty.com.my
      //     </Text>
      //     <Text style={{fontSize: 15, marginTop: '3%'}}>
      //       Search for your dream home
      //     </Text>
      //     <Text style={{fontSize: 15}}>now</Text>
      //   </View>
      //   <Image
      //     style={styles.stretch}
      //     source={{
      //       uri:
      //         'https://i.pinimg.com/originals/8c/b3/f8/8cb3f8ffa63b856906f0cc0ac5819a98.png',
      //     }}
      //   />
      // </View>
    );
  }
}

const styles = StyleSheet.create({
  stretch: {
    width: '100%',
    height: '44%',
    marginTop: '28%',
    alignItems: 'center',
  },
  viewStyle: {
    backgroundColor: 'white',
    height: '100%',
  },
  txt: {
    alignItems: 'center',
    marginTop: '20%',
  },
});

{
  /* <Icon name="chevron-back-outline" color={'purple'} size={17} /> */
}
