import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  View,
  TextInput,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import API from '../../Constant/API';
import * as color from '../../Constant/Colors';
import { Button, Card } from 'native-base';
import { ActivityIndicator } from 'react-native-paper';
export default class ForgotPass extends Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      code: '',
      loader: false,
    };
  }
  async Verify() {
    this.setState({ loader: true });
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    console.log(this.state.code, 'mailll sst');
    const response = await this.api.forgt_pass_veri(
      this.state.code,
      parse.token,
    );
    console.log(response, '-----foggot ');

    if (response.status == true) {
      this.props.navigation.navigate('paswordVerification', {
        email: this.state.code,
      });
      this.setState({ loader: false });
    } else {
      alert(response.message)
      this.setState({ loader: false });
    }
  }
  render() {
    return (
      <View style={{ padding: '5%', justifyContent: 'center' }}>
        <Text
          onPress={() => this.props.navigation.goBack()}
          style={{ fontSize: 18, padding: 12 }}>
          <Icon name="chevron-back-outline" color={'cadetblue'} size={19} />
          Change Password
        </Text>

        <View style={{ paddingTop: '5%', marginTop: '52%' }}>
          <Text style={{ fontWeight: 'bold', marginBottom: '2%' }}>
            Enter Your Email:
          </Text>
          <TextInput
            onChangeText={(txt) => this.setState({ code: txt })}
            style={{
              height: 40,
              borderColor: 'cadetblue',
              borderRightWidth: 1,
              borderBottomWidth: 1,
            }}
            placeholder="Email Address"
            keyboardType="email-address"
          // value={value}
          />
        </View>

        <View style={styles.btn}>
          <Button
            onPress={() => this.Verify()}
            style={{
              backgroundColor: 'black',
              width: 160,
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            {this.state.loader ? (
              <ActivityIndicator color="white" />
            ) : (
              <Text style={{ color: 'white' }}>VERIFY</Text>
            )}
          </Button>
        </View>

        {/* <View style={styles.card}></View> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  stretch: {
    width: '100%',
    height: '30%',
    resizeMode: 'stretch',
    marginTop: '8%',
  },
  card: {
    padding: 10,
    right: 2,
    borderColor: 'cadetblue',
    // borderWidth: 0.1,
    width: '90%',
    alignSelf: 'center',
    elevation: 4,
    padding: '10%',
    borderRadius: 30,
    marginTop: '5%',
    backgroundColor: 'white',
    borderWidth: 0.5,
  },
  btn: {
    paddingTop: '15%',
  },
});

{
  /* <Icon name="chevron-back-outline" color={'purple'} size={17} /> */
}
