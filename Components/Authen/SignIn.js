import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  View,
  TextInput,
  StyleSheet,
  Image,
  Text,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import API from '../../Constant/API';
import {Dialog, Portal, Button} from 'react-native-paper';
import ValidationComponent from 'react-native-form-validator';
import AsyncStorage from '@react-native-async-storage/async-storage';

import * as color from '../../Constant/Colors';
export default class SignIn extends ValidationComponent {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      email: '',
      password: '',
      hide_pass: true,
      email_req: '',
      icon: 'eye-off',
      signin_loader: false,
      visible: false,
      data: '',
    };
  }
  showDialog = () => this.setState({visible: true});
  handle_ok() {
    this.logout(), this.setState({visible: false});
  }
  async logout() {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    console.log(parse.token);

    const response = await this.api.logout(parse.token);
    console.log(response, 'RESPONSE');

    if (response.data[0].Logout) {
      this._storeUserData('', '', '', '');
      this.props.navigation.reset({
        index: 0,
        routes: [{name: 'SignIn'}],
      });
      // this.props.navigation.navigate('SignIn');
    } else {
      alert('error');
    }
  }

  _onSubmit() {
    // Call ValidationComponent validate method
    this.validate({
      email: {emsail: true},
      email_req: {required: true},
      password: {required: true},
    });
    if (this.isFormValid()) {
      this.user_Login(this.state.email, this.state.password);
      console.log('ok');
    }
  }
  _storeUserData = async (_name, _email, _id, _usertype, img, _token) => {
    try {
      let obj = {
        name: _name,
        email: _email,
        id: _id,
        user_type: _usertype,
        draw_img: img,
        token: _token,
      };

      await AsyncStorage.setItem('user_data', JSON.stringify(obj));
    } catch (error) {
      alert(error);
    }
    console.log(_name, _email, _id, _token, '---> User Data Stored');
  };

  async user_Login(_email, _password) {
    this.setState({signin_loader: true});
    try {
      console.log(_email, _password, '---> CHECK');
      const response = await this.api.login(_email, _password);
      console.log(response, 'RESPONSE --------signin');

      if (response.data.id) {
        //console.log('if');
        this._storeUserData(
          response.data.name,
          response.data.email,
          response.data.id,
          response.data.user_type_id,
          response.data.image,
          response.token,
        );
        try {
          const apptoken = await AsyncStorage.getItem('mobile_token');
          console.log(
            apptoken,
            response.token,
            '---------------------------------<<>><<<>><<<<<<<>>><<><<><',
          );
          const res = await this.api.App_token_submit(apptoken,response.token);
          console.log(res, 'TOKEN SAVED OR NOT');
        } catch (er) {
          console.log(er, 'error feting mobile token');
        }
        if (response.data.user_type_id == '1') {
          if (response.data.approval == 'pending') {
            console.log('pending');
            this.showDialog();
          } else if (response.data.approval == 'not applied') {
            console.log('Not Applied');
            if (response.data.verified == 0) {
              this.props.navigation.navigate('Verification', {
                sub_val: response.data.subscription,
              });
            } else this.props.navigation.navigate('MainStack');
            //if (response.data.subscription == 0) {
            //   this.props.navigation.navigate('Subscriptionscr');
            // } else {
            //   this.props.navigation.navigate('MainStack');
            // }
          } else {
            console.log('approved');
            this.props.navigation.navigate('MainStack');
          }
        } else {
          if (response.data.verified == 0) {
            this.props.navigation.navigate('Verification', {
              sub_val: 1,
            });
          } else {
            this.props.navigation.navigate('MainStack');
          }
          // this.props.navigation.navigate('MainStack')
        }

        // if (response.data.verified == 0) {
        //   this.props.navigation.navigate('Verification');

        // } else {
        //   this.props.navigation.navigate('MainStack');
        // }

        this.setState({data: response, signin_loader: false});
        console.log(response, '-----------------SIGN');
      } else {
        //console.log('Else');
        alert(response.data[0].error);
        this.setState({signin_loader: false});
      }
    } catch (err) {
      console.log('Error" ', err);
      this.setState({signin_loader: false});
    }
  }
  PasswordVisibility = () => {
    console.log('ckick');
    if (this.state.hide_pass == true) {
      this.setState({hide_pass: false, icon: 'eye'});
    } else {
      this.setState({hide_pass: true, icon: 'eye-off'});
    }
  };
  render() {
    console.log(this.props.navigation.navigate, 'hodee');
    return (
      <View style={{backgroundColor: 'white', height: '100%'}}>
        <Image
          style={styles.stretch}
          source={{
            uri:
              'https://image.freepik.com/free-photo/city-buildings-skyline-metropolitan-area_31965-7268.jpg',
          }}
        />
        <View style={styles.card}>
          <Text
            style={{
              fontSize: 23,
              color: `${color.theme_Color}`,
              alignSelf: 'center',
            }}>
            SignIn!
          </Text>
          <View style={{paddingTop: '5%'}}>
            <TextInput
              style={{
                height: 40,
                borderColor: `${color.theme_Color}`,
                borderBottomWidth: 1,
              }}
              placeholder="Email Address"
              keyboardType="email-address"
              value={this.state.email}
              onChangeText={(text) =>
                this.setState({email: text, email_req: text})
              }
            />
          </View>
          {this.isFieldInError('email') &&
            this.getErrorsInField('email').map((errorMessage, i) => (
              <Text
                key={i}
                style={{
                  color: 'red',
                }}>{`Invalid email address`}</Text>
            ))}
          {this.isFieldInError('email_req') &&
            this.getErrorsInField('email_req').map((errorMessage, i) => (
              <Text
                key={i}
                style={{
                  color: 'red',
                }}>{`Email is required`}</Text>
            ))}
          <View style={{paddingTop: '10%'}}>
            <TextInput
              style={{
                height: 40,
                borderColor: `${color.theme_Color}`,
                borderBottomWidth: 1,
              }}
              onChangeText={(text) => this.setState({password: text})}
              secureTextEntry={this.state.hide_pass}
              placeholder="Password"
              value={this.state.password}
              // keyboardType="visible-password"
            />
            <Icon
              style={{
                position: 'absolute',
                alignSelf: 'center',
                top: '100%',
                left: '88%',
              }}
              onPress={() => this.PasswordVisibility()}
              name={this.state.icon}
              color={color.icon_color}
              size={17}
            />
          </View>
          {this.isFieldInError('password') &&
            this.getErrorsInField('password').map((errorMessage, i) => (
              <Text
                key={i}
                style={{
                  color: 'red',
                }}>{`Enter your password`}</Text>
            ))}
          <TouchableOpacity
            // onPress={() => this.props.navigation.navigate('Subscriptionscr')}
            onPress={() => this.props.navigation.navigate('ForgotPass')}>
            <Text
              style={{
                marginTop: '3%',
                alignSelf: 'center',
                fontSize: 13,
                marginLeft: '43%',
              }}>
              FORGOT PASSWORD?
            </Text>
          </TouchableOpacity>
          <View style={[styles.btn, {alignItems: 'center'}]}>
            <TouchableOpacity
              onPress={() => this._onSubmit()}
              // onPress={() => this.props.navigation.navigate('MainStack')}
              style={{
                height: 50,
                width: 200,
                borderRadius: 5,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: `${color.theme_Color}`,
              }}>
              {this.state.signin_loader ? (
                <ActivityIndicator color={'white'} />
              ) : (
                <Text style={{color: 'white'}}>SIGNIN</Text>
              )}
            </TouchableOpacity>
            {/* <Button
              onPress={() =>
                this.user_Login(this.state.email, this.state.password)
              }
              title="SignIn"
              color="cadetblue"
              accessibilityLabel="Learn more about this purple button"
            /> */}
          </View>
          <View style={{alignSelf: 'center', marginTop: '5%'}}>
            <Text>DON'T HAVE AN ACCOUNT?</Text>
            <TouchableOpacity>
              <Text
                onPress={() => this.props.navigation.navigate('SignUp')}
                style={{
                  color: `${color.theme_Color}`,
                  alignSelf: 'center',
                  marginTop: '2%',
                }}>
                REGISTER
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <Portal>
          <Dialog
            visible={this.state.visible}
            // onDismiss={this.hideDialog_update}
          >
            <Dialog.Title>Alert</Dialog.Title>

            <Dialog.Content>
              <Text>
                Your account request is pending.Once approved by admin you got a
                confirm email and then you can login to your account
              </Text>
            </Dialog.Content>
            <Dialog.Actions>
              {/* // onPress={() =>
                      this.feature_add(
                        this.props.route.params.key,
                        this.state.feature_id,
                      )
                    } */}

              <Button
                onPress={() => this.handle_ok()}
                style={{
                  width: '30%',
                  marginRight: '3%',
                  height: '90%',
                  backgroundColor: `${color.theme_Color}`,
                }}>
                <Text style={{color: 'white', marginLeft: '25%'}}>Ok</Text>
              </Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  stretch: {
    alignSelf: 'center',
    width: '100%',
    height: '30%',
    resizeMode: 'stretch',
  },
  card: {
    padding: 10,
    marginTop: 30,
    right: 2,
    borderColor: 'white',
    // borderWidth: 0.1,
    width: '90%',
    alignSelf: 'center',
    elevation: 4,
    padding: '10%',
    borderTopEndRadius: 30,
    // marginTop: '15%',
    backgroundColor: 'white',
  },
  btn: {
    paddingTop: '15%',
  },
});
