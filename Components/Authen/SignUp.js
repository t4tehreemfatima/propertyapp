import React, {Component} from 'react';
import {Picker} from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  View,
  TextInput,
  Button,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  ImageBackground,
  CheckBox,
  Modal,
} from 'react-native';

import API from '../../Constant/API';
import * as color from '../../Constant/Colors';
import ValidationComponent from 'react-native-form-validator';
import {Container, Content, Tab, Tabs} from 'native-base';
import Privicy from '../../Screens/Privicy';
import Agree from '../../Screens/Agree';
export default class SignUp extends ValidationComponent {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      password: true,
      icon: 'eye-off',
      cicon: 'eye-off',
      confirmpass: true,
      inputVal: '',
      signup_loader: false,
      //input fiedss state
      firstname: '',
      lastname: '',
      user: '1',
      email_field: '',
      req_email_field: '',
      password_field: '',
      confirm_pass_field: '',
      mobile: '',
      address: '',
      req_mobile: '',
      radio: false,
      dialogshow: false,
    };
  }
  _onSubmit() {
    // Call ValidationComponent validate method
    console.log(this.state.number_valid, '0------');
    this.validate({
      firstname: {required: true},
      email_field: {email: true},
      req_email_field: {required: true},
      password_field: {required: true},
      confirm_pass_field: {equalPassword: this.state.password_field},
      mobile: {required: true},
      req_mobile: {minlength: 9, maxlength: 9},
    });
    if (this.isFormValid()) {
      this.user_Signup(
        `${this.state.firstname} ${this.state.lastname}`,
        this.state.user,
        this.state.email_field,
        this.state.password_field,
        '+61' + this.state.mobile,
        this.state.address,
      );
      console.log('ok');
    }
  }
  mobile(_no) {
    this.setState({mobile: _no, req_mobile: _no});
  }
  //
  //registration------------------------------------------------------
  async user_Signup(_name, _id, _email, _password, _mobile, _address) {
    this.setState({signup_loader: true});
    try {
      const response = await this.api.signup(
        _name,
        _id,
        _email,
        _password,
        _mobile,
        _address,
      );
      console.log(response.data, 'RESPONSE signup');
      if (response.data.register) {
        alert('Registration Sucessfully');
        this.props.navigation.navigate('SignIn');
        this.setState({signup_loader: false});
      } else {
        alert('Email already exists');
        this.setState({signup_loader: false});
      }
    } catch (error) {
      console.log('Error" ', error);
      this.setState({signup_loader: false});
    }
  }
  //-----------------------------------------
  PasswordVisibility = () => {
    console.log('ckick');
    if (this.state.password == true) {
      this.setState({password: false, icon: 'eye'});
    } else {
      this.setState({password: true, icon: 'eye-off'});
    }
  };
  ConfirmPasswordVisibility = () => {
    console.log('ckick');
    if (this.state.confirmpass == true) {
      this.setState({confirmpass: false, cicon: 'eye'});
    } else {
      this.setState({confirmpass: true, cicon: 'eye-off'});
    }
  };
  render() {
    console.log(this.state.radio, '--------------');
    return (
      <Container>
        <Content>
          <View style={{backgroundColor: 'white', height: '100%'}}>
            <ImageBackground
              style={styles.stretch}
              source={{
                uri:
                  'https://images.unsplash.com/photo-1554031010-cda237318c87?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MjN8fGJ1aWxkaW5nfGVufDB8fDB8&ixlib=rb-1.2.1&w=1000&q=80',
              }}>
              <View style={styles.card}>
                <Text style={styles.txt}>Create Account</Text>
                <View style={{paddingTop: '5%'}}>
                  <TextInput
                    style={{
                      height: 40,
                      borderColor: `${color.theme_Color}`,
                      borderBottomWidth: 1,
                    }}
                    placeholder="First Name"
                    onChangeText={(text) => this.setState({firstname: text})}
                  />
                </View>
                {this.isFieldInError('firstname') &&
                  this.getErrorsInField('firstname').map((errorMessage, i) => (
                    <Text
                      key={i}
                      style={{
                        color: 'red',
                      }}>{`First name is required`}</Text>
                  ))}

                <View style={{paddingTop: '5%'}}>
                  <TextInput
                    style={{
                      height: 40,
                      borderColor: `${color.theme_Color}`,
                      borderBottomWidth: 1,
                    }}
                    placeholder="Email Address"
                    keyboardType="email-address"
                    onChangeText={(text) =>
                      this.setState({email_field: text, req_email_field: text})
                    }
                    value={this.state.email_field}
                  />
                </View>
                {this.isFieldInError('email_field') &&
                  this.getErrorsInField('email_field').map(
                    (errorMessage, i) => (
                      <Text
                        key={i}
                        style={{
                          color: 'red',
                        }}>{`Invalid email address`}</Text>
                    ),
                  )}

                {this.isFieldInError('req_email_field') &&
                  this.getErrorsInField('req_email_field').map(
                    (errorMessage, i) => (
                      <Text
                        key={i}
                        style={{
                          color: 'red',
                        }}>{`Email is required`}</Text>
                    ),
                  )}

                <View
                  style={{
                    borderBottomWidth: 1,
                    marginTop: '2%',
                    borderColor: `${color.theme_Color}`,
                    marginBottom: '-2%',
                  }}>
                  <Picker
                    mode="dropdown"
                    selectedValue={this.state.user}
                    style={{
                      height: 50,
                      width: '102%',
                      color: 'cadetblue',
                      marginBottom: '-2.5%',
                      right: '1.5%',
                    }}
                    itemStyle={{backgroundColor: 'red', borderBottomWidth: 55}}
                    onValueChange={(itemValue, itemIndex) =>
                      this.setState({user: itemValue})
                    }>
                    <Picker.Item label="Landlord" value="1" />
                    <Picker.Item label="Tenant" value="2" />
                  </Picker>
                </View>
                <View style={{paddingTop: '5%'}}>
                  <TextInput
                    style={{
                      height: 40,
                      borderColor: `${color.theme_Color}`,
                      borderBottomWidth: 1,
                    }}
                    placeholder="Password"
                    secureTextEntry={this.state.password}
                    onChangeText={(text) =>
                      this.setState({password_field: text})
                    }
                  />
                  <Icon
                    style={{
                      // position: 'absolute',
                      alignSelf: 'center',
                      top: '-50%',
                      left: '40%',
                    }}
                    onPress={() => this.PasswordVisibility()}
                    name={this.state.icon}
                    color={'cadetblue'}
                    size={17}
                  />
                </View>
                {this.isFieldInError('password_field') &&
                  this.getErrorsInField('password_field').map(
                    (errorMessage, i) => (
                      <Text
                        key={i}
                        style={{
                          color: 'red',
                          marginTop: '-7%',
                        }}>{`Enter your password`}</Text>
                    ),
                  )}

                <View style={{}}>
                  <TextInput
                    style={{
                      height: 40,
                      borderColor: `${color.theme_Color}`,
                      borderBottomWidth: 1,
                    }}
                    placeholder="Confirm Password"
                    secureTextEntry={this.state.confirmpass}
                    onChangeText={(text) =>
                      this.setState({confirm_pass_field: text})
                    }
                  />
                  <Icon
                    style={{
                      alignSelf: 'center',
                      top: '-50%',
                      left: '40%',
                    }}
                    onPress={() => this.ConfirmPasswordVisibility()}
                    name={this.state.cicon}
                    color={'cadetblue'}
                    size={17}
                  />
                </View>
                {this.isFieldInError('confirm_pass_field') &&
                  this.getErrorsInField('confirm_pass_field').map(
                    (errorMessage, i) => (
                      <Text
                        key={i}
                        style={{
                          color: 'red',
                          marginTop: '-7%',
                        }}>{`Password doesnot match`}</Text>
                    ),
                  )}
                <View style={{paddingTop: '5%', marginTop: '-6%'}}>
                  <Text
                    style={{
                      marginTop: '11%',
                      fontSize: 15,
                      position: 'absolute',
                      color: `${color.theme_Color}`,
                      fontWeight: 'bold',
                    }}>
                    +61
                  </Text>
                  <TextInput
                    keyboardType="phone-pad"
                    style={{
                      height: 40,
                      borderColor: `${color.theme_Color}`,
                      borderBottomWidth: 1,
                      width: '84%',
                      marginLeft: '15%',
                    }}
                    placeholder="Mobile"
                    onChangeText={(text) => this.mobile(text)}
                  />
                  {this.isFieldInError('mobile') &&
                    this.getErrorsInField('mobile').map((errorMessage, i) => (
                      <Text
                        key={i}
                        style={{
                          color: 'red',
                          marginLeft: '16%',
                        }}>{`Invalid number`}</Text>
                    ))}
                  {this.isFieldInError('req_mobile') &&
                    this.getErrorsInField('req_mobile').map(
                      (errorMessage, i) => (
                        <Text
                          key={i}
                          style={{
                            color: 'red',
                            marginLeft: '16%',
                          }}>{`Number must be 9 digits`}</Text>
                      ),
                    )}
                  <View style={{paddingTop: '5%'}}>
                    <TextInput
                      style={{
                        height: 40,
                        borderColor: `${color.theme_Color}`,
                        borderBottomWidth: 1,
                      }}
                      placeholder="Address"
                      onChangeText={(text) => this.setState({address: text})}
                    />
                  </View>
                </View>
                {/* <CheckBox checked={this.state.radio} color="green" />
                
                */}

                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <CheckBox
                    value={this.state.radio}
                    onValueChange={(e) => this.setState({radio: e})}
                  />
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Agree')}>
                    <Text style={{color: 'grey'}}>
                      Accept Terms and Conditions
                    </Text>
                  </TouchableOpacity>
                </View>
                {this.state.radio == false ? (
                  <View
                    style={{
                      height: 50,
                      width: 200,
                      borderRadius: 5,
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: '5%',
                      backgroundColor: 'grey',
                    }}>
                    <Text style={{color: 'white'}}>SIGNUP</Text>
                  </View>
                ) : (
                  <TouchableOpacity
                    onPress={() => this._onSubmit()}
                    style={{
                      height: 50,
                      width: 200,
                      borderRadius: 5,
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: '5%',
                      backgroundColor: `${color.theme_Color}`,
                    }}>
                    {this.state.signup_loader ? (
                      <ActivityIndicator color={'white'} />
                    ) : (
                      <Text style={{color: 'white'}}>SIGNUP</Text>
                    )}
                  </TouchableOpacity>
                )}
                {/* <View style={styles.btn}>
              
              <Button
                title="SignUp"
                color="cadetblue"
                onPress={() =>
                  this.user_Signup(
                    `${this.state.firstname} ${this.state.lastname}`,
                    this.state.user,
                    this.state.email_field,
                    this.state.password_field,
                  )
                }
                accessibilityLabel="Learn more about this purple button"
              />
            </View> */}
                <View style={{alignSelf: 'center', marginTop: '5%'}}>
                  <Text>YOU HAVE AN ACCOUNT?</Text>
                  <TouchableOpacity>
                    <Text
                      onPress={() => this.props.navigation.navigate('SignIn')}
                      style={{
                        color: `${color.theme_Color}`,
                        alignSelf: 'center',
                        marginTop: '2%',
                      }}>
                      LogIn
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ImageBackground>
          </View>

          {/* <Dialog.Container visible={this.state.dialogshow}> */}
          {/* <Agree /> */}
          {/* <Tabs
                onChangeTab={this.onClick}
                tabBarUnderlineStyle={{
                  backgroundColor: `${color.theme_Color}`,
                }}>
                <Tab
                  textStyle={{color: 'black', fontWeight: 'bold'}}
                  activeTextStyle={{color: 'black', fontWeight: 'bold'}}
                  tabStyle={{backgroundColor: 'white'}}
                  activeTabStyle={{backgroundColor: 'white'}}
                  heading="Privicy">
                  <Privicy />
                </Tab>
                <Tab
                  textStyle={{color: 'black', fontWeight: 'bold'}}
                  activeTextStyle={{color: 'black', fontWeight: 'bold'}}
                  tabStyle={{backgroundColor: 'white'}}
                  activeTabStyle={{backgroundColor: 'white'}}
                  heading="Requests"></Tab>
              </Tabs> */}
          {/* <Dialog.Button
                onPress={() => this.setState({dialogshow: false})}
                label="Ok"
              />
            </Dialog.Container> */}
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  stretch: {
    alignSelf: 'center',
    width: '100%',
    height: '100%',
  },
  card: {
    padding: 10,
    marginTop: 30,
    right: 2,
    borderColor: 'white',
    // borderWidth: 0.1,
    width: '90%',
    alignSelf: 'center',
    elevation: 4,
    padding: '10%',
    borderTopEndRadius: 30,
    marginTop: '25%',
    backgroundColor: 'white',
  },
  btn: {
    paddingTop: '15%',
  },
  txt: {
    fontSize: 20,
    color: `${color.theme_Color}`,
    alignSelf: 'center',
    fontWeight: 'bold',
    marginTop: '5%',
    marginBottom: '10%',
  },
});

{
  /* <Icon name="chevron-back-outline" color={'purple'} size={17} /> */
}
