// const Api = 'https://property.gtb2bexim.com/api'; // SYSTEM ACCESS IP
const Api = 'https://tekumatics.com/api'; // SYSTEM ACCESS IP
const nodeapi = 'http://tekumatics.com:3000';
// const Api = 'https://property.gtb2bexim.com/public/index.php/api'; // SYSTEM ACCESS IP

const fetchJSON = async ({ url, token }) => {
  const res = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      // prettier-ignore
      'Accept': 'application/json',
      // prettier-ignore
      'Authorization': `Bearer ${token}`,
    },
  });

  return res.json();
};

const fetchJSONPOST = async ({ url, token }) => {
  const res = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      // prettier-ignore
      'Accept': 'application/json',
      // prettier-ignore
      'Authorization': `Bearer ${token}`,
    },
  });

  return res.json();
};

const fetchJSONPOSTwithbody = async ({ url, body, token }) => {
  console.log(body, '88888888888888888');
  const res = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      // prettier-ignore
      'Authorization': `Bearer ${token}`,
      // prettier-ignore
      'Accept': 'application/json',
    },
    body: JSON.stringify(body),
  });

  // console.log(body, 'okkkkk');
  // console.log(res, 'JSON____________________');
  return res.json();
};
const NODEJSONPOSTwithbody = async ({ url, body }) => {
  const res = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      // prettier-ignore
      // 'Authorization': `Bearer ${token}`,
      // prettier-ignore
    },
    body: JSON.stringify(body),
  });

  // console.log(body, 'okkkkk');
  // console.log(res.text, 'JSON____________________');
  return res.json();
};

const videoapi = async ({ url, body, token }) => {
  const res = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      // prettier-ignore
      // prettier-ignore
      // 'Accept': 'application/json',
    },
    body: JSON.stringify(body),
  });
  return res.json();
};

//alert(localStorage.getItem("token"))

const API = (credentials) => ({
  login: (_email, _password) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/login`,
      body: { email: _email, password: _password },
    }),
  getlistconvo: (_userid) =>
    NODEJSONPOSTwithbody({
      url: `${nodeapi}/Get_convo_list`,
      body: { userid: _userid },
    }),
  Addtoprivate: (_userid1, _userid2, status) =>
    NODEJSONPOSTwithbody({
      url: `${nodeapi}/Addpeer`,
      body: { user1: _userid1, user2: _userid2, status: status },
    }),
  select: (_token) => fetchJSON({ url: `${Api}/user/select`, token: _token }),
  logout: (_token) =>
    fetchJSONPOST({
      url: `${Api}/user/logout`,
      token: _token,
    }),
  signup: (_name, _id, _email, _password, _mobile, _address) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/signup`,
      body: {
        name: _name,
        user_type_id: _id,
        email: _email,
        password: _password,
        mobile: _mobile,
        address: _address,
      },
    }),
  passwordreset: (_id, _current_password, _new_password, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/user/update/password`,
      body: {
        id: _id,
        current_password: _current_password,
        new_password: _new_password,
      },
      token: _token,
    }),
  selectProperty: (
    _userid,
    _name,
    _min,
    _max,
    _city,
    _state,
    _zipCode,
    _token,
  ) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/select`,
      body: {
        user_id: _userid,
        name: _name,
        rent_min: _min,
        rent_max: _max,
        city: _city,
        state: _state,
        zip_code: _zipCode,
      },
      token: _token,
    }),
  property_delete: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/delete`,
      body: {
        id: _id,
      },
      token: _token,
    }),
  property_tendent_select: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/tendent/select`,
      body: {
        tendent_id: _id,
      },
      token: _token,
    }),
  current_tendent_delete: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/tendent/delete`,
      body: {
        // tendent_id: _tendentId,
        // property_id: _propertyId,
        id: _id,
      },
      token: _token,
    }),
  property_filter: (_name, _id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/tendent/select`,
      body: {
        name: _name,
        tendent_id: _id,
      },
      token: _token,
    }),
  detailofProperty: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/select/one`,
      body: {
        id: _id,
      },
      token: _token,
    }),
  updateProfile: (_id, _name, _usertypeid, _email, _mobile, _address, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/user/update`,
      body: {
        id: _id,
        name: _name,
        user_type_id: _usertypeid,
        email: _email,
        mobile: _mobile,
        address: _address,
      },
      token: _token,
    }),
  userData: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/user/select/one`,
      body: {
        id: _id,
      },
      token: _token,
    }),
  landlordDetail: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/select/one`,
      body: {
        id: _id,
      },
      token: _token,
    }),
  applyProperty: (_id, _pid, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/request/add`,
      body: {
        tendent_id: _id,
        property_id: _pid,
      },
      token: _token,
    }),
  getTasks: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/task/assign/select`,
      body: {
        user_id: _id,
      },
      token: _token,
    }),
  updateTasks: (_id, _status, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/task/assign/update`,
      body: {
        id: _id,
        status: _status,
      },
      token: _token,
    }),
  insertProperty: (
    _id,
    _name,
    _bedrooms,
    _bathrooms,
    _toilets,
    _description,
    _state,
    _rent,
    _yearbuild,
    _city,
    _street,
    _zipCode,
    _currency_id,
    _days,
    _limits,
    _token,
  ) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/insert`,
      body: {
        user_id: _id,
        property_name: _name,
        bed_rooms: _bedrooms,
        bath_rooms: _bathrooms,
        toilets: _toilets,
        description: _description,
        state: _state,
        rent: _rent,
        year_build: _yearbuild,
        city: _city,
        street: _street,
        zip_code: _zipCode,
        currency_id: _currency_id,
        rent_days: _days,
        limit: _limits,
      },
      token: _token,
    }),
  response_list: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/request/tendent`,
      body: {
        user_id: _id,
      },
      token: _token,
    }),
  current_tendent: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/tendent/approved`,
      body: {
        user_id: _id,
      },
      token: _token,
    }),

  profile_image: (_id, _img_name, _image, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/user/update/image`,
      body: {
        id: _id,
        image_name: _img_name,
        image: _image,
      },
      token: _token,
    }),
  edit_profile_current_tendent: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/select/one`,
      body: {
        id: _id,
      },
      token: _token,
    }),
  update_Property: (
    _id,
    _userid,
    _name,
    _bedrooms,
    _bathrooms,
    _toilets,
    _description,
    _state,
    _rent,
    _yearbuild,
    _city,
    _street,
    _zipCode,
    _currency_id,
    _days,
    _limits,
    _token,
  ) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/update	`,
      body: {
        id: _id,
        user_id: _userid,
        property_name: _name,
        bed_rooms: _bedrooms,
        bath_rooms: _bathrooms,
        toilets: _toilets,
        description: _description,
        state: _state,
        rent: _rent,
        year_build: _yearbuild,
        city: _city,
        street: _street,
        currency_id: _currency_id,
        zip_code: _zipCode,
        rent_days: _days,
        limit: _limits,
      },
      token: _token,
    }),
  approve_request: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/request/approve`,
      body: {
        id: _id,
      },
      token: _token,
    }),
  img_upload: (_id, _images, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/update/image`,
      body: {
        property_id: _id,
        images: _images,
      },
      token: _token,
    }),
  approve_request_delete: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/request/delete	`,
      body: {
        id: _id,
      },
      token: _token,
    }),
  list_features: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/features/select`,
      body: {
        property_id: _id,
      },
      token: _token,
    }),
  delete_features: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/features/delete`,
      body: {
        id: _id,
      },
      token: _token,
    }),
  model_feature: (_token) =>
    fetchJSON({ url: `${Api}/features/select`, token: _token }),
  add_features: (_propertyId, _featureId, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/features/insert`,
      body: {
        property_id: _propertyId,
        features_id: _featureId,
      },
      token: _token,
    }),
  //----------------------------------------------------------------- >>TASK
  task_list: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/task/assign/select`,
      body: {
        user_id: _id,
      },
      token: _token,
    }),
  task_model_list: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/task/select`,
      body: {
        user_id: _id,
      },
      token: _token,
    }),
  propertyOnTendend: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/tendent/tendentonproperty`,
      body: {
        property_id: _id,
      },
      token: _token,
    }),
  task_assign_add_btn: (_userid, _taskid, _date, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/task/assign/add`,
      body: {
        user_id: _userid,
        task_id: _taskid,
        repetition: _date,
      },
      token: _token,
    }),
  task_assign_delete: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/task/assign/delete`,
      body: {
        id: _id,
      },
      token: _token,
    }),
  task_assign_update: (_id, _status, _date, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/task/assign/update`,
      body: {
        id: _id,
        status: _status,
        repetition: _date,
      },
      token: _token,
    }),
  //-------------------------------------------------------->create task screen
  create_screen_list: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/task/select`,
      body: {
        user_id: _id,
      },
      token: _token,
    }),
  single_assigned_task: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/task/select`,
      body: {
        id: _id,
      },
      token: _token,
    }),
  task_add_btn: (_id, _task, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/task/add`,
      body: {
        property_id: _id,
        task: _task,
      },
      token: _token,
    }),
  cuurent_tendent_property: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/select`,
      body: {
        user_id: _id,
      },
      token: _token,
    }),
  task_update: (_id, _task, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/task/update`,
      body: {
        id: _id,
        task: _task,
      },
      token: _token,
    }),
  task_delete: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/task/delete`,
      body: {
        id: _id,
      },
      token: _token,
    }),

  display_imaage: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/select/image`,
      body: {
        property_id: _id,
      },
      token: _token,
    }),
  delete_image: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/delete/image	`,
      body: {
        id: _id,
      },
      token: _token,
    }),

  //----------------------------------------------------------------Rent
  rent_pay_history: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/rent/pay/select`,
      body: {
        user_id: _id,
      },
      token: _token,
    }),
  rent_pay_date: (_token) =>
    fetchJSONPOST({
      url: `${Api}/rent/pay/date`,
      token: _token,
    }),
  rent_pay_add: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/rent/pay/add`,
      body: {
        user_id: _id,
      },
      token: _token,
    }),
  tendent_property_old: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/tendent/old`,
      body: {
        user_id: _id,
      },
      token: _token,
    }),
  tendent_detail: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/tendent/detail`,
      body: {
        user_id: _id,
      },
      token: _token,
    }),
  rent_select: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/rent/pay/select`,
      body: {
        property_id: _id,
      },
      token: _token,
    }),
  tendent_history: (_id, _pid, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/rent/pay/select`,
      body: {
        user_id: _id,
        property_id: _pid,
      },
      token: _token,
    }),
  //----------------------------------------------------------UTILITY
  property_select: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/property/select`,
      body: {
        user_id: _id,
      },
      token: _token,
    }),
  utility_select: (_id, _token) =>
    fetchJSON({
      url: `${Api}/utility/select/${_id}`,

      token: _token,
    }),
  add_utility: (_id, _name, _period, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/utility/add`,
      body: {
        property_id: _id,
        utility_name: _name,
        period: _period,
      },
      token: _token,
    }),
  utility_update: (_id, _name, _period, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/utility/update`,
      body: {
        id: _id,
        utility_name: _name,
        period: _period,
      },
      token: _token,
    }),
  utility_delete: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/utility/delete`,
      body: {
        id: _id,
      },
      token: _token,
    }),
  utilitypaid_select: (
    _token, //--------
  ) =>
    fetchJSONPOST({
      url: `${Api}/utilitypaid/select`,

      token: _token,
    }),
  picker_utility_select: (
    _id,
    _token, //--------
  ) =>
    fetchJSON({
      url: `${Api}/utility/select/${_id}`,
      token: _token,
    }),
  utility_add_tendent: (
    _uid,
    _pid,
    _total,
    _paid,
    _id,
    _token, //-----------
  ) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/utilitypaid/add`,
      body: {
        utility_id: _uid,
        property_id: _pid,
        total_amount: _total,
        paid_by_user: _paid,
        user_id: _id,
      },
      token: _token,
    }),
  view_utility: (_id, _utilId, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/utilitypaid/select`,
      body: {
        property_id: _id,
        utility_id: _utilId,
      },
      token: _token,
    }),
  //--------------------------------ACCOUNT
  upcoming: (_token) =>
    fetchJSONPOST({
      url: `${Api}/rent/pay/upcoming`,
      token: _token,
    }),
  rent_sum: (_token) =>
    fetchJSONPOST({
      url: `${Api}/rent/pay/sum`,
      token: _token,
    }),
  utilitypaid_avg: (_token) =>
    fetchJSONPOST({
      url: `${Api}/utilitypaid/avg`,
      token: _token,
    }),
  graph: (_token) =>
    fetchJSONPOST({
      url: `${Api}/rent/pay/graph`,
      token: _token,
    }),
  video: (_base, _name, _pid, _userid, _time, _type) =>
    videoapi({
      url: `${nodeapi}/upload_video`,
      body: {
        base: _base,
        name: _name,
        property_id: _pid,
        user_id: _userid,
        time: _time,
        type: _type,
      },
    }),

  //--------------------------------------------VERIFICATION
  verify: (_token) =>
    fetchJSON({
      url: `${Api}/user/verifition/code`,
      token: _token,
    }),
  code: (
    _id,
    _token, //--------
  ) =>
    fetchJSON({
      url: `${Api}/user/match/${_id}`,
      token: _token,
    }),
  forgt_pass_veri: (_email, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/forgot/password/code`,
      body: {
        email: _email,
      },
      token: _token,
    }),
  pass_code: (
    _code,
    _token, //--------
  ) =>
    fetchJSON({
      url: `${Api}/forgot/password/${_code}`,
      token: _token,
    }),
  change_forgotPass: (_pass, _email, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/forgot/password/change`,
      body: {
        password: _pass,
        email: _email
      },
      token: _token,
    }),
  getmessages: (
    _id,
    _token, //--------
  ) =>
    fetchJSON({
      url: `${Api}/messages/select/${_id}`,
      token: _token,
    }),
  //--------------------------------------------------------SUBSCRIPTION

  subscr_select: (
    _token, //--------
  ) =>
    fetchJSON({
      url: `${Api}/subscription/select`,
      token: _token,
    }),
  subs_user_add: (
    _id,
    _cardno,
    _month,
    _year,
    _cvv,
    _method,
    _amount,
    _token,
  ) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/user/subscription/add`,
      body: {
        subs_id: _id,
        card_no: _cardno,
        exp_month: _month,
        exp_year: _year,
        cvv: _cvv,
        pay_method: _method,
        amount: _amount,
      },
      token: _token,
    }),
  subs_user_trial_add: (_id, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/user/subscription/add`,
      body: {
        subs_id: _id,
      },
      token: _token,
    }),
  subs_bank_add: (
    _amount,
    _subs_id,
    _paymethod,
    _name,
    _image,
    _bankinfo,
    _token,
  ) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/user/subscription/add`,
      body: {
        amount: _amount,
        subs_id: _subs_id,
        pay_method: _paymethod,
        image: [
          {
            name: _name,
            image: _image,
          },
        ],
        bank_info: _bankinfo,
      },
      token: _token,
    }),
  user_subscr_select: (
    _token, //--------
  ) =>
    fetchJSON({
      url: `${Api}/user/subscription/select`,
      token: _token,
    }),
  privicy: (
    _token, //--------
  ) =>
    fetchJSON({
      url: `${Api}/policy`,
      token: _token,
    }),
  bank_data: (
    _token, //--------
  ) =>
    fetchJSON({
      url: `${Api}/bank/details`,
      token: _token,
    }),
  notification_select: (
    _token, //--------
  ) =>
    fetchJSON({
      url: `${Api}/notifications/select`,
      token: _token,
    }),
  notification_update: (
    _id,
    _token, //--------
  ) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/notifications/update`,
      body: {
        id: _id,
      },
      token: _token,
    }),

  social_selct: (
    _token, //--------
  ) =>
    fetchJSON({
      url: `${Api}/social/select`,
      token: _token,
    }),
  currency: (
    _token, //--------
  ) =>
    fetchJSON({
      url: `${Api}/currency/select`,
      token: _token,
    }),
  state: (
    _token, //--------
  ) =>
    fetchJSON({
      url: `${Api}/states/select`,
      token: _token,
    }),
  cities: (
    _id,
    _token, //--------
  ) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/cities/select`,
      body: {
        state_name: _id,
      },
      token: _token,
    }),
  App_token_submit: (_apptoken, _token) =>
    fetchJSONPOSTwithbody({
      url: `${Api}/user/apptoken`,
      body: {
        app_token: _apptoken,
      },
      token: _token,
    }),
  messages_user_list: (
    _id,
    _token, //--------
  ) =>
    fetchJSON({
      url: `${Api}/messages/users/select/${_id}`,
      token: _token,
    }),
  tenant_home_list: (
    _id,
    _token, //--------
  ) =>
    fetchJSON({
      url: `${Api}/property/tendent/${_id}`,
      token: _token,
    }),
  // subs_bank_add: (_data, _token) =>
  //   fetchJSONPOSTwithbody({
  //     url: `${Api}/user/subscription/add`,
  //     body: {
  //       _data,
  //     },
  //     token: _token,
  //   }),
});

export default API;

// userDetails: (_id) => fetchJSON({url: `${Api}/userDetails/${_id}`}),

// signup: (_email, _password, _name) =>
//     fetchJSONPOSTwithbody({
//       url: `${Api}/signup`,
//       body: {email: _email, password: _password, name: _name},
//     }),
