import {View, Text} from 'react-native';
import React from 'react';
import * as color from '../Constant/Colors';
import Icon from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import 'react-native-gesture-handler';
import API from '../Constant/API';
import CreateProperties from '../Screens/CreateProperty';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {NavigationContainer} from '@react-navigation/native';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';

import {
  Container,
  Header,
  Content,
  Footer,
  FooterTab,
  Button,
  StyleProvider,
} from 'native-base';
import Account from '../Screens/Account';
import Chat from '../Screens/Chat';
import Home from '../Screens/Home';
import Inspection from '../Screens/Inspection';
import Repairs from '../Screens/Repairs';
import Tasks from '../Screens/Tasks';
import Chat_tenant from '../Screens/Chat_tenant';
const Tab = createMaterialBottomTabNavigator();

export default class BottomTab extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      type: '',
    };
  }
  async tabfunction() {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    this.setState({type: parse.user_type});
    console.log(parse.user_type, 'typeep--------------');
    // const response = await this.api.userData(parse.id, parse.token);
  }
  componentDidMount() {
    this.tabfunction();
  }
  render() {
    if (this.state.type == '1') {
      return (
        <Tab.Navigator
          // initialRouteName="MyHome"
          activeColor={color.ActiveTabNavbar_color}
          inactiveColor={color.TabNavbar_color}
          barStyle={{backgroundColor: `${color.theme_Color}`}}>
          <Tab.Screen
            name="Properties"
            component={Home}
            // navigation={this.props.navigation}
            options={{
              tabBarIcon: ({color}) => (
                <Icon name="home" color={color} size={23} />
              ),
            }}
          />
          <Tab.Screen
            name="Create"
            component={CreateProperties}
            options={{
              tabBarIcon: ({color}) => (
                <Icon name="md-add-circle-sharp" color={color} size={23} />
              ),
            }}
          />
          <Tab.Screen
            name="Task"
            component={Tasks}
            options={{
              tabBarIcon: ({color}) => (
                <MaterialIcons name="add-task" color={color} size={23} />
              ),
            }}
          />
          {/* <Tab.Screen
          name="Repairs"
          component={Repairs}
          options={{
            tabBarIcon: ({color}) => (
              <MIcon name="tools" color={color} size={23} />
            ),
          }}
        /> */}
          <Tab.Screen
            name="Messages"
            component={Chat}
            options={{
              tabBarIcon: ({color}) => (
                <Icon name="chatbubbles-outline" color={color} size={23} />
              ),
            }}
          />
          <Tab.Screen
            name="Account"
            component={Account}
            options={{
              tabBarIcon: ({color}) => (
                <MIcon name="account" color={color} size={23} />
              ),
            }}
          />
        </Tab.Navigator>
      );
    } else {
      return (
        <Tab.Navigator
          // initialRouteName="MyHome"
          activeColor={color.ActiveTabNavbar_color}
          inactiveColor={color.TabNavbar_color}
          barStyle={{backgroundColor: `${color.theme_Color}`}}>
          <Tab.Screen
            name="Properties"
            component={Home}
            options={{
              tabBarIcon: ({color}) => (
                <Icon name="home" color={color} size={23} />
              ),
            }}
          />
          <Tab.Screen
            name="Tasks"
            component={Tasks}
            options={{
              tabBarIcon: ({color}) => (
                <MaterialIcons name="add-task" color={color} size={23} />
              ),
            }}
          />
          {/* <Tab.Screen
          name="Repairs"
          component={Repairs}
          options={{
            tabBarIcon: ({color}) => (
              <MIcon name="tools" color={color} size={23} />
            ),
          }}
        /> */}
          <Tab.Screen
            name="Messages"
            component={Chat}
            options={{
              tabBarIcon: ({color}) => (
                <Icon name="chatbubbles-outline" color={color} size={23} />
              ),
            }}
          />
          {/* <Tab.Screen
            name="Messages"
            component={Chat_tenant}
            options={{
              tabBarIcon: ({color}) => (
                <Icon name="chatbubbles-outline" color={color} size={23} />
              ),
            }}
          /> */}
          {/* <Tab.Screen
            name="Account"
            component={Account}
            options={{
              tabBarIcon: ({color}) => (
                <MIcon name="account" color={color} size={23} />
              ),
            }}
          /> */}
        </Tab.Navigator>
      );
    }
  }
}
