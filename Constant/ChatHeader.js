import React from 'react';
import {ImageBackground, Text, View} from 'react-native';
import MyIcon from 'react-native-vector-icons/MaterialIcons';
export default class ChatHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <ImageBackground
        style={{
          width: '100%',
          height: 53,
        }}
        source={{
          uri:
            'https://i.pinimg.com/originals/c1/5c/ba/c15cbae66a8a930a1cb292aaf60bb815.jpg',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            top: '3%',
          }}>
          <MyIcon
            style={{right: '100%', marginLeft: '14%'}}
            name="menu"
            color="white"
            size={30}
            onPress={this.props.leftIcon}
          />
          <Text
            style={{
              fontSize: 20,
              color: 'white',
              marginRight: '70%',
              marginTop: '0.8%',
            }}>
            Chat
          </Text>
        </View>
      </ImageBackground>
    );
  }
}
