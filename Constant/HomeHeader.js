import React from 'react';
import {ImageBackground, Text, View} from 'react-native';
import MyIcon from 'react-native-vector-icons/MaterialIcons';
import MyHeader from './MyHeader';

export default class HomeHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <ImageBackground
        style={{
          width: '100%',
          height: '28%',
          marginBottom: '-38%',
        }}
        source={{
          uri: 'https://coverfiles.alphacoders.com/101/101957.jpg',
        }}></ImageBackground>
    );
  }
}
