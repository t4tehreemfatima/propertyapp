import React, {Component} from 'react';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Title,
  Alert,
  View,
} from 'native-base';
import * as color from '../Constant/Colors';
import {Image, ImageBackground} from 'react-native';

import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import MyIcon from 'react-native-vector-icons/MaterialIcons';

export default class MyHeader extends Component {
  render() {
    switch (this.props.headername) {
      case '1':
        return (
          // <ImageBackground
          //   style={{
          //     width: '100%',
          //     height: '28%',
          //     marginBottom: '-38%',
          //   }}
          //   source={{
          //     uri: 'https://coverfiles.alphacoders.com/101/101957.jpg',
          //   }}>
          <Header
            hasTabs
            androidStatusBarColor={color.statusbar_color}
            style={{backgroundColor: `${color.header1_color}`}}>
            <Left>
              <Button
                onPress={this.props.leftIconPress}
                transparent
                style={{right: '10%'}}>
                <MIcon name="menu" color="#2D495D" size={30} />
              </Button>
            </Left>
            <Body>
              <Title>{this.props.name}</Title>
            </Body>
            {this.props.type == 'ten' ? (
              <Right>
                <Button transparent onPress={this.props.rightIconPress}>
                  <MIcon
                    name={this.props.rightIconName}
                    color="#2D495D"
                    size={30}
                  />
                </Button>
              </Right>
            ) : (
              <Right>
                <Button transparent onPress={this.props.rightIconPress}>
                  <MyIcon
                    name={this.props.rightIconName}
                    color="#2D495D"
                    size={30}
                  />
                </Button>
              </Right>
            )}
          </Header>
          // </ImageBackground>
        );
        break;

      case '2':
        return (
          // <ImageBackground
          //   style={{
          //     width: '100%',
          //     marginTop: '5%',
          //     height: '32%',
          //   }}
          //   source={{
          //     uri:
          //       'https://static.vecteezy.com/system/resources/thumbnails/001/331/311/small/futuristic-geometric-background-free-vector.jpg',
          //   }}>
          <Header
            transparent
            androidStatusBarColor={color.statusbar_color}
            style={{marginTop: '-5%'}}>
            <Left>
              <Button
                onPress={this.props.leftIconPress}
                transparent
                style={{right: '10%'}}>
                <MIcon name="menu" color="white" size={30} />
              </Button>
            </Left>
            <Body>
              <Title>{this.props.name}</Title>
            </Body>
            <Right>
              <Button transparent>
                <MyIcon
                  name={this.props.rightIconName}
                  color="#2D495D"
                  size={30}
                  onPress={this.props.rightIconPress}
                />
              </Button>
            </Right>
          </Header>
          // </ImageBackground>
        );
        break;
      case '3':
        return (
          <Header
            androidStatusBarColor={color.statusbar_color}
            style={{
              backgroundColor: `${color.header3_color}`,
            }}>
            <Left>
              <Button transparent style={{right: '10%'}}>
                <MIcon
                  name="chevron-left"
                  color="white"
                  size={30}
                  onPress={this.props.onPressIcon}
                />
              </Button>
            </Left>
            <Body>
              <Title>{this.props.name}</Title>
            </Body>
            <Right>
              <Button transparent onPress={this.props.rightIconPress}>
                <MyIcon
                  name={this.props.rightIconName}
                  color="white"
                  size={30}
                />
              </Button>
            </Right>
          </Header>
        );
        break;
      case '4':
        return (
          <Header
            androidStatusBarColor={color.statusbar_color}
            style={{
              backgroundColor: `${color.header3_color}`,
            }}>
            <Left>
              <Button transparent style={{right: '10%'}}>
                <MIcon
                  name="chevron-left"
                  color="white"
                  size={30}
                  onPress={this.props.onPressIcon}
                />
              </Button>
            </Left>
            <Body>
              <Title>{this.props.name}</Title>
            </Body>
          </Header>
        );
        break;
      case '5':
        return (
          <Header
            androidStatusBarColor={color.statusbar_color}
            style={{
              backgroundColor: `${color.header3_color}`,
            }}>
            {/* <Left>
              <Button transparent style={{right: '10%'}}>
                <MIcon
                  name="chevron-left"
                  color="white"
                  size={30}
                  onPress={this.props.onPressIcon}
                />
              </Button>
            </Left> */}
            <Body style={{alignItems: 'center'}}>
              <Title>{this.props.name}</Title>
            </Body>
          </Header>
        );
        break;
      case '7':
        return (
          <Header
            androidStatusBarColor={color.statusbar_color}
            style={{
              backgroundColor: `${color.header3_color}`,
            }}>
            <Left>
              <Button transparent>
                <MIcon
                  name="chevron-left"
                  color="white"
                  size={30}
                  onPress={this.props.onPressIcon}
                />
              </Button>
            </Left>
            <Body>
              <Title>{this.props.name}</Title>
            </Body>
            <Right>
              <View></View>
            </Right>
          </Header>
        );
        break;
      case 'chat':
        return (
          <Header
            androidStatusBarColor={color.statusbar_color}
            style={{
              backgroundColor: `${color.header3_color}`,
            }}>
            <Left>
              <Button transparent style={{right: '10%'}}>
                <MIcon
                  name="chevron-left"
                  color="white"
                  size={30}
                  onPress={this.props.onPressIcon}
                />
              </Button>
            </Left>
            <Body>
              <Title>{this.props.name}</Title>
            </Body>
            <Right>
              <Button transparent onPress={this.props.rightIconPress}>
                <MIcon
                  name="format-list-bulleted-square"
                  color="white"
                  size={30}
                />
              </Button>
            </Right>
          </Header>
        );
        break;
    }
  }
}
