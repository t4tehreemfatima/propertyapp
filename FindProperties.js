import {Container, Content, Title, Header} from 'native-base';
import * as color from './Constant/Colors';
import React from 'react';
import Collapsible from 'react-native-collapsible';
import MyHeader from './Constant/MyHeader';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {TextInput} from 'react-native';
import {View, Text} from 'react-native';
import {RadioButton} from 'react-native-paper';
import API from './Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import {Tab, Tabs, Thumbnail} from 'native-base';
export default class FindProperties extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      status: 'unchecked',
      collapsed: true,
      myData: '',
      isLoading: true,
      input_search: '',
    };
  }
  radioBtn() {
    this.setState({status: 'checked'});
    console.log('ckick');
    if (this.state.status == 'unchecked') {
      this.setState({status: 'checked', collapsed: false});
    } else {
      this.setState({status: 'unchecked', collapsed: true});
    }
  }

  async property_list() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      console.log(parse.token, 'token ten');
      const response = await this.api.selectProperty(
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        parse.token,
      );
      console.log(response, 'Data ---------------Response');
      this.setState({myData: response, isLoading: false});
      // console.log(this.state.myData, 'ppppppppdata');
    } catch (err) {
      console.log('Error: ', err);
    }
  }
  componentDidMount() {
    this.property_list();
  }
  //
  openHomeList = () => {
    if (this.state.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: '55%',
          }}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      if (this.state.myData.property_data[0] == null) {
        return (
          <View>
            <Text
              onPress={() => this.props.navigation.goBack()}
              style={{
                color: '#C5C5C5',
                fontSize: 24,
                marginTop: '75%',
                alignSelf: 'center',
              }}>
              No Property
            </Text>
            <Text
              onPress={() => this.props.navigation.goBack()}
              style={{alignSelf: 'center', color: `${color.theme_Color}`}}>
              GoBack
            </Text>
          </View>
        );
      } else {
        return this.state.myData.property_data.map((element, i) => {
          return (
            <View
              key={i}
              style={{
                flexDirection: 'row',
                marginLeft: '3%',
                marginRight: '3%',
                alignItems: 'center',
                borderWidth: 0.1,
                elevation: 1,
                padding: '3%',
                marginTop: '5%',
              }}>
              {element.images.length > 0 ? (
                <Thumbnail
                  square
                  large
                  source={{
                    uri: `${color.link}${element.images[0].path}`,
                  }}
                />
              ) : (
                <Thumbnail
                  square
                  large
                  source={{
                    uri:
                      'https://i.pinimg.com/474x/b4/8f/3b/b48f3bb31399b66b2d5000a161032a1d.jpg',
                  }}
                />
              )}

              <View style={{marginLeft: '5%'}}>
                <Text
                  onPress={() =>
                    this.props.navigation.navigate('ViewCartDetails', {
                      key: element.id,
                      status: '',
                    })
                  }
                  style={{fontWeight: 'bold', fontSize: 16}}>
                  {element.name}
                </Text>
                <Text>Rent: {element.rent}</Text>
                <Text style={{color: 'grey'}}>City: {element.city}</Text>
              </View>
              {/* <View style={{marginLeft: '15%'}}>
                <Text style={{fontWeight: 'bold', color: 'green'}}>Status</Text>
                <Text style={{color: 'green', fontWeight: 'bold'}}>
                  {element.status}
                </Text>
              </View> */}
            </View>
          );
        });
      }
    }
  };

  render() {
    console.log(this.state.input_search, 'input');
    return (
      <Container>
        <MyHeader
          headername="3"
          name="Find Properties"
          rightIconPress={() => this.props.navigation.navigate('SearchPg1')}
          rightIconName="filter-alt"
          onPressIcon={() => this.props.navigation.goBack()}
        />
        <Content>
          {/* <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              alignSelf: 'center',
              marginLeft: '39%',
            }}>
            <View>
              <RadioButton
                value="Second"
                color={color.header3_color}
                status={this.state.status}
                onPress={() => this.radioBtn()}
              />
            </View>
            <View>
              <Text>Couldn't find address?</Text>
            </View>
          </View>
          <View>
            <Collapsible collapsed={this.state.collapsed}>
              <View
                style={{
                  marginTop: '5%',
                  marginLeft: '5%',
                  marginRight: '5%',
                }}>
                <Text style={{fontWeight: 'bold', fontSize: 15}}>Address</Text>
                <TextInput
                  style={{
                    marginTop: '-3%',
                    borderColor: 'cadetblue',
                    borderBottomWidth: 1,
                  }}
                  keyboardType="email-address"
                  placeholder="Enter Unit/House number & street number"
                />
              </View>
            </Collapsible>
          </View> */}

          <View style={{marginBottom: '4%'}}>{this.openHomeList()}</View>
        </Content>
      </Container>
    );
  }
}
