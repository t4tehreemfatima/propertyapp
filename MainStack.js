// drawer navigator

import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import SignUp from './Components/Authen/SignUp';
import SignIn from './Components/Authen/SignIn';
import Account from './Screens/Account';
import BottomTab from './Constant/BottomTab';
import FlashPg from './Components/Authen/FlashPg';
import ForgotPass from './Components/Authen/FlashPg';
import Home from './Screens/Home';
import AddProperty from './Screens/AddProperty';
import ListProperty from './Screens/ListProperty';
import Chat from './Screens/Chat';
import MessageScreen from './Screens/MessageScreen';
import {createDrawerNavigator} from '@react-navigation/drawer';
import DrawerSrc from './Screens/DrawerScr';
import Settings from './Screens/Settings';
import Notification from './Screens/Notification';
import TermCondition from './Screens/TermCondition';
import ChangePass from './Screens/ChangePass';
import ChatHeader from './Constant/ChatHeader';
import Profile from './Screens/Profile';
import UpdateProfile from './Screens/UpdateProfile';
import RentalDetail from './Screens/RentalDetail';
import WorkDetail from './Screens/WorkDetail';
import FindProperties from './FindProperties';
import SearchPg1 from './Screens/SearchPg1';
import SearchPg2 from './Screens/SearchPg2';
import ViewCartDetails from './Screens/ViewCartDetail';
import Tasks from './Screens/Tasks';
import FilterProperties from './Screens/FilterProperties';
import ApplicationHome from './Screens/ApplicationHome';
import EditProperty from './Screens/EditPropertry';
import Features from './Screens/Features';
import CreateTask from './Screens/CreateTask';
import CreateProperty from './Screens/CreateProperty';
import ManageRent from './Screens/ManageRent';
import History from './Screens/History';
import TendentDetail from './Screens/TendentDetail';
import TendentRent from './Screens/TendentRent';
import Pay from './Screens/Pay';
import Subscriptionscr from './Screens/Subscriptionsrc';
import Verification from './Screens/Verification';
import paswordVerification from './Screens/paswordVerification';
import Payment from './Screens/Payment';
import MySubs from './Screens/MySubs';
import Chat_tenant from './Screens/Chat_tenant';
import inbox_tenant from './Screens/inbox_tenant';
import PreviosHome from './Screens/PreviousHome';
import Agree from './Screens/Agree';
const Drawer = createDrawerNavigator();

const Stack = createStackNavigator();
export default class MainStack extends React.Component {
  render() {
    return (
      <Drawer.Navigator
        drawerContentOptions={{
          activeTintColor: 'red',
          inactiveTintColor: 'blue',
        }}
        drawerContent={(props) => <DrawerSrc {...props} />}
        initialRouteName="Home">
        <Drawer.Screen name="Home" component={BottomTab} />
        <Drawer.Screen name="Settings" component={Settings} />
        {/* <Drawer.Screen name="MessageScreen" component={MessageScreen} /> */}
        <Drawer.Screen name="Notification" component={Notification} />
        <Drawer.Screen name="TermCondition" component={TermCondition} />
        <Drawer.Screen name="ChangePass" component={ChangePass} />
        <Drawer.Screen name="ChatHeader" component={ChatHeader} />
        <Drawer.Screen name="Profile" component={Profile} />
        <Drawer.Screen name="UpdateProfile" component={UpdateProfile} />
        <Drawer.Screen name="RentalDetail" component={RentalDetail} />
        <Drawer.Screen name="WorkDetail" component={WorkDetail} />
        <Drawer.Screen name="FindProperties" component={FindProperties} />
        <Drawer.Screen name="SearchPg1" component={SearchPg1} />
        <Drawer.Screen name="SearchPg2" component={SearchPg2} />
        <Drawer.Screen name="ViewCartDetails" component={ViewCartDetails} />
        <Drawer.Screen name="FilterProperties" component={FilterProperties} />
        <Drawer.Screen name="Tasks" component={Tasks} />
        <Drawer.Screen name="EditProperty" component={EditProperty} />
        <Drawer.Screen name="Features" component={Features} />
        <Drawer.Screen name="CreateTask" component={CreateTask} />
        <Drawer.Screen name="ManageRent" component={ManageRent} />
        <Drawer.Screen name="ApplicationHome" component={ApplicationHome} />
        <Drawer.Screen name="History" component={History} />
        <Drawer.Screen name="TendentDetail" component={TendentDetail} />
        <Drawer.Screen name="TendentRent" component={TendentRent} />
        <Drawer.Screen name="Pay" component={Pay} />
        <Drawer.Screen name="Subscriptionscr" component={Subscriptionscr} />
        <Drawer.Screen name="Verification" component={Verification} />
        <Drawer.Screen name="Payment" component={Payment} />
        <Drawer.Screen name="MySubs" component={MySubs} />
        {/* <Drawer.Screen name="CreateProperty" component={CreateProperty} /> */}
        <Drawer.Screen name="Chat_tenant" component={Chat_tenant} />
        <Drawer.Screen name="inbox_tenant" component={inbox_tenant} />
        <Drawer.Screen name="PreviosHome" component={PreviosHome} />
        <Drawer.Screen name="Agree" component={Agree} />

        <Drawer.Screen
          name="paswordVerification"
          component={paswordVerification}
        />

        {/* <Drawer.Screen name="CreateProperty" component={CreateProperty} /> */}
      </Drawer.Navigator>
    );
  }
}

{
  /* <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen name="SignIn" component={SignIn} />
          <Stack.Screen name="SignUp" component={SignUp} />
          <Stack.Screen name="FlashPg" component={FlashPg} />
          <Stack.Screen name="BottomTab" component={BottomTab} />
          <Stack.Screen name="ForgotPass" component={ForgotPass} />
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="AddProperty" component={AddProperty} />
          <Stack.Screen name="ListProperty" component={ListProperty} />
          <Stack.Screen name="Chat" component={Chat} />
          <Stack.Screen name="Account" component={Account} />
          <Stack.Screen name="MessageScreen" component={MessageScreen} />
        </Stack.Navigator> */
}
