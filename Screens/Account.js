import React from 'react';
import {
  View,
  Text,
  Touchable,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {Container, Content, Card, CardItem, Body} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import MyHeader from '../Constant/MyHeader';
import {Picker} from '@react-native-picker/picker';
import * as color from '../Constant/Colors';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import CIcon from 'react-native-vector-icons/FontAwesome';
import {ScrollView} from 'react-native-gesture-handler';
import {StackedBarChart} from 'react-native-chart-kit';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
// const data = {
//   labels: ['Test1', 'Test2', 'Test2', 'Test2'],
//   legend: ['L1', 'L2', 'L3'],
//   data: [
//     [20, 30, 20],
//     [60, 20, 20],
//     [60, 40, 30],
//     [60, 60, 60],
//   ],
//   barColors: ['#33acdd', '#65c0e5', '#6464ff'],
// };

// prettier-ignore
const month_names = {
  '1': 'Jan',
  '2': 'Feb',
  '3': 'Mar',
  '4': 'Apr',
  '5': 'May',
  '6': 'June',
  '7': 'July',
  '8': 'Aug',
  '9': 'Sep',
  '10': 'Oct',
  '11': 'Nov',
  '12': 'Dec',
};

let data;
const screenWidth = Dimensions.get('window').width;

const chartConfig = {
  backgroundColor: '#e26a00',
  backgroundGradientFrom: '#dae9e9',
  backgroundGradientTo: '#98d5ee',

  // barPercentage: 4,
  decimalPlaces: 2, // optional, defaults to 2dp
  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
  labelColor: (opacity = 1) => `rgb(0,0,21)`,
  // propsForLabels: {
  //   marginLeft: 10,
  // },

  style: {
    borderRadius: 16,
    paddingLeft: 20,
  },
  propsForDots: {
    r: '6',
    strokeWidth: '2',
    stroke: '#ffa726',
  },
};
let keys;
let values;

export default class Account extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      property: '',
      duration: '',
      inputVal: '',
      quarter: '',
      isLoading: true,
      upcom_data: '',
      rent_data: '',
      utility_avg: '',
      graph_data: '',
      graphdata: [],
      xValues: [],
      yValues: [],
      propertyName: [],
    };
  }
  async upcoming_data() {
    this.setState({isLoading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);

      const response4 = await this.api.graph(parse.token);
      const response = await this.api.upcoming(parse.token);
      const response2 = await this.api.rent_sum(parse.token);
      const response3 = await this.api.utilitypaid_avg(parse.token);

      this.setState({
        upcom_data: response,
        rent_data: response2,
        utility_avg: response3,
        graph_data: response4,
      });
      console.log(response3, '--------->GRAPH DATA');

      keys = Object.keys(response4.data);

      let months = [];

      keys.forEach((element) => {
        //console.log(month_names[element], 'hhhhhhhhhhhhhhh');

        months.push(month_names[element]);
      });

      let yVal = [];
      let prp_name = [];

      //console.log(response4.data, 'HEKEKEKEKEK');
      let tmp = [];

      let color_name = '#33acdd';
      let convert_number;
      let inc;

      for (const property in response4.data) {
        let amo_list = [];
        let checkpoint = false;
        response4.data[property].forEach((element) => {
          //console.log('dddd', element);
          if (checkpoint == true) {
            if (!(element.property_name in yVal)) {
              yVal.push(element.property_name);

              convert_number = parseInt(color_name.slice(-2), 16);

              // convert_number++;
            }
          } else {
            checkpoint = true;
          }

          console.log(convert_number, 'CONVERTTTTTTT');

          // inc = convert_number++;

          amo_list.push(parseInt(element.amoutn));
        });
        //console.log(amo_list, 'zzzzz');
        tmp.push(amo_list);
      }

      let build_colors = function (start, end, n) {
        //Distance between each color
        let steps = [
          (end[0] - start[0]) / n,
          (end[1] - start[1]) / n,
          (end[2] - start[2]) / n,
        ];

        //Build array of colors
        let colors = [start];
        for (let ii = 0; ii < n - 1; ++ii) {
          colors.push([
            Math.floor(colors[ii][0] + steps[0]),
            Math.floor(colors[ii][1] + steps[1]),
            Math.floor(colors[ii][2] + steps[2]),
          ]);
        }
        colors.push(end);

        return colors;
      };

      let colors = build_colors([0, 255, 212], [0, 0, 255], yVal.length);

      function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? '0' + hex : hex;
      }

      function rgbToHex(r, g, b) {
        return '#' + componentToHex(r) + componentToHex(g) + componentToHex(b);
      }
      let hexcolors = [];
      for (var ii = 0; ii < yVal.length; ++ii) {
        hexcolors.push(rgbToHex(colors[ii][0], colors[ii][1], colors[ii][2]));
      }
      console.log('dfdfdfdf', hexcolors);
      data = {
        labels: months,
        legend: yVal,
        data: tmp,
        barColors: hexcolors,
      };

      this.setState({
        xValues: keys,
        yValues: yVal,
        propertyName: tmp,
        isLoading: false,
      });

      console.log(keys, '--------------key');
    } catch (error) {
      this.setState({isLoading: false});
      console.log(error, 'errr');
    }
  }
  componentDidMount() {
    this.upcoming_data();
  }

  // graphData() {}

  upcoming_card = () => {
    if (this.state.upcom_data.data.length == 0) {
      return (
        <Card
          style={{
            marginBottom: '5%',
          }}>
          <CardItem>
            <Body style={{alignItems: 'center'}}>
              <Text style={{color: '#e0e0e0'}}>No Upcoming</Text>
            </Body>
          </CardItem>
        </Card>
      );
    } else {
      return this.state.upcom_data.data.map((element, i) => {
        return (
          <Card
            style={{
              height: '75%',
            }}>
            <CardItem>
              <Body style={{alignItems: 'center'}}>
                <Text>{element.property_name}</Text>
                <Text>{element.Payable_date}</Text>
                <Text style={{fontSize: 11}}>
                  Left Days:
                  <Text style={{color: 'green', fontWeight: 'bold'}}>
                    {element.days_left}
                  </Text>
                </Text>
                <Text>Amount</Text>
                <Text style={{fontWeight: 'bold', fontSize: 15}}>
                  {element.amount}
                </Text>
              </Body>
            </CardItem>
            <CardItem
              footer
              style={{
                backgroundColor: '#bbedf8',
                width: '100%',
              }}>
              {/* <Text style={{fontWeight: 'bold'}}>ViewDetails</Text> */}
            </CardItem>
          </Card>
        );
      });
    }
  };
  //---------------------------------------------------RENT
  rent_card = () => {
    if (this.state.rent_data.data.length == 0) {
      return (
        <Card
          style={{
            marginBottom: '5%',
          }}>
          <CardItem>
            <Body style={{alignItems: 'center'}}>
              <Text style={{color: '#e0e0e0'}}>No Rent</Text>
            </Body>
          </CardItem>
        </Card>
      );
    } else {
      return this.state.rent_data.data.map((element, i) => {
        return (
          <Card
            style={{
              height: '75%',
            }}>
            <CardItem>
              <Body style={{alignItems: 'center'}}>
                <Text>{element.property_name}</Text>
                {/* <Text>{element.Payable_date}</Text>
                <Text style={{fontSize: 11}}>
                  Left Days
                  <Text style={{color: 'green', fontWeight: 'bold'}}>
                    {element.days_left}
                  </Text>
                </Text> */}
                <Text>Amount</Text>
                <Text style={{fontWeight: 'bold', fontSize: 15}}>
                  {element.sum_of_rent}
                </Text>
              </Body>
            </CardItem>
            <CardItem
              footer
              style={{
                backgroundColor: '#bbedf8',
                width: '100%',
              }}>
              {/* <Text style={{fontWeight: 'bold'}}>ViewDetails</Text> */}
            </CardItem>
          </Card>
        );
      });
    }
  };
  utility_card = () => {
    if (this.state.utility_avg.data.length == 0) {
      return (
        <Card
          style={{
            marginBottom: '5%',
          }}>
          <CardItem>
            <Body style={{alignItems: 'center'}}>
              <Text style={{color: '#e0e0e0'}}>No Utility</Text>
            </Body>
          </CardItem>
        </Card>
      );
    } else {
      return this.state.utility_avg.data.map((element, i) => {
        return (
          <Card
            style={{
              height: '75%',
            }}>
            <CardItem>
              <Body style={{alignItems: 'center'}}>
                <Text>{element.property_name}</Text>

                <Text>Amount</Text>
                <Text style={{fontWeight: 'bold', fontSize: 15}}>
                  {element.avg_of_utility}
                </Text>
              </Body>
            </CardItem>
            <CardItem
              footer
              style={{
                backgroundColor: '#bbedf8',
                width: '100%',
              }}>
              {/* <Text style={{fontWeight: 'bold'}}>View Details</Text> */}
            </CardItem>
          </Card>
        );
      });
    }
  };
  render() {
    // console.log(
    //   this.state.yValues,
    //   this.state.xValues,
    //   this.state.propertyName,
    //   'CHEK YYYYYYYY',
    // );
    if (this.state.isLoading == true) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: '35%',
          }}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      return (
        <Container>
          <MyHeader
            headername="1"
            name="Account"
            leftIconPress={() => this.props.navigation.openDrawer()}
          />
          {this.state.graph_data.length == 0 ? (
            <Content>
              <View>
                <Text>No data</Text>
              </View>
            </Content>
          ) : (
            <Content style={{marginTop: '2%'}}>
              {this.state.graph_data.data.length == 0 ? (
                <View>
                  <Text
                    style={{
                      fontSize: 17,
                      alignSelf: 'center',
                      position: 'absolute',
                      elevation: 2,
                      marginTop: '25%',
                    }}>
                    No Data Found
                  </Text>
                  <View style={{opacity: 0.3, backgroundColor: '#e0e0e0'}}>
                    <StackedBarChart
                      data={data}
                      width={screenWidth}
                      height={220}
                      chartConfig={chartConfig}
                    />
                  </View>
                </View>
              ) : (
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: -1,
                  }}>
                  <StackedBarChart
                    data={data}
                    width={screenWidth}
                    height={220}
                    chartConfig={chartConfig}
                  />
                </View>
              )}

              {/* --------------------------------------------------------UCOMONG---------- */}

              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: '4%',
                  marginTop: '2%',
                  alignItems: 'center',
                }}>
                <Text style={{color: 'green', fontWeight: 'bold'}}>
                  UPCOMMING
                </Text>
                <Icon
                  name="arrow-circle-right"
                  color={`${color.card1_Color}`}
                  size={24}
                  style={{right: '5%', marginLeft: '3%'}}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',

                  marginLeft: '2%',
                  marginRight: '2%',
                }}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {this.upcoming_card()}
                </ScrollView>
              </View>

              {/* -------------------------------------------------UILITy---------------- */}

              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: '4%',
                  marginTop: '2%',
                  alignItems: 'center',
                }}>
                <Text style={{color: 'green', fontWeight: 'bold'}}>
                  UTILITY
                </Text>
                <Icon
                  name="arrow-circle-right"
                  color={`${color.card1_Color}`}
                  size={24}
                  style={{right: '5%', marginLeft: '3%'}}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',

                  marginLeft: '2%',
                  marginRight: '2%',
                }}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {this.utility_card()}
                </ScrollView>
              </View>
              {/* -------------------------------------------------RENt----------------------------- */}

              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: '4%',
                  marginTop: '2%',
                  alignItems: 'center',
                }}>
                <Text style={{color: 'green', fontWeight: 'bold'}}>RENT</Text>
                <Icon
                  name="arrow-circle-right"
                  color={`${color.card1_Color}`}
                  size={24}
                  style={{right: '5%', marginLeft: '3%'}}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',

                  marginLeft: '2%',
                  marginRight: '2%',
                }}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {this.rent_card()}
                </ScrollView>
              </View>
            </Content>
          )}
        </Container>
      );
    }
  }
}
