import {Container, Content, Tab, Tabs, Text} from 'native-base';
import React from 'react';
import Privicy from './Privicy';
import * as color from '../Constant/Colors';
import Services from './Services';
import MyHeader from '../Constant/MyHeader';
export default class Agree extends React.Component {
  render() {
    return (
      <Container>
        <MyHeader
          headername="4"
          name="Terms & Conditions"
          // rightIconPress={() => alert('ACCOUNT PRESS')}
          // rightIconName="settings"
          onPressIcon={() => this.props.navigation.goBack()}
        />
        <Tabs
          tabBarUnderlineStyle={{
            backgroundColor: `${color.theme_Color}`,
          }}>
          <Tab
            textStyle={{color: 'black', fontWeight: 'bold'}}
            activeTextStyle={{color: 'black', fontWeight: 'bold'}}
            tabStyle={{backgroundColor: 'white'}}
            activeTabStyle={{backgroundColor: 'white'}}
            heading="Privicy">
            <Privicy />
          </Tab>
          <Tab
            textStyle={{color: 'black', fontWeight: 'bold'}}
            activeTextStyle={{color: 'black', fontWeight: 'bold'}}
            tabStyle={{backgroundColor: 'white'}}
            activeTabStyle={{backgroundColor: 'white'}}
            heading="Terms Services">
            <Services />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
