import React from 'react';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import {
  Container,
  Content,
  Title,
  Tab,
  Tabs,
  Thumbnail,
  Card,
} from 'native-base';
import {
  View,
  Text,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import * as color from '../Constant/Colors';
import {useFocusEffect} from '@react-navigation/native';
import AntDesign from 'react-native-vector-icons/AntDesign';
const backAction = () => {
  BackHandler.exitApp();
  return true;
};

function HardwareBackBtn() {
  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', backAction);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
    }, []),
  );

  return null;
}

export default class ApplicationHome extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      myData: {},
      isLoading: true,
      filter_input: '',
      filter_loader: false,
    };
  }
  //Filter by name
  async filter_property(_name) {
    this.setState({filter_loader: true});

    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);

      const response = await this.api.property_filter(
        _name,
        parse.id,
        parse.token,
      );

      this.setState({filter_input: response, filter_loader: false});
      console.log(response[0], 'response h yaa ');
    } catch (error) {
      console.log(error, 'err');
    }
  }
  //property data
  async tendent_select() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.property_tendent_select(
        parse.id,
        parse.token,
      );

      console.log(
        response,
        'AOPLICATION HONE______----------------------------------------0000',
      );

      this.setState({myData: response, isLoading: false});
      // console.log(this.state.myData, 'mtdata');
    } catch (err) {
      console.log('Error: ', err);
    }
  }
  //MAP FUNTION FOR DATA VIEW
  openHomeList = () => {
    return this.state.myData.data.map((element, i) => {
      console.log(
        element,
        '-------------elelemem imsg-------))))_s------------------',
      );

      return (
        <Card
          key={i}
          style={{
            flexDirection: 'row',
            marginLeft: '3%',
            marginRight: '3%',
            alignItems: 'center',
            borderWidth: 0.1,
            elevation: 1,
            padding: '3%',
            marginTop: '3%',

            // justifyContent: 'space-between',
            backgroundColor: `${color.backgroundColor}`,
          }}>
          {element.image.length > 0 ? (
            <Thumbnail
              square
              source={{
                uri: `${color.link}${element.image[0].path}`,
              }}
            />
          ) : (
            <Thumbnail square source={require('../assets/home.png')} />
          )}

          <View style={{width: '70%', marginLeft: '5%'}}>
            <Text
              style={{fontWeight: 'bold', fontSize: 16}}
              onPress={() =>
                this.props.navigation.navigate('ViewCartDetails', {
                  key: element.property_id,
                  status: element.status,
                })
              }>
              {element.name.length < 20
                ? `${element.name}`
                : `${element.name.substring(0, 20)}...`}
              {/* {element.name} */}
            </Text>
            <Text>{element.approved_date}</Text>
            <Text>Rent: {element.rent}</Text>
            <Text>City: {element.city}</Text>
          </View>
          <View style={{position: 'absolute', right: 6, top: 4}}>
            <Text style={{fontSize: 11}}>Status</Text>
            <Text
              style={{
                color: 'green',
                fontWeight: 'bold',
                textTransform: 'capitalize',
                fontSize: 11,
              }}>
              {element.status}
            </Text>
          </View>
        </Card>
      );
    });
  };
  // componentDidMount() {
  //   this.tendent_select();

  // }

  componentDidMount() {
    const {navigation} = this.props;

    this.focusListener = navigation.addListener('focus', () => {
      this.tendent_select();
      console.log('cosolllllll');
    });
    // this.focusListenerr = navigation.addListener('blur', () => {
    //   console.log('un cosolllllll');
    // });
    this.tendent_select();
  }
  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }
  //filter List
  filterList() {
    if (this.state.filter_input.data == undefined) {
      console.log(this.state.filter_input.filter_input, 'ress');
      return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      return this.state.filter_input.data.map((element, i) => {
        if (this.state.filter_input.data == undefined) {
          return (
            <View>
              <Text>No record found!</Text>
            </View>
          );
        } else {
          return (
            <Card
              key={i}
              style={{
                flexDirection: 'row',
                marginLeft: '3%',
                marginRight: '3%',
                alignItems: 'center',
                borderWidth: 0.1,
                elevation: 1,
                padding: '3%',
                marginTop: '5%',
              }}>
              {element.image.length > 0 ? (
                <Thumbnail
                  square
                  source={{
                    uri: `${color.link}${element.image[0].path}`,
                  }}
                />
              ) : (
                <Thumbnail square source={require('../assets/home.png')} />
              )}
              <View style={{marginLeft: '5%'}}>
                <Text
                  onPress={() =>
                    this.props.navigation.navigate('ViewCartDetails', {
                      key: element.property_id,
                      status: element.status,
                    })
                  }
                  style={{fontWeight: 'bold', fontSize: 16}}>
                  {element.name.length < 20
                    ? `${element.name}`
                    : `${element.name.substring(0, 20)}...`}
                </Text>
                <Text>Rent: {element.rent}</Text>
                <Text>City: {element.city}</Text>
              </View>
              <View style={{position: 'absolute', right: 6, top: 4}}>
                <Text style={{fontSize: 11}}>Status</Text>
                <Text
                  style={{
                    color: 'green',
                    fontWeight: 'bold',
                    textTransform: 'capitalize',
                    fontSize: 11,
                  }}>
                  {element.status}
                </Text>
              </View>
            </Card>
          );
        }
      });
    }
  }
  render() {
    console.log(this.state.myData, 'filterinuttttttttttttt');
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      return (
        <Container>
          <HardwareBackBtn />

          <Content>
            <View
              style={{
                marginTop: '5%',
                marginLeft: '5%',
                marginRight: '5%',
              }}>
              <MaterialIcon
                name="search"
                size={30}
                color="grey"
                style={{
                  position: 'absolute',
                  alignSelf: 'center',
                  right: '5%',
                  top: '10%',
                }}
              />

              <TextInput
                style={{
                  marginTop: '-3%',
                  borderColor: 'cadetblue',
                  borderBottomWidth: 1,
                }}
                keyboardType="email-address"
                placeholder="Find Properties"
                onChangeText={(text) => this.filter_property(text)}
              />
            </View>

            {this.state.filter_input != '' ? (
              !this.state.filter_loader ? (
                <View>{this.filterList()}</View>
              ) : (
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: '10%',
                  }}>
                  <AntDesign name="home" size={30} color="grey" />
                  <Text style={{marginTop: '2%', color: 'grey'}}>
                    No properties found!
                  </Text>
                </View>
              )
            ) : this.state.myData.data.length == 0 ? (
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop: '10%',
                }}>
                <AntDesign name="home" size={30} color="grey" />
                <Text style={{marginTop: '2%', color: 'grey'}}>
                  You haven't apply for any properties yet!
                </Text>
              </View>
            ) : (
              <View>{this.openHomeList()}</View>
            )}

            {/* {this.state.filter_input !== '' ? (
              !this.state.filter_loader ? (
                <View>{this.filterList()}</View>
              ) : (
                <View>
                  <Text>Heloooo</Text>
                </View>
              )
            ) : (
              <View>{this.openHomeList()}</View>
            )} */}
            {/* <View>{this.openHomeList()}</View> */}
          </Content>
        </Container>
      );
    }
  }
}
