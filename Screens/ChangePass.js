import React from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ImageBackground,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import MyHeader from '../Constant/MyHeader';
import * as color from '../Constant/Colors';
import Icon from 'react-native-vector-icons/Ionicons';
import {Button} from 'react-native-paper';
import {Container, Content, Image} from 'native-base';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ValidationComponent from 'react-native-form-validator';

class ChangePass extends ValidationComponent {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      oldPass: true,
      pass: true,
      icon: 'eye-off',
      cicon: 'eye-off',
      oicon: 'eye-off',
      confirmpass: true,
      isLoading: true,
      data: {},
      //password field state
      password: '',
      oldPassword: '',
      confirmpassword: '',
      _loader: false,
    };
  }
  //validation

  _onSubmit() {
    this.validate({
      password: {required: true},
      oldPassword: {required: true},
      confirmpassword: {equalPassword: this.state.password},
    });
    if (this.isFormValid()) {
      this.passwordReset(this.state.oldPassword, this.state.password);
      console.log('ok');
    }
  }
  //////
  async passwordReset(_current_password, _new_password) {
    this.setState({_loader: true});
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response = await this.api.passwordreset(
      parse.id,
      _current_password,
      _new_password,
      parse.token,
    );
    console.log(parse.id, _current_password, _new_password, parse.token);
    console.log(response, 'PASS RESET');
    if (response.data[0].password_reset) {
      this.setState({_loader: false});
      alert(response.data[0].password_reset);
      this.props.navigation.navigate('SignIn');
    } else {
      this.setState({_loader: false});
      alert('Try Again');
    }
  }

  OldPasswordVisibility = () => {
    if (this.state.oldPass == true) {
      this.setState({oldPass: false, oicon: 'eye'});
    } else {
      this.setState({oldPass: true, oicon: 'eye-off'});
    }
  };
  PasswordVisibility = () => {
    if (this.state.pass == true) {
      this.setState({pass: false, icon: 'eye'});
    } else {
      this.setState({pass: true, icon: 'eye-off'});
    }
  };
  ConfirmPasswordVisibility = () => {
    if (this.state.confirmpass == true) {
      this.setState({confirmpass: false, cicon: 'eye'});
    } else {
      this.setState({confirmpass: true, cicon: 'eye-off'});
    }
  };
  render() {
    return (
      <Container>
        <MyHeader
          headername="4"
          name="Change Password"
          // rightIconPress={() => alert('ACCOUNT PRESS')}
          // rightIconName="settings"
          onPressIcon={() => this.props.navigation.goBack()}
        />

        {/* <View
          style={{
            borderWidth: 0.0,
            elevation: 1,
            height: '63%',
            width: '95%',
            alignSelf: 'center',
            padding: '5%',
            marginTop: '3%',
            
          }}> */}
        <Content>
          <View style={{marginLeft: '3%', marginRight: '3%', marginTop: '8%'}}>
            <View>
              <TextInput
                onChangeText={(text) => this.setState({oldPassword: text})}
                style={{
                  height: 40,
                  borderColor: 'gray',
                  borderBottomWidth: 1,
                }}
                placeholder="Current Password"
                secureTextEntry={this.state.oldPass}
              />
              <Icon
                name={this.state.oicon}
                onPress={() => this.OldPasswordVisibility()}
                color={color.icon_color}
                size={20}
                style={{position: 'absolute', right: 12, top: 10}}
              />
              {this.isFieldInError('oldPassword') &&
                this.getErrorsInField('oldPassword').map((errorMessage, i) => (
                  <Text
                    key={i}
                    style={{
                      color: 'red',
                    }}>{`Current password is required`}</Text>
                ))}
            </View>

            <View style={{marginTop: 33}}>
              <TextInput
                onChangeText={(text) => this.setState({password: text})}
                style={{
                  height: 40,
                  borderColor: 'gray',
                  borderBottomWidth: 1,
                }}
                placeholder="New Password"
                secureTextEntry={this.state.pass}
              />
              <Icon
                name={this.state.icon}
                onPress={() => this.PasswordVisibility()}
                color={color.icon_color}
                size={20}
                style={{position: 'absolute', right: 12, top: 10}}
              />
              {this.isFieldInError('password') &&
                this.getErrorsInField('password').map((errorMessage, i) => (
                  <Text
                    key={i}
                    style={{
                      color: 'red',
                    }}>{`Enter new password`}</Text>
                ))}
            </View>
            <View style={{marginTop: 33}}>
              <TextInput
                onChangeText={(text) => this.setState({confirmpassword: text})}
                style={{
                  height: 40,
                  borderColor: 'gray',
                  borderBottomWidth: 1,
                }}
                placeholder="Confirm Password"
                secureTextEntry={this.state.confirmpass}
              />
              <Icon
                name={this.state.cicon}
                onPress={() => this.ConfirmPasswordVisibility()}
                color={color.icon_color}
                size={20}
                style={{position: 'absolute', right: 12, top: 10}}
              />

              {this.isFieldInError('confirmpassword') &&
                this.getErrorsInField('confirmpassword').map(
                  (errorMessage, i) => (
                    <Text
                      key={i}
                      style={{
                        color: 'red',
                      }}>{`Password doesnot match`}</Text>
                  ),
                )}
            </View>
            <TouchableOpacity
              onPress={() => this._onSubmit()}
              style={{
                height: 50,
                width: 200,
                borderRadius: 5,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: `${color.header3_color}`,
                marginTop: '10%',
                alignSelf: 'center',
              }}>
              {this.state._loader ? (
                <ActivityIndicator color={'white'} />
              ) : (
                <Text style={{color: 'white'}}>CHANGE PASSWORD</Text>
              )}
            </TouchableOpacity>
            {/* <Button
              style={{
                width: '80%',
                backgroundColor: `${color.btn_Color}`,
                alignSelf: 'center',
                marginTop: 32,
                borderRadius: 4,
              }}
              mode="contained"
              onPress={() =>
                this.passwordReset(this.state.oldPassword, this.state.password)
              }>
              Change Password
            </Button> */}
          </View>
        </Content>
      </Container>
      // </View>
    );
  }
}
export default ChangePass;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#fff',
    // justifyContent: 'center',
    // alignSelf:'center'
  },
  input: {
    // width: '90%',
    alignSelf: 'center',
    marginTop: '5%',
  },
});
