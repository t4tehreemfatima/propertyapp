import React from 'react';
import {View, TouchableOpacity, BackHandler} from 'react-native';
import ChatHeader from '../Constant/ChatHeader';
import {useFocusEffect} from '@react-navigation/native';

import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
  Text,
} from 'native-base';
import API from '../Constant/API';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

import MyHeader from '../Constant/MyHeader';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import * as color from '../Constant/Colors';
const backAction = () => {
  BackHandler.exitApp();
  return true;
};

function HardwareBackBtn() {
  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', backAction);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
    }, []),
  );

  return null;
}
export default class Chat_tenant extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      isLoading: true,
      myData: '',
    };
    //console.log(this.props.navigation, 'constru ka h');
  }
  async chat_property_list() {
    this.setState({isLoading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      console.log(parse.token, 'token ten');
      const response = await this.api.property_tendent_select(
        parse.id,
        parse.token,
      );
      console.log(parse.id, parse.token, 'Data Response---------------');
      this.setState({myData: response, isLoading: false});
      // console.log(response, 'ppppppppdata');
    } catch (err) {
      console.log('Error: ', err);
    }
  }
  getconvolist(){
    console.log("alksdlkjlkj form data")
            var formdata = new FormData();
        formdata.append("userid", "4");

        var requestOptions = {
          method: 'POST',
          body: formdata,
          redirect: 'follow'
        };

        fetch("http://f450-175-107-219-203.ngrok.io/Get_convo_list", requestOptions)
          .then(response => response.text())
          .then(result => console.log(result,"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssss"))
          .catch(error => console.log('error', error));
  }

 componentDidMount () {
    const {navigation} = this.props;
    this.focusListener = navigation.addListener('focus', async() => {
      // console.log('cosolllllll');
      this.getconvolist()
      this.chat_property_list();
    });
    this.chat_property_list();
  }

  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }
  //   componentDidMount() {
  //     this.chat_property_list();
  //   }
  openHomeList = () => {
    if (this.state.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: '80%',
          }}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      // if (this.state.myData.property_data[0]==null) {
      //   return(
      //     <View>
      //     <Text
      //       style={{
      //         marginTop: '80%',
      //         alignSelf: 'center',
      //         fontSize: 30,
      //         color: '#e0e0e0',
      //       }}>
      //       No Chat
      //     </Text>
      //   </View>
      //   )
      // } else {

      return this.state.myData.data.map((element, i) => {
        if (element.status == 'approved') {
          console.log(element, 'jj');
          return (
            <List>
              <ListItem
                avatar
                onPress={() =>
                  this.props.navigation.navigate('MessageScreen', {
                    name: element.name,
                    id: element.id,
                  })
                }>
                {element.image.length > 0 ? (
                  <Thumbnail
                    square
                    source={{
                      uri: `${color.link}${element.image[0].path}`,
                    }}
                  />
                ) : (
                  <Thumbnail
                    square
                    source={{
                      uri:
                        'https://cdn.iconscout.com/icon/free/png-256/business-group-2172454-1823675.png',
                    }}
                  />
                )}

                <Body>
                  <Text>{element.name}</Text>
                  <Text note>No Messsage</Text>
                </Body>
                <Right>{/* <Text note>3:43 pm</Text> */}</Right>
              </ListItem>
            </List>
            //----------------------------------------------
            // <View
            //   key={i}
            //   style={{
            //     flexDirection: 'row',
            //     marginLeft: '3%',
            //     marginRight: '3%',
            //     alignItems: 'center',
            //     borderWidth: 0.1,
            //     elevation: 1,
            //     padding: '3%',
            //     marginTop: '5%',
            //   }}>
            // {element.images.length > 0 ? (
            //   <Thumbnail
            //     square
            //     large
            //     source={{
            //       uri: `http://property.gtb2bexim.com/${element.images[0].path}`,
            //     }}
            //   />
            // ) : (
            //   <Thumbnail
            //     square
            //     large
            //     source={{
            //       uri:
            //         'https://i.pinimg.com/474x/b4/8f/3b/b48f3bb31399b66b2d5000a161032a1d.jpg',
            //     }}
            //   />
            // )}

            //   <View style={{marginLeft: '5%'}}>
            //     <Text
            //       onPress={() =>
            //         this.props.navigation.navigate('ViewCartDetails', {
            //           key: element.id,
            //         })
            //       }
            //       style={{fontWeight: 'bold', fontSize: 16}}>
            //       {element.name}
            //     </Text>
            //     <Text>Rent: {element.rent}$</Text>
            //     <Text style={{color: 'grey'}}>City: {element.city}</Text>
            //   </View>
            //   <View style={{marginLeft: '15%'}}>
            //     <Text style={{fontWeight: 'bold', color: 'green'}}>Status</Text>
            //     <Text style={{color: 'green', fontWeight: 'bold'}}>
            //       {element.status}
            //     </Text>
            //   </View>
            // </View>
          );
        }
      });
      // }
    }
  };

  render(props) {
    return (
      <Container>
        {/* <ChatHeader leftIcon={() => this.props.navigation.openDrawer()} /> */}
        <MyHeader
          name="Chat"
          headername="1"
          leftIconPress={() => this.props.navigation.openDrawer()}
        />
        <HardwareBackBtn />
        {this.state.isLoading ? (
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: '35%',
            }}>
            <Spinner type="Circle" color={color.header3_color} />
          </View>
        ) : this.state.myData.data.length == 0 ? (
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <SimpleLineIcons name="envelope-open" size={80} color="grey" />
            <Text style={{color: 'grey', marginTop: '2%'}}>
              No messages found!
            </Text>
          </View>
        ) : (
          <Content style={{marginTop: '3%'}}>{this.openHomeList()}</Content>
        )}
      </Container>
    );
  }
}
