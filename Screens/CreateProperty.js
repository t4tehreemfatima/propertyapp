import MyHeader from '../Constant/MyHeader';
import React from 'react';
import {Container, Content, Title, Tab, Tabs, Thumbnail} from 'native-base';
import * as color from '../Constant/Colors';
import {View, Text} from 'react-native';
import Spinner from 'react-native-spinkit';
import CreatePropertyForm from './CreatePropertyForm';
import CurrentProperties from './CurrentProperty';
export default class CreateProperty extends React.Component {
  constructor(props) {
    super(props);
 
    this.second_bacha=React.createRef()
    this.state = {
      type: '',
      index:0,
      isLoading:true
    };
  }
  onClick = (e) => {
    console.log(e,"dfdfdfdfdfdfdfdfdfdfdf")
    if(e.from != 0){
      this.setState({isLoading:true})
      setTimeout(() => {
        console.log(this.state.index)
        this.setState({'index':Math.random()})
        this.setState({isLoading:false})      
      }, 2000);
  
    }

  };
  async componentDidMount() {
    this.setState({isLoading:false})
  }
  createtab=()=>{
    return  <CreatePropertyForm navigation={this.props.navigation}  />
  }
  render() {
    if (this.state.isLoading) {
      return(
<Container>
            <MyHeader
              headername="1"
            name="Create Property"
              leftIconPress={() => this.props.navigation.openDrawer()}
            />
              <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
          </Container>
      )
    } else {
      return (
      <Container>
        <MyHeader
          headername="1"
          name="Create Property"
          //   rightIconPress={() => alert('ACCOUNT PRESS')}
          //   rightIconName="settings"
          // onPressIcon={() => this.props.navigation.navigate()}
          leftIconPress={() => this.props.navigation.openDrawer()}/>
          <Tabs 
             onChangeTab={this.onClick}
        tabBarUnderlineStyle={{backgroundColor: `${color.theme_Color}`}}>
          <Tab
            textStyle={{color: 'black', fontWeight: 'bold'}}
            activeTextStyle={{color: 'black', fontWeight: 'bold'}}
            tabStyle={{backgroundColor: 'white'}}
            activeTabStyle={{backgroundColor: 'white'}}
            heading="Create Property">
              { this.createtab()}
            {/* <CreatePropertyForm navigation={this.props.navigation} /> */}
            {/* <ApplicationHome navigation={this.props.navigation} /> */}
          </Tab>
          <Tab
            textStyle={{color: 'black', fontWeight: 'bold'}}
            activeTextStyle={{color: 'black', fontWeight: 'bold'}}
            tabStyle={{backgroundColor: 'white'}}
            activeTabStyle={{backgroundColor: 'white'}}
            heading="Current properties">
            <CurrentProperties ref={instance => { this.second_bacha = instance; }} navigation={this.props.navigation} />
            {/* <OpenHome navigation={this.props.navigation} /> */}
          </Tab>
        </Tabs>
        {/* </Content> */}
      </Container>
    );
    }
  }
}
