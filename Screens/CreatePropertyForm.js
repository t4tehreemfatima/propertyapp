import React from 'react';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import DateTimePicker from '@react-native-community/datetimepicker';
import ValidationComponent from 'react-native-form-validator';
import {useFocusEffect} from '@react-navigation/native';
import {Picker} from '@react-native-picker/picker';

import {
  Container,
  Content,
  Title,
  Tab,
  Tabs,
  Thumbnail,
  Button,
} from 'native-base';
import {
  View,
  Text,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
  BackHandler,
} from 'react-native';

import moment from 'moment';
import AntDesign from 'react-native-vector-icons/AntDesign';
import * as color from '../Constant/Colors';
import {ScrollView} from 'react-native';
const backAction = () => {
  BackHandler.exitApp();
  return true;
};

function HardwareBackBtn() {
  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', backAction);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
    }, []),
  );

  return null;
}
export default class CreatePropertyForm extends ValidationComponent {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      propertyName: '',
      street: '',
      state: '',
      city: '',
      zipCode: '',
      bedRoom: '0',
      bathRoom: '0',
      rent: '600',
      description: '',
      yearBuild: '',
      images: [],
      isLoading: false,
      data: '',
      loader: false,
      date: new Date(1598051730000),
      mode: 'date',
      show: false,
      currency: '',
      data_currency: '',
      isLoading: true,
      state_data: '',
      city_data: '',
      toilet: '0',
      timelimit: '',
      tenantlimit: '',
    };
  }
  _onSubmit() {
    // Call ValidationComponent validate method
    this.validate({
      propertyName: {required: true},
      rent: {required: true},
      street: {required: true},
      state: {required: true},
      description: {required: true},
      city: {required: true},
      zipCode: {required: true},
      yearBuild: {required: true},
      tenantlimit: {required: true},
      timelimit: {required: true},
    });
    if (this.isFormValid()) {
      this.insret_property();
      console.log('ok');
    }
  }
  //--------------------------------DATE PICKER
  // showMode = (currentMode) => {
  //   this.setState({show: true, mode: currentMode});
  // };

  showDatepicker = () => {
    this.setState({show: true});
  };

  convertTimestamp(_timestamp) {
    let tmpDate = moment(_timestamp.nativeEvent.timestamp).format('YYYY-MM-DD');
    console.log(tmpDate, '---> TMP DATE');
    this.setState({yearBuild: tmpDate, show: false});
  }

  //--------------------------------
  handleConfirm = (date) => {
    alert('rfrfrf');
    console.warn('A date has been picked: ', date);
    this.setState({DateTimePickerModal: false});
  };
  async insret_property() {
    this.setState({loader: true});
    try {
      console.log('---->TRY<--------');
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.insertProperty(
        parse.id,
        this.state.propertyName,
        this.state.bedRoom,
        this.state.bathRoom,
        this.state.toilet,
        this.state.description,
        this.state.state,
        this.state.rent,
        this.state.yearBuild,
        this.state.city,
        this.state.street,
        this.state.zipCode,
        this.state.currency,
        this.state.timelimit,
        this.state.tenantlimit,
        parse.token,
      );

      console.log(response, '---------------------->>>>>insert property resss');
      if (response.data[0].property) {
        this.setState({loader: false});
        // this.props.navigation.navigate("CurrentProperties")
        alert(response.data[0].property);
        this.props.navigation.navigate('EditProperty', {
          key: response.data[0].id,
        });
      } else {
        alert('Try Again');
      }
    } catch (error) {
      this.setState({loader: false});
      console.log('------>CATCH<-------');
      console.log(error, 'err');
    }
  }
  async get_currency() {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response2 = await this.api.state(parse.token);
    // this.setState({city:response2.data[0].name,})

    const response3 = await this.api.cities(this.state.state, parse.token);

    const response = await this.api.currency(parse.token);

    console.log(response3, '--uuuuuu');
    this.setState({
      data_currency: response,
      currency: response.data[0].id,
      state_data: response2,
      city_data: response3,
      // state:response2.data[0].state,
      isLoading: false,
    });
  }

  componentDidMount() {
    this.get_currency();
  }
  picker = () => {
    if (this.state.isLoading) {
      return <ActivityIndicator />;
    } else {
      return this.state.data_currency.data.map((element, i) => {
        return <Picker.Item label={element.currency} value={element.id} />;
      });
    }
  };
  state_picker = () => {
    return this.state.state_data.data.map((element, i) => {
      this.state.city;
      return <Picker.Item label={element.state} value={element.state} />;
    });
  };
  city_picker = () => {
    return this.state.city_data.data.map((element, i) => {
      console.log(element, '-----------elelment----------------');
      return <Picker.Item label={element.city} value={element.city} />;
    });
  };
  async onChange_api(_id) {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response3 = await this.api.cities(_id, parse.token);
    console.log(response3, '00000000000000000000000000000');
    this.setState({city_data: response3, state: _id});
  }
  render() {
    console.log(this.state.city, 'CHECK YEAR');
    if (this.state.isLoading == true) {
      return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      return (
        <Container>
          <ScrollView showsVerticalScrollIndicator={false}>
            <Content style={{marginLeft: '5%', marginRight: '5%'}}>
              <HardwareBackBtn />
              <View style={{paddingTop: '5%'}}>
                <TextInput
                  style={{
                    height: 40,
                    borderColor: 'cadetblue',
                    borderBottomWidth: 1,
                  }}
                  placeholder="Property Name"
                  keyboardType="email-address"
                  onChangeText={(text) => this.setState({propertyName: text})}
                />
              </View>
              {this.isFieldInError('propertyName') &&
                this.getErrorsInField('propertyName').map((errorMessage, i) => (
                  <Text
                    key={i}
                    style={{
                      color: 'red',
                    }}>{`Enter your property name*`}</Text>
                ))}
              <Text
                style={{fontWeight: 'bold', position: 'absolute', top: '10%'}}>
                Address
              </Text>
              {/* ..------------------------------------2nd row----------------------------------------- */}
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginTop: '5%',
                }}>
                <View style={{paddingTop: '5%', width: '45%'}}>
                  <TextInput
                    style={{
                      height: 40,
                      borderColor: 'cadetblue',
                      borderBottomWidth: 1,
                    }}
                    placeholder="Street"
                    keyboardType="email-address"
                    onChangeText={(text) => this.setState({street: text})}
                  />
                  {this.isFieldInError('street') &&
                    this.getErrorsInField('street').map((errorMessage, i) => (
                      <Text
                        key={i}
                        style={{
                          color: 'red',
                        }}>{`required*`}</Text>
                    ))}
                </View>

                <View style={{paddingTop: '5%', width: '45%'}}>
                  <TextInput
                    style={{
                      height: 40,
                      borderColor: 'cadetblue',
                      borderBottomWidth: 1,
                    }}
                    placeholder="Zip/Postal Code"
                    keyboardType="number-pad"
                    onChangeText={(text) => this.setState({zipCode: text})}
                  />
                  {this.isFieldInError('zipCode') &&
                    this.getErrorsInField('zipCode').map((errorMessage, i) => (
                      <Text
                        key={i}
                        style={{
                          color: 'red',
                        }}>{`required*`}</Text>
                    ))}
                </View>

                {/* <View style={{paddingTop: '5%', width: '45%'}}>
                <TextInput
                  style={{
                    height: 40,
                    borderColor: 'cadetblue',
                    borderBottomWidth: 1,
                  }}
                  placeholder="State"
                  keyboardType="email-address"
                  onChangeText={(text) => this.setState({state: text})}
                />
                 {this.isFieldInError('state') &&
            this.getErrorsInField('state').map((errorMessage, i) => (
              <Text
                key={i}
                style={{
                  color: 'red',
                }}>{`required*`}</Text>
            ))}
              </View> */}
              </View>
              {/* -------------------------------==============-3rd row------------------------------------------ */}
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}></View>
              <View
                style={{
                  borderBottomWidth: 1,
                  borderColor: `${color.header3_color}`,
                }}>
                <Text style={{color: 'grey'}}>State</Text>
                <Picker
                  mode="dialog"
                  selectedValue={this.state.state}
                  style={{
                    height: 50,

                    width: '100%',
                    marginLeft: '-1%',
                    marginBottom: '-2.5%',
                    right: '-1%',
                  }}
                  itemStyle={{
                    backgroundColor: 'red',
                    borderBottomWidth: 55,
                  }}
                  onValueChange={
                    (itemValue, itemIndex) =>
                      itemValue != 0 ? this.onChange_api(itemValue) : console

                    // this.setState({state: itemValue}
                  }>
                  <Picker.Item label="Please select state..." value="0" />
                  {this.state_picker()}
                </Picker>
              </View>
              {this.isFieldInError('state') &&
                this.getErrorsInField('state').map((errorMessage, i) => (
                  <Text
                    key={i}
                    style={{
                      color: 'red',
                    }}>{`required*`}</Text>
                ))}
              <View
                style={{
                  borderBottomWidth: 1,
                  borderColor: `${color.header3_color}`,
                }}>
                <Text style={{color: 'grey', marginLeft: '1%'}}>City</Text>
                <Picker
                  mode="dialog"
                  selectedValue={this.state.city}
                  style={{
                    height: 50,
                    width: '100%',
                    marginLeft: '-1%',
                    marginBottom: '-2.5%',
                    right: '-1%',
                  }}
                  itemStyle={{
                    backgroundColor: 'red',
                    borderBottomWidth: 55,
                  }}
                  onValueChange={(itemValue, itemIndex) =>
                    // this.onChange_api(itemValue)
                    itemValue != 0 ? this.setState({city: itemValue}) : console
                  }>
                  <Picker.Item label="Please select city..." value="0" />

                  {this.city_picker()}
                </Picker>
              </View>
              {this.isFieldInError('city') &&
                this.getErrorsInField('city').map((errorMessage, i) => (
                  <Text
                    key={i}
                    style={{
                      color: 'red',
                    }}>{`required*`}</Text>
                ))}
              {/* -------------------------------------------------bed room ------------------------------------------bath room */}
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',

                  // marginRight: '10%',
                }}>
                <View style={{paddingTop: '5%', width: '30%'}}>
                  <Text>Bed Room</Text>
                  <TextInput
                    style={{
                      height: 40,

                      borderColor: 'cadetblue',
                      borderWidth: 1,
                    }}
                    placeholder="2 "
                    keyboardType="number-pad"
                    onChangeText={(text) => this.setState({bedRoom: text})}
                  />
                </View>
                <View style={{paddingTop: '5%', width: '30%'}}>
                  <Text>Bath Room</Text>
                  <TextInput
                    style={{
                      height: 40,
                      borderColor: 'cadetblue',
                      borderWidth: 1,
                    }}
                    placeholder="1"
                    keyboardType="number-pad"
                    onChangeText={(text) => this.setState({bathRoom: text})}
                  />
                </View>
                <View style={{paddingTop: '5%', width: '30%'}}>
                  <Text>Toilet</Text>
                  <TextInput
                    style={{
                      height: 40,
                      borderColor: 'cadetblue',
                      borderWidth: 1,
                    }}
                    placeholder="1"
                    keyboardType="number-pad"
                    onChangeText={(text) => this.setState({toilet: text})}
                  />
                </View>
              </View>

              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View
                  style={{
                    width: '50%',
                    height: 40,
                    marginTop: '10%',
                    marginRight: '10%',
                    borderColor: 'cadetblue',
                    borderWidth: 1,
                  }}>
                  <Picker
                    mode="dialog"
                    selectedValue={this.state.currency}
                    style={{
                      height: 50,
                      right: '-1%',
                      bottom: '14%',
                    }}
                    itemStyle={{
                      backgroundColor: 'red',
                      borderBottomWidth: 55,
                    }}
                    onValueChange={(itemValue, itemIndex) =>
                      // this.onChange_api(itemValue)
                      this.setState({currency: itemValue})
                    }>
                    {this.picker()}
                  </Picker>
                </View>
                <View style={{width: '50%', marginTop: '3%'}}>
                  <Text style={{fontWeight: 'bold', color: 'green'}}>RENT</Text>
                  <TextInput
                    style={{
                      height: 40,
                      width: '100%',
                      borderColor: 'cadetblue',
                      borderBottomWidth: 1,
                    }}
                    onChangeText={(text) => this.setState({rent: text})}
                    placeholder="6000"
                    keyboardType="number-pad"
                  />

                  {this.isFieldInError('rent') &&
                    this.getErrorsInField('rent').map((errorMessage, i) => (
                      <Text
                        key={i}
                        style={{
                          color: 'red',
                        }}>{`required*`}</Text>
                    ))}
                </View>
              </View>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                {/* ///--------------------------------------------------------------------------------RENTTT--- */}
                <View style={{width: '45%', marginTop: '3%'}}>
                  <Text style={{fontWeight: 'bold', color: 'green'}}>
                    Days for Rent
                  </Text>
                  <TextInput
                    style={{
                      height: 40,
                      width: '100%',
                      borderColor: 'cadetblue',
                      borderBottomWidth: 1,
                    }}
                    onChangeText={(text) => this.setState({timelimit: text})}
                    placeholder="15 Days"
                    keyboardType="number-pad"
                  />

                  {this.isFieldInError('timelimit') &&
                    this.getErrorsInField('timelimit').map(
                      (errorMessage, i) => (
                        <Text
                          key={i}
                          style={{
                            color: 'red',
                          }}>{`required*`}</Text>
                      ),
                    )}
                </View>
                <View style={{width: '45%', marginTop: '3%'}}>
                  <Text style={{fontWeight: 'bold', color: 'green'}}>
                    Limits of Tenants
                  </Text>
                  <TextInput
                    style={{
                      height: 40,
                      width: '100%',
                      borderColor: 'cadetblue',
                      borderBottomWidth: 1,
                    }}
                    onChangeText={(text) => this.setState({tenantlimit: text})}
                    placeholder="e.g: 5"
                    keyboardType="number-pad"
                  />

                  {this.isFieldInError('tenantlimit') &&
                    this.getErrorsInField('tenantlimit').map(
                      (errorMessage, i) => (
                        <Text
                          key={i}
                          style={{
                            color: 'red',
                          }}>{`required*`}</Text>
                      ),
                    )}
                </View>
              </View>
              <View style={{paddingTop: '5%', marginTop: '5%'}}>
                <TextInput
                  style={{
                    height: 40,
                    borderColor: 'cadetblue',
                    borderBottomWidth: 1,
                  }}
                  placeholder="Description"
                  keyboardType="email-address"
                  onChangeText={(text) => this.setState({description: text})}
                />
                {this.isFieldInError('description') &&
                  this.getErrorsInField('description').map(
                    (errorMessage, i) => (
                      <Text
                        key={i}
                        style={{
                          color: 'red',
                        }}>{`required*`}</Text>
                    ),
                  )}
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View style={{paddingTop: '5%', marginTop: '5%'}}>
                  <TextInput
                    style={{
                      height: 40,
                      borderColor: 'cadetblue',
                      borderBottomWidth: 1,
                    }}
                    placeholder="Year of Build : 2021-05-29"
                    keyboardType="email-address"
                    value={this.state.yearBuild}
                  />
                  {this.isFieldInError('yearBuild') &&
                    this.getErrorsInField('yearBuild').map(
                      (errorMessage, i) => (
                        <Text
                          key={i}
                          style={{
                            color: 'red',
                          }}>{`required*`}</Text>
                      ),
                    )}
                </View>

                <View style={{width: 90, left: 22, top: 10}}>
                  <Text
                    style={{
                      fontSize: 9,
                      position: 'absolute',
                      top: 34,
                      right: 51,
                    }}>
                    Select Date
                  </Text>
                  <AntDesign
                    onPress={this.showDatepicker}
                    style={{marginTop: '8%'}}
                    name="calendar"
                    size={30}
                  />
                </View>
              </View>

              <View>
                {this.state.show && (
                  <DateTimePicker
                    testID="dateTimePicker"
                    value={this.state.date}
                    mode={this.state.mode}
                    // maximumDate={new Date()}
                    // is24Hour={true}
                    display="default"
                    onChange={(text) => this.convertTimestamp(text)}
                  />
                )}
              </View>

              <TouchableOpacity
                onPress={() => this._onSubmit()}
                style={{
                  height: 50,
                  width: 200,
                  borderRadius: 5,
                  alignItems: 'center',
                  alignSelf: 'center',
                  justifyContent: 'center',
                  backgroundColor: `${color.newbtn}`,
                  marginTop: '5%',
                  marginBottom: '5%',
                }}>
                {this.state.loader ? (
                  <ActivityIndicator color={'white'} />
                ) : (
                  <Text style={{color: 'white'}}>Create</Text>
                )}
              </TouchableOpacity>
              {/* <Text onPress={()=>this.props.navigation.navigate('Subscriptionscr')}>check krooooo?</Text> */}
            </Content>
          </ScrollView>
        </Container>
      );
    }
  }
}
