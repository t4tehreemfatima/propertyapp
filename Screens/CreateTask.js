import {
  Card,
  Container,
  Content,
  Title,
  CardItem,
  Right,
  Left,
  Body,
  Button,
} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Paragraph, Dialog, Portal} from 'react-native-paper';
import MyHeader from '../Constant/MyHeader';
import {Picker} from '@react-native-picker/picker';
import React from 'react';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import * as color from '../Constant/Colors';
import {View, Text, TextInput, ActivityIndicator} from 'react-native';

export default class CreateTask extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      isLoading: true,
      taskList: '',
      visible: false,
      currentTendent: '',
      id: '',
      taskName: '',
      updateState: '',
      visible_update: false,
      updateId: '',
      updateTask: '',
      loader: false,

      update_loader: false,
    };
  }
  async task_create_list() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      //task screenn list
      const response = await this.api.create_screen_list(parse.id, parse.token);
      const response2 = await this.api.cuurent_tendent_property(
        parse.id,
        parse.token,
      );
      console.log(response2, 'res-----------------22');
      this.setState({
        taskList: response,
        currentTendent: response2,
        id: '',
        isLoading: false,
      });
      if (response2.property_data[0].length == 0) {
        this.setState({
          id: '',
          isLoading: false,
        });
      } else {
        this.setState({
          id: response2.property_data[0].id,
          isLoading: false,
        });
      }
      console.log(response2, 'shjhjhdshhhdhsdh');
    } catch (error) {
      console.log(error, 'errrr');
    }
  }
  async single_task(_id) {
    this.setState({update_loader: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      console.log(parse.token);
      //task screenn list
      const response = await this.api.single_assigned_task(_id, parse.token);
      this.setState({
        updateState: response,
        updateTask: response.data[0].task,
        loader: true,
        update_loader: false,
      });
      console.log(response, '-----------single task');
    } catch (err) {
      this.setState({update_loader: false});
      console.log(err, 'ERROR');
    }
  }
  map_tendent_list = () => {
    return this.state.currentTendent.property_data.map((element, i) => {
      console.log(element.name, '---+++++++++++++++++++++++=-///');
      return <Picker.Item label={element.name} value={element.id} />;
    });
  };
  async delete_task(_id) {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.task_delete(_id, parse.token);
      console.log(response, '/task/delete   delelel');
      this.setState({isLoading: true});
      this.task_create_list();
    } catch (err) {
      console.log(err, 'err');
    }
  }
  async update_task() {
    try {
      this.hideDialog_update();
      this.setState({isLoading: true});
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.task_update(
        this.state.updateState.data[0].id,
        this.state.updateTask,
        parse.token,
      );
      console.log(response, 'uppppppppppppp');
      this.task_create_list();
    } catch (error) {
      console.log(error, 'error');
    }
  }
  async add_btn() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.task_add_btn(
        this.state.id,
        this.state.taskName,
        parse.token,
      );
      if (response.data[0].task == 'inserted') {
        this.hideDialog();
        this.setState({isLoading: true});
        this.task_create_list();
      } else {
        alert('Not Inserted');
      }
      console.log(response, 'btn ressss');
    } catch (error) {
      console.log(error, 'err');
    }
  }
  componentDidMount() {
    this.task_create_list();
  }
  showDialog = () => this.setState({visible: true});
  hideDialog = () => this.setState({visible: false});
  showDialog_update = () => this.setState({visible_update: true});
  hideDialog_update = () => this.setState({visible_update: false});
  map_create_task_list = () => {
    if (this.state.taskList.data[0] == null) {
      return (
        <View>
          <Text
            style={{
              marginTop: '80%',
              alignSelf: 'center',
              fontSize: 30,
              color: '#e0e0e0',
            }}>
            No Tasks
          </Text>
        </View>
      );
    } else {
      return this.state.taskList.data.map((element, i) => {
        console.log(element.id, '--------------------------elelmem');
        return (
          <Card
            key={i}
            style={{
              backgroundColor: '#8fbbbd',
              marginLeft: '2%',
              marginRight: '2%',
            }}>
            <CardItem
              style={{
                backgroundColor: `${color.backgroundColor}`,
                paddingBottom: '-4%',
              }}>
              <Body
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  // alignItems: 'center',
                }}>
                <View style={{flexDirection: 'column', marginTop: '3%'}}>
                  <Text>
                    Task:
                    <Text>
                      <Text
                        style={{
                          color: 'green',
                          fontWeight: 'bold',
                          fontSize: 17,
                        }}>
                        {element.task.length < 20
                          ? `${element.task}`
                          : `${element.task.substring(0, 20)}...`}
                      </Text>
                    </Text>
                  </Text>
                  <Text style={{fontSize: 15, color: 'black'}}>
                    Property:
                    <Text
                      style={{
                        fontWeight: 'bold',
                      }}>
                      {element.property_name.length < 20
                        ? `${element.property_name}`
                        : `${element.property_name.substring(0, 20)}...`}
                      {/* {element.property_name} */}
                    </Text>
                  </Text>
                </View>
                <View
                  style={
                    {
                      // flexDirection: 'column',
                    }
                  }>
                  <Button
                    onPress={() => {
                      this.single_task(element.id);
                      // if (this.state.loader) {
                      this.showDialog_update();
                    }}
                    style={{
                      height: 30,
                      width: 80,
                      backgroundColor: `${color.header3_color}`,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text style={{color: 'white'}}>Update</Text>
                  </Button>
                  <Button
                    onPress={() => this.delete_task(element.id)}
                    style={{
                      height: 30,
                      width: 80,
                      backgroundColor: `${color.newbtn}`,
                      marginTop: '5%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{
                        color: 'white',
                      }}>
                      Delete
                    </Text>
                  </Button>
                  <Text>{element.date}</Text>
                </View>
              </Body>
            </CardItem>
          </Card>
        );
      });
    }
  };
  render() {
    console.log(this.state.id, '000000');
    if (this.state.isLoading) {
      return (
        <Container>
          <MyHeader
            headername="3"
            name="Create Tasks"
            rightIconPress={() => this.showDialog()}
            rightIconName="add"
            onPressIcon={() => this.props.navigation.goBack()}
          />
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: '20%',
            }}>
            <Spinner type="Circle" color={color.header3_color} />
          </View>
        </Container>
      );
    } else {
      return (
        <Container>
          <MyHeader
            headername="3"
            name="Create Tasks"
            rightIconPress={() => this.showDialog()}
            rightIconName="add"
            onPressIcon={() => this.props.navigation.goBack()}
          />

          <Content style={{marginTop: '1%'}}>
            {this.map_create_task_list()}
            <Portal>
              <Dialog
                style={{backgroundColor: '#dae9e9'}}
                visible={this.state.visible}
                onDismiss={this.hideDialog}>
                <Dialog.Title>Create Task</Dialog.Title>

                <Dialog.Content>
                  <Text
                    style={{
                      color: `${color.theme_Color}`,
                      fontWeight: 'bold',
                      marginTop: '5%',
                    }}>
                    Select Property:
                  </Text>
                  <View
                    style={{
                      borderBottomWidth: 1,
                      borderColor: `${color.header3_color}`,
                    }}>
                    <Picker
                      mode="dialog"
                      selectedValue={this.state.id}
                      style={{
                        height: 50,
                        width: '102%',

                        marginBottom: '-2.5%',
                        right: '1.5%',
                      }}
                      itemStyle={{
                        backgroundColor: 'red',
                        borderBottomWidth: 55,
                      }}
                      onValueChange={(itemValue, itemIndex) =>
                        this.setState({id: itemValue})
                      }>
                      {this.map_tendent_list()}
                    </Picker>
                  </View>

                  <View>
                    <Text
                      style={{
                        color: `${color.theme_Color}`,
                        fontWeight: 'bold',
                        marginTop: '5%',
                      }}>
                      Task Name:
                    </Text>
                    <TextInput
                      style={{
                        borderBottomWidth: 1,
                        borderColor: `${color.theme_Color}`,
                      }}
                      placeholder="Enter your Task......"
                      onChangeText={(text) => this.setState({taskName: text})}
                    />
                  </View>
                </Dialog.Content>
                <Dialog.Actions>
                  <Button
                    style={{
                      width: '20%',
                      marginRight: '3%',
                      height: '90%',
                      backgroundColor: `${color.theme_Color}`,
                    }}
                    onPress={() => this.add_btn()}>
                    <Text style={{color: 'white', marginLeft: '25%'}}>Add</Text>
                  </Button>
                  <Button
                    style={{
                      width: '20%',
                      height: '90%',
                      backgroundColor: `${color.newbtn}`,
                    }}
                    onPress={this.hideDialog}>
                    <Text style={{color: 'white', marginLeft: '15%'}}>
                      Cancel
                    </Text>
                  </Button>
                </Dialog.Actions>
              </Dialog>
            </Portal>
            {/* //--------------------------------------------------------------------------------Updadte */}
            <Portal>
              <Dialog
                style={{backgroundColor: '#dae9e9'}}
                visible={this.state.visible_update}
                onDismiss={this.hideDialog_update}>
                <Dialog.Title>Update Task</Dialog.Title>

                <Dialog.Content>
                  <View>
                    <Text
                      style={{
                        color: `${color.theme_Color}`,
                        fontWeight: 'bold',
                        marginTop: '5%',
                      }}>
                      Task Name:
                    </Text>
                    {this.state.loader == false ? (
                      <ActivityIndicator color={'white'} />
                    ) : (
                      <TextInput
                        style={{
                          borderBottomWidth: 1,
                          borderColor: `${color.theme_Color}`,
                        }}
                        placeholder="Enter your Task......"
                        value={this.state.updateTask}
                        onChangeText={(text) =>
                          this.setState({updateTask: text})
                        }
                      />
                    )}
                  </View>
                </Dialog.Content>
                <Dialog.Actions>
                  <Button
                    style={{
                      width: '30%',
                      marginRight: '3%',
                      height: '90%',
                      backgroundColor: `${color.theme_Color}`,
                    }}
                    onPress={() => this.update_task()}>
                    <Text style={{color: 'white', marginLeft: '25%'}}>
                      Update
                    </Text>
                  </Button>
                  <Button
                    style={{
                      width: '20%',
                      height: '90%',
                      backgroundColor: `${color.newbtn}`,
                    }}
                    onPress={this.hideDialog_update}>
                    <Text style={{color: 'white', marginLeft: '15%'}}>
                      Cancel
                    </Text>
                  </Button>
                </Dialog.Actions>
              </Dialog>
            </Portal>
          </Content>
        </Container>
      );
    }
  }
}
