import {Button, Avatar} from 'react-native-paper';
import {View, Text, ImageBackground} from 'react-native';
import * as color from '../Constant/Colors';
import {Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import {Container, Content, Title, Tab, Tabs, Thumbnail} from 'native-base';
import React from 'react';
import MyHeader from '../Constant/MyHeader';
import AntDesign from 'react-native-vector-icons/AntDesign';
import BottomTab from '../Constant/BottomTab';
import {ActivityIndicator} from 'react-native';
import Spinner from 'react-native-spinkit';

import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RentTedent from './RentTendent';
import Rent from './Rent';
import {Divider} from 'react-native-elements';
import {TouchableOpacity} from 'react-native';
export default class CurrentHomeTendent extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      mydata: '',
      isloading: true,
    };
  }
  componentDidMount() {
    this.current_home();
  }
  async current_home() {
    // this.setState({isloading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.tendent_detail(parse.id, parse.token);
      console.log(
        response,
        parse.id,
        parse.token,
        'responsc-------------- tendent detal',
      );

      this.setState({mydata: response, isloading: false});
    } catch (error) {
      console.log(error, 'errr');
    }
  }
  current_list = () => {
    // if (this.state.mydata.status==false) {
    //   return<View><Text style={{color:'#E0E0E0',fontSize:26,alignSelf:'center',marginTop:'55%'}}>No Property</Text></View>
    // } else {

    return this.state.mydata.data.map((element, i) => {
      console.log(element, 'ele55555met');
      return (
        <View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: '3%',
            }}>
            <View
              style={{
                borderLeftWidth: 1,
                padding: '1%',
                justifyContent: 'center',
                width: '55%',
              }}>
              <Text>Property:</Text>
              <Text style={{fontWeight: 'bold'}}>{element.property_name}</Text>
            </View>
            <View
              style={{
                borderLeftWidth: 1,
                padding: '1%',
                width: '30%',
                justifyContent: 'center',
              }}>
              <Text>Date:</Text>
              <Text style={{color: 'grey', fontWeight: 'bold'}}>
                {element.date}
              </Text>
              <Text>Rent Amount:</Text>
              <Text style={{fontWeight: 'bold', color: 'green'}}>
                {element.rent}
              </Text>
            </View>
          </View>
          <TouchableOpacity
            style={{
              width: '62%',
              alignSelf: 'center',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              borderWidth: 0.1,
              backgroundColor: `${color.header3_color}`,
              // elevation: 1,
              marginTop: '5%',
            }}
            onPress={() => this.props.navigation.navigate('TendentRent')}>
            {/* <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                borderWidth: 0.1,
                backgroundColor: `${color.header3_color}`,
                // elevation: 1,
                marginTop: '5%',

              }}> */}
            <MaterialIcon name="payment" size={30} color="white" />
            <Text style={{fontWeight: 'bold', color: 'white'}}>
              Rent Payment
            </Text>
            {/* </View> */}
          </TouchableOpacity>
          <Divider style={{marginTop: '5%'}} />
        </View>
      );
    });
    // }
  };
  render() {
    return (
      <Container>
        {this.state.isloading ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
            <Spinner type="Circle" color={color.header3_color} />
          </View>
        ) : this.state.mydata.status == false ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
            <AntDesign name="home" size={80} color="grey" />
            <Text style={{marginTop: '2%', color: 'grey'}}>
              No current property found!
            </Text>
          </View>
        ) : (
          <Content style={{marginLeft: '5%', marginRight: '5%'}}>
            <View>{this.current_list()}</View>
          </Content>
        )}
        {/* <Content style={{marginLeft: '5%', marginRight: '5%'}}>
          <View>
            {this.state.isloading ? (
              <Spinner
                type="Circle"
                color={color.header3_color}
                style={{alignSelf: 'center'}}
              />
            ) : (
              this.current_list()
            )}
          </View>
        </Content> */}
      </Container>
    );
  }
}
