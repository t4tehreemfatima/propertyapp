import MyHeader from '../Constant/MyHeader';
import React from 'react';
import * as color from '../Constant/Colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import API from '../Constant/API';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {
  Card,
  Container,
  Content,
  Title,
  CardItem,
  Right,
  Left,
  Body,
  Thumbnail,
} from 'native-base';
import {Dialog, Portal, Button} from 'react-native-paper';

import {
  View,
  Text,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {ScrollView} from 'react-native';
export default class CurrentProperties extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      status: 'unchecked',
      collapsed: true,
      myData: '',
      isLoading: true,
      input_search: '',
      visible: false,
      delete_id: '',
    };
  }

  _storeKey = async (_key) => {
    try {
      let obj = {
        key: _key,
      };

      await AsyncStorage.setItem('tendent_id', JSON.stringify(obj));
    } catch (error) {
      alert(error);
    }
    console.log(_key, '---> Tend ID Stored');
  };

  handleManage(_key) {
    this._storeKey(_key);
    this.props.navigation.navigate('ManageRent');
  }
  showDialog = (_id) => {
    console.log(_id, 'delel id h ya');
    this.setState({delete_id: _id, visible: true});
  };

  async property_list() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      console.log(parse.token, 'token t\\');
      const response = await this.api.selectProperty(
        parse.id,
        '',
        '',
        '',
        '',
        '',
        '',
        parse.token,
      );
      // console.log(response, 'Data Response');
      this.setState({myData: response, isLoading: false});
      // console.log(this.state.myData, 'ppppppppdata');
    } catch (err) {
      console.log('Error: ', err);
    }
  }

  async delete_property(_id) {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      console.log(parse.token, 'token t\\');
      const response = await this.api.property_delete(_id, parse.token);
      console.log(response, 'Data Respons------------e De');
      if (response.status == false) {
        alert(response.message);
        this.setState({visible: false});
      } else {
        this.property_list();
        this.setState({visible: false});
        alert('Property Deleted');
      }
    } catch (err) {
      console.log('Error: ', err);
    }
  }

  componentDidMount() {
    const {navigation} = this.props;

    this.focusListener = navigation.addListener('focus', () => {
      this.property_list();
      console.log('cosolllllll');
    });
    this.focusListenerr = navigation.addListener('blur', () => {
      console.log('un cosolllllll');
    });
    this.property_list();
  }
  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }
  // componentDidMount() {
  //   console.log('rererererererererer');
  //   this.property_list();
  // }
  openHomeList = () => {
    if (this.state.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: '75%',
          }}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      // console.log(this.state.myData, 'property data comsole');
      if (this.state.myData.property_data.length == 0) {
        return (
          <View style={{alignItems: 'center', marginTop: '60%'}}>
            <AntDesign name="home" size={80} color="grey" />
            <Text style={{color: 'gray'}}>No property found!</Text>
            {/* <Text
              onPress={() => this.props.navigation.navigate('CreateProperty')}
              style={{color: `${color.theme_Color}`, fontWeight: 'bold'}}>
              Create Property
            </Text> */}
          </View>
        );
      } else {
        return this.state.myData.property_data.map((element, i) => {
          return (
            <View
              style={{
                flexDirection: 'row',
                elevation: 1,
                // borderWidth: 0.1,
                borderRadius: 1,
                padding: 10,
                marginBottom: 10,
                marginLeft: '2%',
                marginRight: '2%',
              }}>
              <View>
                {element.images.length > 0 ? (
                  <Thumbnail
                    square
                    style={{width: 70, height: 70, maxWidth: 70, maxHeight: 70}}
                    small
                    source={{
                      uri: `${color.link}${element.images[0].path}`,
                    }}
                  />
                ) : (
                  <Thumbnail
                    square
                    style={{width: 70, height: 70, maxWidth: 70, maxHeight: 70}}
                    small
                    source={require('../assets/myhome.jpg')}
                  />
                )}
              </View>

              <View style={{marginLeft: '3%'}}>
                <Text
                  onPress={() =>
                    this.props.navigation.navigate('ViewCartDetails', {
                      key: element.id,
                      status: '',
                    })
                  }
                  style={{fontWeight: 'bold', fontSize: 16}}>
                  {element.name.length < 20
                    ? `${element.name}`
                    : `${element.name.substring(0, 20)}...`}
                </Text>
                <Text>Rent: {element.rent}</Text>
                <Text style={{color: 'grey'}} numberOfLines={2}>
                  City:{' '}
                  {element.city.length < 20
                    ? `${element.city}`
                    : `${element.city.substring(0, 20)}...`}
                </Text>
              </View>

              <View
                style={{
                  position: 'absolute',
                  right: 5,
                  top: 25,
                  flexDirection: 'row',
                }}>
                <MaterialIcons
                  onPress={() =>
                    this.props.navigation.navigate('EditProperty', {
                      key: element.id,
                    })
                  }
                  name="edit"
                  size={30}
                  color="green"
                />
                <MaterialCommunityIcons
                  // onPress={() => this.delete_property(element.id)}
                  onPress={() => this.showDialog(element.id)}
                  name="delete"
                  size={30}
                  color="red"
                />
                <MaterialIcons
                  onPress={() =>
                    this.props.navigation.navigate('ManageRent', {
                      key: element.id,
                    })
                  }
                  name="settings"
                  size={30}
                />
              </View>

              <Portal>
                <Dialog
                  visible={this.state.visible}
                  // onDismiss={this.hideDialog_update}
                >
                  <Dialog.Title>Alert</Dialog.Title>

                  <Dialog.Content>
                    <Text>Are you sure you want to delete this property?</Text>
                  </Dialog.Content>
                  <Dialog.Actions>
                    <Button
                      onPress={() => this.delete_property(this.state.delete_id)}
                      style={{
                        width: '30%',
                        marginRight: '3%',
                        height: '100%',
                        backgroundColor: `${color.theme_Color}`,
                      }}>
                      <Text style={{color: 'white', marginLeft: '25%'}}>
                        Yes
                      </Text>
                    </Button>
                    <Button
                      onPress={() => this.setState({visible: false})}
                      style={{
                        width: '30%',
                        marginRight: '3%',
                        height: '100%',
                        backgroundColor: `${color.newbtn}`,
                      }}>
                      <Text style={{color: 'white', marginLeft: '25%'}}>
                        No
                      </Text>
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>
            </View>
          );
        });
      }
    }
  };

  render() {
    return (
      <Container>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Content>
            <View style={{marginTop: '3%', marginBottom: '2%'}}>
              {this.openHomeList()}
            </View>
          </Content>
        </ScrollView>
      </Container>
    );
  }
}
