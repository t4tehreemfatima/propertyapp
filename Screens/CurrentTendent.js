import MyHeader from '../Constant/MyHeader';
import React from 'react';
import * as color from '../Constant/Colors';
import {View, Text, BackHandler, TouchableOpacity} from 'react-native';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Dialog, Portal, Button} from 'react-native-paper';
import {useFocusEffect} from '@react-navigation/native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import {
  Container,
  Content,
  Title,
  Tab,
  Tabs,
  Thumbnail,
  Card,
} from 'native-base';
const backAction = () => {
  BackHandler.exitApp();
  return true;
};

function HardwareBackBtn() {
  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', backAction);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
    }, []),
  );

  return null;
}
export default class CurrentTendent extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      myData: '',
      isLoading: true,
      visible: false,
      delete_id: '',
      property_data: '',
    };
  }
  async request_properties() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      console.log(parse.token, 'token ten');
      const response1 = await this.api.selectProperty(
        parse.id,
        '',
        '',
        '',
        '',
        '',
        '',
        parse.token,
      );
      const response = await this.api.current_tendent(parse.id, parse.token);
      console.log(
        response1,
        parse.id,
        '00000000000000000yyy00000&&&&&&7777000000000000000000000000000000000000000000000',
      );
      this.setState({
        myData: response,
        property_data: response1,
        isLoading: false,
      });
    } catch (err) {
      console.log('Error: ', err);
    }
  }
  showDialog = (_id) => {
    console.log(_id, 'delel id h ya');
    this.setState({delete_id: _id, visible: true});
  };
  async tendent_delete(_id) {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      console.log(parse.token, 'token ten');
      const response = await this.api.current_tendent_delete(_id, parse.token);
      if (response.data[0].tendent_to_property === 'deleted') {
        this.request_properties();
        alert('Tenant Deleted');
        this.setState({visible: false});
      } else {
        alert('Try Again');
      }
      console.log(
        response,
        _id,
        parse.token,
        '00000000000000000000TEB000000000000000000000000000000deleter00000000000000000',
      );
    } catch (err) {
      console.log('Error: ', err);
    }
  }

  componentDidMount() {
    this.request_properties();
  }

  //----------------------------------------------------------------------------------------list
  request_property_list = () => {
    // if (this.state.myData.data.length == 0) {
    //   return (
    //     <View>
    //       <Text
    //         style={{
    //           fontSize: 30,
    //           marginTop: '65%',
    //           color: '#e0e0e0',
    //           alignSelf: 'center',
    //         }}>
    //         No Tenant
    //       </Text>
    //     </View>
    //   );
    // } else {
    return this.state.myData.data.map((element, i) => {
      console.log(element.image, '=============-88===========>>.');
      // this.setState({tendent_id:element.data[]})
      return (
        <Card
          key={i}
          style={{
            flexDirection: 'row',
            marginLeft: '3%',
            marginRight: '3%',
            alignItems: 'center',
            // borderWidth: 0.1,
            elevation: 1,
            padding: '3%',
            marginTop: '3%',
            justifyContent: 'space-between',
            borderRadius: 10,
            marginBottom: '2%',
            backgroundColor: `${color.backgroundColor}`,
          }}>
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginLeft: 5}}>
            <View>
              {element.image == null ? (
                <Thumbnail source={require('../assets/gender.png')} />
              ) : (
                <Thumbnail source={{uri: `${color.link}${element.image}`}} />
              )}
            </View>
            <View
              style={{marginLeft: '3%'}}
              onPress={() =>
                this.props.navigation.navigate('ViewCartDetails', {
                  key: element.property_id,
                  status: '',
                })
              }>
              <Text style={{fontWeight: 'bold', fontSize: 14, color: 'grey'}}>
                {element.property_name.length < 20
                  ? `${element.property_name}`
                  : `${element.property_name.substring(0, 20)}...`}
              </Text>
              <Text
                style={{fontWeight: 'bold', fontSize: 14, color: 'green'}}
                onPress={() =>
                  this.props.navigation.navigate('ViewCartDetails', {
                    key: element.property_id,
                    status: '',
                  })
                }>
                {element.name.length < 20
                  ? `${element.name}`
                  : `${element.name.substring(0, 20)}...`}
              </Text>
              <Text
                style={{fontWeight: 'bold', fontSize: 14}}
                onPress={() =>
                  this.props.navigation.navigate('ViewCartDetails', {
                    key: element.property_id,
                    status: '',
                  })
                }>
                {element.email.length < 20
                  ? `${element.email}`
                  : `${element.email.substring(0, 20)}...`}
                {/* {element.email} */}
              </Text>
              <Text>Approved Date:{element.approved_date}</Text>
              <Text
                style={{fontWeight: 'bold', fontSize: 14}}
                onPress={() =>
                  this.props.navigation.navigate('ViewCartDetails', {
                    key: element.property_id,
                    status: '',
                  })
                }>
                <MaterialCommunityIcons name="cellphone-iphone" size={15} />{' '}
                {element.mobile.length < 25
                  ? `${element.mobile}`
                  : `${element.mobile.substring(0, 20)}...`}
                {/* {element.email} */}
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '25%',
            }}>
            <MaterialCommunityIcons
              onPress={() => this.showDialog(element.id)}
              name="delete-outline"
              size={30}
              style={{marginLeft: '33%'}}
            />
          </View>
        </Card>
      );
    });
    // }
  };

  render() {
    // if (this.state.isLoading) {
    //   return (
    //     <View
    //       style={{
    //         flex: 1,
    //         alignItems: 'center',
    //         justifyContent: 'center',
    //         marginTop: '8%',
    //       }}>
    //       <Spinner type="Circle" color={color.header3_color} />
    //     </View>
    //   );
    // } else {
    return (
      <Container>
        <HardwareBackBtn />
        {this.state.isLoading ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
            <Spinner type="Circle" color={color.header3_color} />
          </View>
        ) : 'status' in this.state.myData ? (
          <View
            style={{
              alignSelf: 'center',
              justifyContent: 'center',
              flex: 1,
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 20, color: '#AEAEAE'}}>
              You need to subscribe first!
            </Text>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('Subscriptionscr', {
                  from: 'mysub',
                })
              }>
              <Text style={{color: `${color.theme_Color}`}}>Click here</Text>
            </TouchableOpacity>
          </View>
        ) : this.state.myData.data.length == 0 ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
            <FontAwesome5 name="users" size={80} color="grey" />
            <Text style={{marginTop: '2%', color: 'grey'}}>
              No tenants found!
            </Text>
            {this.state.property_data.property_data.length == 0 ? (
              <View style={{alignItems: 'center'}}>
                <Text style={{marginTop: '2%', color: 'grey'}}>
                  You don't have any property yet
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate('Create', {
                      ...this.props,
                    })
                  }
                  style={{
                    backgroundColor: `${color.theme_Color}`,
                    width: 120,
                    alignItems: 'center',
                    // height: 20,
                    borderRadius: 5,
                    padding: '1%',
                    marginTop: '3%',
                  }}>
                  <Text style={{color: 'white'}}>Create Property</Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View></View>
            )}
          </View>
        ) : (
          <Content>{this.request_property_list()}</Content>
        )}
        <Portal>
          <Dialog
            visible={this.state.visible}
            // onDismiss={this.hideDialog_update}
          >
            <Dialog.Title>Alert</Dialog.Title>

            <Dialog.Content>
              <Text>Do you want to delete this tenant?</Text>
            </Dialog.Content>
            <Dialog.Actions>
              <Button
                onPress={() => this.tendent_delete(this.state.delete_id)}
                style={{
                  width: '30%',
                  marginRight: '3%',
                  height: '100%',
                  backgroundColor: `${color.theme_Color}`,
                }}>
                <Text style={{color: 'white', marginLeft: '25%'}}>Yes</Text>
              </Button>
              <Button
                onPress={() => this.setState({visible: false})}
                style={{
                  width: '30%',
                  marginRight: '3%',
                  height: '100%',
                  backgroundColor: `${color.newbtn}`,
                }}>
                <Text style={{color: 'white', marginLeft: '25%'}}>No</Text>
              </Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
      </Container>
    );
  }
  // }
}
