import React, {Component, useEffect} from 'react';
import {View, Text, Footer, Title, Container, Content} from 'native-base';
import * as color from '../Constant/Colors';
import AntIcon from 'react-native-vector-icons/AntDesign';
import {StyleSheet, Linking} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import API from '../Constant/API';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useFocusEffect} from '@react-navigation/native';

import {
  useTheme,
  Avatar,
  Caption,
  Paragraph,
  Drawer,
  TouchableRipple,
  Switch,
  Divider,
} from 'react-native-paper';
import {TouchableHighlight} from 'react-native';
import {TouchableOpacity} from 'react-native';

let user_img = '';

const userImg = async () => {
  const UsreData = await AsyncStorage.getItem('user_data');
  const parse = JSON.parse(UsreData);
  // console.log(parse, '---> IMG');
  user_img = parse.draw_img;
};

function User_Img() {
  useEffect(() => {
    // Update the document title using the browser API
    userImg();
  });

  return () => {
    // console.log('This will be logged on unmount');
  };
}
export default class DrawerSrc extends Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      name: '',
      image: '',
      email: '',
      data: '',
      user_type: '',
      check: 'home',
    };
  }
  async social() {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response = await this.api.social_selct(parse.token);
    // console.log(response, 'res imag');
    this.setState({data: response});
  }
  async componentDidMount() {
    this.social();
    this._unsubscribe = this.props.navigation.addListener('focus', async () => {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      this.setState({user_type: parse.user_type});
      console.log(
        parse.user_type,
        'check use_________****   **_________________________r data',
      );
      this.setState({
        name: parse.name,
        email: parse.email,
        image: parse.draw_img,
      });

      console.log('---------> ComponentDIDMOUNT <----TT-----');
    });
  }
  componentWillUnmount() {
    this._unsubscribe();
    // console.log('---------> ComponentWillMOUNT <---------');
  }
  _storeUserData = async (_name, _email, _id, _token) => {
    try {
      let obj = {
        name: _name,
        email: _email,
        id: _id,
        token: _token,
      };

      await AsyncStorage.setItem('user_data', JSON.stringify(obj));
    } catch (error) {
      alert(error);
    }
    console.log('User Data clear');
  };
  _user_imgclear = async (_image) => {
    try {
      let obj = {
        user_image: _image,
      };

      await AsyncStorage.setItem('user_img', JSON.stringify(obj));
    } catch (error) {
      alert(error);
    }
    console.log(_image, '---> User Data Stored');
  };
  async logout() {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    console.log(parse.token);

    const response = await this.api.logout(parse.token);
    console.log(response, 'RESPONSE');

    if (response.data[0].Logout) {
      this._storeUserData('', '', '', '', '', '');
      this._user_imgclear('');
      this._storeUserEmailAndID('');
      this.props.navigation.reset({
        index: 0,
        routes: [{name: 'SignIn'}],
      });
      // this.props.navigation.navigate('SignIn');
    } else {
      alert('error');
    }
  }

  async _storeUserEmailAndID(_email) {
    try {
      let obj = {
        email: _email,
      };

      await AsyncStorage.setItem('check_user', JSON.stringify(obj));
    } catch (error) {
      alert(error);
    }
    console.log(_email, '---> Check user stored');
  }
  check(val) {
    if (val == 'home') {
      this.setState({
        check: 'home',
      });
    } else if (val == 'findprop') {
      this.setState({check: 'findprop'});
    } else if (val == 'tc') {
      this.setState({check: 'tc'});
    } else if (val == 'pay') {
      this.setState({check: 'pay'});
    } else {
      this.setState({check: 'pp'});
    }
  }
  render(props) {
    console.log(
      this.state.check,
      '-------------------CHECKKKK------------------',
    );
    return (
      <Container>
        <User_Img />
        <Content>
          <View style={{marginTop: '7%'}}>
            <View
              style={{
                marginTop: '5%',
                alignSelf: 'center',
                borderWidth: 0.1,
                borderColor: 'red',
                backgroundColor: '#15b7da',
                padding: 2,

                borderRadius: 60,

                borderColor: 'grey',
              }}>
              {user_img == null ? (
                <Avatar.Image
                  size={104}
                  style={{backgroundColor: 'white'}}
                  source={require('../assets/user.png')}
                />
              ) : (
                <Avatar.Image
                  size={104}
                  source={{
                    uri: `${color.link}${user_img}`,
                  }}
                />
              )}
            </View>

            <View style={{alignItems: 'center', marginTop: '4%'}}>
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                {this.state.name}
              </Text>
              <Text>{this.state.email}</Text>
            </View>
            <View
              style={{
                backgroundColor: `${color.theme_Color}`,
                width: '14%',
                height: '8%',
                alignSelf: 'center',
                justifyContent: 'center',
                borderRadius: 20,
                position: 'absolute',
                top: '20%',
                right: '30%',
              }}>
              <Icon
                style={{left: '13%'}}
                name="camera"
                color={color.icon_color}
                size={30}
                onPress={() => this.props.navigation.navigate('Profile')}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                marginTop: '10%',
              }}>
              <Icon
                name="notifications-sharp"
                size={30}
                color={color.drwIcon}
                onPress={() => this.props.navigation.navigate('Notification')}
              />
              <Icon
                name="ios-settings"
                size={30}
                color={color.drwIcon}
                onPress={() => this.props.navigation.navigate('Settings')}
              />
              <Icon
                name="log-out-outline"
                size={30}
                color={color.drwIcon}
                onPress={() => this.logout()}
              />
            </View>
            <Divider
              style={{
                borderColor: `${color.theme_Color}`,
                borderWidth: 1,
                marginTop: '5%',
              }}
            />
            <View style={{marginLeft: '5%', marginTop: '9%'}}>
              <View
                style={{
                  backgroundColor: `${
                    this.state.check == 'home'
                      ? `${color.backgroundColor}`
                      : 'white'
                  }`,
                  padding: 5,
                }}
                onPress={() => {
                  this.props.navigation.navigate('Home');
                  this.check('home');
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Icon
                    name="home-outline"
                    size={20}
                    color={color.icon_color}
                  />
                  <Text style={styles.itemText}>Home</Text>
                </View>
              </View>
              {/* <View style={{flexDirection: 'row', marginTop: '7%'}}>
                <MaterialIcons
                  name="payment"
                  size={20}
                  color={color.icon_color}
                />
                <Text style={styles.itemText}>Manage Payment Option</Text>
              </View> */}

              <View
                style={{
                  flexDirection: 'row',
                  marginTop: '5%',
                  backgroundColor: `${
                    this.state.check == 'findprop'
                      ? `${color.backgroundColor}`
                      : 'white'
                  }`,
                  padding: 5,
                }}>
                <Icon name="md-search" size={20} color={color.icon_color} />
                <Text
                  onPress={() => {
                    this.props.navigation.navigate('FindProperties');
                    this.check('findprop');
                  }}
                  style={styles.itemText}>
                  Find Properties
                </Text>
              </View>

              {this.state.user_type == '2' ? (
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: '5%',
                    padding: 5,
                    backgroundColor: `${
                      this.state.check == 'pay'
                        ? `${color.backgroundColor}`
                        : 'white'
                    }`,
                  }}>
                  <Icon
                    name="wallet-outline"
                    size={20}
                    color={color.icon_color}
                  />
                  <Text
                    style={styles.itemText}
                    onPress={() => {
                      this.props.navigation.navigate('TendentDetail');
                      this.check('pay');
                    }}>
                    Payment
                  </Text>
                </View>
              ) : (
                <View></View>
              )}
              {this.state.user_type == '2' ? (
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: '5%',
                    padding: 5,
                    backgroundColor: `${
                      this.state.check == 'pp'
                        ? `${color.backgroundColor}`
                        : 'white'
                    }`,
                  }}>
                  <MaterialCommunityIcons
                    name="home-city-outline"
                    size={20}
                    color={color.icon_color}
                  />
                  <Text
                    style={styles.itemText}
                    onPress={() => {
                      this.props.navigation.navigate('PreviosHome');
                      this.check('pp');
                    }}>
                    Previous Properties
                  </Text>
                </View>
              ) : (
                <View></View>
              )}
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: '5%',
                  padding: 5,
                  backgroundColor: `${
                    this.state.check == 'tc'
                      ? `${color.backgroundColor}`
                      : 'white'
                  }`,
                }}>
                <Icon
                  name="ios-lock-closed-outline"
                  size={20}
                  color={color.icon_color}
                />
                <Text
                  style={styles.itemText}
                  onPress={() => {
                    this.props.navigation.navigate('Agree');
                    this.check('tc');
                  }}>
                  Terms and Condition
                </Text>
              </View>
            </View>
          </View>
        </Content>
        <Footer
          style={{
            alignItems: 'center',
            backgroundColor: `${color.theme_Color}`,
          }}>
          <Icon
            style={{right: 98}}
            onPress={() => Linking.openURL(this.state.data.data.facebook)}
            name="logo-facebook"
            size={20}
            color={color.icon_color}
          />
          <Icon
            style={{right: 87}}
            onPress={() => Linking.openURL(this.state.data.data.insta)}
            name="ios-logo-instagram"
            size={20}
            color={color.icon_color}
          />
          {/* <Icon
            onPress={() => Linking.openURL(this.state.data.data.twitter)}
            name="logo-twitter"
            size={20}
            color={color.icon_color}
          /> */}
          {/* <Icon
            onPress={() => Linking.openURL(this.state.data.data.linkedin)}
            name="logo-linkedin"
            size={20}
            color={color.icon_color}
          /> */}
        </Footer>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  itemText: {
    alignItems: 'center',
    marginLeft: '5%',
  },
});
