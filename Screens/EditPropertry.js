import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Thumbnail,
} from 'native-base';
import MyHeader from '../Constant/MyHeader';
import React from 'react';
import { Picker } from '@react-native-picker/picker';
import DocumentPicker from 'react-native-document-picker';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import * as color from '../Constant/Colors';
import { View, Text } from 'react-native';
import { ThemeConsumer } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ImgToBase64 from 'react-native-image-base64';
import { ScrollView, BackHandler } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
// import ImagePicker from 'react-native-image-crop-picker';
import Entypo from 'react-native-vector-icons/Entypo';
export default class EditProperty extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      isLoading: true,
      prppertyName: '',
      display_images: '',
      street: '',
      state: '',
      city: '',
      zipcode: '',
      bedroom: '',
      bathroom: '',
      rent: '',
      description: '',
      date: '',
      linkImage: '',
      myArray: [],
      imagesData: [],
      loader: false,
      image_loader: false,
      currency: '',
      data_currency: '',
      curLoading: true,
      state_data: '',
      city: '',
      toilets: '',
      update_loader: false,
      timelimit: '',
      tenantlimit: '',
    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }
  async get_currency() {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response = await this.api.currency(parse.token);
    const response2 = await this.api.state(parse.token);
    console.log(response2, 'rse 2');
    this.setState({
      data_currency: response,
      currency: response.data[0].id,
      state_data: response2,
      state: response2.data[0].state,
      curLoading: false,
    });
  }
  picker = () => {
    if (this.state.curLoading) {
      return <ActivityIndicator />;
    } else {
      return this.state.data_currency.data.map((element, i) => {
        return <Picker.Item label={element.currency} value={element.id} />;
      });
    }
  };
  state_picker = () => {
    return this.state.state_data.data.map((element, i) => {
      //  console.log(element,'--------------elle--------------elel')
      return <Picker.Item label={element.state} value={element.state} />;
    });
  };
  city_picker = () => {
    return this.state.city_data.data.map((element, i) => {
      // console.log(element,'--------------elle--------------elel')
      return <Picker.Item label={element.city} value={element.city} />;
    });
  };

  async onChange_api(_id) {
    this.setState({ state: _id });
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response3 = await this.api.cities(_id, parse.token);
    console.log(response3, '00000000000099990099=------00000000000000000');
    this.setState({ city_data: response3 });
  }
  async upload_img(_images) {
    //this.setState({isLoading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      console.log(_images, '___)))_- -- -- - - - - --  - -token ten');
      const response = await this.api.img_upload(
        this.props.route.params.key,
        _images,
        parse.token,
      );
      console.log(response, 'image ka ress');

      this.update_on_button();
    } catch (err) {
      console.log('Error: ', err);
    }
  }
  async getimage() {
    // let allim = [];
    // let arr = this.state.myArray;
    // let imagearray = [];
    let binaryCode = [];
    let allImages = [];
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.images],
      });
      console.log(results, ">>>>>>> check results")
      for (const res of results) {
        console.log(
          res.uri,
          res.type, // mime type
          res.name,
          res.size,
        );
        ImgToBase64.getBase64String(res.uri)
          .then((base64String) => {
            console.log(this.state.imagesData, this.state.imagesData.length, 'basss');


            let name = res.name;

            binaryCode.push(base64String);

            allImages.push({
              name: name,
              image: base64String,
            });

            if (results[results.length - 1]) {
              this.setState({
                imagesData: binaryCode,
                myArray: allImages
              })
            }

          })
          .catch((err) => console.log(err));
      }


      // this.setState({
      //   imagesData: binaryCode,
      //   myArray: allImages,
      // });

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }
  //------------------------------------------------------------------------PICKER

  multipleImagePicker = () => {
    ImagePicker.openPicker({
      multiple: true,
      includeBase64: true,
    }).then((images) => {
      console.log(images);

      let binaryCode = [];
      let allImages = [];

      images.forEach((binary) => {
        // console.log(binary.data, '----> BINARY');

        let file = String(binary.path).split('/');
        let name = file[file.length - 1];

        binaryCode.push(binary.data);

        allImages.push({
          name: name,
          image: binary.data,
        });
      });

      this.setState({
        imagesData: binaryCode,
        myArray: allImages,
      });
    });
  };
  // HANDLE SAVE
  handleSave() {
    this.upload_img(this.state.myArray);
  }

  componentDidMount() {
    const { navigation } = this.props;

    this.focusListener = navigation.addListener('focus', () => {
      this.details();

      console.log('cosolllllll');
    });
    this.focusListenerr = navigation.addListener('blur', () => {
      console.log('un cosolllllll');
      this.setState({ isLoading: true });
    });
  }
  handleBackButtonClick() {
    this.props.navigation.goBack();
    this.setState({ myArray: [], imagesData: [] });
    return true;
  }
  componentWillMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }
  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  async details() {
    console.log(this.props.route.params.key, '--------');
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    console.log(parse.token, 'token ten');
    const response = await this.api.edit_profile_current_tendent(
      this.props.route.params.key,
      parse.token,
    );
    console.log(response.property_data[0], '---dateAAAA----------------->&&&&');

    const response2 = await this.api.display_imaage(
      this.props.route.params.key,
      parse.token,
    );
    const response3 = await this.api.state(parse.token);
    this.setState({ city: 'Australian Capital Territory' });
    const responsecity = await this.api.cities(this.state.state, parse.token);
    console.log(
      response.property_data[0].images,
      '----------------------------------------000000--------',
    );

    this.setState({ city_data: responsecity });

    const responsecu = await this.api.currency(parse.token);
    this.setState({
      data_currency: responsecu,
      currency: responsecu.data[0].id,
      curLoading: false,
    });
    const data = response.property_data[0];
    // this.setState({});
    this.setState({
      state: response3.data[0].state,
      prppertyName: data.name,
      street: data.street,

      // state: data.state,
      // city: data.city,
      zipcode: data.zip_code,
      bedroom: response.property_data[0].bed_rooms,
      bathroom: data.bath_rooms,
      toilets: data.toilets,
      rent: data.price,
      description: data.description,
      date: data.year_build,
      display_images: response2,
      state_data: response3,
      isLoading: false,
      myArray: data.images,
      timelimit: data.rent_days,
      tenantlimit: data.limit,
    });

    // this.setState({isLoading: false});
  }
  async update_on_button() {
    this.setState({ update_loader: true });

    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);

    const response = await this.api.update_Property(
      this.props.route.params.key,
      parse.id,
      this.state.prppertyName,
      this.state.bedroom,
      this.state.bathroom,
      this.state.toilets,
      this.state.description,
      this.state.state,
      this.state.rent,
      this.state.date,
      this.state.city,
      this.state.street,
      this.state.zipcode,
      this.state.currency,
      this.state.timelimit,
      this.state.tenantlimit,
      parse.token,
    );
    console.log(
      response,
      this.state.toilets,
      '______________________>>>>>respone on save',
    );
    if (response.data[0].property) {
      alert(response.data[0].property);

      this.setState({
        isLoading: true,
        update_loader: false,
        myArray: [],
        imagesData: [],
      });
      this.details();
    }
  }
  async img() {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);

    const response = await this.api.img_upload(parse.token);

    console.log(response, '______________________>>>>>respone on save');
  }
  async delete_image(_id) {
    this.setState({ loader: true });
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.delete_image(_id, parse.token);
      console.log(response, 'deleted inamge-------------------');
      if (response.data.image == 'deleted') {
        this.details();
        this.setState({ loader: false });
      }
    } catch (error) {
      console.log(error, 'err');
      this.setState({ loader: false });
    }
  }
  map_img_array() {
    //console.log('kkkkkkkkkkkkkk');
    return this.state.imagesData.map((element, i) => {
      // console.log(element, 'eleeeeeeee');
      return (
        <View key={i} style={{ marginLeft: 10, marginRight: 10 }}>
          <Thumbnail
            square
            large
            source={{
              uri: `data:image/gif;base64,${element}`,
            }}
          />
        </View>
      );
    });
  }
  display_imaage_uploded() {
    //console.log('kkkkkkkkkkkkkk');
    return this.state.display_images.data.map((element, i) => {
      // console.log(element, 'eleeeeeeee--------------===');
      return (
        <View
          key={i}
          style={{ marginLeft: 10, marginRight: 10, marginTop: '5%' }}>
          <Entypo
            onPress={() => this.delete_image(element.id)}
            name="cross"
            size={30}
            color="red"
            style={{ position: 'absolute', elevation: 2, right: -13, top: -15 }}
          />

          <Thumbnail
            square
            large
            source={{
              uri: `${color.link}${element.name_dir}`,
            }}
          />
        </View>
      );
    });
  }

  handleBack() {
    this.props.navigation.goBack();
    this.setState({
      myArray: [],
      imagesData: [],
    });
  }

  render() {
    console.log(
      this.state.currency,
      '-----',
      this.state.city,
      'MY kdkhsdmh------ARRAY',
    );
    if (this.state.isLoading) {
      return (
        <Container>
          <MyHeader
            headername="3"
            name="Edit Property"
            // rightIconPress={() => alert('ACCOUNT PRESS')}
            // rightIconName="settings"
            onPressIcon={() => this.handleBack()}
          />
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: '10%',
            }}>
            <Spinner type="Circle" color={color.header3_color} />
          </View>
        </Container>
      );
    } else {
      // console.log(this.state.imagesData.length, this.state.myArray.length, "?>>>>> length loop render")
      return (
        <Container>
          <MyHeader
            headername="3"
            name="Edit Property"
            // rightIconPress={() => alert('ACCOUNT PRESS')}
            // rightIconName="settings"
            onPressIcon={() => this.handleBack()}
          />

          <Content
            style={{ marginLeft: '2%', marginRight: '2%', marginBottom: '5%' }}>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              {this.state.loader ? (
                <ActivityIndicator />
              ) : (
                this.display_imaage_uploded()
              )}
            </ScrollView>

            <Form>
              <Item stackedLabel>
                <Label>Property Name </Label>
                <Input
                  value={this.state.prppertyName}
                  onChangeText={(text) => this.setState({ prppertyName: text })}
                />
              </Item>
              <View
                style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Item stackedLabel last style={{ width: '45%' }}>
                  <Label>Street</Label>
                  <Input
                    value={this.state.street}
                    onChangeText={(text) => this.setState({ street: text })}
                  />
                </Item>
                <Item stackedLabel last style={{ width: '45%' }}>
                  <Label>Zip Code</Label>
                  <Input
                    value={String(this.state.zipcode)}
                    onChangeText={(text) => this.setState({ zipcode: text })}
                  />
                </Item>
                {/* <Item stackedLabel last style={{width: '65%'}}>
                  <Label>State</Label>
                  <Picker
                      mode="dialog"
                      selectedValue={this.state.state}
                      style={{
                        height: 50,
                      marginLeft:'-55%',
                        width: '90%',

                        marginBottom: '-2.5%',
                        right: '-13%',
                      }}
                      itemStyle={{
                        backgroundColor: 'red',
                        borderBottomWidth: 55,
        
                      }}
                      onValueChange={
                        (itemValue, itemIndex) => 
                        // this.onChange_api(itemValue)
                        this.setState({state: itemValue})
                      }>
                      {this.state_picker()}
                    </Picker>
                </Item> */}
              </View>
              <View
                style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Item stackedLabel last style={{ width: '95%' }}>
                  <Label>State</Label>
                  <Picker
                    mode="dialog"
                    selectedValue={this.state.state}
                    style={{
                      height: 50,
                      marginLeft: '-41%',
                      width: '90%',
                      marginBottom: '-2.5%',
                      right: '-13%',
                    }}
                    itemStyle={{
                      backgroundColor: 'red',
                      borderBottomWidth: 55,
                    }}
                    onValueChange={
                      (itemValue, itemIndex) => {
                        this.onChange_api(itemValue);
                        console.log(
                          itemValue,
                          'A A A A A A  A  A A A A A A A ',
                        );
                      }
                      // this.setState({state: itemValue})
                    }>
                    {this.state_picker()}
                  </Picker>
                </Item>
              </View>
              <Item stackedLabel last style={{ width: '95%' }}>
                <Label>City</Label>
                <Picker
                  mode="dialog"
                  selectedValue={this.state.city}
                  style={{
                    height: 50,
                    marginRight: '-5%',
                    marginLeft: '-45%',
                    width: '90%',

                    marginBottom: '-2.5%',
                    right: '-13%',
                  }}
                  itemStyle={{
                    backgroundColor: 'red',
                    borderBottomWidth: 55,
                  }}
                  onValueChange={(itemValue, itemIndex) =>
                    // this.onChange_api(itemValue)
                    this.setState({ city: itemValue })
                  }>
                  {this.city_picker()}
                </Picker>
              </Item>

              <View
                style={{
                  // borderBottomWidth: 1,
                  // width: '30%',
                  alignItems: 'center',
                  borderColor: 'grey',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: '44%',
                    marginTop: '3%',
                    borderColor: 'grey',
                  }}>
                  <Label
                    style={{
                      fontSize: 13,
                      color: 'grey',
                      marginBottom: '-5%',
                      marginLeft: '5%',
                    }}>
                    Currency
                  </Label>
                  <Picker
                    mode="dialog"
                    selectedValue={this.state.currency}
                    style={{
                      height: 50,
                      width: '90%',
                      marginTop: '5%',
                      marginBottom: '-2.5%',
                      marginLeft: '5%',
                    }}
                    itemStyle={{
                      backgroundColor: 'red',
                      borderBottomWidth: 55,
                    }}
                    onValueChange={(itemValue, itemIndex) =>
                      // this.onChange_api(itemValue)
                      this.setState({ currency: itemValue })
                    }>
                    {this.picker()}
                  </Picker>
                </View>
                <Item stackedLabel last style={{ width: '45%' }}>
                  <Label>Rent</Label>
                  <Input
                    value={String(this.state.rent)}
                    onChangeText={(text) => this.setState({ rent: text })}
                  />
                </Item>
              </View>
              {/* //-----------------------------------------------------------------------------------------------??? */}

              <View
                style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Item stackedLabel last style={{ width: '45%' }}>
                  <Label>Days for Rent</Label>
                  <Input
                    keyboardType="numeric"
                    value={String(this.state.timelimit)}
                    onChangeText={(text) => this.setState({ timelimit: text })}
                  />
                </Item>

                <Item stackedLabel last style={{ width: '45%' }}>
                  <Label>Limits of Tenants</Label>
                  <Input
                    keyboardType="numeric"
                    value={String(this.state.tenantlimit)}
                    onChangeText={(text) => this.setState({ tenantlimit: text })}
                  />
                </Item>
              </View>
              {/* //------------------------------------------------------ */}
              <View
                style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Item stackedLabel last style={{ width: '25%' }}>
                  <Label>Bed</Label>
                  <Input
                    keyboardType="numeric"
                    value={String(this.state.bedroom)}
                    onChangeText={(text) => this.setState({ bedroom: text })}
                  />
                </Item>
                <Item stackedLabel last style={{ width: '25%' }}>
                  <Label>Bath </Label>
                  <Input
                    keyboardType="numeric"
                    value={String(this.state.bathroom)}
                    onChangeText={(text) => this.setState({ bathroom: text })}
                  />
                </Item>

                <Item stackedLabel last style={{ width: '28%' }}>
                  <Label>Toilet</Label>
                  <Input
                    keyboardType="numeric"
                    value={String(this.state.toilets)}
                    onChangeText={(text) => this.setState({ toilets: text })}
                  />
                </Item>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  alignSelf: 'center',
                  marginTop: '5%',
                  //   left: '20%',
                }}>
                <Ionicons
                  name="add-circle"
                  size={30}
                  color={color.theme_Color}
                  onPress={() =>
                    this.props.navigation.navigate('Features', {
                      key: this.props.route.params.key,
                    })
                  }
                />
                <Text
                  onPress={() =>
                    this.props.navigation.navigate('Features', {
                      key: this.props.route.params.key,
                    })
                  }
                  style={{ fontWeight: 'bold' }}>
                  Add Features
                </Text>
              </View>
              <Item stackedLabel last>
                <Label>Description</Label>
                <Input
                  value={String(this.state.description)}
                  onChangeText={(text) => this.setState({ description: text })}
                />
              </Item>
              <Item stackedLabel last>
                <Label>Date</Label>
                <Input
                  value={String(this.state.date)}
                  onChangeText={(text) => this.setState({ date: text })}
                />
              </Item>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: '3%',
                }}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {this.map_img_array()}
                </ScrollView>
              </View>
              <Text
                onPress={() => this.getimage()}
                style={{ fontSize: 20, color: 'green', left: '55%' }}>
                upload Images
              </Text>
              <Button
                onPress={() => this.handleSave()}
                style={{
                  backgroundColor: 'green',
                  width: '35%',
                  alignSelf: 'center',
                  marginTop: '5%',
                  justifyContent: 'center',
                }}>
                {this.state.update_loader ? (
                  <ActivityIndicator color="white" />
                ) : (
                  <Text style={{ color: 'white', fontSize: 16 }}>SAVE</Text>
                )}
              </Button>
              {/* <Button onPress={() => this.imagePicker()}>
                <Text>image</Text>
              </Button> */}
            </Form>

            {/* <Text onPress={() => console.log(this.state.myArray, 'MY ARRAY')}>
              Press
            </Text> */}
          </Content>
        </Container>
      );
    }
  }
}
