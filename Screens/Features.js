import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Text,
  Body,
  Button,
} from 'native-base';
import {Picker} from '@react-native-picker/picker';
import {Paragraph, Dialog, Portal, ActivityIndicator} from 'react-native-paper';

import MyHeader from '../Constant/MyHeader';
import React from 'react';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import * as color from '../Constant/Colors';
import {View} from 'react-native';

export default class Features extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      isLoading: true,
      features: '',
      delRes: '',
      visible: false,
      user: '',
      modelFeature: '',
      token: '',
      feature_id: '',
      feature_loader: false,
    };
  }
  async details() {
    console.log(this.props.route.params.key, '--------');

    // STORAGE
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    console.log(parse.token, 'token ten');
    this.setState({token: parse.token});

    // FIRST API
    const response = await this.api.list_features(
      this.props.route.params.key,
      parse.token,
    );
    console.log(response, '------------res');
    // SECOND API
    const responseFeature = await this.api.model_feature(parse.token);
    // console.log(responseFeature.data[0], 'respms-----====');

    if (responseFeature.data.length == 0) {
      console.log('Empty LIST');
    } else {
      this.setState({
        modelFeature: responseFeature.data,
        feature_id: responseFeature.data[0].id,
      });
    }
    // SET STATE
    this.setState({
      features: response,

      isLoading: false,
    });
  }

  async feature_delete(_id) {
    this.setState({isLoading: true});
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    console.log(parse.token, 'token ten');
    const response = await this.api.delete_features(_id, parse.token);

    console.log(
      response,
      '...........dekl.....................',
      _id,
      '.......rsss',
    );
    this.details();
  }
  async feature_add(_propertyId, _featureId) {
    this.setState({feature_loader: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);

      const response = await this.api.add_features(
        _propertyId,
        _featureId,
        this.state.token,
      );
      console.log(response, 'ad featurees ');
      if (response.data.status) {
        console.log('IF---->');
        this.hideDialog();
        this.details();
        this.setState({feature_loader: false});
      } else {
        console.log('ERRRO--->');
        this.setState({feature_loader: false});
      }
    } catch (err) {
      console.log(err, 'ERROR');
      this.setState({feature_loader: false});
    }
  }
  // async feature_model() {
  //   this.setState({isLoading: true});
  //   const UsreData = await AsyncStorage.getItem('user_data');
  //   const parse = JSON.parse(UsreData);
  //   console.log(parse.token, 'token ten');
  //   const response = await this.api.model_feature(parse.token);
  //   this.setState({modelFeature: response, isLoading: false});
  //   console.log(response, '-------models feaaffa');
  // }

  componentDidMount() {
    const {navigation} = this.props;

    this.focusListener = navigation.addListener('focus', () => {
      this.details();
      console.log('cosolllllll');
    });
    this.focusListenerr = navigation.addListener('blur', () => {
      console.log('un cosolllllll');
      this.setState({feature: []});
      this.setState({isLoading: true});
    });
  }
  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }
  features_list = () => {
    if (this.state.features.data.length == 0) {
      return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          {/* <Octicons name="tasklist" size={80} color="grey" /> */}
          <Text style={{color: 'grey', marginTop: '2%'}}>
            No Features found!
          </Text>
        </View>
      );
    } else {
      return this.state.features.data.map((element, i) => {
        console.log(element.id, '----------------------->elelmet');
        return (
          <View key={i}>
            <Card
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: '5%',
                backgroundColor: `${color.backgroundColor}`,
                marginLeft: '3%',
                marginRight: '3%',
                marginTop: '2%',
              }}>
              <Text style={{fontSize: 18}}>{element.feature}</Text>
              <Button
                style={{
                  width: 85,
                  height: 40,
                  backgroundColor: `${color.theme_Color}`,
                }}
                onPress={() => this.feature_delete(element.id)}>
                <Text>Delete</Text>
              </Button>
            </Card>
            {/* <Card
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: '3%',
                marginLeft: '3%',
                marginRight: '3%',
                marginTop: '3%',
                backgroundColor: `${color.backgroundColor}`,
              }}>
              <CardItem
                style={{backgroundColor: '#cee1e2'}}
                header
                button
                onPress={() => alert('This is Card Header')}>
                <Text style={{fontSize: 18}}>{element.feature}</Text>
              </CardItem>
              <CardItem style={{backgroundColor: '#cee1e2'}}>
                <Button
                  style={{
                    width: 83,
                    height: 40,
                    backgroundColor: `${color.header3_color}`,
                  }}
                  onPress={() => this.feature_delete(element.id)}>
                  <Text>Delete</Text>
                </Button>
              </CardItem>
            </Card> */}
          </View>
        );
      });
    }
  };
  model_list = () => {
    if (this.state.modelFeature.length == 0) {
      return (
        <View>
          <Text>No data available!</Text>
        </View>
      );
    } else {
      return this.state.modelFeature.map((element, i) => {
        console.log(element, '----------------------->elelmet');
        return <Picker.Item label={element.feature} value={element.id} />;
      });
    }
  };

  showDialog = () => this.setState({visible: true});
  hideDialog = () => this.setState({visible: false});
  render() {
    console.log(this.props.route.params.key, '--------');
    console.log(this.state.feature_id, 'id');
    if (this.state.isLoading) {
      return (
        <Container>
          <MyHeader
            headername="3"
            name="Add Features"
            rightIconPress={() => this.showDialog()}
            rightIconName="add"
            onPressIcon={() => this.props.navigation.goBack()}
          />
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: '10%',
            }}>
            <Spinner type="Circle" color={color.header3_color} />
          </View>
        </Container>
      );
    } else {
      return (
        <Container>
          <MyHeader
            headername="3"
            name="Add Features"
            rightIconPress={() => this.showDialog()}
            rightIconName="add"
            onPressIcon={() => this.props.navigation.goBack()}
          />

          <Content style={{marginTop: '2%'}}>
            <View>{this.features_list()}</View>
            <View>
              {/* <Button onPress={this.showDialog}>
                <Text>Show Dialog</Text>
              </Button> */}
              <Portal>
                <Dialog
                  visible={this.state.visible}
                  onDismiss={this.hideDialog}>
                  <Dialog.Title>Select Feauture</Dialog.Title>
                  <Dialog.Content>
                    <Picker
                      mode="dialog"
                      selectedValue={this.state.feature_id}
                      style={{
                        height: 50,
                        width: '102%',
                        color: 'cadetblue',
                        marginBottom: '-2.5%',
                        right: '1.5%',
                      }}
                      itemStyle={{
                        backgroundColor: 'red',
                        borderBottomWidth: 55,
                      }}
                      onValueChange={(itemValue, itemIndex) =>
                        this.setState({feature_id: itemValue})
                      }>
                      {this.model_list()}
                    </Picker>
                  </Dialog.Content>
                  <Dialog.Actions>
                    <Button
                      onPress={() =>
                        this.feature_add(
                          this.props.route.params.key,
                          this.state.feature_id,
                        )
                      }
                      style={{
                        marginRight: '3%',
                        justifyContent: 'center',
                        backgroundColor: `${color.theme_Color}`,
                      }}>
                      {this.state.feature_loader ? (
                        <Text>
                          <ActivityIndicator color="white" />
                        </Text>
                      ) : (
                        <Text>Add</Text>
                      )}
                    </Button>
                    <Button
                      style={{backgroundColor: `${color.newbtn}`}}
                      onPress={this.hideDialog}>
                      <Text>Cancel</Text>
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>
            </View>
          </Content>
        </Container>
      );
    }
  }
}
