import {Container, Content, Title, Header} from 'native-base';
import * as color from '../Constant/Colors';
import React from 'react';
import Collapsible from 'react-native-collapsible';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {TextInput} from 'react-native';
import {View, Text} from 'react-native';
import {RadioButton} from 'react-native-paper';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import {Tab, Tabs, Thumbnail} from 'native-base';
export default class FilterProperties extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      myData: this.props.route.params.dataRes,
      isLoading: false,
      input_search: '',
    };
  }

  List = () => {
    if (this.state.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: '55%',
          }}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      console.log(this.state.myData, 'param stat la ka dataa-----&');
      return this.state.myData.property_data.map((element, i) => {
        return (
          <View
            key={i}
            style={{
              flexDirection: 'row',
              marginLeft: '3%',
              marginRight: '3%',
              alignItems: 'center',
              borderWidth: 0.1,
              elevation: 1,
              padding: '3%',
              marginTop: '5%',
            }}>
            {element.images.length > 0 ? (
              <Thumbnail
                square
                large
                source={{
                  uri: `http://property.gtb2bexim.com/${element.images[0].path}`,
                }}
              />
            ) : (
              <Thumbnail
                square
                large
                source={require('../assets/myhome.jpg')}
              />
            )}

            <View style={{marginLeft: '5%'}}>
              <Text style={{fontWeight: 'bold', fontSize: 16}}>
                {element.name}
              </Text>
              <Text>Rent: {element.rent}$</Text>
              <Text style={{color: 'grey'}}>City: {element.city}</Text>
            </View>
            <View style={{marginLeft: '15%'}}>
              <Text style={{fontWeight: 'bold', color: 'green'}}>Status</Text>
              <Text style={{color: 'green', fontWeight: 'bold'}}>
                {element.status}
              </Text>
            </View>
          </View>
        );
      });
    }
  };

  render() {
    // console.log(this.state.myData, 'param stat la ka dataa-----&');
    return (
      <Container>
        <Content>
          <View
            style={{
              backgroundColor: `${color.header3_color}`,
              padding: '5%',
            }}>
            <Ionicons
              name="search"
              size={30}
              style={{
                position: 'absolute',
                alignSelf: 'center',
                elevation: 20,
                right: '10%',
                top: '50%',
              }}
            />
            <TextInput
              onChangeText={(text) => this.setState({input_search: text})}
              placeholder="Search by location"
              style={{backgroundColor: 'white', borderRadius: 20}}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              alignSelf: 'center',
              marginLeft: '39%',
            }}></View>

          <View>{this.List()}</View>
        </Content>
      </Container>
    );
  }
}
