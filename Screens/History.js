import MyHeader from '../Constant/MyHeader';
import React from 'react';
import {Container, Content, Title, Tab, Tabs, Thumbnail} from 'native-base';
import * as color from '../Constant/Colors';
import {View, Text} from 'react-native';
import API from '../Constant/API';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import {Divider} from 'react-native-elements';
export default class History extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      data: '',
      isloading: true,
    };
  }
  async all_tedent_rent_select() {
    this.setState({isloading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.tendent_history(
        this.props.route.params.key,
        this.props.route.params.key2,
        parse.token,
      );
      console.log(response, 'data afafagaggasgashadghad');

      this.setState({
        data: response,
        isloading: false,
      });
    } catch (error) {
      console.log(error, 'errr');
    }
  }
  async componentDidMount() {
    const {navigation} = this.props;
    this.focusListener = navigation.addListener('focus', () => {
      this.all_tedent_rent_select();
    });
    this.all_tedent_rent_select();
  }
  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }

  map = () => {
    return this.state.data.data.map((element, i) => {
      console.log(
        element,
        '----------------------------------------------------------',
      );
      return (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: '3%',
          }}>
          <View
            style={{
              borderLeftWidth: 1,
              padding: '1%',
              justifyContent: 'center',
              width: '55%',
            }}>
            <Text>Tenant:</Text>
            <Text style={{fontWeight: 'bold'}}>{element.tendent_name}</Text>
            <Text>Property:</Text>
            <Text style={{fontWeight: 'bold'}}>{element.property_name}</Text>
          </View>
          <View
            style={{
              borderLeftWidth: 1,
              padding: '1%',
              width: '30%',
              justifyContent: 'center',
            }}>
            <Text>Date:</Text>
            <Text style={{color: 'grey', fontWeight: 'bold'}}>
              {element.date}
            </Text>
            <Text>Paid Amount:</Text>
            <Text style={{fontWeight: 'bold', color: 'green', fontSize: 17}}>
              {element.amount}
            </Text>
          </View>
          <View>
            <Divider style={{marginTop: '5%'}} />
          </View>
        </View>
      );
    });
  };
  render() {
    console.log(
      this.props.route.params.key,
      '00000000000000000>>>>>>>>>>>>>>>>.',
    );
    return (
      <Container>
        <MyHeader
          headername="3"
          name="History"
          onPressIcon={() => this.props.navigation.goBack()}
        />
        <Content style={{marginLeft: '5%', marginRight: '5%'}}>
          {/* <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <Text
                style={{
                  fontSize: 17,
                  fontWeight: 'bold',
                  color: `${color.header3_color}`,
                }}>
                Tendent Name
              </Text>
            </View>
            <View>
              <Text
                style={{
                  fontSize: 17,
                  fontWeight: 'bold',
                  color: `${color.header3_color}`,
                }}>
                Date
              </Text>
            </View>
            <View>
              <Text
                style={{
                  fontSize: 17,
                  fontWeight: 'bold',
                  color: `${color.header3_color}`,
                }}>
                Paid
              </Text>
            </View>
          </View> */}
          {/* //---------------------------------------------------------------------------------------MAP */}
          <View>
            {this.state.isloading ? (
              <Spinner
                type="Circle"
                color={color.header3_color}
                style={{alignSelf: 'center'}}
              />
            ) : (
              this.map()
            )}
          </View>

          {/* //================== */}
        </Content>
      </Container>
    );
  }
}
