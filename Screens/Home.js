//landlord screen
import {Button, Avatar} from 'react-native-paper';
import {View, Text, ImageBackground} from 'react-native';
import * as color from '../Constant/Colors';
import {Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import HomeHeader from '../Constant/HomeHeader';
import {Container, Content, Title, Tab, Tabs, Thumbnail} from 'native-base';
import React from 'react';
import MyHeader from '../Constant/MyHeader';
import AntDesign from 'react-native-vector-icons/AntDesign';
import BottomTab from '../Constant/BottomTab';
import {ActivityIndicator} from 'react-native';
import Spinner from 'react-native-spinkit';
import ApplicationHome from './ApplicationHome';
import OpenHome from './OpenHome';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Request from './Request';
import CurrentTendent from './CurrentTendent';
import {colors} from 'react-native-elements';

export default class Home extends React.Component {
  constructor(props) {
    super(props);

    this.second_bacha = React.createRef();
    this.api = API();
    this.state = {
      type: '',
      index: 0,
      isLoading: true,
    };
  }
  // async tabfunction(_user_type) {

  //   // const UsreData = await AsyncStorage.getItem('user_data');
  //   // const parse = JSON.parse(UsreData);
  //
  //   // console.log(parse.user_type, 'typeep--------------');
  // }
  async componentDidMount() {
    try {
      // setTimeout(async() => {

      const UsreData = await AsyncStorage.getItem('user_data');
      const parse3 = JSON.parse(UsreData);

      this.setState({type: parse3.user_type});
      console.log(parse3, 'HOMEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE');
      const emailAndId = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(emailAndId);

      this._storeUserEmailAndID(parse.email);

      console.log('dfdfdfdfdfdfdfdfdfdfdfdfdfdf');

      this.setState({isLoading: false});
      // }, 3000);
    } catch (error) {
      console.log(error, 'erreie');
    }
  }

  async _storeUserEmailAndID(_email) {
    try {
      let obj = {
        email: _email,
      };

      await AsyncStorage.setItem('check_user', JSON.stringify(obj));
    } catch (error) {
      alert(error);
    }
    console.log(_email, '---> Check user stored');
  }

  async selectApi() {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    console.log(parse.token, 'selct api');
    const response = await this.api.select(parse.token);
    console.log(response.data, 'RESPONSE');
  }
  openHomeList = () => {
    if (this.state.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: '55%',
          }}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      console.log(this.state.property_data, 'property data comsole');
      return this.state.myData.property_data.map((element, i) => {
        return (
          <View
            key={i}
            style={{
              flexDirection: 'row',
              marginLeft: '3%',
              marginRight: '3%',
              alignItems: 'center',
              borderWidth: 0.1,
              elevation: 1,
              padding: '3%',
              marginTop: '5%',
            }}>
            {element.images.length > 0 ? (
              <Thumbnail
                square
                large
                source={{
                  uri: `http://property.gtb2bexim.com/${element.images[0].path}`,
                }}
              />
            ) : (
              <Thumbnail
                square
                large
                source={{
                  uri:
                    'https://i.pinimg.com/474x/b4/8f/3b/b48f3bb31399b66b2d5000a161032a1d.jpg',
                }}
              />
            )}

            <View style={{marginLeft: '5%'}}>
              <Text
                onPress={() =>
                  this.props.navigation.navigate('ViewCartDetails', {
                    key: element.id,
                    status: '',
                  })
                }
                style={{fontWeight: 'bold', fontSize: 16}}>
                {element.name}
              </Text>
              <Text>Rent: {element.rent}$</Text>
              <Text style={{color: 'grey'}}>City: {element.city}</Text>
            </View>
            <View style={{marginLeft: '15%'}}>
              <Text style={{fontWeight: 'bold', color: 'green'}}>Status</Text>
              <Text style={{color: 'green', fontWeight: 'bold'}}>
                {element.status}
              </Text>
            </View>
          </View>
        );
      });
    }
  };
  onClick = (e) => {
    console.log(e, 'dfdfdfdfdfdfdfdfdfdfdf');
    if (e.from != 0) {
      this.setState({isLoading: true});
      setTimeout(() => {
        console.log(this.state.index);
        this.setState({index: Math.random()});
        this.setState({isLoading: false});
      }, 2000);
    }
  };

  createtab = () => {
    return <Request navigation={this.props.navigation} />;
  };
  render() {
    console.log(this.state.type, '-------------------------TYPR   __');
    if (this.state.type < 2) {
      if (this.state.isLoading) {
        return (
          <Container>
            <MyHeader
              headername="1"
              name=""
              leftIconPress={() => this.props.navigation.openDrawer()}
            />
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Spinner type="Circle" color={color.header3_color} />
            </View>
          </Container>
        );
      } else {
        return (
          <Container>
            <MyHeader
              headername="1"
              name="Tenants"
              leftIconPress={() => this.props.navigation.openDrawer()}
            />

            <Tabs
              onChangeTab={this.onClick}
              tabBarUnderlineStyle={{backgroundColor: `${color.theme_Color}`}}>
              <Tab
                textStyle={{color: 'black', fontWeight: 'bold'}}
                activeTextStyle={{color: 'black', fontWeight: 'bold'}}
                tabStyle={{backgroundColor: 'white'}}
                activeTabStyle={{backgroundColor: 'white'}}
                heading="Current Tenant">
                <CurrentTendent
                  ref={(instance) => {
                    this.second_bacha = instance;
                  }}
                  navigation={this.props.navigation}
                />
              </Tab>
              <Tab
                textStyle={{color: 'black', fontWeight: 'bold'}}
                activeTextStyle={{color: 'black', fontWeight: 'bold'}}
                tabStyle={{backgroundColor: 'white'}}
                activeTabStyle={{backgroundColor: 'white'}}
                heading="Requests">
                {this.createtab()}
              </Tab>
            </Tabs>
            {/* </Content> */}
          </Container>
        );
      }
    } else {
      //===========================================================================----------else
      return (
        <Container>
          <MyHeader
            headername="1"
            name="Properties"
            // rightIconPress={() =>
            //   this.props.navigation.navigate('TendentDetail')
            // }
            type="ten"
            // rightIconName="home-circle-outline"
            leftIconPress={() => this.props.navigation.openDrawer()}
          />
          {/* <HomeHeader leftIcon={() => this.props.navigation.openDrawer()} /> */}
          {/* <Content> */}
          <Tabs
            tabBarUnderlineStyle={{backgroundColor: `${color.theme_Color}`}}>
            <Tab
              textStyle={{color: 'black', fontWeight: 'bold'}}
              activeTextStyle={{color: 'black', fontWeight: 'bold'}}
              tabStyle={{backgroundColor: 'white'}}
              activeTabStyle={{backgroundColor: 'white'}}
              heading="Application">
              <ApplicationHome navigation={this.props.navigation} />
            </Tab>
            <Tab
              textStyle={{color: 'black', fontWeight: 'bold'}}
              activeTextStyle={{color: 'black', fontWeight: 'bold'}}
              tabStyle={{backgroundColor: 'white'}}
              activeTabStyle={{backgroundColor: 'white'}}
              heading="My Home">
              <OpenHome navigation={this.props.navigation} />
            </Tab>
          </Tabs>
        </Container>
      );
    }
  }
}
