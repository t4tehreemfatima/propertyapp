import React from 'react';
import {View, Text, Image} from 'react-native';
import {Container, Content, Label, Input, Item} from 'native-base';
import MyHeader from '../Constant/MyHeader';
import Icon from 'react-native-vector-icons/Ionicons';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as color from '../Constant/Colors';

export default class Inspection extends React.Component {
  render() {
    return (
      <Container>
        <MyHeader
          headername="1"
          name="Inspection"
          rightIconPress={() => alert('ACCOUNT PRESS')}
          rightIconName="settings"
          leftIconPress={() => this.props.navigation.openDrawer()}
        />
        <Content>
          <View
            style={{
              backgroundColor: `${color.searchBox_color}`,
              padding: '5%',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View style={{backgroundColor: 'white', width: '80%'}}>
              <Item>
                <Input placeholder="Search" />
              </Item>
            </View>
            <View
              style={{
                backgroundColor: 'white',
                width: '15%',
                alignItems: 'center',
              }}>
              <MIcon
                style={{top: '20%'}}
                color={color.icon_color}
                name="filter"
                size={26}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: '2%',
              marginLeft: '1.5%',
              marginRight: '1.5%',
              elevation: 1.6,
              borderColor: 'grey',
              marginTop: '3%',
            }}>
            <View>
              <Image
                source={{
                  uri:
                    'https://images.unsplash.com/photo-1576941089067-2de3c901e126?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1000&q=80',
                }}
                style={{height: 130, width: 150, borderRadius: 20}}
              />
            </View>
            <View style={{marginLeft: '2%', marginRight: '5%'}}>
              <Text style={{fontWeight: 'bold', fontSize: 18}}>
                Nacart Apartment
              </Text>
              <Text style={{color: 'grey'}}>
                <Icon name="ios-location-sharp" size={16} color="green" />
                Address #2527 USA
              </Text>
              <View
                style={{
                  backgroundColor: 'gold',
                  width: '30%',
                  borderRadius: 20,
                }}>
                <Text> For Sale</Text>
              </View>

              {/* cahangeeeee+++++++++++++++++++ */}
              <View style={{flexDirection: 'row', marginTop: '3%'}}>
                <View
                  style={{
                    backgroundColor: 'skyblue',
                    width: '38%',
                    borderRadius: 20,
                  }}>
                  <Text>
                    <Icon name="md-bed-outline" size={16} color="green" />
                    bedroom
                  </Text>
                </View>
                <View
                  style={{
                    backgroundColor: 'yellow',
                    width: '38%',
                    borderRadius: 20,
                  }}>
                  <Text>
                    <MIcon name="toilet" size={16} color="green" /> toilet
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginTop: '3%'}}>
                <View
                  style={{
                    backgroundColor: 'pink',
                    width: '38%',
                    borderRadius: 20,
                  }}>
                  <Text>
                    <Icon name="md-wifi-sharp" size={16} color="green" />
                    internet
                  </Text>
                </View>
                <View
                  style={{
                    backgroundColor: 'orange',
                    width: '38%',
                    borderRadius: 20,
                  }}>
                  <Text>
                    <Icon name="car-sport-outline" size={16} color="green" />
                    parking
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
