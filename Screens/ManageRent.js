//landlord screen
import {Button, Avatar} from 'react-native-paper';
import {View, Text, ImageBackground} from 'react-native';
import * as color from '../Constant/Colors';
import {Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import {Container, Content, Title, Tab, Tabs, Thumbnail} from 'native-base';
import React from 'react';
import MyHeader from '../Constant/MyHeader';
import AntDesign from 'react-native-vector-icons/AntDesign';
import BottomTab from '../Constant/BottomTab';
import {ActivityIndicator} from 'react-native';
import Spinner from 'react-native-spinkit';

import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RentTedent from './RentTendent';
import Rent from './Rent';
import Utility_landlord from './Utility_landlord';
import Utility_add from './Utility_add';

export default class ManageRent extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {};
  }
  async tabfunction() {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
  }
  componentDidMount() {}

  render() {
    console.log(this.props.route.params.key, 'ELEMENT KEy');
    return (
      <Container>
        <MyHeader
          headername="4"
          name="Manage Property"
          //   rightIconPress={() => this.props.navigation.navigate('History')}
          //   rightIconName="history"
          onPressIcon={() => this.props.navigation.goBack()}
        />
        {/* <HomeHeader leftIcon={() => this.props.navigation.openDrawer()} /> */}
        {/* <Content> */}
        <Tabs tabBarUnderlineStyle={{backgroundColor: `${color.theme_Color}`}}>
          <Tab
             textStyle={{color: 'black', fontWeight: 'bold'}}
              activeTextStyle={{color: 'black', fontWeight: 'bold'}}
              tabStyle={{backgroundColor: 'white'}}
              activeTabStyle={{backgroundColor: 'white'}}
            heading="Rent">
            <RentTedent
              navigation={this.props.navigation}
              kuchb={this.props.route.params.key}
            />
            {/* <ApplicationHome navigation={this.props.navigation} /> */}
          </Tab>
          <Tab
            textStyle={{color: 'black', fontWeight: 'bold'}}
            activeTextStyle={{color: 'black', fontWeight: 'bold'}}
            tabStyle={{backgroundColor: 'white'}}
            activeTabStyle={{backgroundColor: 'white'}}
            heading="Utility Paid">
            <Utility_landlord
              navigation={this.props.navigation}
              kuchb={this.props.route.params.key}
            />
            {/* <OpenHome  /> */}
          </Tab>
          <Tab
            textStyle={{color: 'black', fontWeight: 'bold'}}
            activeTextStyle={{color: 'black', fontWeight: 'bold'}}
            tabStyle={{backgroundColor: 'white'}}
            activeTabStyle={{backgroundColor: 'white'}}
            heading="Utility">
            <Utility_add
              navigation={this.props.navigation}
              kuchb={this.props.route.params.key}
            />

            {/* <OpenHome  /> */}
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
