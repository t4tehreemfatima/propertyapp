import React, { useState, useCallback, useEffect } from 'react';
import {
  GiftedChat,
  Bubble,
  Actions,
  ActionsProps,
} from 'react-native-gifted-chat';
import { io } from 'socket.io-client';
import API from '../Constant/API';
import Icon from 'react-native-vector-icons/Ionicons';
import MyHeader from '../Constant/MyHeader';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { View, Header, Body, Left, Right, Title } from 'native-base';
import DocumentPicker from 'react-native-document-picker';
import ImgToBase64 from 'react-native-image-base64';
import RNFS from 'react-native-fs';
import * as color from '../Constant/Colors';
import Video from 'react-native-video';
import Spinner from 'react-native-spinkit';

// var currentdate = new Date();
// var datetime =
//   currentdate.getFullYear() +
//     '-' +
//     (currentdate.getMonth() + 1) +
//     '-' +
//     currentdate.getDate() <
//   2
//     ? '0'
//     : '' +
//       ' ' +
//       currentdate.getHours() +
//       ':' +
//       currentdate.getMinutes() +
//       ':' +
//       currentdate.getSeconds();

export default function MessageScreen(route) {
  const [messages, setMessages] = useState([]);
  const [id, setId] = useState('');
  const [loader, setloader] = useState(true);
  const [paused, isPaused] = useState(true);

  let socket = '';
  let userid = '';
  let username = '';
  let imageuser = '';
  let imglink = '';
  let imgname = '';

  useEffect((props) => {
    // console.log()
    (async () => {
      console.log(new Date(), 'date');
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      username = parse.name;
      userid = parse.id;
      imageuser = parse.draw_img;

      // console.log(route, 'chkkk PROOOOPS')

      console.log(route.route.params.id, '000000000000000000UUUUUU');
      socket = io('http://tekumatics.com:3000', {
        transports: ['websocket', 'polling'],
        upgrade: false,
        query: {
          private: 0,
        },
      });
      socket.on('reciveroommessage', (msg) => {
        console.log(msg, 'aaaasdasd');
        setMessages((previousMessages) =>
          GiftedChat.append(previousMessages, msg),
        );
      });
      socket.on('getvideomessage', (msg) => {
        console.log(msg, 'videiooooooooo');
        setMessages((previousMessages) =>
          GiftedChat.append(previousMessages, msg),
        );
      });
      socket.emit('socketidsub', userid);
      socket.emit('connectionsocket', userid);
      // socket = io('http://c3b9-175-107-219-203.ngrok.io');
      // socket.emit('messagetoroom',route.route.params.id, userid,"saasdasdasdasd" ,null,null,new Date().toLocaleDateString(),1 );

      // console.log('-----------<<>>><<>><<>><<>><<>>check consol<<><><><<___');
      let mounted = true;
      let tmp = [];
      socket.on('get_chat_message', (msg) => {
        console.log(msg, 'uu88----------------u');
        if (msg.length != 0) {
          setMessages((previousMessages) =>
            GiftedChat.append(previousMessages, msg.message),
          );
        }

        //setloader(false);
      });

      socket.emit(
        'get_old_messages',
        route.route.params.id,
        route.route.params.type,
      );
      // alert("Dfdf")
      socket.on('set_old_messages', (msg) => {
        console.log(msg, 'old messges ---------------u');
        if (mounted) {
          setMessages((previousMessages) => GiftedChat.append([], msg));
        }
        setloader(false);
      });
      socket.on('get_image_message', (msg) => {
        console.log(msg, 'asssasddllllllllllllllllllllkjjjjjjjjjjjjjjjjj');
        setMessages((previousMessages) =>
          GiftedChat.append(previousMessages, msg),
        );
      });
      return () => (mounted = false);
    })();
  }, []);

  const renderMessageVideo = (props) => {
    // console.log(props,'prps');
    return (
      <View style={{ position: 'relative', height: 150, width: 250 }}>
        <Video
          style={{
            position: 'absolute',
            left: 0,
            top: 0,
            height: 150,
            width: 250,
            borderRadius: 20,
          }}
          resizeMode="cover"
          height={150}
          width={250}
          controls
          // muted={true}
          //paused={paused}
          fullscreen={true}
          source={{ uri: props.currentMessage.video }}
          allowsExternalPlayback={false}></Video>
      </View>
    );
  };
  const handlePickImage = useCallback(async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.images, DocumentPicker.types.video],
      });
      imgname = res.name;
      console.log(
        res.uri,
        res.type, // mime type
        res.name,
        res.size,
      );
      console.log(DocumentPicker.types.video, res.type, 'tyrppp image');



      if (String(res.type).includes('video')) {
        const UsreData = await AsyncStorage.getItem('user_data');
        const parse = JSON.parse(UsreData);
        console.log('kuch b');
        RNFS.readFile(res.uri, 'base64').then(async (video) => {
          const response = await API().video(
            video,
            imgname,
            route.route.params.id,
            parse.id,
            new Date(),
            route.route.params.type,
          );
          console.log(response, 'video here');
        });
      } else {
        ImgToBase64.getBase64String(res.uri)
          .then(
            (base64String) => {
              imglink = base64String;

              socket.emit(
                'image_message',
                route.route.params.id,
                userid,
                imgname,
                imglink,
                new Date().toLocaleDateString(),
                route.route.params.type,
              );
              //console.log(imglink, imgname, '-Name-----LINK------');

              // setMessages([
              //   {
              //     _id: '134',
              //     user: {
              //       _id: 4,
              //       avatar: 'https://facebook.github.io/react/img/logo_og.png',
              //       name: 'Maaz',
              //     },
              //   },
              // ]);
            },
            // (imglink = base64String),
            // this.upload_img(this.state.id, res.name, base64String),
            // this.setState({linkImage: base64String}),
          )
          .catch((err) => console.log(err));
      }
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }, []);

  const onSend = useCallback((messages = []) => {
    console.log(messages[0].text, 'datee------------------>');
    // setMessages((previousMessages) =>
    //   GiftedChat.append(previousMessages, [{
    //               _id: 123,
    //               text: messages,
    //               createdAt: new Date().toLocaleDateString(),
    //               user: {
    //                 _id: userid,
    //                 name: username,
    //                 avatar: `${color.link}${imageuser}`,
    //               },
    //             }]),
    // );
    socket.emit(
      'messagetoroom',
      route.route.params.id,
      userid,
      messages[0].text,
      null,
      null,
      new Date().toLocaleDateString(),
      route.route.params.type,
    );
  }, []);
  const renderActions = (props) => {
    // console.log(route.route.params.details.property[0].id,'ssssaaaaaaaaaadddddddd')
    return (
      <Actions
        {...props}
        options={{
          ['Send Image']: () => handlePickImage(),
        }}
        icon={() => <Icon name="image-outline" size={28} color="blue" />}
        onSend={(args) => console.log(args, '--------0-0---')}
      />
    );
  };
  return (
    <View style={{ flex: 1 }}>
      {route.route.params.type == 1 ? (
        <MyHeader
          headername="chat"
          name={route.route.params.name}
          rightIconPress={() =>
            route.navigation.navigate('inbox_tenant', {
              property_id: route.route.params.details.property[0].id,
            })
          }
          onPressIcon={() => route.navigation.goBack()}
        />
      ) : (
        <MyHeader
          headername="chat"
          name={route.route.params.name}
          rightIconPress={() =>
            route.navigation.navigate('inbox_tenant', {
              property_id: route.route.params.id,
            })
          }
          onPressIcon={() => route.navigation.goBack()}
        />
      )}
      {loader == true ? (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: '40%',
          }}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      ) : (
        <GiftedChat
          renderBubble={(props) => {
            return (
              <Bubble
                {...props}
                textStyle={{
                  right: {
                    color: 'black',
                  },
                  left: {
                    color: 'black',
                  },
                }}
                wrapperStyle={{
                  right: {
                    backgroundColor: '#cee8f9',
                  },
                  left: {
                    backgroundColor: '#97e4f5',
                  },
                }}
              />
            );
          }}
          messages={messages}
          alwaysShowSend={true}
          quickReplyStyle={{ backgroundColor: 'red' }}
          renderMessageVideo={renderMessageVideo}
          showAvatarForEveryMessage={true}
          renderUsernameOnMessage={true}
          renderActions={renderActions}
          onSend={(messages) => onSend(messages)}
          inverted={true}
          user={{
            _id: 4,
          }}
        />
      )}
    </View>
  );
}
