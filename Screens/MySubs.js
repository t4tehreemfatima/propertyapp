import {Card, Container, Content, Title} from 'native-base';
import MyHeader from '../Constant/MyHeader';
import React from 'react';
import * as color from '../Constant/Colors';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import API from '../Constant/API';
import {Divider} from 'react-native-paper';
export default class MySubs extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      data: '',
      isLoading: true,
    };
  }
  async my_subs() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.user_subscr_select(parse.token);
      console.log(parse.id, parse.token, 'Data Response---------------');
      this.setState({data: response, isLoading: false});
      console.log(response, 'ppppppppdata');
    } catch (err) {
      console.log('Error: ', err);
    }
  }
  componentDidMount() {
    const {navigation} = this.props;

    this.focusListener = navigation.addListener('focus', () => {
      this.my_subs();
    });
    this.my_subs();
  }

  componentWillUnmount() {
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }
  // componentDidMount() {
  //   this.my_subs();
  // }
  render() {
    if (this.state.isLoading) {
      return (
        <Container>
          <MyHeader
            headername="7"
            name="My Subscription"
            rightIconPress={() => alert('ACCOUNT PRESS')}
            rightIconName="settings"
            onPressIcon={() => this.props.navigation.goBack()}
          />
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: '2%',
            }}>
            <Spinner type="Circle" color={color.header3_color} />
          </View>
        </Container>
      );
    } else {
      return (
        <Container>
          <MyHeader
            headername="3"
            name="My Subscription"
            rightIconPress={() =>
              this.props.navigation.navigate('Subscriptionscr', {
                from: 'mysubs',
              })
            }
            rightIconName="edit"
            onPressIcon={() => this.props.navigation.goBack()}
          />
          {'error' in this.state.data.data[0] ? (
            <View
              style={{alignSelf: 'center', justifyContent: 'center', flex: 1}}>
              <Text style={{fontSize: 20, color: '#AEAEAE'}}>
                You need to subscribe first!
              </Text>
            </View>
          ) : (
            <Content style={{}}>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: '35%',
                  alignSelf: 'center',
                }}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 22,
                    alignSelf: 'center',
                  }}>
                  My Plan
                </Text>
                <Icon
                  style={{}}
                  name="md-checkmark-done-sharp"
                  size={25}
                  color="darkgreen"
                />
              </View>
              <Card
                style={{
                  marginBottom: '6%',
                  alignItems: 'center',
                  width: '75%',
                  marginTop: '10%',
                  padding: '11%',
                  alignSelf: 'center',
                  backgroundColor: `${color.backgroundColor}`,
                }}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Text
                    style={{
                      fontSize: 25,
                      textTransform: 'uppercase',
                      borderBottomWidth: 3,
                      borderColor: 'black',
                    }}>
                    {this.state.data.data[0].subscription}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: '4%',
                  }}>
                  <Text style={{fontSize: 13, color: 'green'}}>Period:</Text>
                  <Text style={{fontSize: 20}}>
                    {this.state.data.data[0].period}
                  </Text>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Text style={{fontSize: 13, color: 'green'}}>
                    Expiry Date:
                  </Text>
                  <Text style={{fontSize: 20}}>
                    {this.state.data.data[0].expiry_date}
                  </Text>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Text style={{fontSize: 13, color: 'green'}}>Left Days:</Text>
                  <Text style={{fontSize: 20}}>
                    {this.state.data.data[0].days_left}
                  </Text>
                </View>
              </Card>
            </Content>
          )}
        </Container>
      );
    }
  }
}
