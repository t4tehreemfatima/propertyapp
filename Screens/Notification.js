import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
} from 'native-base';
import Spinner from 'react-native-spinkit';
import MyHeader from '../Constant/MyHeader';
import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import API from '../Constant/API';
import * as color from '../Constant/Colors';import {View, Text} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'

export default class Notification extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      data:'',
      loader:true,
    };
  }
  async get_notification(){
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response = await this.api.notification_select(parse.token);
    console.log(response, '--------12222--');
    this.setState({data:response,loader:false})
   response.data[0].forEach(element => {
     console.log(element,'-----elellele')
      this.update(element.id)
    });
  }
  async update(_id){
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response = await this.api.notification_update(_id,parse.token);
   
    // if (response.status==true) {
    //   alert("Updated")
    // } else {
    //   alert('Not Updated')
    // }

  }
componentDidMount(){
  this.get_notification()
}

openHomeList = () => {
  if (this.state.data.data==null) {
    return(
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Ionicons name="notifications-outline" size={80} color="grey" />
      <Text style={{color: 'grey'}}>No notifications available!</Text>
    </View>
    )
  } else {
     return this.state.data.data.map((element, i) => {
        console.log(
        element.id,
        'open home id*********************************8',
      );
      return (
        <List style={element.status==0?{backgroundColor:`${color.backgroundColor}`}:{backgroundColor:'white'}}>
        <ListItem avatar>
          <Left>
            <Thumbnail small source={require('../assets/bell.png')} />
          </Left>
          {/* {element} */}
          <Body>
            <Text>{element.description}</Text>
            {/* <Text note>{element.description}</Text> */}
          </Body>
          {/* <Right>
            <Text note>3:43 pm</Text>
          </Right> */}
        </ListItem>
      </List>
      );
    
  });
  }
 
};
  render() {
    return (
      <Container>
        <MyHeader
          headername="3"
          name="Notifications"
          // rightIconPress={() => alert('ACCOUNT PRESS')}
          // rightIconName="settings"
          onPressIcon={() => this.props.navigation.goBack()}
        />
        <Container style={{margin:'3%'}}>
           {this.state.loader?
                 <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
                   <Spinner type="Circle" color={color.header3_color} />
                 </View>
                 :
                 this.openHomeList()
                 }
        </Container>
       

      </Container>
    );
  }
}
