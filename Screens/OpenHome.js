import React from 'react';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import {Container, Content, Title, Tab, Tabs, Thumbnail} from 'native-base';
import {View, Text, ImageBackground} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import * as color from '../Constant/Colors';
export default class OpenHome extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      myData: '',
      isLoading: true,
      loadingFilter: true,
    };
  }
  //property data
  async tendent_select() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      console.log(parse.id, parse.token, 'token ten');
      const response = await this.api.property_tendent_select(
        parse.id,
        parse.token,
      );
      console.log(
        response,
        'Data Response-==++++++++++++OPENNN++++++++++++++++++++++++++++++++++++++++=',
      );
      this.setState({myData: response, isLoading: false});
    } catch (err) {
      console.log('Error: ', err);
    }
  }
  //MAP FUNTION FOR DATA VIEW
  openHomeList = () => {
    return this.state.myData.data.map((element, i) => {
      if (element.status == 'approved') {
        console.log(
          element.status,
          'open home id*********************************8',
        );
        return (
          <View
            key={i}
            style={{
              flexDirection: 'row',
              marginLeft: '3%',
              marginRight: '3%',
              alignItems: 'center',
              borderWidth: 0.1,
              elevation: 1,
              padding: '3%',
              marginTop: '5%',
            }}>
            {element.image.length > 0 ? (
              <Thumbnail
                square
                large
                source={{
                  uri: `${color.link}${element.image[0].path}`,
                }}
              />
            ) : (
              <Thumbnail square source={require('../assets/home.png')} />
            )}

            <View style={{marginLeft: '5%'}}>
              <Text
                style={{fontWeight: 'bold', fontSize: 16}}
                onPress={() =>
                  this.props.navigation.navigate('ViewCartDetails', {
                    key: element.property_id,
                    status: element.status,
                  })
                }>
                {element.name.length < 20
                  ? `${element.name}`
                  : `${element.name.substring(0, 20)}...`}
              </Text>
              <Text>Approved Date:{element.approved_date}</Text>
              <Text>Rent: {element.rent}</Text>
              <Text>City: {element.city}</Text>
            </View>
            <View style={{position: 'absolute', right: 4, top: 2}}>
              <Text style={{fontSize: 11}}>Status</Text>
              <Text
                style={{
                  color: 'green',
                  fontWeight: 'bold',
                  textTransform: 'capitalize',
                }}>
                {element.status}
              </Text>
            </View>
          </View>
        );
      }
    });
  };
  componentDidMount() {
    this.tendent_select();
  }
  render() {
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      return (
        <Container>
          <Content style={{flex: 1}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                alignSelf: 'center',
                marginTop: '5%',
                borderWidth: 1,
                borderColor: 'grey',
                paddingLeft: '10%',
                paddingRight: '10%',
              }}>
              <MaterialIcon name="search" size={30} color="grey" />
              <Text
                onPress={() => this.props.navigation.navigate('FindProperties')}
                style={{
                  fontWeight: 'bold',
                  fontSize: 17,
                  color: 'grey',
                  marginLeft: 5,
                }}>
                Find Properties
              </Text>
            </View>
            <View>{this.openHomeList()}</View>
          </Content>
        </Container>
      );
    }
  }
}
