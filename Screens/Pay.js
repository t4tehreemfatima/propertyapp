import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Toast,
} from 'native-base';
import MyHeader from '../Constant/MyHeader';
import Spinner from 'react-native-spinkit';

import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React from 'react';
import * as color from '../Constant/Colors';
import {View, Text} from 'react-native';
import {cos} from 'react-native-reanimated';

export default class Pay extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      data: '',
      isloading: true,
      date: new Date().toISOString().slice(0, 10),
    };
  }
  async rent_pay() {
    // this.setState({isloading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.rent_pay_add(parse.id, parse.token);
      console.log(response, '12222');

      if (response.data.error) {
        Toast.show({
          text: `${response.data.error}`,
          buttonText: 'Okay',
          duration: 4000,
        });
        this.props.navigation.navigate('TendentRent');
      } else {
        Toast.show({
          text: 'Sucessfully',
          buttonText: 'Okay',
          duration: 4000,
        });

        this.props.navigation.navigate('TendentRent');
      }
    } catch (error) {
      console.log(error, 'errr');
    }
  }
  async input_detail() {
    this.setState({isloading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      console.log(parse.id, 'USER ID');
      const response = await this.api.rent_pay_history(parse.id, parse.token);
      const response2 = await this.api.rent_pay_date(parse.token);
      console.log(response2, 'RESPONSE HISTORYYYYYYYYYY');

      this.setState({
        data: response2.data,
        isloading: false,
      });
    } catch (error) {
      console.log(error, 'errr');
    }
  }

  componentDidMount() {
    const {navigation} = this.props;
    this.focusListener = navigation.addListener('focus', () => {
      this.input_detail();
    });
  }

  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }
  render() {
    console.log(this.state.data, 'data h ya');
    // console.log(this.props.route.params.key, 'key');
    return (
      <Container>
        <MyHeader
          headername="3"
          name="Payment"
          onPressIcon={() => this.props.navigation.goBack()}
        />

        <Content>
          <Form style={{marginLeft: '3%', marginRight: '3%', marginTop: '5%'}}>
            <Item stackedLabel>
              <Label>Property Name</Label>
              <Input disabled value={this.state.data.property_name} />
            </Item>
            <Item stackedLabel last>
              <Label>Date</Label>
              <Input
                disabled
                value={
                  this.state.data.Payable_date == ''
                    ? this.state.date
                    : this.state.data.Payable_date
                }
              />
            </Item>
            <Item stackedLabel last>
              <Label>Amount</Label>
              <Input disabled value={String(this.state.data.amount)} />
            </Item>
            <Button
              onPress={() => this.rent_pay()}
              style={{
                height: 30,
                padding: '5%',
                width: 130,
                backgroundColor: `${color.header3_color}`,
                justifyContent: 'center',
                marginTop: '5%',
                alignSelf: 'center',
              }}>
              <Text style={{color: 'white'}}>Pay Now</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}
