import {Container, Content, Title, Picker, Thumbnail} from 'native-base';
import MyHeader from '../Constant/MyHeader';
import React from 'react';

import DocumentPicker from 'react-native-document-picker';
import ImgToBase64 from 'react-native-image-base64';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  ScrollView,
  StyleSheet,
  Alert
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import API from '../Constant/API';
import AntDesign from 'react-native-vector-icons/AntDesign';
import * as color from '../Constant/Colors';
import Spinner from 'react-native-spinkit';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/AntDesign';
import {
  TextInput,
  Button,
  Paragraph,
  Dialog,
  Portal,
  Modal,
  Chip,
} from 'react-native-paper';
import ValidationComponent from 'react-native-form-validator';
// import Picker from '@react-native-picker/picker';
export default class Payment extends ValidationComponent {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      req_card_no: '',
      bank_loader: false,
      req_cvv: '',
      loader: true,
      paymentmethod: 'stripe',
      expiry_month: '1',
      expiry_year: new Date().getFullYear(),
      image: '',
      selected_stripe: true,
      selected_bank_transfer: false,
      image_uri: '',
      image: '',
      visible: false,
      pakg_id: this.props.route.params.pkg_id,
      amount: this.props.route.params.pkg_amount,
      name: '',
      cvv: '',
      extra_loader: false,
      card_no: '',
      detail_of_bank: '',
      bank_data_transfer: {
        amount: '',
        subs_id: '',
        pay_method: '',
        image: [
          {
            name: '',
            image: '',
          },
        ],
        bank_info: '',
      },
    };
  }
  showDialog = () => this.setState({visible: true});
  //------------------------------------------------------------------APIS
  _onSubmit() {
    // Call ValidationComponent validate method
    this.validate({
      cvv: {minlength: 3, maxlength: 4},
      name: {required: true},
      req_cvv: {required: true},
      req_card_no: {required: true},
      card_no: {minlength: 16},
    });
    if (this.isFormValid()) {
      this.add_subs();
      console.log('ok');
    }
  }
  _onSubmit_bank() {
    // Call ValidationComponent validate method
    this.validate({
      image: {required: true},
    });
    if (this.isFormValid()) {
      this.add_subs_bank(
        this.props.route.params.pkg_amount,
        this.props.route.params.pkg_id,
        this.state.paymentmethod,
        this.state.image,
        this.state.image_uri,
        'bank information',
      );
      console.log('ok');
    }
  }
  handle_ok() {
    this.logout(), this.setState({visible: false});
  }
  async add_subs() {
    this.setState({extra_loader: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.subs_user_add(
        this.state.pakg_id,
        this.state.card_no,
        this.state.expiry_month,
        this.state.expiry_year,
        this.state.cvv,
        this.state.paymentmethod,
        this.state.amount,
        parse.token,
      );

      console.log(response, 'add spaaaa  ----------- subs');
      this.setState({extra_loader: false});

      if (response.status == true) {
        this.setState({extra_loader: false});
        
        this.props.navigation.goBack();
        this.props.navigation.navigate('MainStack');
        Alert.alert("Subscribed","Subscription Succesfull")
      } else {
        alert(response.data[0].error);
        this.setState({extra_loader: false});
      }
    } catch (error) {
      console.log(error, 'err');
      this.setState({extra_loader: false});
    }
  }

  async add_subs_trial() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.subs_user_trial_add(this.state.pakg_id);

      console.log(response, 'add spaaaa   subs');
      if (response.status == true) {
        this.showDialog();
      } else {
        alert(response.data[0].error);
      }
    } catch (error) {
      console.log(error, 'err');
    }
  }
  async add_subs_bank(_amount, _subs_id, _paymethod, _name, _image, _bankinfo) {
    this.setState({bank_loader: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);

      const response = await this.api.subs_bank_add(
        _amount,
        _subs_id,
        _paymethod,
        _name,
        _image,
        _bankinfo,
        parse.token,
      );
      console.log(response, 'add subs');

      if (response.status == true) {
        this.showDialog();

        this.setState({bank_loader: false});
      } else {
        console.log('ELSE');
        alert('Already Subscribed');
        this.setState({bank_loader: false});
      }
    } catch (error) {
      this.setState({bank_loader: false});
      console.log(error, 'err');
    }
  }
  add_bank_data(_amount, _subs_id) {
    this.setState({
      bank_data_transfer: {
        amount: _amount,
        subs_id: _subs_id,
        pay_method: '',
        image: [
          {
            name: '',
            image: '',
          },
        ],
        bank_info: '',
      },
    });
  }
  //-----------------------------------------------------------------------////////////
  update_chip(_name) {
    if (_name == 'stripe') {
      this.setState({
        paymentmethod: _name,
        selected_stripe: true,
        selected_bank_transfer: false,
      });
    } else {
      this.setState({
        paymentmethod: _name,
        selected_stripe: false,
        selected_bank_transfer: true,
      });
    }

    console.log(_name, '----077------');
  }
  getYear() {
    let years = [];
    for (
      let i = new Date().getFullYear();
      i <= new Date().getFullYear() + 4;
      i++
    ) {
      years.push(
        <Picker.Item key={i} value={String(i - 2000)} label={String(i)} />,
      );
      //console.log(i, '----> I CONSOLE');
    }
    return years;
  }

  // DOCUMENT PICKER
  documentPicker = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
      });

      console.log(
        res.uri,
        res.type, // mime type
        res.name,
        res.size,
      );

      let res_image = {
        uri: res.uri,
        name: res.name,
        type: res.type,
        size: res.size,
      };

      //this.setState({image: res_image.name, image_uri: res_image.uri});

      ImgToBase64.getBase64String(res_image.uri)
        .then((base64String) =>
          this.setState({image_uri: base64String, image: res_image.name}),
        )
        .catch((err) => console.log(err));
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };
  ///---------------------------------------------------------------
  _storeUserData = async (_name, _email, _id, _token) => {
    try {
      let obj = {
        name: _name,
        email: _email,
        id: _id,
        token: _token,
      };

      await AsyncStorage.setItem('user_data', JSON.stringify(obj));
    } catch (error) {
      alert(error);
    }
    console.log('User Data clear');
  };

  async logout() {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    console.log(parse.token);

    const response = await this.api.logout(parse.token);
    console.log(response, 'RESPONSE');

    if (response.data[0].Logout) {
      this._storeUserData('', '', '', '');
      this.props.navigation.reset({
        index: 0,
        routes: [{name: 'SignIn'}],
      });
      // this.props.navigation.navigate('SignIn');
    } else {
      alert('error');
    }
  }
  async get_bank_data() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.bank_data(parse.token);
      this.setState({detail_of_bank: response.data, loader: false});
      console.log(
        this.state.detail_of_bank,
        'ss--------------><-----------------',
      );
    } catch (error) {
      console.log(error);
    }
  }
  componentDidMount() {
    this.get_bank_data();
  }
  render() {
    console.log(this.state.image, 'BKKKKK DATAA');
    return (
      <Container>
        {this.props.route.params.from == 'verification' ? (
          <MyHeader
            headername="3"
            name="Payment"
            rightIconPress={() => this.logout()}
            rightIconName="logout"
            onPressIcon={() => this.props.navigation.goBack()}
          />
        ) : (
          <MyHeader
            headername="3"
            name="Payment"
            // rightIconPress={() => this.logout()}
            // rightIconName="logout"
            onPressIcon={() => this.props.navigation.goBack()}
          />
        )}

        <Content>
          <View
            style={{
              flexDirection: 'row',

              marginLeft: '5%',
              marginRight: '5%',
              marginTop: '3%',
            }}>
            <Chip
              icon={() =>
                this.state.selected_stripe ? (
                  <Ionicons
                    name="checkmark"
                    size={20}
                    color={color.theme_Color}
                  />
                ) : (
                  <View></View>
                )
              }
              onPress={() => this.update_chip('stripe')}>
              Stripe
            </Chip>
            <Chip
              style={{marginLeft: '5%'}}
              icon={() =>
                this.state.selected_bank_transfer ? (
                  <Ionicons
                    name="checkmark"
                    size={20}
                    color={color.theme_Color}
                  />
                ) : (
                  <View></View>
                )
              }
              onPress={() => this.update_chip('bank')}>
              Bank Transfer
            </Chip>
          </View>

          {/* //------------------------------------------------------------------------------------
           */}
          {this.state.paymentmethod == 'stripe' ? (
            <View
              style={{
                //alignItems: 'center',
                marginTop: '6%',
                //   backgroundColor: '#DCDCDC',
                //   backgroundColor: '#bbedf8',
                backgroundColor: '#dff7fc',
                marginRight: '5%',
                marginLeft: '5%',
                padding: '3%',
                borderRadius: 5,
                marginBottom: '6%',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  // borderTopWidth: 1,
                  // borderTopColor: '#c4c4c4',
                  marginLeft: '2%',
                  marginRight: '2%',
                  marginBottom: '2%',
                }}>
                <View style={{width: '73%', paddingTop: '3%'}}>
                  <TextInput
                    label="Owner"
                    value={this.state.name}
                    mode="outlined"
                    onChangeText={(text) => this.setState({name: text})}
                    //style={{width: '69%', paddingTop: '3%'}}
                    theme={{
                      colors: {
                        primary: `${color.theme_Color}`,
                        underlineColor: 'transparent',
                      },
                    }}
                  />
                  {this.isFieldInError('name') &&
                    this.getErrorsInField('name').map((errorMessage, i) => (
                      <Text
                        key={i}
                        style={{
                          color: 'red',
                          marginLeft: '5%',
                          fontSize: 12,
                        }}>{`Owner Name is required`}</Text>
                    ))}
                </View>
                <View
                  style={{
                    width: '31%',
                    marginLeft: '3%',
                    paddingTop: '3%',
                  }}>
                  <TextInput
                    label="CVV"
                    value={this.state.cvv}
                    mode="outlined"
                    onChangeText={(text) =>
                      this.setState({cvv: text, req_cvv: text})
                    }
                    keyboardType="number-pad"
                    //style={{width: '29%', marginLeft: '2%', paddingTop: '3%'}}
                    theme={{
                      colors: {
                        primary: `${color.theme_Color}`,
                        underlineColor: 'transparent',
                      },
                    }}
                  />
                  {this.isFieldInError('cvv') &&
                    this.getErrorsInField('cvv').map((errorMessage, i) => (
                      <Text
                        key={i}
                        style={{
                          color: 'red',
                          marginLeft: '5%',
                          fontSize: 12,
                        }}>{`CVV must be 3 or 4 digits`}</Text>
                    ))}
                  {this.isFieldInError('req_cvv') &&
                    this.getErrorsInField('req_cvv').map((errorMessage, i) => (
                      <Text
                        key={i}
                        style={{
                          color: 'red',
                          marginLeft: '5%',
                          fontSize: 12,
                        }}>{`CVV is required`}</Text>
                    ))}
                </View>
              </View>

              <TextInput
                label="Card Number"
                mode="outlined"
                value={this.state.card_no}
                onChangeText={(text) =>
                  this.setState({card_no: text, req_card_no: text})
                }
                keyboardType="number-pad"
                //   style={styles.textInputStyle}
                theme={{
                  colors: {
                    primary: `${color.theme_Color}`,
                    underlineColor: 'transparent',
                  },
                }}
              />
              {this.isFieldInError('card_no') &&
                this.getErrorsInField('card_no').map((errorMessage, i) => (
                  <Text
                    key={i}
                    style={{
                      color: 'red',
                      marginLeft: '5%',
                      fontSize: 12,
                    }}>{`Card number must be 16 digits long`}</Text>
                ))}
              {this.isFieldInError('req_card_no') &&
                this.getErrorsInField('req_card_no').map((errorMessage, i) => (
                  <Text
                    key={i}
                    style={{
                      color: 'red',
                      marginLeft: '5%',
                      fontSize: 12,
                    }}>{`Card number required`}</Text>
                ))}

              <View>
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: '#484848',
                    marginLeft: '2%',
                    marginTop: '2%',
                  }}>
                  Expiration Date
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    marginLeft: '3%',
                    marginTop: '-2%',
                  }}>
                  <View style={{width: '64%', flexDirection: 'row'}}>
                    <Picker
                      style={{
                        height: 50,
                        width: '75%',
                        color: `${color.header3_color}`,
                      }}
                      selectedValue={this.state.expiry_month}
                      onValueChange={(text) =>
                        this.setState({expiry_month: text})
                      }>
                      <Picker.Item label="January" value="1" />
                      <Picker.Item label="February" value="2" />
                      <Picker.Item label="March" value="3" />
                      <Picker.Item label="April" value="4" />
                      <Picker.Item label="May" value="5" />
                      <Picker.Item label="June" value="6" />
                      <Picker.Item label="July" value="7" />
                      <Picker.Item label="August" value="8" />
                      <Picker.Item label="September" value="9" />
                      <Picker.Item label="October" value="10" />
                      <Picker.Item label="November" value="11" />
                      <Picker.Item label="December" value="12" />
                    </Picker>

                    <Picker
                      style={{
                        height: 50,
                        width: '90%',
                        color: `${color.header3_color}`,
                      }}
                      selectedValue={this.state.expiry_year}
                      onValueChange={(text) =>
                        this.setState({expiry_year: text})
                      }>
                      {this.getYear()}
                    </Picker>
                  </View>

                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: '5%',
                      marginLeft: '2%',
                    }}>
                    <Image
                      source={require('../assets/visa.png')}
                      style={{height: 20, width: 30}}
                    />
                    <Image
                      source={require('../assets/master.png')}
                      style={{height: 20, width: 30, marginLeft: '3%'}}
                    />
                    <Image
                      source={require('../assets/amex.png')}
                      style={{height: 20, width: 40}}
                    />
                  </View>
                </View>
              </View>

              <View style={{alignItems: 'center', marginBottom: '2%'}}>
                <TouchableOpacity
                  style={{
                    backgroundColor: `${color.theme_Color}`,
                    padding: '3%',
                    borderRadius: 55,
                    width: '50%',
                  }}
                  // style={[styles.proceedButton, {width: '100%'}]}
                  onPress={() => this._onSubmit()}>
                  {this.state.stripe_loader ? (
                    <View
                      style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <ActivityIndicator size="small" color="#ffffff" />
                    </View>
                  ) : this.state.extra_loader ? (
                    <ActivityIndicator color="white" />
                  ) : (
                    <Text
                      style={{
                        color: '#ffffff',
                        textAlign: 'center',
                        fontWeight: 'bold',
                      }}>
                      PROCEED
                    </Text>
                  )}
                </TouchableOpacity>
              </View>

              <Portal>
                <Dialog
                  visible={this.state.dialogVisible}
                  onDismiss={this.hideDialog}>
                  <Dialog.Title>Alert</Dialog.Title>
                  <Dialog.Content>
                    <Paragraph>
                      Are you sure to confirm this transaction?
                    </Paragraph>
                  </Dialog.Content>
                  <Dialog.Actions>
                    <Button onPress={this.hideDialog}>
                      <Text style={{color: 'red'}}>Cancel</Text>
                    </Button>
                    <Button
                      onPress={() =>
                        this.StripePayment(
                          this.props.route.params.rec_currency_id,
                          this.props.route.params.amount_transfer,
                          this.props.route.params.sen_id,
                          this.props.route.params.sen_currency_id,
                          this.state.card_no,
                          this.state.cvv,
                          this.state.expiry_month,
                          this.state.expiry_year,
                        )
                      }>
                      <Text style={{color: 'red'}}>Ok</Text>
                    </Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>
            </View>
          ) : (
            <View
              style={{
                alignItems: 'center',
                marginTop: '6%',
                backgroundColor: '#dff7fc',
                marginRight: '5%',
                marginLeft: '5%',
                padding: '3%',
                borderRadius: 5,
                marginBottom: '6%',
              }}>
              <Text style={{fontSize: 13, textAlign: 'center', color: 'green'}}>
                Make the transfer to the following account and
              </Text>
              <Text style={{fontSize: 13, textAlign: 'center', color: 'green'}}>
                upload proof of payment
              </Text>
              <View style={{marginTop: '5%'}}>
                <Text style={{fontSize: 13, textAlign: 'center'}}>
                  Company Account Details:
                </Text>
                {this.state.loader ? (
                  <Spinner
                    type="ThreeBounce"
                    color={color.header3_color}
                    style={{alignSelf: 'center'}}
                  />
                ) : (
                  <View style={{alignItems: 'center'}}>
                    <Text style={{textAlign: 'center'}}>
                      Bank:
                      <Text style={{fontWeight: 'bold'}}>
                        {this.state.detail_of_bank[0].bank_name}
                      </Text>
                    </Text>
                    <Text style={{textAlign: 'center'}}>
                      Name:
                      <Text style={{fontWeight: 'bold'}}>
                        {this.state.detail_of_bank[0].account_name}
                      </Text>
                    </Text>
                    <Text>
                      Acc No:
                      <Text style={{fontWeight: 'bold'}}>
                        {this.state.detail_of_bank[0].account_number}
                      </Text>
                    </Text>
                    <Text>
                      BSB:
                      <Text style={{fontWeight: 'bold'}}>
                        {this.state.detail_of_bank[0].bsb}
                      </Text>
                    </Text>
                  </View>
                )}
                <View style={{marginTop: '20%', alignItems: 'center'}}>
                  <Text>Upload Payment Screenshot</Text>

                  {this.state.image ? (
                    <View>
                      <Text style={{color: `#4BB543`}}>uploaded</Text>
                    </View>
                  ) : (
                    <View></View>
                  )}

                  <TouchableOpacity onPress={() => this.documentPicker()}>
                    <View style={{flexDirection: 'row', marginTop: '5%'}}>
                      <Icon name="upload" size={20} color="green" />
                      <Text
                        style={{
                          color: `${color.header3_color}`,
                          marginLeft: '5%',
                          fontSize: 12,
                        }}>
                        SELECT DOCUMENT
                      </Text>
                    </View>
                  </TouchableOpacity>
                  {this.isFieldInError('image') &&
                    this.getErrorsInField('image').map((errorMessage, i) => (
                      <Text
                        key={i}
                        style={{
                          color: 'red',
                          marginLeft: '5%',
                        }}>{`select document`}</Text>
                    ))}

                  <TouchableOpacity
                    style={{
                      marginTop: '8%',
                      width: '100%',
                      alignSelf: 'center',
                      height: 40,
                      borderRadius: 5,
                      backgroundColor: `${color.theme_Color}`,
                    }}
                    mode="contained"
                    onPress={() => this._onSubmit_bank()}>
                    {this.state.bank_loader ? (
                      <View
                        style={{
                          flex: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}>
                        <ActivityIndicator size="small" color="#ffffff" />
                      </View>
                    ) : (
                      <View
                        style={{
                          flex: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            color: 'white',
                            textAlign: 'center',
                            fontWeight: 'bold',
                          }}>
                          PROCEED
                        </Text>
                        {/* <MaterialIcons
                          name="keyboard-arrow-right"
                          size={20}
                          color={'white'}
                        /> */}
                      </View>
                    )}
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
          <Portal>
            <Dialog
              visible={this.state.visible}
              // onDismiss={this.hideDialog_update}
            >
              <Dialog.Title>Alert</Dialog.Title>

              <Dialog.Content>
                <Text>
                  Your account request is pending.Once approved by admin you got
                  a confirm email and then you can login to your account
                </Text>
              </Dialog.Content>
              <Dialog.Actions>
                {/* // onPress={() =>
                      this.feature_add(
                        this.props.route.params.key,
                        this.state.feature_id,
                      )
                    } */}

                <Button
                  onPress={() => this.handle_ok()}
                  style={{
                    width: '30%',
                    marginRight: '3%',
                    height: '90%',
                    backgroundColor: `${color.theme_Color}`,
                  }}>
                  <Text style={{color: 'white', marginLeft: '25%'}}>Ok</Text>
                </Button>
                {/* <Button
                    style={{
                      width: '20%',
                      height: '90%',
                      backgroundColor: '#6fa8aa',
                    }}
                    onPress={this.hideDialog}>
                    <Text style={{color: 'white', marginLeft: '15%'}}>
                      Cancel
                    </Text>
                  </Button> */}
              </Dialog.Actions>
            </Dialog>
          </Portal>
          {/* <Text onPress={() => this.showDialog()}>jjhhjhjhjh</Text> */}
        </Content>
      </Container>
    );
  }
}
