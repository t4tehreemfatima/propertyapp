import {Container, Content, Text, View} from 'native-base';
import React from 'react';

export default class Privicy extends React.Component {
  render() {
    return (
      <Content style={{margin: '3%'}}>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>Privicy</Text>
        <Text style={{fontSize: 15}}>
          This Privacy Policy describes our rules and procedures for collecting,
          storing, and usage of your information. Users can gain access to the
          Tekumatics service via our company website and mobile app at
          www.tekumatics.com. This Privacy Policy controls your right to use the
          tekumatics system irrespective of how it is accessed. Using our
          services, you agree to the collection, refinement, storage,
          disclosure, transfer, and other uses described in this Privacy Policy.
          All the different forms of data, content, and information described
          are collectively referred to as "information."
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>
          The collection of information
        </Text>
        <Text style={{fontSize: 15}}>
          We may collect certain information about you depending on the service
          you are applying for when using tekumatics, such as:
        </Text>
        <Text style={{fontWeight: 'bold'}}> a. Personal Data</Text>
        <Text style={{fontSize: 15}}>
          When you create an account, we collect some personally identifiable
          information such as name, phone number, e-mail address, and home or
          postal business addresses.
        </Text>
        <Text style={{fontWeight: 'bold'}}>
          b. Cookies and Tracking Technologies
        </Text>
        <Text style={{fontSize: 15}}>
          We collect information (via our website or third party services) by
          employing cookies and logging. We use "cookies" to ensure you're able
          to log in to the site and stay logged in for the duration of your
          browsing session. A cookie is a small text file stored on your
          computer that records your session identification information and
          nothing more. No personal identifying information is stored in the
          cookies used by tekumatics. Just like other websites, we automatically
          record information about your activity when you access our website.
          This may include your Internet Protocol ("IP") address, the operating
          system and browser type, the information you search for on our
          website, locale preferences, identification numbers associated with
          your tekumatics account, date and time stamps, referring/exit pages,
          system configuration information, clickstream data, metadata
          concerning your Files, and other interactions with our services.
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>
          <Text style={{fontWeight: 'bold', fontSize: 20}}>
            The collection of information
          </Text>
        </Text>
        <Text style={{fontWeight: 'bold'}}>
          <Text style={{fontWeight: 'bold'}}>a. Personal Data</Text>
        </Text>
        <Text style={{fontSize: 15}}>
          While using our services, we may collect your personal information for
          identification and notification purposes ("Personally Identifiable
          Information (PII)"). Personally Identifiable Information is or may be
          used for:
        </Text>
        <View style={{marginLeft: '3%'}}>
          <Text style={{fontSize: 15}}>
            i. the provision and enhancement of our services
          </Text>
          <Text style={{fontSize: 15}}>
            ii. managing your use of our services
          </Text>
          <Text style={{fontSize: 15}}>
            iii. offering software updates and dispensing product announcements
          </Text>
          <Text style={{fontSize: 15}}>iv. processing transactions.</Text>
        </View>

        <Text style={{fontSize: 15}}>
          Excluding the description hereinabove, your information, be it public
          or private, will not be traded, exchanged, transferred, or presented
          to any other company for any reason whatsoever, without your
          permission, aside from the express purpose of delivering the
          tekumatics.{' '}
        </Text>
        <Text style={{fontWeight: 'bold'}}>b.Analytics</Text>
        <Text style={{fontSize: 15}}>
          Information acquired via logging and cookies can sometimes be
          associated with Personal Information. We use this information to
          monitor and examine the traffic of our services. We may link the
          referring page to clients' and visitors' information to promote our
          services and a timestamp and IP address to participants and client
          information to prepare an audit trail. The technical administration
          executes this to increase the functionality of our services, make our
          system user-friendly, and ensure users authentication.
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>
          Distribution and Disclosure of Information
        </Text>
        <Text style={{fontWeight: 'bold'}}>a. Your Use</Text>
        <Text style={{fontSize: 15}}>
          Information shared by clients will be revealed to registered tenants
          and landlords, therefore we advise that you consider the kind of
          information you share.
        </Text>
        <Text style={{fontWeight: 'bold'}}>b. Third-Parties</Text>
        <Text style={{fontSize: 15}}>
          Third-party companies or/and individuals are employed to analyze and
          improve our service features. These said parties might be granted
          access to your information to aid our business activities in
          performing tasks such as maintenance, web analytics, database
          management and other responsibilities akin to those in this Privacy
          Policy
        </Text>
        <Text style={{fontWeight: 'bold'}}>c. Business Transfers</Text>
        <Text style={{fontSize: 15}}>
          If involved in an acquisition, merger, or sale of all or a portion of
          our assets, tekumatics may transfer your information as part of that
          transaction, but we will notify you (via e-mail and/or a prominent
          notice on our website) of any change in administrative control or use
          of your Personal Identifiable Information or files, or if either
          become subject to a different Privacy Policy. We will also advise you
          of choices you may have regarding the information.
        </Text>
        <Text style={{fontWeight: 'bold'}}>
          d. Non-private or Non-Personal Information
        </Text>
        <Text style={{fontSize: 15}}>
          We may divulge your non-private, aggregated, or otherwise non-personal
          information, such as the usage statistics of our service.
        </Text>
        <Text style={{fontWeight: 'bold'}}>e. Third-Party Sites</Text>
        <Text style={{fontSize: 15}}>
          We may occasionally offer or include third-party services or products
          on our website. Different and independent privacy policies govern
          these third-party. Therefore, we do not assume liability or
          responsibility for the activities and contents of these linked
          third-party websites. We, however, seek to safeguard our site's
          integrity and welcome any suggestions or feedback regarding these
          websites.
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>Data Retention</Text>
        <Text style={{fontSize: 15}}>
          Tekumatics will continue to hold on to your information provided your
          account is active or needed to provide you services or according to
          our internal archiving policies. Although you may cease to use our
          services at any time, we may retain and make use of your information
          when necessary to comply with our legal obligations, resolve disputes,
          and enforce agreements.
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>Data Security</Text>
        <Text style={{fontSize: 15}}>
          We abide by generally accepted standards to safeguard the information
          presented to us during transmission and on our database. Tekumatics
          website is regularly inspected for security vulnerabilities and is
          also PCI (Payment Card Industry) compliant as determined by a PCI ASV
          (Payment Card Industry Approved Scanning Vendor). Nonetheless, no and
          contents of these linked third-party websites. We, however, seek to
          safeguard our site's integrity and welcome any suggestions or feedback
          regarding these websites. Data Retention Tekumatics will continue to
          hold on to your information provided your account is active or needed
          to provide you services or according to our internal archiving
          policies. Although you may cease to use our services at any time, we
          may retain and make use of your information when necessary to comply
          with our legal obligations, resolve disputes, and enforce agreements.
          Data Security We abide by generally accepted standards to safeguard
          the information presented to us during transmission and on our
          database. Tekumatics website is regularly inspected for security
          vulnerabilities and is also PCI (Payment Card Industry) compliant as
          determined by a PCI ASV (Payment Card Industry Approved Scanning
          Vendor). Nonetheless, no method of electronic transmission or storage
          is 100% secure. Hence, we cannot guarantee its absolute security.
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>
          PRIVACY POLICY EFFECTIVE DATE
        </Text>
        <Text style={{fontSize: 15}}>
          This Privacy Policy was last modified on May 24, 2021.
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>
          Changes made to our Privacy Policy
        </Text>
        <Text style={{fontSize: 15}}>
          This Privacy Policy is susceptible to change. If a change is made to
          this Privacy Policy which we believe significantly reduces your
          rights, we will provide you with notice (by e-mail). We may also
          notify you of changes via other forms of communications that we deem
          suitable based on the circumstances. By continuing to use our services
          after these changes become effective, you agree to be bound by the
          revised Privacy Policy.
        </Text>
      </Content>
    );
  }
}
