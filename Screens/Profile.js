import {
  Container,
  Content,
  Title,
  Body,
  Right,
  Accordion,
  List,
  ListItem,
  Left,
  Button,
} from 'native-base';
import {StyleSheet, TouchableOpacity} from 'react-native';
import API from '../Constant/API';
import MyHeader from '../Constant/MyHeader';
import React from 'react';
import Collapsible from 'react-native-collapsible';
import {View, Text, ImageBackground, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Spinner from 'react-native-spinkit';
import * as color from '../Constant/Colors';
import AIcon from 'react-native-vector-icons/AntDesign';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import {Avatar, Divider} from 'react-native-paper';
import FIcon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DocumentPicker from 'react-native-document-picker';
import ImgToBase64 from 'react-native-image-base64';
import {ActivityIndicator} from 'react-native';
export default class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      collapsedRentalDetails: true,
      bank_name: 'bank name',
      bank_acc_no: 7686868,
      collapsedWorkDetails: true,
      dataState: '',
      isLoading: true,
      imageLoader: false,
      linkImage: '',
      userImage: '',

      id: '',
      user_type: '',
      name: '',
      token: '',
      email: '',
    };
  }
  toggleExpandedRental = () => {
    //Toggling the state of single Collapsible
    this.setState({collapsedRentalDetails: !this.state.collapsedRentalDetails});
    console.log('TOGGLE');
  };
  toggleExpandedWork = () => {
    //Toggling the state of single Collapsible
    this.setState({collapsedWorkDetails: !this.state.collapsedWorkDetails});
    console.log('TOGGLE');
  };
  //===========
  async upload_img(_id, _img_name, _image) {
    console.log(_img_name, 'IMAGE NAME');

    this.setState({imageLoader: true});
    try {
      this.setState({linkImage: _image});
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.profile_image(
        _id,
        _img_name,
        _image,
        parse.token,
      );
      console.log(response, 'res imag');
      if (response.data[0].image == 'updated') {
        //  this._storeUserData(this.state.dataState.data[0].image);
        this.dataUser();
        this.setState({imageLoader: false});
      }
    } catch (error) {
      this.setState({imageLoader: false});
      console.log(error, 'erre');
    }
  }
  async imagePicker() {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
      });
      console.log(
        res.uri,
        res.type, // mime type
        res.name,
        res.size,
      );

      //-------image to base64
      ImgToBase64.getBase64String(res.uri)
        .then(
          (base64String) =>
            this.upload_img(this.state.id, res.name, base64String),
          // this.setState({linkImage: base64String}),
        )
        .catch((err) => console.log(err));
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }
  //---------------------
  async dataUser() {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    this.setState({id: parse.id});
    // console.log(parse.user_type,'h')
    this.setState({
      user_type: parse.user_type,
      token: parse.token,
      name: parse.name,
      email: parse.email,
    });
    const response = await this.api.userData(parse.id, parse.token);
    this.setState({
      dataState: response,
      userImage: response.data[0].image,
      isLoading: false,
    });

    this._storeUserData(response.data[0].image);

    console.log(response, '-----resdspsba98asausi');
  }
  // componentDidMount() {
  //   this.dataUser();
  // }
  componentDidMount() {
    const {navigation} = this.props;

    this.focusListener = navigation.addListener('focus', () => {
      this.dataUser();
    });
    this.dataUser();
  }

  componentWillUnmount() {
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }

  _storeUserData = async (_image) => {
    try {
      let obj = {
        name: this.state.name,
        email: this.state.email,
        id: this.state.id,
        user_type: this.state.user_type,
        token: this.state.token,
        draw_img: _image,
      };

      await AsyncStorage.setItem('user_data', JSON.stringify(obj));
    } catch (error) {
      alert(error);
    }
    console.log(_image, '---> User Data Stored');
  };
  render() {
    if (this.state.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: '15%',
          }}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      //[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]
      if (this.state.user_type == '1') {
        return (
          <Container style={{}}>
            <MyHeader
              headername="3"
              name="Profile"
              rightIconPress={() =>
                this.props.navigation.navigate('UpdateProfile', {
                  key: this.state.dataState,
                })
              }
              rightIconName="edit"
              onPressIcon={() => this.props.navigation.goBack()}
            />

            <Content style={{}}>
              <View>
                <Image
                  style={{width: '100%', height: '40%'}}
                  source={{
                    uri:
                      'https://res.infoq.com/articles/angular-components-tips/en/headerimage/GettyImages-493686402.jpg',
                  }}
                />
              </View>

              <View
                style={{
                  marginTop: '-50%',
                  alignSelf: 'center',
                  borderWidth: 1,
                  borderRadius: 60,
                  marginBottom: '5%',

                  borderColor: 'grey',
                }}>
                {this.state.userImage == null ? (
                  <Avatar.Image
                    size={104}
                    style={{backgroundColor: 'white'}}
                    source={require('../assets/user.png')}
                  />
                ) : this.state.imageLoader ? (
                  <ActivityIndicator color="black" size={30} />
                ) : (
                  <Avatar.Image
                    size={104}
                    source={{
                      uri: `${color.link}${this.state.dataState.data[0].image}`,
                    }}
                  />
                )}
              </View>
              <View
                style={{
                  alignSelf: 'center',
                }}>
                <Icon
                  style={{
                    position: 'absolute',
                    top: -50,
                    left: 19,
                    elevation: 8,
                  }}
                  name="camera"
                  color={color.newbtn}
                  size={34}
                  onPress={() => this.imagePicker()}
                />
              </View>
              <View style={{marginLeft: '5%', marginTop: '5%'}}>
                <Text
                  style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    color: `${color.profileicon_color}`,
                  }}>
                  Personal Details
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: '5%',
                  }}>
                  <FIcon
                    name="user"
                    size={20}
                    color={color.profileicon_color}
                  />
                  <View style={{marginLeft: '6%'}}>
                    <Text style={{fontWeight: 'bold'}}>Name</Text>
                    <Text> {this.state.dataState.data[0].name}</Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: '5%',
                  }}>
                  <FIcon
                    name="envelope"
                    size={20}
                    color={color.profileicon_color}
                  />
                  <View style={{marginLeft: '5%'}}>
                    <Text style={{fontWeight: 'bold'}}>Email</Text>
                    <Text>{this.state.dataState.data[0].email}</Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: '5%',
                  }}>
                  <FIcon
                    name="phone"
                    size={20}
                    color={color.profileicon_color}
                  />
                  <View style={{marginLeft: '5%'}}>
                    <Text style={{fontWeight: 'bold'}}>Phone Number</Text>
                    <Text>{this.state.dataState.data[0].mobile}</Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: '5%',
                  }}>
                  <Icon
                    name="location"
                    size={20}
                    color={color.profileicon_color}
                  />
                  <View style={{marginLeft: '5%'}}>
                    <Text style={{fontWeight: 'bold'}}>Address</Text>

                    <Text>{this.state.dataState.data[0].address}</Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: '5%',
                  }}>
                  <Icon
                    name="md-alarm-sharp"
                    size={20}
                    color={color.profileicon_color}
                  />
                  <View style={{marginLeft: '5%'}}>
                    <Text style={{fontWeight: 'bold'}}>Citizenship Type</Text>

                    <Text>{this.state.dataState.data[0].user_type_name}</Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: '5%',
                    marginBottom: '100%',
                  }}>
                  <Icon
                    name="md-wallet-sharp"
                    size={20}
                    color={color.profileicon_color}
                  />
                  <View style={{marginLeft: '5%'}}>
                    <Text style={{fontWeight: 'bold'}}>Subscription</Text>
                  </View>
                  <Button
                    onPress={() => this.props.navigation.navigate('MySubs')}
                    style={{
                      backgroundColor: `${color.theme_Color}`,
                      width: 50,
                      height: 30,
                      marginLeft: '40%',
                      justifyContent: 'center',
                    }}>
                    <Text>View</Text>
                  </Button>
                </View>
              </View>
              <View></View>
            </Content>
          </Container>
        );
      } else {
        //o999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
        return (
          <Container style={{}}>
            <MyHeader
              headername="3"
              name="Profile"
              rightIconPress={() =>
                this.props.navigation.navigate('UpdateProfile', {
                  key: this.state.dataState,
                })
              }
              rightIconName="edit"
              onPressIcon={() => this.props.navigation.goBack()}
            />

            <Content>
              {/* <View>
          <Image
            style={{width: '100%', height: '50%'}}
            source={{
              uri:
                'https://res.infoq.com/articles/angular-components-tips/en/headerimage/GettyImages-493686402.jpg',
            }}
          />
        </View> */}

              <View
                style={{
                  marginTop: '10%',
                  alignSelf: 'center',
                  borderWidth: 3,
                  borderRadius: 60,
                  marginBottom: '5%',

                  borderColor: `${color.theme_Color}`,
                }}>
                {this.state.userImage == null ? (
                  <Avatar.Image
                    size={104}
                    style={{backgroundColor: 'white'}}
                    source={require('../assets/user.png')}
                  />
                ) : this.state.imageLoader ? (
                  <ActivityIndicator color="black" size={30} />
                ) : (
                  <Avatar.Image
                    size={104}
                    source={{
                      uri: `${color.link}${this.state.dataState.data[0].image}`,
                    }}
                  />
                )}

                {/* {this.state.linkImage == "" ?
            <Avatar.Image
            size={104}
            style={{backgroundColor: 'white'}}
            source={require('../assets/user.png')}
          /> : this.state.imageLoader ?
          <Avatar.Image
              size={104}
              style={{backgroundColor: 'white'}}
              source={{
                uri: `https://ak.picdn.net/shutterstock/videos/23800711/thumb/1.jpg`,
              }}
            /> : this.state.linkImage != "" ?
            <Avatar.Image
              size={104}
              source={{
                uri: `data:image/gif;base64,${this.state.linkImage}`,
                // uri:`http://property.gtb2bexim.com/public/${this.state.dataState.data[0].image}`
              }}
            /> :
          <Avatar.Image
              size={104}
              source={{
                uri:
                  `http://property.gtb2bexim.com/public/${this.state.dataState.data[0].image}`,
              }}
            /> 
           } */}

                {/* {this.state.linkImage == '' ? 
          
          (
            <Avatar.Image
              size={104}
              source={{
                uri:
                  `http://property.gtb2bexim.com/public/${this.state.dataState.data[0].image}`,
              }}
            />
          ) : this.state.imageLoader ? (
            <Avatar.Image
              size={104}
              style={{backgroundColor: 'white'}}
              source={{
                uri: `https://ak.picdn.net/shutterstock/videos/23800711/thumb/1.jpg`,
              }}
            />
          ) : (
            <Avatar.Image
              size={104}
              source={{
                uri: `data:image/gif;base64,${this.state.linkImage}`,
                // uri:`http://property.gtb2bexim.com/public/${this.state.dataState.data[0].image}`
              }}
            />
          )} */}
              </View>
              {/* <MIcon
          onPress={() =>
            this.props.navigation.navigate('UpdateProfile', {
              key: this.state.dataState,
            })
          }
          name="edit"
          size={40}
          color="green"
          style={{
            position: 'absolute',
            alignSelf: 'center',
            top: '20%',
            right: '5%',
          }}
        /> */}
              <View
                style={{
                  alignSelf: 'center',
                }}>
                <Icon
                  style={{
                    position: 'absolute',
                    top: -50,
                    left: 19,
                    elevation: 8,
                  }}
                  name="camera"
                  color={color.newbtn}
                  size={34}
                  onPress={() => this.imagePicker()}
                />
              </View>
              <View style={{marginLeft: '5%', marginTop: '5%'}}>
                <Text
                  style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    color: `${color.theme_Color}`,
                  }}>
                  Personal Details
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: '5%',
                  }}>
                  <FIcon name="user" size={20} color={color.theme_Color} />
                  <View style={{marginLeft: '6%'}}>
                    <Text style={{fontWeight: 'bold'}}>Name</Text>
                    <Text> {this.state.dataState.data[0].name}</Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: '5%',
                  }}>
                  <FIcon name="envelope" size={20} color={color.theme_Color} />
                  <View style={{marginLeft: '5%'}}>
                    <Text style={{fontWeight: 'bold'}}>Email</Text>
                    <Text>{this.state.dataState.data[0].email}</Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: '5%',
                  }}>
                  <FIcon name="phone" size={20} color={color.theme_Color} />
                  <View style={{marginLeft: '5%'}}>
                    <Text style={{fontWeight: 'bold'}}>Phone Number</Text>
                    <Text>{this.state.dataState.data[0].mobile}</Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: '5%',
                  }}>
                  <Icon name="location" size={20} color={color.theme_Color} />
                  <View style={{marginLeft: '5%'}}>
                    <Text style={{fontWeight: 'bold'}}>Address</Text>

                    <Text>{this.state.dataState.data[0].address}</Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: '5%',
                  }}>
                  <Icon
                    name="md-alarm-sharp"
                    size={20}
                    color={color.theme_Color}
                  />
                  <View style={{marginLeft: '5%'}}>
                    <Text style={{fontWeight: 'bold'}}>Citizenship Type</Text>

                    <Text>{this.state.dataState.data[0].user_type_name}</Text>
                  </View>
                </View>
              </View>
              <View></View>
            </Content>
          </Container>
        );
      }
    }
  }
}
const styles = StyleSheet.create({
  stretch: {
    width: '100%',
    height: '44%',
  },
});
