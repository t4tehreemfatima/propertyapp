import MyHeader from '../Constant/MyHeader';
import React from 'react';
import {
  Container,
  Content,
  Title,
  Tab,
  Tabs,
  Thumbnail,
  Right,
} from 'native-base';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import * as color from '../Constant/Colors';
import {View, Text} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import {Divider} from 'react-native-elements';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
export default class RentTedent extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      data: '',
      tendent_id: '',
      isloading: true,
    };
  }

  async all_tedent_rent_select() {
    this.setState({isloading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.rent_select(
        this.props.kuchb,
        parse.token,
      );
      console.log(
        response,
        parse.token,
        '++++++______________-  all tedent list',
      );

      this.setState({
        data: response,
        isloading: false,
      });
    } catch (error) {
      console.log(error, 'erruuur');
    }
  }
  async componentDidMount() {
    const {navigation} = this.props;
    this.focusListener = navigation.addListener('focus', () => {
      console.log('cosolllllll');
      this.all_tedent_rent_select();
    });
    const tend_id = await AsyncStorage.getItem('tendent_id');
    const parse = JSON.parse(tend_id);
    this.setState({tendent_id: parse.key});
    this.all_tedent_rent_select();
  }
  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }
  map_tendent_list = () => {
    // if (this.state.data.data[0] == null) {
    //   return (
    //     <View>
    //       <Text
    //         style={{
    //           fontSize: 33,
    //           color: '#DCDCDC',
    //           alignSelf: 'center',
    //           marginTop: '50%',
    //         }}>
    //         No Data
    //       </Text>
    //     </View>
    //   );
    // } else {
    return this.state.data.data.map((element, i) => {
      console.log(element, 'all list elelmet--------------<<<<');
      return (
        <View key={i}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: '3%',
            }}>
            <View
              style={{
                borderLeftWidth: 1,
                padding: '1%',
                justifyContent: 'center',
                width: '44%',
              }}>
              <Text>{element.tendent_name}</Text>
            </View>
            <View
              style={{
                borderLeftWidth: 1,
                padding: '1%',
                width: '26%',
                justifyContent: 'center',
              }}>
              {element.date == '' ? (
                <Text>N/A</Text>
              ) : (
                <Text>{element.date}</Text>
              )}
            </View>
            <View style={{borderLeftWidth: 1, padding: '1%', width: '10%'}}>
              {element.rent == 'paid' ? (
                <AntDesign name="check" size={30} color="green" />
              ) : (
                <Entypo name="cross" color="red" size={30} />
              )}

              {/* 
              <Entypo name="cross" size={30} /> */}
            </View>
            <View
              style={{
                borderLeftWidth: 1,
                padding: '1%',
                width: '10%',
                justifyContent: 'center',
                marginRight: '1%',
              }}>
              <FontAwesome5
                onPress={() =>
                  this.props.navigation.navigate('History', {
                    key: element.tendent_id,
                    key2: this.props.kuchb,
                  })
                }
                style={{left: '13%'}}
                name="history"
                size={20}
                color="green"
              />
            </View>
          </View>
          <Divider style={{marginTop: '5%'}} />
        </View>
      );
    });
    // }
  };
  render() {
    console.log(this.props.kuchb, 'property id pass by params ID');
    return (
      <Container>
        {this.state.isloading ? (
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <Spinner type="Circle" color={color.header3_color} />
          </View>
        ) : (
          <Content
            style={{marginLeft: '5%', marginRight: '5%', marginTop: '3%'}}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <View>
                <Text
                  style={{
                    fontSize: 17,
                    fontWeight: 'bold',
                    color: `${color.text}`,
                  }}>
                  Tenant Name
                </Text>
              </View>
              <View style={{marginLeft: '-4%'}}>
                <Text
                  style={{
                    fontSize: 17,
                    fontWeight: 'bold',
                    color: `${color.text}`,
                  }}>
                  Date
                </Text>
              </View>
              <View style={{marginRight: '13%'}}>
                <Text
                  style={{
                    fontSize: 17,
                    fontWeight: 'bold',
                    color: `${color.text}`,
                  }}>
                  Paid
                </Text>
              </View>
            </View>
            {/* //---------------------------------------------------------------------------------------MAP */}
            <View>
              {this.state.data.data.length == 0 ? (
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: '10%',
                  }}>
                  <Text style={{marginTop: '2%', color: 'grey'}}>
                    No data found!
                  </Text>
                </View>
              ) : (
                this.map_tendent_list()
              )}
            </View>
          </Content>
        )}
      </Container>
    );
  }
}
