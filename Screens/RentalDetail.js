import {Container, Content, Title} from 'native-base';
import MyHeader from '../Constant/MyHeader';
import {Picker} from '@react-native-picker/picker';
import React from 'react';

import {
  TextInput,
  Button,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import {View, Text} from 'react-native';
export default class RentalDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: 'Landlord',
    };
  }

  render() {
    return (
      <Container>
        <MyHeader
          headername="3"
          name="Rental Details"
          rightName="Save"
          onPressIcon={() => this.props.navigation.goBack()}
          rightSavePress={() => alert('Save')}
        />

        <Content>
          <View style={{marginLeft: '5%', marginRight: '5%', marginTop: '6%'}}>
            <Text
              style={{fontSize: 16, fontWeight: 'bold', color: 'cadetblue'}}>
              Rental Detail
            </Text>

            <Text style={{fontWeight: 'bold', marginTop: '5%'}}>
              Current living State
            </Text>
            <View
              style={{
                borderBottomWidth: 1,
                borderColor: 'cadetblue',
                marginTop: '-1%',
              }}>
              <Picker
                mode="dropdown"
                selectedValue={this.state.user}
                style={{
                  height: 50,
                  width: '102%',
                  color: 'grey',
                  marginBottom: '-2.5%',
                  right: '1.5%',
                }}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({user: itemValue})
                }>
                <Picker.Item label="Landlord" value="Landlord" />
                <Picker.Item label="Tenants" value="Tenants" />
              </Picker>
            </View>
            {/*  */}

            <Text style={{fontWeight: 'bold', marginTop: '5%'}}>
              Dependents
            </Text>
            <View
              style={{
                borderBottomWidth: 1,
                borderColor: 'cadetblue',
                marginTop: '-1%',
              }}>
              <Picker
                mode="dropdown"
                selectedValue={this.state.user}
                style={{
                  height: 50,
                  width: '102%',
                  color: 'grey',
                  marginBottom: '-2.5%',
                  right: '1.5%',
                }}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({user: itemValue})
                }>
                <Picker.Item label="No Dependents" value="No Dependents" />
                <Picker.Item label="Tenants" value="Tenants" />
              </Picker>
            </View>
            {/* 3rd */}
            <Text style={{fontWeight: 'bold', marginTop: '5%'}}>Pets</Text>
            <View
              style={{
                borderBottomWidth: 1,
                borderColor: 'cadetblue',
                marginTop: '-1%',
              }}>
              <Picker
                mode="dropdown"
                selectedValue={this.state.user}
                style={{
                  height: 50,
                  width: '102%',
                  color: 'grey',
                  marginBottom: '-2.5%',
                  right: '1.5%',
                }}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({user: itemValue})
                }>
                <Picker.Item label="no Pets" value="No Pets" />
                <Picker.Item label="Tenants" value="Tenants" />
              </Picker>
            </View>
            {/* 4th */}
            <Text style={{fontWeight: 'bold', marginTop: '5%'}}>Vehicles</Text>
            <View
              style={{
                borderBottomWidth: 1,
                borderColor: 'cadetblue',
                marginTop: '-1%',
              }}>
              <Picker
                mode="dropdown"
                selectedValue={this.state.user}
                style={{
                  height: 50,
                  width: '102%',
                  color: 'grey',
                  marginBottom: '-2.5%',
                  right: '1.5%',
                }}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({user: itemValue})
                }>
                <Picker.Item label="No Vehicles" value="no Vehicles" />
                <Picker.Item label="Tenants" value="Tenants" />
              </Picker>
            </View>
            <Text style={{fontWeight: 'bold', fontSize: 15, marginTop: '5%'}}>
              Current Address
            </Text>
            <TextInput
              style={{
                marginTop: '-3%',
                borderColor: 'cadetblue',
                borderBottomWidth: 1,
              }}
              keyboardType="email-address"
              placeholder="Enter Address"
            />
          </View>
        </Content>
      </Container>
    );
  }
}
