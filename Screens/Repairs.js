import React from 'react';
import {View, Text} from 'react-native';
import {Button, Container, Content} from 'native-base';
import MyHeader from '../Constant/MyHeader';
import {Picker} from '@react-native-picker/picker';
import * as color from '../Constant/Colors';

export default class Repairs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      property: '',
      duration: '',
      inputVal: '',
    };
  }
  render() {
    return (
      <Container>
        <MyHeader
          headername="1"
          name="Repairs"
          rightIconPress={() => alert('ACCOUNT PRESS')}
          rightIconName="settings"
          leftIconPress={() => this.props.navigation.openDrawer()}
        />
        <Content>
          <View
            style={{
              backgroundColor: `${color.searchBox_color}`,
              padding: '1%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginRight: '2%',
              marginLeft: '2%',
              marginTop: '2%',
              borderRadius: 10,
            }}>
            <View style={{width: '45%', marginLeft: '1%', marginBottom: '4%'}}>
              <Text style={{color: 'white'}}>Choose your Property</Text>
              <View style={{backgroundColor: 'white'}}>
                <Picker
                  mode="dropdown"
                  selectedValue={this.state.property}
                  style={{
                    height: 50,
                    width: '100%',
                    color: 'cadetblue',
                    marginBottom: '-2.5%',
                    right: '1.5%',
                  }}
                  itemStyle={{backgroundColor: 'red', borderBottomWidth: 55}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({property: itemValue})
                  }>
                  <Picker.Item label="name1" value="name1" />
                  <Picker.Item label="name2" value="name2" />
                </Picker>
              </View>
            </View>
            <View style={{width: '45%', marginRight: '1%'}}>
              <Text style={{color: 'white'}}>Filter by Duration</Text>
              <View style={{backgroundColor: 'lavender'}}>
                <Picker
                  mode="dropdown"
                  selectedValue={this.state.duration}
                  style={{
                    height: 50,
                    width: '100%',
                    color: 'cadetblue',
                    marginBottom: '-2.5%',
                    right: '1.5%',
                  }}
                  itemStyle={{backgroundColor: 'red', borderBottomWidth: 55}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({duration: itemValue})
                  }>
                  <Picker.Item label="name1" value="name1" />
                  <Picker.Item label="name2" value="name2" />
                </Picker>
              </View>
            </View>
          </View>
          <View style={{marginLeft: '5%', marginTop: '5%'}}>
            <Text style={{fontWeight: 'bold', fontSize: 21}}>Lodge Repair</Text>
            <View style={{marginTop: '5%'}}>
              <Text
                style={{color: 'green', fontWeight: 'bold', marginTop: '3%'}}>
                1 Step
              </Text>
              <Text>Recive/Create a repair job</Text>
              <Text
                style={{color: 'green', fontWeight: 'bold', marginTop: '3%'}}>
                2 Step
              </Text>
              <Text>Choose varified tredesmen,select mutiple option for</Text>
              <Text> the quote that,s right for you</Text>
              <Text
                style={{color: 'green', fontWeight: 'bold', marginTop: '3%'}}>
                3 Step
              </Text>
              <Text>Instarent chat will connect you to the tradesmen,to</Text>
              <Text> review quotes and assign the job</Text>
            </View>
            <Button
              style={{
                backgroundColor: `${color.btn_Color}`,
                width: '80%',
                borderRadius: 20,
                justifyContent: 'center',
                alignSelf: 'center',
                marginTop: '5%',
              }}>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Add Repair</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
