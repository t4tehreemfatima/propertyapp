import MyHeader from '../Constant/MyHeader';
import React from 'react';
import * as color from '../Constant/Colors';
import {View, Text} from 'react-native';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import {Dialog, Portal, Button} from 'react-native-paper';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {
  Container,
  Content,
  Title,
  Tab,
  Tabs,
  Thumbnail,
  Card,
} from 'native-base';
export default class Request extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      myData: '',
      isLoading: true,
      visible: false,
      delete_id: '',
    };
  }
  showDialog = (_id) => {
    console.log(_id, 'delel id h ya');
    this.setState({delete_id: _id, visible: true});
  };
  async approve(_id) {
    this.setState({isLoading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      console.log(parse.token, parse.id, 'id---------------token ten');
      const response = await this.api.approve_request(_id, parse.token);

      console.log(
        response,
        _id,
        '-------------APPROVED---------respo-------------------------',
      );
      if (response.data.error) {
        alert(response.data.error);
        this.setState({isLoading: false});
      } else {
        console.log('Approved');
        this.setState({isLoading: false});
        this.request_properties();
      }
      // try {
      // const response = await this.api.approve_request(_id, parse.token);
      // } catch (error) {
      // console.log('Approved');
      //   this.setState({isLoading: false});
      //   this.request_properties();
      // }
    } catch (err) {
      console.log('Error: ', err);
    }
  }
  async decline(_id) {
    this.setState({isLoading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      console.log(parse.token, 'token ten');
      const response = await this.api.approve_request_delete(_id, parse.token);
      console.log(
        response,
        _id,
        '----------------------respo-------------------------',
      );
      this.setState({isLoading: false});
      this.request_properties();
      this.setState({visible: false});
    } catch (err) {
      console.log('Error: ', err);
    }
  }
  async request_properties() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      console.log(parse.token, 'token ten');
      const response = await this.api.response_list(parse.id, parse.token);
      console.log(response, 'List');
      this.setState({myData: response, isLoading: false});
      // console.log(this.state.myData.data, 'state my data');
    } catch (err) {
      console.log('Error: ', err);
    }
  }
  componentDidMount() {
    this.request_properties();
  }

  //----------------------------------------------------------------------------------------list
  request_property_list = () => {
    console.log('dsdsdsd', this.state.myData.data.length);

    if (this.state.myData.data[0][0] == null) {
      return (
        <View>
          <Text
            style={{
              fontSize: 30,
              marginTop: '65%',
              color: '#e0e0e0',
              alignSelf: 'center',
            }}>
            No Request
          </Text>
        </View>
      );
    } else {
      return this.state.myData.data[0].map((element, i) => {
        console.log(element, '----------------------->elelmet');
        return (
          <Card
            key={i}
            style={{
              flexDirection: 'row',
              marginLeft: '3%',
              marginRight: '3%',
              alignItems: 'center',
              // borderWidth: 0.1,
              elevation: 1,
              padding: '3%',
              marginTop: '3%',
              borderRadius: 1,
              justifyContent: 'space-between',
              marginBottom: '3%',
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View>
                {element.image == null ? (
                  <Thumbnail source={require('../assets/gender.png')} />
                ) : (
                  <Thumbnail source={{uri: `${color.link}${element.image}`}} />
                )}
              </View>
              <View style={{marginLeft: '3%'}}>
                <Text
                  style={{fontWeight: 'bold', fontSize: 14, color: 'grey'}}
                  onPress={() =>
                    this.props.navigation.navigate('ViewCartDetails', {
                      key: element.property_id,
                      status: '',
                    })
                  }>
                  {element.property_name.length < 20
                    ? `${element.property_name}`
                    : `${element.property_name.substring(0, 20)}...`}
                </Text>
                <Text
                  style={{fontWeight: 'bold', fontSize: 14, color: 'green'}}
                  onPress={() =>
                    this.props.navigation.navigate('ViewCartDetails', {
                      key: element.property_id,
                      status: '',
                    })
                  }>
                  {element.name.length < 20
                    ? `${element.name}`
                    : `${element.name.substring(0, 20)}...`}
                </Text>
                <Text
                  style={{fontWeight: 'bold', fontSize: 14}}
                  onPress={() =>
                    this.props.navigation.navigate('ViewCartDetails', {
                      key: element.property_id,
                      status: '',
                    })
                  }>
                  {element.email.length < 20
                    ? `${element.email}`
                    : `${element.email.substring(0, 20)}...`}
                </Text>
                <Text
                  style={{fontWeight: 'bold', fontSize: 14}}
                  onPress={() =>
                    this.props.navigation.navigate('ViewCartDetails', {
                      key: element.property_id,
                      status: '',
                    })
                  }>
                  <MaterialCommunityIcons name="cellphone-iphone" size={15} />{' '}
                  {element.mobile.length < 25
                    ? `${element.mobile}`
                    : `${element.mobile.substring(0, 20)}...`}
                  {/* {element.email} */}
                </Text>
              </View>
            </View>
            <View>
              <Ionicons
                onPress={() => this.approve(element.id)}
                name="checkmark-circle"
                size={30}
                color={color.theme_Color}
              />

              <Entypo
                onPress={() => this.showDialog(element.id)}
                name="circle-with-cross"
                size={30}
                color={color.header3_color}
                style={{marginTop: 10}}
              />
              {/* <Button
              onPress={() => this.decline(element.id)}
              style={{
                backgroundColor: `${color.header3_color}`,
                width: 70,
                height: 30,
                marginTop: '5%',
              }}>
              <Text style={{color: 'white', marginLeft: '13%'}}>Decline</Text>
            </Button> */}
            </View>
          </Card>
        );
      });
    }
  };

  render() {
    if (this.state.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: '3%',
          }}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      console.log('dsdsd---------sd', this.state.myData.data.length);

      return (
        <Container>
          {this.state.myData.data[0].length == 0 ? (
            <View
              style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
              <FontAwesome5 name="user-friends" size={80} color="grey" />
              <Text style={{marginTop: '2%', color: 'grey'}}>
                No requests found!
              </Text>
            </View>
          ) : (
            <Content>
              <View>{this.request_property_list()}</View>
            </Content>
          )}
          <Portal>
            <Dialog
              visible={this.state.visible}
              // onDismiss={this.hideDialog_update}
            >
              <Dialog.Title>Alert</Dialog.Title>

              <Dialog.Content>
                <Text>Do you want to delete this tendent?</Text>
              </Dialog.Content>
              <Dialog.Actions>
                <Button
                  onPress={() => this.decline(this.state.delete_id)}
                  style={{
                    width: '30%',
                    marginRight: '3%',
                    height: '100%',
                    backgroundColor: `${color.theme_Color}`,
                  }}>
                  <Text style={{color: 'white', marginLeft: '25%'}}>Yes</Text>
                </Button>
                <Button
                  onPress={() => this.setState({visible: false})}
                  style={{
                    width: '30%',
                    marginRight: '3%',
                    height: '100%',
                    backgroundColor: `${color.newbtn}`,
                  }}>
                  <Text style={{color: 'white', marginLeft: '25%'}}>No</Text>
                </Button>
              </Dialog.Actions>
            </Dialog>
          </Portal>
        </Container>
      );
    }
  }
}
