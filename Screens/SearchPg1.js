import {Container, Content, Title} from 'native-base';
import React from 'react';
import * as color from '../Constant/Colors';
import {
  View,
  Text,
  ActivityIndicator,
  TouchableOpacity,
  Image,
} from 'react-native';
import {TextInput} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Slider from 'rn-range-slider';
import Label from './slider-component/Label';
import {Picker} from '@react-native-picker/picker';
import Notch from './slider-component/Notch';
import Rail from './slider-component/Rail';
import API from '../Constant/API';
import Spinner from 'react-native-spinkit';
import MyHeader from '../Constant/MyHeader';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {Tab, Tabs, Thumbnail} from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RailSelected from './slider-component/RailSelected';

import Thumb from './slider-component/Thumb';

export default class SearchPg1 extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      value: '',
      // loader: false,
      min_range: '0',
      max_range: '0',
      name: '',
      city: '',
      state: '',
      zipCode: '',
      dataResponse: '',
      isRender: true,
      loader: true,
      city_data: '',
      state_data: '',
    };
  }

  async get_data() {
    console.log(this.state.loader, '----------------');
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    // console.log(parse, 'PARSE');
    const response = await this.api.state(parse.token);
    console.log(response, '---------------eeeeeeeeeeeeeeeee-');
    this.setState({state: response.data[0].state});
    const response3 = await this.api.cities(this.state.state, parse.token);
    console.log(response3, '------REPONS  ##3----------#');
    this.setState({city_data: response3, state_data: response, loader: false});
  }
  async onChange_api(_id) {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response3 = await this.api.cities(_id, parse.token);
    console.log(response3, '00000000000000000000000000000');
    this.setState({city_data: response3, state: _id});
  }
  state_picker = () => {
    return this.state.state_data.data.map((element, i) => {
      console.log(element, 'ELEMENT');
      return <Picker.Item label={element.state} value={element.state} />;
    });
  };
  city_picker = () => {
    return this.state.city_data.data.map((element, i) => {
      return <Picker.Item label={element.city} value={element.city} />;
    });
  };
  componentDidMount() {
    console.log('COMPONENT DID MOUNTTTTTT');
    const {navigation} = this.props;
    this.focusListener = navigation.addListener('focus', () => {
      this.get_data();
      console.log('cosolllllll');
      this.setState({isRender: true});
    });
  }
  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }

  async filterOnSubmit() {
    this.setState({loader: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);

      const response = await this.api.selectProperty(
        parse.id,
        this.state.name,
        this.state.min_range,
        this.state.max_range,
        this.state.city,
        this.state.state,
        this.state.zipCode,
        parse.token,
      );

      this.setState({dataResponse: response, isRender: false, loader: false});
      console.log(this.state.dataResponse, 'mera staate Data Response');
      console.log(
        this.state.name,
        this.state.min_range,
        this.state.max,
        this.state.city,
        this.state.state,
        this.state.zipCode,
        'statee',
      );
    } catch (error) {
      console.log(error, 'err');
    }
  }
  //-----------------------------------------------------list after filter
  List = () => {
    if (this.state.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: '55%',
          }}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      if (this.state.dataResponse.property_data[0] == null) {
        return (
          <View style={{marginVertical: '60%'}}>
            <View
              style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
              <AntDesign name="home" size={80} color="grey" />
              <Text style={{marginTop: '2%', color: 'grey'}}>
                No property found!
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <AntDesign
                name="arrowleft"
                color={color.theme_Color}
                size={10}
                onPress={() => this.props.navigation.goBack()}
              />
              <Text
                onPress={() => this.props.navigation.goBack()}
                style={{
                  alignSelf: 'center',
                  color: `${color.theme_Color}`,
                  marginLeft: 10,
                }}>
                Go back
              </Text>
            </View>
          </View>
        );
      } else {
        return this.state.dataResponse.property_data.map((element, i) => {
          console.log(element, '-------ELELEMENT');
          return (
            <View
              key={i}
              style={{
                flexDirection: 'row',
                marginLeft: '3%',
                marginRight: '3%',
                alignItems: 'center',
                borderWidth: 0.1,
                elevation: 1,
                padding: '3%',
                marginTop: '5%',
              }}>
              {element.images.length > 0 ? (
                <Thumbnail
                  square
                  large
                  source={{
                    uri: `${color.link}${element.images[0].path}`,
                  }}
                />
              ) : (
                <Thumbnail
                  square
                  large
                  source={require('../assets/myhome.jpg')}
                />
              )}

              <View style={{marginLeft: '5%'}}>
                <Text
                  style={{fontWeight: 'bold', fontSize: 16}}
                  onPress={() =>
                    this.props.navigation.navigate('ViewCartDetails', {
                      key: element.id,
                      status: '',
                    })
                  }>
                  {element.name}
                </Text>
                <Text>Rent: {element.rent}</Text>
                <Text style={{color: 'grey'}}>City: {element.city}</Text>
              </View>
              {/* <View style={{marginLeft: '15%'}}>
                <Text style={{fontWeight: 'bold', color: 'green'}}>Status</Text>
                <Text style={{color: 'green', fontWeight: 'bold'}}>
                  {element.status}
                </Text>
              </View> */}
            </View>
          );
        });
      }
    }
  };

  renderThumb = () => {
    return <Thumb />;
  };

  renderRail = () => {
    return <Rail />;
  };

  renderRailSelected = () => {
    return <RailSelected />;
  };

  renderLabel = (text) => {
    return <Label text={text} />;
  };

  renderNotch = () => {
    return <Notch />;
  };

  handleValueChange = (val) => {
    this.setState({
      value: val,
    });
  };

  handleNameeChange = (val) => {
    this.setState({
      name: val,
    });
    console.log(this.state.name);
  };
  handleMinRangeChange = (val, extra) => {
    this.setState({
      min_range: val,
    });
    console.log(this.state.min_range, val, 'val stat');
  };
  handleMaxRangeChange = (val, extra) => {
    this.setState({
      max_range: val,
    });
  };
  render() {
    console.log(this.state.state, '---------------------------');
    if (this.state.isRender == false) {
      //----------------------------------------------------------if
      return (
        <Container>
          <MyHeader
            headername="3"
            name="Properties"
            onPressIcon={() => this.setState({isRender: true})}
          />

          <Content>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                alignSelf: 'center',
                marginLeft: '39%',
              }}></View>

            <View>{this.List()}</View>
          </Content>
        </Container>
      );
    } else {
      //-----------------------------------------------------------else
      return (
        <Container>
          <MyHeader
            headername="3"
            name="Filter Properties"
            onPressIcon={() => this.props.navigation.goBack()}
          />
          {this.state.loader ? (
            <View
              style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Spinner type="Circle" />
            </View>
          ) : (
            <Content>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={{marginLeft: '3%', marginTop: '-5%'}}>
                  <Text style={{fontSize: 23, fontWeight: 'bold'}}>
                    Find your
                  </Text>
                  <Text
                    style={{
                      fontSize: 23,
                      fontWeight: 'bold',
                      marginLeft: '11%',
                    }}>
                    perfect Home
                  </Text>
                </View>

                <Image
                  source={require('../assets/png.png')}
                  style={{
                    width: '50%',
                    height: 138,
                    marginTop: '1%',
                    marginRight: '15%',
                  }}
                />
              </View>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 15,
                  marginTop: '1%',
                  marginLeft: '5%',
                }}>
                Rent Min
                <Text style={{color: `${color.header1_color}`}}>
                  {' '}
                  (Monthly)
                </Text>
              </Text>
              <Slider
                disableRange={true}
                style={{
                  flex: 1,
                  marginTop: '1%',
                  marginLeft: '3%',
                  marginRight: '3%',
                }}
                min={0}
                max={1000}
                step={1}
                floatingLabel
                allowLabelOverflow
                renderThumb={this.renderThumb}
                renderRail={this.renderRail}
                renderRailSelected={this.renderRailSelected}
                renderLabel={(text) => this.renderLabel(text)}
                renderNotch={this.renderNotch}
                onValueChanged={this.handleMinRangeChange}
              />

              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 15,
                  marginTop: '5%',
                  marginLeft: '5%',
                }}>
                Rent Max
                <Text style={{color: `${color.header1_color}`}}>
                  {' '}
                  (Monthly)
                </Text>
              </Text>
              <Slider
                disableRange={true}
                style={{
                  flex: 1,
                  marginTop: '5%',
                  marginLeft: '3%',
                  marginRight: '3%',
                }}
                min={0}
                max={1000}
                step={1}
                floatingLabel
                allowLabelOverflow
                renderThumb={this.renderThumb}
                renderRail={this.renderRail}
                renderRailSelected={this.renderRailSelected}
                renderLabel={(text) => this.renderLabel(text)}
                renderNotch={this.renderNotch}
                onValueChanged={this.handleMaxRangeChange}
              />

              <View
                style={{
                  marginTop: '5%',
                  marginLeft: '5%',
                  marginRight: '5%',
                }}>
                <Text style={{fontWeight: 'bold', fontSize: 15}}>
                  Property Name
                </Text>
                <TextInput
                  style={{
                    marginTop: '-3%',
                    borderColor: 'cadetblue',
                    borderBottomWidth: 1,
                  }}
                  keyboardType="email-address"
                  placeholder="Enter Name"
                  onChangeText={(text) => this.handleNameeChange(text)}
                />
              </View>

              <View
                style={{
                  marginTop: '5%',
                  marginLeft: '5%',
                  marginRight: '5%',
                }}>
                <Text style={{fontWeight: 'bold', fontSize: 15}}>State</Text>
                <View style={{borderBottomWidth: 1, borderColor: 'cadetblue'}}>
                  <Picker
                    mode="dialog"
                    selectedValue={this.state.state}
                    style={{
                      height: 50,
                      width: '100%',
                      marginLeft: '-1%',
                      marginBottom: '-2.5%',
                      right: '-1%',
                    }}
                    itemStyle={{
                      backgroundColor: 'red',
                      borderBottomWidth: 55,
                    }}
                    onValueChange={
                      (itemValue, itemIndex) => this.onChange_api(itemValue)
                      // this.setState({state: itemValue}
                    }>
                    {this.state_picker()}
                  </Picker>
                </View>
              </View>
              <View
                style={{
                  marginTop: '5%',
                  marginLeft: '5%',
                  marginRight: '5%',
                }}>
                <Text style={{fontWeight: 'bold', fontSize: 15}}>City</Text>
                <View style={{borderBottomWidth: 1, borderColor: 'cadetblue'}}>
                  <Picker
                    mode="dialog"
                    selectedValue={this.state.city}
                    style={{
                      height: 50,
                      width: '100%',
                      marginLeft: '-1%',
                      marginBottom: '-2.5%',
                      right: '-1%',
                    }}
                    itemStyle={{
                      backgroundColor: 'red',
                      borderBottomWidth: 55,
                    }}
                    onValueChange={(itemValue, itemIndex) =>
                      // this.onChange_api(itemValue)
                      this.setState({city: itemValue})
                    }>
                    {this.city_picker()}
                  </Picker>
                </View>
              </View>
              <View
                style={{
                  marginTop: '5%',
                  marginLeft: '5%',
                  marginRight: '5%',
                }}>
                <Text style={{fontWeight: 'bold', fontSize: 15}}>Zip Code</Text>
                <TextInput
                  style={{
                    marginTop: '-3%',
                    borderColor: 'cadetblue',
                    borderBottomWidth: 1,
                  }}
                  keyboardType="email-address"
                  placeholder="Zip Code"
                  onChangeText={(text) => this.setState({zipCode: text})}
                />
              </View>
              <TouchableOpacity
                onPress={() => this.filterOnSubmit()}
                style={{
                  height: 50,
                  width: 200,
                  borderRadius: 5,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: `${color.header3_color}`,
                  alignSelf: 'center',
                  marginTop: '5%',
                  marginBottom: '2%',
                }}>
                {this.state.loader ? (
                  <ActivityIndicator color={'white'} />
                ) : (
                  <Text style={{color: 'white', fontSize: 15}}>Search</Text>
                )}
              </TouchableOpacity>
            </Content>
          )}
        </Container>
      );
    }
  }
}
