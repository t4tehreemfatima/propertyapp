import {Container, Header, Content, Card, CardItem, Body} from 'native-base';
import MyHeader from '../Constant/MyHeader';
import React from 'react';
import * as color from '../Constant/Colors';
import {View, Text, Image} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {TouchableOpacity} from 'react-native';
export default class SearchPg2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container>
        <MyHeader
          headername="3"
          name="properties"
          rightIconPress={() => alert('ACCOUNT PRESS')}
          rightIconName="settings"
          onPressIcon={() => this.props.navigation.goBack()}
        />

        <Content>
          <View>
            <Card>
              <CardItem header>
                <View
                  style={{
                    position: 'absolute',
                    elevation: 2,
                    backgroundColor: `${color.theme_Color}`,
                    top: '8.5%',
                    right: '5.5%',
                  }}>
                  <Text style={{fontSize: 18}}>$2030 pw</Text>
                </View>

                <Image
                  source={{
                    uri:
                      'http://www.brandsynario.com/wp-content/uploads/real-estate-houses-in-karachi.jpg',
                  }}
                  style={{height: 200, width: null, flex: 1}}
                />
              </CardItem>
              <CardItem>
                <Body>
                  <View
                    style={{
                      borderBottomWidth: 2,
                      width: '100%',
                      padding: '3%',
                      marginTop: '-5%',
                      borderColor: `${color.header3_color}`,
                    }}>
                    <Text
                      onPress={() =>
                        this.props.navigation.navigate('ViewCartDetails')
                      }
                      style={{
                        fontWeight: 'bold',
                        color: `${color.header3_color}`,
                      }}>
                      APPARTMENT OR UNIT
                    </Text>
                    <Text style={{alignItems: 'center'}}>
                      <Ionicons name="md-location-outline" size={18} />
                      Block#62 ,abc area ,USA
                    </Text>
                  </View>
                </Body>
              </CardItem>
              <CardItem footer>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',

                    width: '80%',
                    marginLeft: '9%',
                    marginRight: '9%',
                  }}>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Ionicons
                      name="ios-bed-outline"
                      size={22}
                      color={color.icon_color}
                    />
                    <Text style={{fontWeight: 'bold'}}> 2</Text>
                  </View>
                  {/* 2nd */}
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <FontAwesome
                      name="bathtub"
                      size={20}
                      color={color.icon_color}
                    />
                    <Text style={{fontWeight: 'bold'}}> 6</Text>
                  </View>
                  {/* 3rd */}
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Ionicons
                      name="ios-car-sharp"
                      size={22}
                      color={color.icon_color}
                    />
                    <Text style={{fontWeight: 'bold'}}>2+</Text>
                  </View>
                </View>
              </CardItem>
            </Card>
          </View>
        </Content>
      </Container>
    );
  }
}
