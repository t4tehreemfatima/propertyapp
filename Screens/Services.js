import React from 'react';
import {Text} from 'react-native-paper';
import {Content, View} from 'native-base';
export default class Services extends React.Component {
  render() {
    return (
      <Content style={{margin: '3%'}}>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>Terms of Service</Text>
        <Text style={{fontSize: 15}}>
          By utilizing or accessing any tekumatics product, data feed, and/or
          services dispensed through the tekumatics website (www.
          tekumatics.com) and the tekumatics mobile application (on Android and
          IOS,) you agree to{' '}
        </Text>
        <Text style={{fontSize: 15}}>
          (1) these terms and conditions (the "Terms of Service") and{' '}
        </Text>
        <Text style={{fontSize: 15}}>
          (2) tekumatics privacy policy, and incorporated herein by reference
          (the "Privacy Policy").{' '}
        </Text>
        <Text style={{fontSize: 15}}>
          If you do not agree to any of the Terms of Service or the Privacy
          Policy, you should not use the services provided and immediately leave
          this website.
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>
          Limited License & Use of Service
        </Text>

        <Text style={{fontSize: 15}}>
          a. Subject to these Terms of Service, you are accorded a
          non-exclusive, non-transferable, limited license to gain access to and
          use our service. These Terms of Service apply to all users of our
          service, including the Authorised Users "Client," Tenant, Landlords,
          and any other who make use of the service. As used herein, the term
          "Content" shall mean the text, pictures, software, videos, sounds,
          music, scripts, graphics, videos, audiovisuals, interactive mediums,
          and other features you may access via, watch on, or contribute to our
          service but shall explicitly exclude any Personal Identifiable
          Information as explained in the Privacy Policy.
        </Text>
        <Text style={{fontSize: 15}}>
          b. You agree not to reverse engineer, alter, adapt, otherwise tamper
          with the service or revise another website to falsely suggest its
          association with the service or software of tekumatics.
        </Text>
        <Text style={{fontSize: 15}}>
          c. You agree not to use the service to transmit, post, upload or host
          unsolicited bulk e-mail "Spam," viruses, malware, self-cloning
          computer programs, "Worms," or any destructive or malicious code.
        </Text>
        <Text style={{fontSize: 15}}>
          d. The service is secured by the copyright laws of Australia,
          international copyright laws, and treaties, together with other laws
          and treaties. Aside from the non-exclusive authorization granted
          according to this agreement, you accept and accede that all license,
          ownership, intellectual property, other rights, and interests in and
          to the service shall solely remain tekumatics.
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>
          Access to the Service
        </Text>
        <Text style={{fontSize: 15}}>
          a. Only an Authorized User is allowed to access and use the service.
          An "Authorized User" is an individual client or the partners, members,
          employees, temporary employees, and contractors of a client. "tenant
          and landlords" refers to any person designated by an Authorized User
          to share and rent property rooms and apartments.
        </Text>
        <Text style={{fontSize: 15}}>
          b. Notwithstanding section 2(c), Authorized Users are permitted to
          access and use the service using an Application Programming Interface
          ("API") subject to the following conditions:
        </Text>
        <View style={{marginLeft: '3%'}}>
          <Text style={{fontSize: 15}}>
            i. Any use of the service using an API, including use of an API
            through a third-party product that accesses and uses the service, is
            governed by these Terms of Service;
          </Text>
          <Text style={{fontSize: 15}}>
            ii. Tekumatics shall not be responsible for any direct, indirect,
            incidental, special, consequential, or exemplary damages, including
            but not limited to damages for loss of profits, goodwill, use, data,
            or other intangible losses (even if tekumatics has been advised of
            the possibility of such damages), resulting from any use of an API
            or third-party products that access and use the service via an API;
          </Text>
          <Text style={{fontSize: 15}}>
            iii. Excessive utilization of the service using an API may result in
            temporary or permanent suspension of access to the service via an
            API. Tekumatics, in its sole discretion, will ascertain excessive
            use of the service by means API, and will make a practical attempt
            to warn Authorized User before suspension; and
          </Text>
        </View>
        <Text style={{fontSize: 15}}>
          c. The service may contain links to third-party websites that are not
          owned, controlled, or maintained by tekumatics. Hence, tekumatics
          assumes no obligation for the third party's website content, privacy
          policies, or practices. Also, tekumatics cannot and will not expurgate
          or edit the content of any third-party website. You expressly consent
          and acknowledge that tekumatics shall have no liability stemming from
          your use of any third-party website by using the service.
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>
          Security of Passwords
        </Text>
        <Text style={{fontSize: 15}}>
          Authorized Users will be responsible for securing usernames and
          passwords or any other sensitive data associated with the service
          known to them and for the accuracy and adequacy of personally
          identifiable information provided to the service.
        </Text>
        <Text style={{fontSize: 15}}>
          b. Authorized Users will enforce policies and procedures to prevent
          the unauthorized or illegal use of the username or usernames assigned
          to them.
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>
          Cancellation and Termination
        </Text>
        <Text style={{fontSize: 15}}>
          Tekumatics may suspend or cease delivering our service to any Client,
          Authorized User, without notification for failing to adhere to these
          Terms of Service, and pursue any other judicial relief available.
          Tekumatics reserves the sole and exclusive right to determine whether
          a Client, Authorized User is not in compliance with these Terms of
          Service.
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>
          Content submitted by You
        </Text>
        <Text style={{fontSize: 15}}>
          a. As an Authorized User, you may submit content related to the
          property renting service, including text, graphics, photographs,
          documents, and video and audio recordings. You understand that
          tekumatics does not guarantee any confidentiality relating to any
          content you submit.
        </Text>
        <Text style={{fontSize: 15}}>
          b. You shall be solely responsible for your content and the
          consequences of uploading, submitting and publishing your content to
          the service. You affirm, represent, and warrant that you own or have
          the necessary licenses, rights, consents, and permissions to publish
          content you submit; and your license to tekumatics all patent,
          trademark, trade secret, copyright, or other proprietary rights in and
          to such content for publication on the service according to these
          Terms of Service.
        </Text>
        <Text style={{fontSize: 15}}>
          c. You further agree that content you submit to the service will not
          contain third-party copyrighted material unless you have permission
          from the rightful owner of the material or you are otherwise legally
          entitled to post the material and to grant tekumatics all of the
          license rights granted herein.
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>Miscellaneous</Text>
        <Text style={{fontSize: 15}}>
          a. You acknowledge and agree that tekumatics may use third-party
          vendors and hosting partners to provide the necessary hardware,
          software, networking, storage, and related technology required to run
          the service.
        </Text>
        <Text style={{fontSize: 15}}>
          b. You acknowledge and agree that the technical processing and
          transmission of data associated with the service, including content,
          may be transmitted unencrypted and involve:
        </Text>
        <View style={{marginLeft: '3%'}}>
          <Text style={{fontSize: 15}}>
            i. transmissions over various networks; and
          </Text>
          <Text style={{fontSize: 15}}>
            ii. changes to conform and adapt to technical requirements of
            connecting networks or devices.
          </Text>
        </View>
        <Text style={{fontSize: 15}}>
          c. The failure of tekumatics to enforce any provision hereof shall not
          constitute or be construed as a waiver of such provision or of the
          right to enforce it at a later time.
        </Text>
        <Text style={{fontSize: 15}}>
          d. The Terms of Service constitutes the entire agreement between you
          and tekumatics and govern your use of the service, superseding any
          prior agreements between you and tekumatics (including, but not
          limited to, any prior versions of the Terms of Service).
        </Text>
      </Content>
    );
  }
}

{
  /*
      
  <Text style={{fontWeight: 'bold', fontSize: 20}}></Text>
  <Text style={{fontWeight: 'bold'}}></Text>
  <Text style={{fontSize: 15}}></Text> 
  */
}
