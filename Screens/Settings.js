import {Container, Content} from 'native-base';
import React from 'react';
import {Switch} from 'react-native-paper';
import {View, Text} from 'react-native';
import MyHeader from '../Constant/MyHeader';
import * as color from '../Constant/Colors';
import { TouchableOpacity } from 'react-native';
export default class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      switch: false,
    };
  }
  onSwitch() {
    if (this.state.switch == false) {
      this.setState({
        switch: true,
      });
    } else {
      this.setState({
        switch: false,
      });
    }
    console.log(this.state.switch);
  }

  render() {
    return (
      <Container>
        <MyHeader
          headername="3"
          name="Settings"
          // rightIconPress={() => alert('ACCOUNT PRESS')}
          // rightIconName="settings"
          onPressIcon={() => this.props.navigation.goBack()}
        />

        <Content
          style={{
            marginRight: '5%',
            marginLeft: '5%',
            marginTop: '5%',
          }}>
          {/* <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              borderBottomWidth: 1, borderBottomColor: 'grey',
              paddingBottom: '5%'
            }}>
            <View>
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                Notifications
              </Text>
              <Text style={{color: 'grey'}}>Notify me about updates</Text>
            </View>
            <Switch
            
              value={this.state.switch}
              onValueChange={() => this.onSwitch()}
            />
          </View> */}
          
          <TouchableOpacity onPress={() => this.props.navigation.navigate('ChangePass')}>
          <View style={{marginTop: '5%', borderBottomWidth: 1, borderBottomColor: 'grey',
              paddingBottom: '5%'}}>
            <Text
              
              style={{fontSize: 18, fontWeight: 'bold'}}>
              Change Password
            </Text>
            <Text style={{color: 'grey'}}>
              Its a good idea to use strong password
            </Text>
            <Text style={{color: 'grey'}}>that you dont use elsewhere</Text>
          </View>
          </TouchableOpacity>
        </Content>
      </Container>
    );
  }
}
