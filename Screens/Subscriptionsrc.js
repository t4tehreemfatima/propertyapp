import {Button, Card, CardItem, Container, Content, Title} from 'native-base';
import MyHeader from '../Constant/MyHeader';
import React from 'react';
import {useFocusEffect} from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {View, Text} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import API from '../Constant/API';
import AntDesign from 'react-native-vector-icons/AntDesign';
import * as color from '../Constant/Colors';
import Spinner from 'react-native-spinkit';
import Unorderedlist from 'react-native-unordered-list';

import {FlatList, Dimensions, StyleSheet, BackHandler} from 'react-native';
import {TouchableOpacity} from 'react-native';
const numColumns = 2;
// const data = [
//   {
//     key: 'A',
//   },
//   {
//     key: 'B',
//   },
//   {
//     key: 'C',
//   },
//   {
//     key: 'D',
//   },
// ];
const backAction = () => {
  BackHandler.exitApp();
  return true;
};

function HardwareBackBtn() {
  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', backAction);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
    }, []),
  );

  return null;
}
export default class Subscriptionscr extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      isloading: true,
      data: {},
      pkgid: '',
      amount: '',
      color: 'red',
      selection: [],
    };
  }
  on_press(item, index) {
    let data = this.state.data;
    this.state.data.forEach((element, i) => {
      data[i]['key'] = 0;
    });
    data[index]['key'] = 1;
    this.setState({data: data});
    this.setState({pkgid: item.id, amount: item.amount});

    console.log(this.state.data, '-----------------------(((-nnnn');
  }

  on_press_unselect(item, index) {
    let data = this.state.data;
    this.state.data.forEach((element, i) => {
      data[i]['key'] = 0;
    });
    data[index]['key'] = 1;
    this.setState({data: data});
    this.setState({pkgid: item.id, amount: item.amount});
    console.log(this.state.data, '-----------------------(((-nnnn');
  }
  //--------------------------------------------------///////
  async get_subs() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.subscr_select(parse.token);
      let sel = [];
      sel = response.data;
      response.data.forEach((element, i) => {
        console.log(i, sel);
        sel[i]['key'] = 0;
        console.log('FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF');
      });
      this.setState({selection: sel});
      console.log(this.state.selection, '-------selectinn txt');
      console.log(
        response,
        'Subscrition slellct--aa---------------------- dta',
      );
      this.setState({data: sel, isloading: false});
    } catch (error) {
      console.log(error, 'erre');
    }
  }
  componentDidMount() {
    console.log('SSSSSSSSSSSSSSSSSSSSSSSSSSS');
    this.get_subs();
  }

  formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length / numColumns);

    let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
    while (
      numberOfElementsLastRow !== numColumns &&
      numberOfElementsLastRow !== 0
    ) {
      data.push({
        key: `blank-${numberOfElementsLastRow}`,
        empty: true,
      });
      numberOfElementsLastRow++;
    }
    console.log(data, 'TTTTTT');
    return data;
  };
  renderItem = ({item, index}) => {
    console.log(item, 'TTTTRTTTTTTTTTTTTTTTT');
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]} />;
    }

    if (item.key == 0) {
      return (
        <TouchableOpacity onPress={() => this.on_press(item, index)}>
          <View style={styles.item}>
            <Card
              style={{
                padding: '1%',
                // borderRadius: 13,
                justifyContent: 'center',
                width: 152,
                height: 120,
                marginLeft: '5%',
                backgroundColor: `${color.backgroundColor}`,
              }}>
              <View style={{marginLeft: '5%'}}>
                <Text style={{fontSize: 20}}>{item.name}</Text>
                <Text
                  style={{
                    borderBottomWidth: 1,
                    borderColor: `${color.btn_Color}`,
                  }}></Text>
                <Unorderedlist>
                  <Text>{item.period}</Text>
                </Unorderedlist>
                <Unorderedlist>
                  <Text> {item.type}</Text>
                </Unorderedlist>
                <Unorderedlist>
                  <Text
                    style={{color: 'green', fontSize: 15, fontWeight: 'bold'}}>
                    {item.amount == null ? '0' : item.amount}$
                  </Text>
                </Unorderedlist>
              </View>
            </Card>
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity onPress={() => this.on_press_unselect(item, index)}>
          <View style={styles.item}>
            <Card
              style={{
                padding: '1%',
                // borderRadius: 13,
                justifyContent: 'center',
                width: 152,
                height: 120,
                marginLeft: '5%',
                backgroundColor: `${color.theme_Color}`,
              }}>
              <View style={{marginLeft: '5%'}}>
                <Text style={{fontSize: 20}}>{item.name}</Text>
                <Text style={{borderBottomWidth: 1}}></Text>
                <Unorderedlist>
                  <Text>{item.period}</Text>
                </Unorderedlist>
                <Unorderedlist>
                  <Text> {item.type}</Text>
                </Unorderedlist>
                <Unorderedlist>
                  <Text
                    style={{color: 'green', fontSize: 15, fontWeight: 'bold'}}>
                    {item.amount}$
                  </Text>
                </Unorderedlist>
              </View>
            </Card>
          </View>
        </TouchableOpacity>
      );
    }
  };
  //--------------------------------------
  _storeUserData = async (_name, _email, _id, _token) => {
    try {
      let obj = {
        name: _name,
        email: _email,
        id: _id,
        token: _token,
      };

      await AsyncStorage.setItem('user_data', JSON.stringify(obj));
    } catch (error) {
      alert(error);
    }
    console.log('User Data clear');
  };

  async logout() {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    console.log(parse.token);

    const response = await this.api.logout(parse.token);
    console.log(response, 'RESPONSE');

    if (response.data[0].Logout) {
      this._storeUserData('', '', '', '');
      this.props.navigation.reset({
        index: 0,
        routes: [{name: 'SignIn'}],
      });
      // this.props.navigation.navigate('SignIn');
    } else {
      alert('error');
    }
  }

  async add_subs_trial() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.subs_user_trial_add(
        this.state.pkgid,
        parse.token,
      );

      console.log(response, 'add spaaaa   subs');
      this.props.navigation.navigate('MainStack');
      if (response.status == true) {
        this.showDialog();
      } else {
        alert(response.data[0].error);

        console.log('------------ERORORO REPSO');
      }
    } catch (error) {
      console.log(error, 'err');
    }
  }

  handleBack() {
    if (this.props.route.params.from == 'verification') {
      backAction();
    } else {
      this.props.navigation.goBack();
    }
  }

  render() {
    if (this.state.isloading == true) {
      return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      console.log(this.state.data, this.state.amount, 'yyy');
      return (
        <Container>
          {this.props.route.params.from == 'verification' ? (
            <MyHeader
              headername="3"
              name="Subscription"
              rightIconPress={() => this.logout()}
              rightIconName="logout"
              onPressIcon={() => this.handleBack()}

              // onPressIcon={() => backAction()}
            />
          ) : (
            <MyHeader
              headername="3"
              name="Subscription"
              // rightIconPress={() => this.logout()}
              // rightIconName="logout"
              onPressIcon={() => this.handleBack()}

              // onPressIcon={() => backAction()}
            />
          )}

          <Content>
            {this.props.route.params.from == 'verification' ? (
              <HardwareBackBtn />
            ) : (
              <View></View>
            )}
            <Text
              style={{alignSelf: 'center', fontSize: 30, fontWeight: 'bold'}}>
              Choose your
            </Text>
            <Text
              style={{alignSelf: 'center', fontSize: 30, fontWeight: 'bold'}}>
              Plan
            </Text>
            <FlatList
              extraData={this.state}
              style={styles.container}
              renderItem={this.renderItem}
              keyExtractor={(item) => item.id}
              data={this.formatData(this.state.data, numColumns)}
              numColumns={numColumns}
            />
            {this.state.pkgid == '' ? (
              <Button
                disabled
                style={{
                  backgroundColor: 'grey',
                  width: '76%',
                  alignSelf: 'center',
                  justifyContent: 'center',
                  borderRadius: 68,
                }}>
                <Text style={{fontWeight: 'bold'}}>Continue</Text>
              </Button>
            ) : (
              <Button
                onPress={() => {
                  if (this.state.amount == null) {
                    this.add_subs_trial();
                  } else {
                    this.props.navigation.navigate('Payment', {
                      pkg_id: this.state.pkgid,
                      pkg_amount: this.state.amount,
                      from: this.props.route.params.from,
                    });
                  }
                }}
                style={{
                  backgroundColor: `${color.newbtn}`,
                  width: '76%',
                  alignSelf: 'center',
                  justifyContent: 'center',
                  borderRadius: 68,
                }}>
                <Text style={{fontWeight: 'bold', color: 'white'}}>
                  Continue
                </Text>
              </Button>
            )}
          </Content>
        </Container>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 1,
    height: Dimensions.get('window').width / numColumns, // approximate a square
    justifyContent: 'space-around',
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  itemText: {
    color: '#fff',
  },
  imageItem: {
    color: '#FFFF33',
    // fontFamily: `${Fonts.heading}`,
    fontSize: 15,
  },
});
