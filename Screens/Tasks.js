import {Container, Header, Content, Accordion} from 'native-base';
import MyHeader from '../Constant/MyHeader';
import React from 'react';
import {Button, BackHandler} from 'react-native';
import {List, ListItem, Left, Body, Right, Thumbnail} from 'native-base';
import * as color from '../Constant/Colors';
import {View, Text} from 'react-native';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import {useFocusEffect} from '@react-navigation/native';
import {Dialog, SlideAnimation, DialogContent} from 'react-native-popup-dialog';
import Octicons from 'react-native-vector-icons/Octicons';
import {TouchableOpacity} from 'react-native';
import TasksLandlord from './TasksLandlord';
const backAction = () => {
  BackHandler.exitApp();
  return true;
};

function HardwareBackBtn() {
  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', backAction);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
    }, []),
  );

  return null;
}
export default class Tasks extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();

    this.state = {
      data: '',
      isLoading: true,
      statusData: '',
      type: '',
      visible: false,
    };
  }
  async tabfunction() {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    this.setState({type: parse.user_type});
    console.log(
      parse.user_type,
      '000000000000000003746745754752547typeep--------------',
    );
  }

  async tasks() {
    this.setState({isLoading: true});
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response = await this.api.getTasks(parse.id, parse.token);
    console.log(
      response,
      parse.id,
      '00000000000000000000---------------------',
    );

    this.setState({data: response, isLoading: false});
  }

  async task_update(_id) {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response = await this.api.updateTasks(_id, 'completed', parse.token);
    console.log(response, 'hahahhahhahahh');

    if (response.data[0].task_assign == 'updated') {
      this.setState({isLoading: true});
      this.tasks();
    }
  }
  async componentDidMount() {
    const {navigation} = this.props;
    this.focusListener = navigation.addListener('focus', () => {
      console.log('cosolllllll');
      this.tasks();
      this.tabfunction();
    });
    this.tasks();
    this.tabfunction();
  }
  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }
  // componentDidMount() {
  //   this.tasks();
  //   this.tabfunction();
  // }

  list = () => {
    // if (this.state.data.data[0]==null) {
    //   return(
    //     <View>
    //       <Ionicons name="notifications-outline" size={80} color="grey" />
    //       <Text style={{color: 'grey'}}>No task found!</Text>
    //     </View>
    //   )
    // } else {

    return this.state.data.data.map((element, i) => {
      return (
        <ListItem key={i}>
          <Left>
            <Thumbnail
              source={{
                uri: `https://cdn1.iconfinder.com/data/icons/rounded-set-6/48/todo-list-512.png`,
              }}
            />
          </Left>
          <Body style={{marginLeft: '-38%'}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>
              {element.task}
            </Text>
            <Text>
              Status: <Text style={{color: 'green'}}>{element.status}</Text>
            </Text>
            <Text>{element.date}</Text>
          </Body>
          <Right>
            {element.status == 'completed' ? (
              <View></View>
            ) : (
              <TouchableOpacity
                onPress={() => this.task_update(element.id)}
                style={{
                  height: 40,
                  width: 60,
                  backgroundColor: 'green',
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 5,
                }}>
                <Text style={{color: 'white'}}>Done</Text>
              </TouchableOpacity>
            )}
          </Right>
        </ListItem>
      );
    });
    // }
  };
  render() {
    if (this.state.type == '1') {
      return <TasksLandlord navigation={this.props.navigation} />;
      //-----------------------------------------------------------------------------tenedent id 2
    } else {
      return (
        <Container>
          <MyHeader
            headername="1"
            name="Tasks"
            // rightIconPress={() => alert('ACCOUNT PRESS')}
            // rightIconName="settings"
            leftIconPress={() => this.props.navigation.openDrawer()}
          />
          <HardwareBackBtn />
          {this.state.isLoading ? (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: '35%',
              }}>
              <Spinner type="Circle" color={color.header3_color} />
            </View>
          ) : this.state.data.data.length == 0 ? (
            <View
              style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Octicons name="tasklist" size={80} color="grey" />
              <Text style={{color: 'grey', marginTop: '2%'}}>
                No tasks found!
              </Text>
            </View>
          ) : (
            <Content style={{marginTop: '2%'}}>
              <List>{this.list()}</List>
            </Content>
          )}
        </Container>
      );

      // if (this.state.isLoading) {
      //   return (
      //     <View
      //       style={{
      //         flex: 1,
      //         alignItems: 'center',
      //         justifyContent: 'center',
      //         marginTop: '35%',
      //       }}>
      //       <Spinner type="Circle" color={color.header3_color} />
      //     </View>
      //   );
      // } else {
      //   return (
      //     <Container>
      //       <MyHeader
      //         headername="1"
      //         name="Tasks"
      //         rightIconPress={() => alert('ACCOUNT PRESS')}
      //         rightIconName="settings"
      //         leftIconPress={() => this.props.navigation.openDrawer()}
      //       />

      //       <Content>
      //         <List>{this.list()}</List>
      //       </Content>
      //     </Container>
      //   );
      // }
    }
  }
}

// <List key={i}>
//   <ListItem avatar style={{marginTop: '3%'}}>
//     <Left style={{alignItems: 'center', justifyContent: 'center'}}>
//       <Thumbnail
//         style={{}}
//         source={{
//           uri:
//             'https://cdn1.iconfinder.com/data/icons/rounded-set-6/48/todo-list-512.png',
//         }}
//       />
//     </Left>
//     <Body>
//       <Text style={{fontWeight: 'bold'}}>{element.task}</Text>
//       <Text note style={{flexDirection: 'column'}}>
//         Status:
//         <Text style={{color: 'green'}}>{element.status}</Text>
//       </Text>
//       <Text>Date:{element.date}</Text>
//     </Body>
//     <Right style={{alignItems: 'center', justifyContent: 'center'}}>
//       <Button
//         onPress={() => this.task_update()}
//         style={{
//           backgroundColor: 'green',
//           height: '55%',
//           width: '70%',
//           // padding: '1%',
//           // marginTop: '5%',
//         }}>
//         <Text style={{color: 'white', marginLeft: '30%'}}>Done</Text>
//       </Button>
//     </Right>
//   </ListItem>
// </List>
