import React from 'react';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Button,
  Body,
  DatePicker,
} from 'native-base';
import {useFocusEffect} from '@react-navigation/native';
import MyHeader from '../Constant/MyHeader';
import {Paragraph, Dialog, Portal} from 'react-native-paper';
import {View, Text, BackHandler} from 'react-native';
import API from '../Constant/API';
import * as color from '../Constant/Colors';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import {TouchableNativeFeedback} from 'react-native';
import {ScrollView} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import {TextInput} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import DateTimePicker from '@react-native-community/datetimepicker';
import AntDesign from 'react-native-vector-icons/AntDesign';
import moment from 'moment';
const backAction = () => {
  BackHandler.exitApp();
  return true;
};

function HardwareBackBtn() {
  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', backAction);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
    }, []),
  );

  return null;
}
export default class TasksLandlord extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();

    this.state = {
      isLoading: true,
      data: '',
      visible: false,
      getTaskList: '',
      id: '',
      tendendPicker: '',
      tendend_id: '',
      date: '12-04-2021',
      currTendent: '',
      currTendentId: '',
      userId: '',
      update_visible: false,
      id_update: '',
      state: '',
      status: 'completed',
      date: new Date(1598051730000),
      // date:moment().format('DD-MM-YYYY'),
      mode: 'date',
      show: false,
      yearBuild: '',
      update_date: '',
      update_day: '',
      update_show: false,
      day: '',
    };
  }
  ///--------------------------UPDATE DATE
  update_showDatepicker = () => {
    this.setState({update_show: true});
  };

  update_convertTimestamp(_timestamp) {
    let tmpDate = moment(_timestamp.nativeEvent.timestamp).format('DD-MM-Y');
    console.log(tmpDate, '---------------> TMP DATE');
    this.setState({update_date: tmpDate, update_show: false});
  }

  update_handleConfirm = (date) => {
    alert('rfrfrf');
    console.warn('A date has been picked: ', date);
    this.setState({DateTimePickerModal: false});
  };
  //-------------------------------------------------------------------
  showDatepicker = () => {
    this.setState({show: true});
  };

  convertTimestamp(_timestamp) {
    let tmpDate = moment(_timestamp.nativeEvent.timestamp).format('DD-MM-Y');
    console.log(tmpDate, '---------------> TMP DATE');
    this.setState({yearBuild: tmpDate, show: false});
  }

  //--------------------------------
  handleConfirm = (date) => {
    alert('rfrfrf');
    console.warn('A date has been picked: ', date);
    this.setState({DateTimePickerModal: false});
  };
  //------------------------------------------------------------------------------------
  async task_assign_list() {
    // this.setState({isLoading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      //task screenn list
      const response = await this.api.task_list(null, parse.token); //parse.id
      //task picker
      const response2 = await this.api.task_model_list(parse.id, parse.token);
      //property on tenedend
      const response3 = await this.api.propertyOnTendend(
        this.state.currTendentId,
        parse.token,
      ); //property id
      const response4 = await this.api.cuurent_tendent_property(
        parse.id,
        parse.token,
      );
      console.log(
        response,
        parse.id,
        '-------=+++++++++++++++++++++++++---------esp list ',
      );
      if (response4.property_data.length != 0) {
        this.setState({currTendentId: response4.property_data[0].id});
      }
      if (response2.data[0] != null) {
        this.setState({
          id: response2.data[0].id,
        });
      } else {
        this.setState({id: ''});
      }
      console.log(response3, '777777777777777777');
      this.setState({
        getTaskList: response2,
        data: response,

        tendendPicker: response3,
        // tendend_id: response3.data[0].user_id,
        currTendent: response4,
        // currTendentId: response4.property_data[0].id,
        isLoading: false,
      });
      this.onChange_api(this.state.currTendentId);
      // console.log(
      //   response,
      //   '////////c///////0000000000///////////////',
      //   parse.id,
      // );
    } catch (error) {
      console.log(error, 'er2r');
    }
  }
  async onChange_api(_id) {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response3 = await this.api.propertyOnTendend(_id, parse.token);

      if (response3.data[0] != null) {
        this.setState({userId: response3.data[0].user_id});
      } else {
        this.setState({userId: ''});
      }
      console.log(response3, 'respmse 3');
      this.setState({
        tendendPicker: response3,
        currTendentId: _id,
      });
    } catch (error) {
      console.log(error, 'err on chna nge');
    }
  }
  async delete_task(_id) {
    this.setState({isLoading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.task_assign_delete(_id, parse.token);
      console.log(response, 'delelel');
      if (response.data[0].task_assign == 'deleted') {
        this.task_assign_list();
        alert('Deleted');
        this.setState({isLoading: false});
      } else {
        alert('Try Again');
        this.setState({isLoading: false});
      }
    } catch (error) {
      console.log(error, 'err deler');
    }
  }
  async update_task() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.task_assign_update(
        this.state.id_update,
        this.state.status,
        this.state.update_day,
        parse.token,
      );
      console.log(response, '---------------->response-------------------000');
      if (response.data[0].task_assign == 'updated') {
        this.hideDialog_update();
        this.task_assign_list();
        alert('Updated');
      } else {
        alert('Not Updated');
      }
    } catch (error) {
      console.log(error, 'erupdateer');
    }
  }
  async add_task() {
    this.setState({isLoading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.task_assign_add_btn(
        this.state.userId,
        this.state.id,
        // this.state.yearBuild,
        this.state.day,
        parse.token,
      );
      console.log(parse.id, '5=id h       v/task/assign/select');
      console.log(
        this.state.id,
        this.state.userId,
        this.state.yearBuild,
        '00000000000000000',
      );
      console.log(
        response.data,
        '--------------------responsce 444----------<><>',
      );
      // prettier-ignore
      if (response.data[0].task_assign=='Added') {   
        // if (response.data[0]["task assigned"]=='Added') {   
          alert(response.data[0].task_assign);
          this.hideDialog();
          this.task_assign_list();
          // this.setState({isLoading: false});//-------------relod ni horaha
     
      } else {
        alert(response.data[0].error);
        this.setState({isLoading:false})
        this.hideDialog();
      }
    } catch (error) {
      console.log(error, 'err add task');
    }
  }

  componentDidMount() {
    const {navigation} = this.props;
    this.focusListener = navigation.addListener('focus', () => {
      this.task_assign_list();
      // this.get_task_list();
    });
    this.task_assign_list();
    // this.get_task_list();
  }

  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }
  // componentDidMount() {
  //   this.task_assign_list();
  //   this.get_task_list();
  // }

  model_list = () => {
    return this.state.getTaskList.data.map((element, i) => {
      return <Picker.Item label={element.task} value={element.id} />;
    });
  };
  tendent_model_list = () => {
    return this.state.tendendPicker.data.map((element, i) => {
      return <Picker.Item label={element.user_name} value={element.user_id} />;
    });
  };
  map_current_tendent_list = () => {
    return this.state.currTendent.property_data.map((element, i) => {
      // console.log(element, '---+++++++++++++++++++++++=-///');
      return <Picker.Item label={element.name} value={element.id} />;
    });
  };
  showDialog = () => {
    this.setState({visible: true});
    console.log(this.state.currTendent, 'id h ya current i');
  };
  hideDialog = () => this.setState({visible: false});
  showDialog_update = (_id) =>
    this.setState({id_update: _id, update_visible: true});
  hideDialog_update = () => this.setState({update_visible: false});
  assign_task_map_list = () => {
    if (this.state.data.data[0] == null) {
      return (
        <View>
          <Text
            style={{
              marginTop: '80%',
              alignSelf: 'center',
              fontSize: 30,
              color: '#e0e0e0',
            }}>
            No Tasks
          </Text>
        </View>
      );
    } else {
      return this.state.data.data.map((element, i) => {
        console.log(element.id, 'elelelel');

        return (
          <Card
            key={i}
            style={{
              marginLeft: '2%',
              marginRight: '2%',
            }}>
            <CardItem>
              <Body
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <View style={{flexDirection: 'column'}}>
                  <Text style={{fontSize: 18}}>
                    Task :
                    <Text
                      style={{
                        color: 'green',
                        fontWeight: 'bold',
                        textTransform: 'uppercase',
                      }}>
                      {element.task}
                    </Text>
                  </Text>
                  <Text style={{fontSize: 15, color: 'grey'}}>
                    User Name :
                    <Text
                      style={{
                        fontWeight: 'bold',
                      }}>
                      {element.user_name}
                    </Text>
                  </Text>
                  <Text>
                    Status:
                    <Text style={{color: 'green', textTransform: 'uppercase'}}>
                      {element.status}
                    </Text>
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'column',
                  }}>
                  <Button
                    onPress={() => this.showDialog_update(element.id)}
                    style={{
                      height: 30,
                      width: 80,
                      backgroundColor: `${color.theme_Color}`,
                    }}>
                    <Text style={{marginLeft: '18%', color: 'white'}}>
                      Update
                    </Text>
                  </Button>
                  <Button
                    onPress={() => this.delete_task(element.id)}
                    style={{
                      height: 30,
                      width: 80,
                      backgroundColor: `${color.newbtn}`,
                      marginTop: '5%',
                    }}>
                    <Text
                      style={{
                        marginLeft: '18%',
                        color: 'white',
                      }}>
                      Delete
                    </Text>
                  </Button>
                  <Text>{element.date}</Text>
                </View>
              </Body>
            </CardItem>
          </Card>
        );
      });
    }
  };
  render() {
    console.log(this.state.currTendentId, '----id id id -------<<<<Id>>>>>>>>');

    return (
      <Container>
        <MyHeader
          headername="1"
          name="Tasks"
          rightIconPress={() => this.showDialog()}
          rightIconName="add"
          leftIconPress={() => this.props.navigation.openDrawer()}
        />
        <HardwareBackBtn />
        {this.state.isLoading ? (
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: '5%',
            }}>
            <Spinner type="Circle" color={color.header3_color} />
          </View>
        ) : (
          <Content style={{marginTop: '3%'}}>
            {this.assign_task_map_list()}
            <Portal>
              <Dialog visible={this.state.visible} onDismiss={this.hideDialog}>
                <Dialog.Title>Select Task</Dialog.Title>
                <Ionicons
                  onPress={() => {
                    this.hideDialog();
                    this.props.navigation.navigate('CreateTask');
                  }}
                  style={{
                    position: 'absolute',
                    alignSelf: 'center',
                    right: '9%',
                    top: '5%',
                  }}
                  name="add-circle"
                  size={40}
                  color="green"
                />
                <Dialog.Content>
                  <View
                    style={{
                      borderBottomWidth: 1,
                      borderColor: `${color.header3_color}`,
                    }}>
                    <Text
                      style={{
                        color: `${color.theme_Color}`,
                        fontWeight: 'bold',
                        marginTop: '5%',
                      }}>
                      Property:
                    </Text>
                    <Picker
                      mode="dialog"
                      selectedValue={this.state.currTendentId}
                      style={{
                        height: 50,
                        width: '102%',

                        marginBottom: '-2.5%',
                        right: '1.5%',
                      }}
                      itemStyle={{
                        backgroundColor: 'red',
                        borderBottomWidth: 55,
                      }}
                      onValueChange={
                        (itemValue, itemIndex) => this.onChange_api(itemValue)
                        // this.setState({currTendentId: itemValue})
                      }>
                      {this.map_current_tendent_list()}
                    </Picker>
                  </View>
                  <Text
                    style={{
                      color: `${color.theme_Color}`,
                      fontWeight: 'bold',
                      marginTop: '5%',
                    }}>
                    Tasks:
                  </Text>
                  <View
                    style={{
                      borderBottomWidth: 1,
                      borderColor: `${color.header3_color}`,
                    }}>
                    <Picker
                      mode="dialog"
                      selectedValue={this.state.id}
                      style={{
                        height: 50,
                        width: '102%',

                        marginBottom: '-2.5%',
                        right: '1.5%',
                      }}
                      itemStyle={{
                        backgroundColor: 'red',
                        borderBottomWidth: 55,
                      }}
                      onValueChange={(itemValue, itemIndex) =>
                        this.setState({id: itemValue})
                      }>
                      {this.model_list()}
                    </Picker>
                  </View>
                  <Text
                    style={{
                      color: `${color.theme_Color}`,
                      fontWeight: 'bold',
                      marginTop: '5%',
                    }}>
                    Assign Tenant:
                  </Text>
                  <View
                    style={{
                      borderBottomWidth: 1,
                      borderColor: `${color.header3_color}`,
                    }}>
                    <Picker
                      mode="dialog"
                      selectedValue={this.state.tendend_id}
                      style={{
                        height: 50,
                        width: '102%',

                        marginBottom: '-2.5%',
                        right: '1.5%',
                      }}
                      itemStyle={{
                        backgroundColor: 'red',
                        borderBottomWidth: 55,
                      }}
                      onValueChange={(itemValue, itemIndex) =>
                        this.setState({tendend_id: itemValue})
                      }>
                      {this.tendent_model_list()}
                    </Picker>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '100%'}}>
                      <Text
                        style={{
                          color: `${color.theme_Color}`,
                          fontWeight: 'bold',
                          marginTop: '5%',
                        }}>
                        Enter Day:
                      </Text>
                      <TextInput
                        style={{
                          borderBottomWidth: 1,

                          borderColor: `${color.theme_Color}`,
                        }}
                        // placeholder="12-04-2021"
                        keyboardType="numeric"
                        value={this.state.day}
                        onChangeText={(text) => this.setState({day: text})}
                      />
                    </View>

                    <View style={{top: 22}}>
                      <Text
                        style={{
                          fontSize: 14,
                          position: 'absolute',
                          top: 34,
                          right: 15,
                        }}>
                        Days
                      </Text>
                      {/* <AntDesign
                        onPress={this.showDatepicker}
                        style={{marginTop: '8%'}}
                        name="calendar"
                        size={30}
                      /> */}
                    </View>
                  </View>
                  <View>
                    {this.state.show && (
                      <DateTimePicker
                        testID="dateTimePicker"
                        value={this.state.date}
                        mode={this.state.mode}
                        minimumDate={new Date()}
                        // is24Hour={true}
                        display="default"
                        onChange={(text) => this.convertTimestamp(text)}
                      />
                    )}
                  </View>
                </Dialog.Content>
                <Dialog.Actions>
                  {/* // onPress={() =>
                      this.feature_add(
                        this.props.route.params.key,
                        this.state.feature_id,
                      )
                    } */}

                  <Button
                    style={{
                      width: '20%',
                      marginRight: '3%',
                      height: '90%',
                      backgroundColor: `${color.theme_Color}`,
                    }}
                    onPress={() => this.add_task()}>
                    <Text style={{color: 'white', marginLeft: '25%'}}>Add</Text>
                  </Button>
                  <Button
                    style={{
                      width: '20%',
                      height: '90%',
                      backgroundColor: `${color.newbtn}`,
                    }}
                    onPress={this.hideDialog}>
                    <Text style={{color: 'white', marginLeft: '15%'}}>
                      Cancel
                    </Text>
                  </Button>
                </Dialog.Actions>
              </Dialog>
            </Portal>
            {/* //---------------------------------------------------------------------------UPDATE */}
            <Portal>
              <Dialog
                visible={this.state.update_visible}
                onDismiss={this.hideDialog_update}>
                <Dialog.Title>Update Task</Dialog.Title>

                <Dialog.Content>
                  <View
                    style={{
                      borderBottomWidth: 1,
                      borderColor: `${color.header3_color}`,
                    }}>
                    <Picker
                      mode="dialog"
                      selectedValue={this.state.status}
                      style={{
                        height: 50,
                        width: '102%',

                        marginBottom: '-2.5%',
                        right: '1.5%',
                      }}
                      itemStyle={{
                        backgroundColor: 'red',
                        borderBottomWidth: 55,
                      }}
                      onValueChange={(itemValue, itemIndex) =>
                        this.setState({status: itemValue})
                      }>
                      <Picker.Item label="completed" value="completed" />
                      <Picker.Item label="Uncompleted" value="uncompleted" />
                    </Picker>
                  </View>

                  <View>
                    <Text
                      style={{
                        color: `${color.theme_Color}`,
                        fontWeight: 'bold',
                        marginTop: '5%',
                      }}>
                      Enter Day:
                    </Text>
                    <TextInput
                      keyboardType="numeric"
                      // placeholder="12-04-2021"
                      style={{
                        borderBottomWidth: 1,
                        borderColor: `${color.theme_Color}`,
                      }}
                      // value={this.state.update_day}
                      // placeholder="10"
                      onChangeText={(text) => this.setState({update_day: text})}
                    />
                    <View style={{position: 'absolute', right: 0, top: 52}}>
                      <Text
                        style={{
                          fontSize: 14,
                          // position: 'absolute',
                          // top: 34,
                          right: 6,
                        }}>
                        Days
                      </Text>
                      {/* <AntDesign
                        onPress={this.update_showDatepicker}
                        style={{marginTop: '8%'}}
                        name="calendar"
                        size={30}
                      /> */}
                    </View>
                  </View>
                </Dialog.Content>
                <Dialog.Actions>
                  {/* // onPress={() =>
                      this.feature_add(
                        this.props.route.params.key,
                        this.state.feature_id,
                      )
                    } */}

                  <Button
                    onPress={() => this.update_task()}
                    style={{
                      width: '30%',
                      marginRight: '3%',
                      height: '90%',
                      backgroundColor: `${color.theme_Color}`,
                    }}>
                    <Text style={{color: 'white', marginLeft: '25%'}}>
                      Update
                    </Text>
                  </Button>
                  <Button
                    style={{
                      width: '20%',
                      height: '90%',
                      backgroundColor: `${color.newbtn}`,
                    }}
                    onPress={this.hideDialog_update}>
                    <Text style={{color: 'white', marginLeft: '15%'}}>
                      Cancel
                    </Text>
                  </Button>
                </Dialog.Actions>
              </Dialog>
            </Portal>
            <View>
              {this.state.update_show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={this.state.date}
                  mode={this.state.mode}
                  minimumDate={new Date()}
                  // is24Hour={true}
                  display="default"
                  onChange={(text) => this.update_convertTimestamp(text)}
                />
              )}
            </View>
          </Content>
        )}
      </Container>
    );
    //}
  }
}
