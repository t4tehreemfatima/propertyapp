//landlord screen
import {Button, Avatar} from 'react-native-paper';
import {View, Text, ImageBackground} from 'react-native';
import * as color from '../Constant/Colors';
import {Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import CurrentHomeTendent from './CurrentHomeTendent';
import {Container, Content, Title, Tab, Tabs, Thumbnail} from 'native-base';
import React from 'react';
import MyHeader from '../Constant/MyHeader';
import AntDesign from 'react-native-vector-icons/AntDesign';
import BottomTab from '../Constant/BottomTab';
import {ActivityIndicator} from 'react-native';
import Spinner from 'react-native-spinkit';

import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RentTedent from './RentTendent';
import Rent from './Rent';
import {Divider} from 'react-native-elements';
import {TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native';

export default class TendentDetail extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {};
  }
  async tabfunction() {
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
  }
  componentDidMount() {}

  render() {
    // console.log(this.props.navigation.goBack(),'h');
    return (
      <Container>
        <MyHeader
          headername="4"
          name="Manage Payment"
          //   rightIconPress={() => this.props.navigation.navigate('History')}
          //   rightIconName="history"
          onPressIcon={() => this.props.navigation.goBack()}
        />
        <CurrentHomeTendent navigation={this.props.navigation} />
        {/* <HomeHeader leftIcon={() => this.props.navigation.openDrawer()} /> */}
        {/* <Content> */}
        {/* <Tabs tabBarUnderlineStyle={{backgroundColor: `${color.theme_Color}`}}>
          <Tab
            textStyle={{color: 'black', fontWeight: 'bold'}}
            activeTextStyle={{color: 'black', fontWeight: 'bold'}}
            tabStyle={{backgroundColor: 'white'}}
            activeTabStyle={{backgroundColor: 'white'}}
            heading="Current Property">
           

          </Tab> */}
        {/* <Tab
            textStyle={{color: 'black', fontWeight: 'bold'}}
            activeTextStyle={{color: 'black', fontWeight: 'bold'}}
            tabStyle={{backgroundColor: 'white'}}
            activeTabStyle={{backgroundColor: 'white'}}
            heading="Previous Property">
            <PreviosHome navigation={this.props.navigation} />
          </Tab> */}
        {/* </Tabs> */}
      </Container>
    );
  }
}
//---------------------------------------------------------------------------------------CURRENT HOME

//--------------------------------------------------------------------------------------------------Previos

class PreviosHome extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      data: '',
      isloading: true,
    };
  }
  async preivios_home() {
    this.setState({isloading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.tendent_property_old(
        parse.id,
        parse.token,
      );
      console.log(response, '12222');
      this.setState({data: response, isloading: false});
      console.log(this.state.data.data, '==');
    } catch (error) {
      console.log(error, 'errr');
    }
  }
  componentDidMount() {
    this.preivios_home();
  }
  list = () => {
    return this.state.data.data.map((element, i) => {
      console.log(element, 'eleprevio000000=--------met');
      return (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: '3%',
          }}>
          <View
            style={{
              borderLeftWidth: 1,
              padding: '1%',
              justifyContent: 'center',
              width: '55%',
            }}>
            <Text>Property:</Text>
            <Text style={{fontWeight: 'bold'}}>{element.property_name}</Text>
          </View>
          <View
            style={{
              borderLeftWidth: 1,
              padding: '1%',
              width: '30%',
              justifyContent: 'center',
            }}>
            <Text>Date:</Text>
            <Text style={{color: 'grey', fontWeight: 'bold'}}>
              {element.date}
            </Text>
            <Text>Rent Amount:</Text>
            <Text style={{fontWeight: 'bold', color: 'green'}}>
              {element.rent}
            </Text>
          </View>
          <Divider style={{marginTop: '5%'}} />
        </View>
      );
    });
    // }
  };
  render() {
    return (
      <Container>
        {this.state.isloading ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
            <Spinner type="Circle" color={color.header3_color} />
          </View>
        ) : this.state.data.data.length == 0 ? (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
            <AntDesign name="home" size={80} color="grey" />
            <Text style={{marginTop: '2%', color: 'grey'}}>
              No previous property found!
            </Text>
          </View>
        ) : (
          <ScrollView showsVerticalScrollIndicator={false}>
            <Content style={{marginLeft: '5%', marginRight: '5%'}}>
              <View style={{marginBottom: 8}}>{this.list()}</View>
            </Content>
          </ScrollView>
        )}
        {/* <Content style={{marginLeft: '5%', marginRight: '5%'}}>
          <View>
            {this.state.isloading ? (
              <Spinner
                type="Circle"
                color={color.header3_color}
                style={{alignSelf: 'center'}}
              />
            ) : this.state.data.data.length == 0 ?
            <View style={{alignItems: 'center', justifyContent: 'center', marginTop: '10%'}}>
              <AntDesign name="home" size={30} color="grey" />
              <Text style={{marginTop: '2%', color:"grey"}}>You haven't apply for any properties yet!</Text>
              </View> : (
              this.list()
            )}
          </View>
        </Content> */}
      </Container>
    );
  }
}
