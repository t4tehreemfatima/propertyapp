//landlord screen
import {Avatar} from 'react-native-paper';
import {View, Text, ImageBackground} from 'react-native';
import * as color from '../Constant/Colors';
import {Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import {
  Container,
  Content,
  Title,
  Tab,
  Tabs,
  Thumbnail,
  Card,
  Button,
} from 'native-base';
import React from 'react';
import MyHeader from '../Constant/MyHeader';
import AntDesign from 'react-native-vector-icons/AntDesign';
import BottomTab from '../Constant/BottomTab';
import {ActivityIndicator} from 'react-native';
import Spinner from 'react-native-spinkit';

import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RentTedent from './RentTendent';
import Rent from './Rent';
import {Divider} from 'react-native-elements';
import {TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native';
import TendentRent_Rent from './TendentRent_Rent';
import Utility_tendent from './Utility_tendent';

export default class TendentRent extends React.Component {
  constructor(props) {
    super(props);

    this.second_bacha = React.createRef();
    this.api = API();
    this.state = {
      type: '',
      index: 0,
      isLoading: true,
      button: '',
    };
  }
  onClick = (e) => {
    console.log(e, 'dfdfdfdfdfdfdfdfdfdfdf');
    if (e.from != 0) {
      this.setState({isLoading: true});
      setTimeout(() => {
        console.log(this.state.index);
        this.setState({index: Math.random()});
        this.setState({isLoading: false});
      }, 2000);
    }
  };

  createtab = () => {
    // <Utility_tendent
    //           navigation={this.props.navigation}
    //           hidebtn={this.hideadd}
    //         />
    return (
      <Utility_tendent
        hidebtn={this.hideadd}
        navigation={this.props.navigation}
      />
    );
  };
  hideadd = (hide) => {
    if (hide == true) {
      this.setState({button: ''});
    } else {
      this.setState({button: 'add'});
    }
  };

  render() {
    return (
      <Container>
        <MyHeader
          headername="7"
          name="Pay Rent"
          //   rightIconPress={() => this.props.navigation.navigate('History')}
          rightIconName={this.state.button}
          onPressIcon={() => this.props.navigation.goBack()}
        />
        {/* <HomeHeader leftIcon={() => this.props.navigation.openDrawer()} /> */}
        {/* <Content> */}
        <Tabs
          onChangeTab={this.onClick}
          tabBarUnderlineStyle={{backgroundColor: `${color.theme_Color}`}}>
          <Tab
            textStyle={{color: 'black', fontWeight: 'bold'}}
            activeTextStyle={{color: 'black', fontWeight: 'bold'}}
            tabStyle={{backgroundColor: 'white'}}
            activeTabStyle={{backgroundColor: 'white'}}
            heading="Rent">
            <TendentRent_Rent
              ref={(instance) => {
                this.second_bacha = instance;
              }}
              hidebtn={(e) => this.hideadd(e)}
              navigation={this.props.navigation}
            />
            {/* <TendentRent_Rent
              navigation={this.props.navigation}
              hidebtn={(e) => this.hideadd(e)}
            /> */}

            {/* <ApplicationHome navigation={this.props.navigation} /> */}
          </Tab>
          <Tab
            textStyle={{color: 'black', fontWeight: 'bold'}}
            activeTextStyle={{color: 'black', fontWeight: 'bold'}}
            tabStyle={{backgroundColor: 'white'}}
            activeTabStyle={{backgroundColor: 'white'}}
            heading="Utility">
            {/* <RentHistory /> */}
            {this.createtab()}
            {/* <Utility_tendent
              navigation={this.props.navigation}
              hidebtn={this.hideadd}
            /> */}
            {/* <OpenHome /> */}
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
//
//
//
/////////////////////////////////////
//
//
//
//
//
//....................................
//---------------------------------------------------------------------------------------CURRENT HOME
class MyRent extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      historyData: '',
      isloading: false,
    };
  }
  async tendent_history() {
    this.setState({isloading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.rent_pay_history(parse.id, parse.token);
      console.log(response, 'RESPONSE HISTORYYYYYYYYY__________>>>Y');
      this.setState({historyData: response, isloading: false});
      //   console.log(
      //     this.state.historyData,
      //     'j0000000000000000000000w77777777777777777777777',
      //   );
    } catch (error) {
      console.log(error, 'errr');
    }
  }
  historydata_list = () => {
    if (this.state.historyData[0].length == 0) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: '55%',
          }}>
          <Spinner type="Circle" color={color.header3_color} />
        </View>
      );
    } else {
      return this.state.historyData.data.map((element, i) => {
        console.log(element, '_____________________________________-');
        return (
          <View key={i}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: '3%',
              }}>
              <View
                style={{
                  borderLeftWidth: 1,
                  padding: '1%',
                  justifyContent: 'center',
                  width: '55%',
                }}>
                <Text>Property:</Text>
                <Text style={{fontWeight: 'bold'}}>
                  {element.property_name}
                </Text>
              </View>
              <View
                style={{
                  borderLeftWidth: 1,
                  padding: '1%',
                  width: '30%',
                  justifyContent: 'center',
                }}>
                <Text>Date:</Text>
                <Text style={{color: 'grey', fontWeight: 'bold'}}>
                  2012-02-2
                </Text>
                <Text>Rent Amount:</Text>
                <Text style={{fontWeight: 'bold', color: 'green'}}>
                  {element.amount}
                </Text>
              </View>
            </View>
            <Divider style={{marginTop: '5%'}} />
          </View>
        );
      });
    }
  };
  componentDidMount() {
    const {navigation} = this.props;

    this.focusListener = navigation.addListener('focus', () => {
      this.tendent_history();
      console.log('cosolllllll');
    });
    this.focusListenerr = navigation.addListener('blur', () => {
      console.log('un cosolllllll');
    });
  }
  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }
  // componentDidMount() {
  //   this.tendent_history();
  // }
  render() {
    return (
      <Container>
        <Card style={{padding: '5%'}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View>
              <Text style={{fontSize: 15}}>Property Name</Text>
              <Text>
                Rent:
                <Text style={{fontSize: 17, fontWeight: 'bold'}}>$6256</Text>
              </Text>
            </View>
            <View>
              <Button
                onPress={() => this.props.navigation.navigate('Pay')}
                style={{
                  height: 30,
                  padding: '5%',
                  width: 80,
                  backgroundColor: 'green',
                  justifyContent: 'center',
                }}>
                <Text style={{color: 'white'}}>Pay</Text>
              </Button>
            </View>
          </View>
        </Card>
        <View style={{flexDirection: 'row', marginTop: '5%', marginLeft: '5%'}}>
          <MaterialIcon name="history" size={20} />
          <Text style={{fontSize: 15, fontWeight: 'bold'}}>History</Text>
        </View>
        <Content style={{marginLeft: '3%', marginRight: '3%', marginTop: '5%'}}>
          {/* //--------------------------------------------------------- */}
          <View>{this.historydata_list()}</View>
          {/* <View>
            {this.state.isloading ? (
              <ActivityIndicator />
            ) : (
              this.historydata_list()
            )}
          </View> */}
        </Content>
      </Container>
    );
  }
}
//--------------------------------------------------------------------------------------------------Previos

class RentHistory extends React.Component {
  render() {
    return (
      <Container>
        <Content style={{marginLeft: '5%', marginRight: '5%'}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: '3%',
            }}>
            <View
              style={{
                borderLeftWidth: 1,
                padding: '1%',
                justifyContent: 'center',
                width: '55%',
              }}>
              <Text>Property:</Text>
              <Text style={{fontWeight: 'bold'}}>Saima Buildrrs</Text>
            </View>
            <View
              style={{
                borderLeftWidth: 1,
                padding: '1%',
                width: '30%',
                justifyContent: 'center',
              }}>
              <Text>Date:</Text>
              <Text style={{color: 'grey', fontWeight: 'bold'}}>2012-02-2</Text>
              <Text>Rent Amount:</Text>
              <Text style={{fontWeight: 'bold', color: 'green'}}>7000$</Text>
            </View>
          </View>
          <Divider style={{marginTop: '5%'}} />
        </Content>
      </Container>
    );
  }
}
