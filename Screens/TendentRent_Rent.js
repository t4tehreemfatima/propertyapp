//landlord screen
import {Avatar} from 'react-native-paper';
import {View, Text, ImageBackground} from 'react-native';
import * as color from '../Constant/Colors';
import {Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import {
  Container,
  Content,
  Title,
  Tab,
  Tabs,
  Thumbnail,
  Card,
  Button,
} from 'native-base';
import React from 'react';
import MyHeader from '../Constant/MyHeader';
import AntDesign from 'react-native-vector-icons/AntDesign';
import BottomTab from '../Constant/BottomTab';
import {ActivityIndicator} from 'react-native';
import Spinner from 'react-native-spinkit';

import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RentTedent from './RentTendent';
import Rent from './Rent';
import {Divider} from 'react-native-elements';
import {TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native';
export default class TendentRent_Rent extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      historyData: '',
      isloading: true,
      payAmount: '',
    };
  }

  async tendent_history() {
    console.log(
      'chalaagahgshgaj++++++++++++++++++++++++++++++++++++++++++++++',
    );
    this.setState({isloading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.rent_pay_history(parse.id, parse.token);

      const response2 = await this.api.rent_pay_date(parse.token);
      console.log(parse.token);
      this.setState({
        historyData: response,
        payAmount: response2.data,
        isloading: false,
      });
      console.log(response, '--------66--res------------');
    } catch (error) {}
  }

  historydata_list = () => {
    console.log(this.state.historyData.data, '------------------HISTORY DATA');
    if (this.state.historyData.data.length == 0) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            marginTop: '34%',
          }}>
          <MaterialIcon name="history" size={20} color="grey" />
          <Text style={{color: 'grey'}}>No Rent History Found!</Text>
        </View>
      );
    } else {
      return this.state.historyData.data.map((element, i) => {
        console.log(
          element,
          '_________________------_______________----_________________',
        );
        return (
          <View key={i}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: '3%',
              }}>
              <View
                style={{
                  borderLeftWidth: 1,
                  padding: '1%',
                  justifyContent: 'center',
                  width: '55%',
                }}>
                <Text>Property:</Text>
                <Text style={{fontWeight: 'bold'}}>
                  {element.property_name}
                </Text>
              </View>
              <View
                style={{
                  borderLeftWidth: 1,
                  padding: '1%',
                  width: '30%',
                  justifyContent: 'center',
                }}>
                <Text>Date:</Text>
                <Text style={{color: 'grey', fontWeight: 'bold'}}>
                  {element.date}
                </Text>
                <Text>Rent Amount:</Text>
                <Text style={{fontWeight: 'bold', color: 'green'}}>
                  {element.paid}
                </Text>
              </View>
            </View>
            <Divider style={{marginTop: '5%'}} />
          </View>
        );
      });
    }
    // }
  };
  componentDidMount() {
    const {navigation} = this.props;
    this.focusListener = navigation.addListener('focus', async () => {
      // console.log('cosolllllll');
      this.tendent_history();
    });
  }

  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }

  render() {
    console.log(this.state.payAmount.last_paid_date, 'pay666');
    return (
      <Container>
        {/* {this.state.payAmount[0] == 'not Found' ? (
          <View>
            <Text>No Data</Text>
          </View>
        ) : ( */}
        <Card style={{padding: '5%'}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View>
              <Text style={{fontSize: 15}}>
                {this.state.payAmount.property_name}
              </Text>
              <Text>
                Rent:
                <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                  {this.state.payAmount.amount}
                </Text>
              </Text>
              <Text>
                Next Rental Date:
                {this.state.payAmount.Payable_date == '' ? (
                  <Text style={{color: 'grey'}}>
                    Please proceed & make payment for your rented place
                  </Text>
                ) : (
                  this.state.payAmount.last_paid_date
                )}
              </Text>
              <Text>
                Last Paid Date:
                {this.state.payAmount.last_paid_date == '' ? (
                  <Text style={{color: 'grey'}}> unavailable </Text>
                ) : (
                  this.state.payAmount.last_paid_date
                )}
              </Text>
            </View>
            <View style={{position: 'absolute', right: 2, top: 71}}>
              <Button
                onPress={() =>
                  this.props.navigation.navigate('Pay', {
                    key: this.state.payAmount,
                  })
                }
                style={{
                  height: 30,
                  padding: '5%',
                  width: 90,
                  backgroundColor: 'green',
                  justifyContent: 'center',
                }}>
                <Text style={{color: 'white'}}>Pay</Text>
              </Button>
            </View>
          </View>
        </Card>
        {/* )} */}
        <View style={{flexDirection: 'row', marginTop: '5%', marginLeft: '5%'}}>
          <MaterialIcon name="history" size={20} />
          <Text style={{fontSize: 15, fontWeight: 'bold'}}>Rent History</Text>
        </View>
        <Content style={{marginLeft: '3%', marginRight: '3%', marginTop: '5%'}}>
          {/* //--------------------------------------------------------- */}
          {/* <View>{this.historydata_list()}</View> */}
          <View>
            {this.state.isloading ? (
              <Spinner
                type="Circle"
                color={color.header3_color}
                style={{alignSelf: 'center'}}
              />
            ) : (
              this.historydata_list()
            )}
          </View>
        </Content>
      </Container>
    );
  }
}
