import {Title} from 'native-base';
import React from 'react';
import * as color from '../Constant/Colors';
import {Container, Header, Content, Tab, Tabs} from 'native-base';
import {View, Text} from 'react-native';
import MyHeader from '../Constant/MyHeader';
import { WebView } from 'react-native-webview';
import AsyncStorage from '@react-native-async-storage/async-storage';
import API from '../Constant/API';
import Spinner from 'react-native-spinkit';
export default class TermCondition extends React.Component {
 
  render() {
    return (
      <Container>
        <MyHeader
          headername="4"
          name="Terms & Conditions"
          onPressIcon={() => this.props.navigation.goBack()}
        />
        <Tabs tabBarUnderlineStyle={{backgroundColor: `${color.theme_Color}`}}>
          <Tab
            textStyle={{color: 'black', fontWeight: 'bold'}}
            activeTextStyle={{color: 'black', fontWeight: 'bold'}}
            tabStyle={{backgroundColor: 'white'}}
            activeTabStyle={{backgroundColor: 'white'}}
            heading="Terms & Conditions">
            <Tab1 />
          </Tab>
          <Tab
            textStyle={{color: 'black', fontWeight: 'bold'}}
            activeTextStyle={{color: 'black', fontWeight: 'bold'}}
            tabStyle={{backgroundColor: 'white'}}
            activeTabStyle={{backgroundColor: 'white'}}
            heading="Privacy Policy">
            <Tab2 />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

class Tab1 extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      data:'',
      loader:true
    };
  }
  async getdata(){
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response = await this.api.privicy(parse.token);
    console.log(response, 'res imag');
    this.setState({data:response,loader:false})
  }

  
  componentDidMount(){
    this.getdata()
  }
  render() {
    return (
  <Content>
  {this.state.loader?

<View style={{alignSelf:'center',justifyContent:'center',flex:1,alignItems:'center'}}>
  <Spinner type="Circle" color={color.header3_color}/></View>

:
<View>
           <Text
            style={{
            marginLeft: '5%',
            marginRight: '5%',
            fontSize: 20,
            fontWeight: 'bold',
            marginTop: '3%',
          }}>
          Mai Home -Term aand Condition
        </Text>
        <Text style={{marginLeft:'5%',marginRight:"5%",   marginBottom:'5%'}}>{this.state.data.data}</Text>
        </View>
  }
             </Content>
    );
  }
}

class Tab2 extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      data:'',
      loader:true
    };
  }
  async getdata(){
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response = await this.api.privicy(parse.token);
    console.log(response, 'res imag');
    this.setState({data:response,loader:false})
  }

componentDidMount(){
  this.getdata()
}
  render() {
    return (
      <Content>
      {this.state.loader?

    <View style={{alignSelf:'center',justifyContent:'center',flex:1,alignItems:'center'}}>
      <Spinner type="Circle" color={color.header3_color}/></View>

    :
    <View>
               <Text
                style={{
                marginLeft: '5%',
                marginRight: '5%',
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: '3%',
              }}>
              Mai Home -Term aand Condition
            </Text>
            <Text style={{marginLeft:'5%',marginRight:"5%",   marginBottom:'5%'}}>{this.state.data.data}</Text>
            </View>
      }
                 </Content>
        );
  }
}
