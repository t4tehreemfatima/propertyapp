import {Container, Content, Title, Button} from 'native-base';
import MyHeader from '../Constant/MyHeader';
import React from 'react';
import API from '../Constant/API';
import Spinner from 'react-native-spinkit';
import * as color from '../Constant/Colors';
import {ActivityIndicator} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  View,
  TextInput,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import {ThemeProvider} from '@react-navigation/native';
export default class UpdateProfile extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      name: this.props.route.params.key.data[0].name,
      email: this.props.route.params.key.data[0].email,
      phoneNo: this.props.route.params.key.data[0].mobile,
      address: this.props.route.params.key.data[0].address,
      loader: false,
      profileData: this.props.route.params.key.data[0],
    };
  }
  async profile_update() {
    this.setState({loader: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      let string = parse.token;
      let add = String(this.state.address);
      console.log(parse, 'wikhS');
      const response = await this.api.updateProfile(
        parse.id,
        this.state.name,
        '1',
        this.state.email,
        this.state.phoneNo,
        add,
        string,
      );

      if (response.data != undefined) {
        this.setState({loader: false});
        alert('Sucessfully');
        this.props.navigation.navigate('Profile');
      } else {
        alert('errorr');
      }
    } catch (error) {
      console.log(error, 'err');
    }
  }
  render() {
    return (
      <Container>
        <MyHeader
          headername="3"
          name="Update Profile"
          rightName="Save"
          // rightIconPress={() => alert('ACCOUNT PRESS')}
          // rightIconName="settings"
          onPressIcon={() => this.props.navigation.goBack()}
          rightSavePress={() => alert('Save')}
        />

        <Content>
          <View style={{marginLeft: '5%', marginRight: '5%', marginTop: '5%'}}>
            <Text
              style={{fontSize: 19, fontWeight: 'bold', color: 'cadetblue'}}>
              Personal Details
            </Text>
            <View style={{marginTop: '5%'}}>
              <Text style={{fontWeight: 'bold', fontSize: 15}}>Name</Text>
              <TextInput
                style={{
                  marginTop: '-3%',
                  borderColor: 'cadetblue',
                  borderBottomWidth: 1,
                }}
                onChangeText={(text) => this.setState({name: text})}
                keyboardType="email-address"
                value={this.state.name}
              />

              <Text style={{fontWeight: 'bold', fontSize: 15, marginTop: '5%'}}>
                Email Address
              </Text>
              <TextInput
                style={{
                  marginTop: '-3%',
                  borderColor: 'cadetblue',
                  borderBottomWidth: 1,
                }}
                onChangeText={(text) => this.setState({email: text})}
                keyboardType="email-address"
                value={this.state.email}
              />
              <View>
                <Text
                  style={{fontWeight: 'bold', fontSize: 15, marginTop: '5%'}}>
                  Phone Number
                </Text>
                <TextInput
                  style={{
                    marginTop: '-3%',
                    borderColor: 'cadetblue',
                    borderBottomWidth: 1,
                  }}
                  onChangeText={(text) => this.setState({phoneNo: text})}
                  keyboardType="numeric"
                  value={this.state.phoneNo}
                />
                {/* <Button
                  primary
                  style={{
                    position: 'absolute',
                    left: '70%',
                    top: '20%',
                    width: '25%',
                    backgroundColor: '#2D495D',
                    justifyContent: 'center',
                  }}>
                  <Text style={{color: 'white'}}>Change</Text>
                </Button> */}
              </View>
              <Text style={{fontWeight: 'bold', fontSize: 15, marginTop: '5%'}}>
                Current residential address
              </Text>
              <TextInput
                style={{
                  marginTop: '-3%',
                  borderColor: 'cadetblue',
                  borderBottomWidth: 1,
                }}
                onChangeText={(text) => this.setState({address: text})}
                value={this.state.address}
              />
            </View>
          </View>
          <TouchableOpacity
            onPress={() => this.profile_update()}
            style={{
              height: 50,
              width: 200,
              borderRadius: 5,
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
              backgroundColor: `${color.header3_color}`,
              marginTop: '5%',
            }}>
            {this.state.loader ? (
              <ActivityIndicator color={'white'} />
            ) : (
              <Text style={{color: 'white'}}>Save</Text>
            )}
          </TouchableOpacity>
        </Content>
      </Container>
    );
  }
}
