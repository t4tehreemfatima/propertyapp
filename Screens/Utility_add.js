import MyHeader from '../Constant/MyHeader';
import React from 'react';
import {
  Container,
  Content,
  Title,
  Tab,
  Tabs,
  Thumbnail,
  Right,
  Button,
} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import API from '../Constant/API';
import {Picker} from '@react-native-picker/picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import * as color from '../Constant/Colors';
import {View, Text} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import {Divider} from 'react-native-elements';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {ActivityIndicator, Card} from 'react-native-paper';
import {Paragraph, Dialog, Portal} from 'react-native-paper';
import {TextInput} from 'react-native';
export default class Utility_add extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      data: '',
      isloading: true,
      visible: false,
      update_visible: false,
      selectedPro: '',
      utility_data: '',
      dialog_selectedpro: '',
      name: '',
      date: '',
      update_date: '',
      update_name: '',
      update_selectedpro: '',
      id_select_update: '',
    };
  }
  async utility() {
    this.setState({isloading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.property_select(parse.id, parse.token);
      if (response.property_data[0] != null) {
        this.setState({
          selectedPro: response.property_data[0].id,
        });
      } else {
        this.setState({
          selectedPro: '',
        });
      }

      const response2 = await this.api.utility_select(
        response.property_data[0].id,
        parse.token,
      );
      this.setState({
        data: response,
        utility_data: response2,
        dialog_selectedpro: response.property_data[0].id,

        isloading: false,
      });
      console.log(response, 'picker checker');
    } catch (error) {
      console.log(error, 'errr');
    }
  }
  async on_change_utility(_id) {
    this.setState({selectedPro: _id, loader: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response2 = await this.api.utility_select(_id, parse.token);
      console.log(response2, 'utili ka ress');
      this.setState({
        utility_data: response2,
        loader: false,
        //dialog_selectedpro: response2.property_data[0].id,
      });
      console.log(_id, 'data');
    } catch (error) {
      this.setState({
        loader: false,
      });
      console.log(error, 'errr');
    }
  }
  async add_utility() {
    console.log(
      this.state.dialog_selectedpro,
      this.state.name,
      this.state.date,
      'CHECKEKKRKKRKR ADDDED',
    );
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.add_utility(
        this.state.dialog_selectedpro,
        this.state.name,
        this.state.date,
        parse.token,
      );
      if (response.data[0].utility == 'added') {
        alert('Added');
      } else {
        alert('Try Again');
      }
      this.hideDialog();
      this.utility();
      console.log(response, 'responsce ha a6');
    } catch (error) {
      console.log(error, 'errr');
    }
  }

  async update_utility() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.utility_update(
        this.state.id_select_update,
        this.state.update_name,
        this.state.update_date,
        parse.token,
      );
      if (response.data[0].utility == 'updated') {
        this.update_hideDialog();
        alert('Updated');
        this.utility();
      } else {
        alert('Try Again');
      }
    } catch (error) {
      console.log(error, 'errr');
    }
  }
  async delete_utility(_id) {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.utility_delete(_id, parse.token);
      if (response.data[0].utility == 'deleted') {
        alert('Deleted');
        this.utility();
      } else {
        alert('Try Again');
      }

      console.log(response, 'responsce58596957657687 ha a6');
    } catch (error) {
      console.log(error, 'errr');
    }
  }

  componentDidMount() {
    this.utility();
  }
  showDialog = () => this.setState({visible: true});
  hideDialog = () => this.setState({visible: false});
  //update
  update_showDialog = (_id) =>
    this.setState({id_select_update: _id, update_visible: true});
  update_hideDialog = () => this.setState({update_visible: false});
  map_property_list = () => {
    return this.state.data.property_data.map((element, i) => {
      return <Picker.Item label={element.name} value={element.id} />;
    });
  };
  property_picker_list = () => {
    return this.state.data.property_data.map((element, i) => {
      return <Picker.Item label={element.name} value={element.id} />;
    });
  };
  update_property_picker_list = () => {
    return this.state.data.property_data.map((element, i) => {
      return <Picker.Item label={element.name} value={element.id} />;
    });
  };
  map_utility_list = () => {
    if (this.state.utility_data.data.length === 0) {
      return (
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            alignSelf: 'center',
          }}>
          <Text
            style={{
              fontSize: 20,
              color: '#d8e3e7',
              alignSelf: 'center',
              marginTop: '55%',
            }}>
            No Utility Found
            <MaterialCommunityIcons name="null" color="#d8e3e7" size={20} />
          </Text>
        </View>
      );
    } else {
      return this.state.utility_data.data.map((element, i) => {
        return (
          <View
            style={{
              flexDirection: 'row',
              elevation: 0.6,
              borderWidth: 0.1,
              padding: '3%',
              marginTop: '3%',
              backgroundColor: `${color.backgroundColor}`,
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View style={{}}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 14,
                  }}>
                  Property:
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: 'bold',
                    marginLeft: 5,
                    color: `${color.text}`,
                  }}>
                  {element.property_name.length < 25
                    ? `${element.property_name}`
                    : `${element.property_name.substring(0, 25)}...`}
                </Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  style={{
                    fontSize: 14,
                  }}>
                  Utility:
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: 'bold',
                    color: `${color.text}`,
                    marginLeft: 5,
                  }}>
                  {element.utility_name.length < 20
                    ? `${element.utility_name}`
                    : `${element.utility_name.substring(0, 20)}...`}
                </Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  style={{
                    fontSize: 14,
                  }}>
                  Time Period:
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: 'bold',
                    color: `${color.text}`,
                    marginLeft: 5,
                  }}>
                  {element.period}
                </Text>
              </View>
            </View>
            <View style={{alignItems: 'center'}}>
              <Button
                onPress={() => this.update_showDialog(element.id)}
                style={{
                  width: 55,
                  height: 25,
                  backgroundColor: 'green',
                  justifyContent: 'center',
                }}>
                <Text style={{color: 'white'}}>Update</Text>
              </Button>
              <Button
                onPress={() => this.delete_utility(element.id)}
                style={{
                  width: 55,
                  height: 25,
                  backgroundColor: `${color.newbtn}`,
                  marginTop: '10%',
                  justifyContent: 'center',
                }}>
                <Text style={{color: 'white'}}>Delete</Text>
              </Button>
            </View>
          </View>
        );
      });
    }
  };

  render() {
    console.log(this.state.selectedPro, 'data id');
    return (
      <Container>
        {this.state.isloading ? (
          <Spinner
            type="Circle"
            color={color.header3_color}
            style={{alignSelf: 'center'}}
          />
        ) : (
          <Content style={{marginLeft: '5%', marginRight: '5%'}}>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                marginTop: '3%',
              }}>
              <Ionicons name="add-circle-sharp" size={25} color="green" />
              <Text onPress={() => this.showDialog()} style={{fontSize: 20}}>
                Add Utility
              </Text>
            </View>
            <View style={{borderBottomWidth: 1}}>
              <Picker
                mode="dialog"
                selectedValue={this.state.selectedPro}
                style={{
                  height: 50,
                  width: '102%',

                  marginBottom: '-2.5%',
                  right: '1.5%',
                }}
                itemStyle={{
                  backgroundColor: 'red',
                  borderBottomWidth: 55,
                }}
                onValueChange={(itemValue, itemIndex) =>
                  this.on_change_utility(itemValue)
                }>
                {this.map_property_list()}
              </Picker>
            </View>
            <Portal>
              <Dialog visible={this.state.visible} onDismiss={this.hideDialog}>
                <Dialog.Title>Utility</Dialog.Title>

                <Dialog.Content>
                  <Text style={{fontWeight: 'bold'}}>Select Property:</Text>
                  <View style={{borderBottomWidth: 1, marginBottom: '5%'}}>
                    <Picker
                      mode="dialog"
                      selectedValue={this.state.dialog_selectedpro}
                      style={{
                        height: 50,
                        width: '102%',

                        marginBottom: '-2.5%',
                        right: '1.5%',
                      }}
                      itemStyle={{
                        backgroundColor: 'red',
                        borderBottomWidth: 55,
                      }}
                      onValueChange={(itemValue, itemIndex) =>
                        // .onChange_api(itemValue)
                        this.setState({dialog_selectedpro: itemValue})
                      }>
                      {this.property_picker_list()}
                    </Picker>
                  </View>
                  {/* //--------------------------------------------------------------->PICKER DIALOG */}
                  <View>
                    <Text style={{fontWeight: 'bold'}}>Utility Name:</Text>
                    <TextInput
                      onChangeText={(text) => this.setState({name: text})}
                      style={{borderBottomWidth: 1}}
                      placeholder="Enter Your Utility"
                    />

                    <Text style={{fontWeight: 'bold', marginTop: '2%'}}>
                      Days:
                    </Text>
                    <TextInput
                      onChangeText={(text) => this.setState({date: text})}
                      style={{borderBottomWidth: 1}}
                    />
                  </View>
                </Dialog.Content>
                <Dialog.Actions>
                  <Button
                    onPress={() => this.add_utility()}
                    style={{
                      width: '20%',
                      marginRight: '3%',
                      height: '90%',
                      backgroundColor: `${color.theme_Color}`,
                    }}>
                    <Text style={{color: 'white', marginLeft: '25%'}}>Add</Text>
                  </Button>
                  <Button
                    style={{
                      width: '20%',
                      height: '90%',
                      backgroundColor: `${color.newbtn}`,
                    }}
                    onPress={this.hideDialog}>
                    <Text style={{color: 'white', marginLeft: '15%'}}>
                      Cancel
                    </Text>
                  </Button>
                </Dialog.Actions>
              </Dialog>
            </Portal>
            {this.state.loader ? (
              <View style={{flex: 1, justifyContent: 'center'}}>
                <ActivityIndicator color={color.theme_Color} />
              </View>
            ) : (
              this.map_utility_list()
            )}
            {/* //________________________________________________________________________----UPDATE DIALOG */}
            <Portal>
              <Dialog
                visible={this.state.update_visible}
                onDismiss={this.update_hideDialog}>
                <Dialog.Title>Update Utility</Dialog.Title>

                <Dialog.Content>
                  {/* <Text style={{fontWeight: 'bold'}}>Select Property:</Text>
                  <View style={{borderBottomWidth: 1, marginBottom: '5%'}}>
                    <Picker
                      mode="dialog"
                      selectedValue={this.state.update_selectedpro}
                      style={{
                        height: 50,
                        width: '102%',

                        marginBottom: '-2.5%',
                        right: '1.5%',
                      }}
                      itemStyle={{
                        backgroundColor: 'red',
                        borderBottomWidth: 55,
                      }}
                      onValueChange={(itemValue, itemIndex) =>
                        // .onChange_api(itemValue)
                        this.setState({update_selectedpro: itemValue})
                      }>
                      {this.update_property_picker_list()}
                    </Picker>
                  </View> */}
                  {/* //--------------------------------------------------------------->PICKER DIALOG */}
                  <View>
                    <Text style={{fontWeight: 'bold'}}>Utility Name:</Text>
                    <TextInput
                      onChangeText={(text) =>
                        this.setState({update_name: text})
                      }
                      style={{borderBottomWidth: 1}}
                      placeholder="Enter Your Utility"
                    />

                    <Text style={{fontWeight: 'bold', marginTop: '2%'}}>
                      Days:
                    </Text>
                    <TextInput
                      onChangeText={(text) =>
                        this.setState({update_date: text})
                      }
                      style={{borderBottomWidth: 1}}
                    />
                  </View>
                </Dialog.Content>
                <Dialog.Actions>
                  <Button
                    onPress={() => this.update_utility()}
                    style={{
                      width: '20%',
                      marginRight: '3%',
                      height: '90%',
                      backgroundColor: '#4c7e80',
                    }}>
                    <Text style={{color: 'white', marginLeft: '15%'}}>
                      Update
                    </Text>
                  </Button>
                  <Button
                    style={{
                      width: '20%',
                      height: '90%',
                      backgroundColor: '#6fa8aa',
                    }}
                    onPress={this.update_hideDialog}>
                    <Text style={{color: 'white', marginLeft: '15%'}}>
                      Cancel
                    </Text>
                  </Button>
                </Dialog.Actions>
              </Dialog>
            </Portal>
          </Content>
        )}
      </Container>
    );
  }
}
