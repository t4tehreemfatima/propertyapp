import MyHeader from '../Constant/MyHeader';
import React from 'react';
import {
  Container,
  Content,
  Title,
  Tab,
  Tabs,
  Thumbnail,
  Right,
  Button,
} from 'native-base';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import * as color from '../Constant/Colors';
import {View, Text} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import {Divider} from 'react-native-elements';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {Card} from 'react-native-paper';
import {Paragraph, Dialog, Portal} from 'react-native-paper';
export default class Utility_landlord extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      data: '',
      data: '2',
      list: '',
      isloading: true,
      visible: false,
      loader: true,
    };
  }
  async utility() {
    this.setState({isloading: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.utility_select(
        this.props.kuchb,
        parse.token,
      );
      console.log(response, '============>>>>>rses');
      this.setState({data: response, isloading: false});
    } catch (error) {
      console.log(error, 'err');
    }
  }
  async view(_id) {
    this.showDialog();
    this.setState({loader: true});
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response = await this.api.view_utility(
      this.props.kuchb,
      _id,
      parse.token,
    );
    this.setState({list: response, loader: false});
    console.log(response, 'rs-------e');
  }
  componentDidMount() {
    const {navigation} = this.props;
    this.focusListener = navigation.addListener('focus', () => {
      console.log('cosolllllll');
      this.utility();
    });
    this.utility();
  }

  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }
  showDialog = () => this.setState({visible: true});
  hideDialog = () => this.setState({visible: false});
  map_utilityPAid_list = () => {
    if (this.state.data.data[0] == null) {
      return (
        <View>
          <Text
            style={{
              fontSize: 20,
              alignSelf: 'center',
              marginTop: '75%',
              color: '#D0D0D0',
            }}>
            No Utility Paid
          </Text>
        </View>
      );
    } else {
      return this.state.data.data.map((element, i) => {
        // console.log(element, 'all list elelmet--------------<<<<');
        return (
          <Card>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginTop: '5%',
                marginLeft: '3%',
                marginRight: '3%',
              }}>
              <View>
                <Text style={{fontWeight: 'bold', fontSize: 15}}>
                  {element.property_name}
                </Text>
                <Text>
                  Name:
                  <Text style={{color: 'green'}}>{element.utility_name}</Text>
                </Text>
                <Text style={{fontSize: 15}}>Time Period:{element.period}</Text>
              </View>
              <Button
                style={{
                  width: 60,
                  height: 30,
                  backgroundColor: `${color.header3_color}`,
                  justifyContent: 'center',
                  marginTop: '5%',
                }}
                onPress={() => this.view(element.id)}>
                <Text style={{color: 'white'}}>View</Text>
              </Button>
            </View>
            <Divider
              style={{height: 1, backgroundColor: 'black', marginTop: '2%'}}
            />
          </Card>
        );
      });
    }
  };

  map_tendent_list = () => {
    if (this.state.loader) {
      return (
        <View>
          <Spinner
            type="Circle"
            color={color.header3_color}
            style={{alignSelf: 'center'}}
          />
        </View>
      );
    } else {
      if (this.state.list.data[0] == null) {
        return (
          <View style={{alignItems: 'center', marginTop: '8%'}}>
            <Entypo name="cross" size={35} color="#C5C5C5" />
            <Text style={{color: '#C5C5C5'}}>Not Paid</Text>
          </View>
        );
      } else {
        return this.state.list.data.map((element, i) => {
          console.log(element, 'all list elelmet--------------<<<<');
          return (
            <View key={i}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View>
                  <Text
                    style={{
                      fontSize: 17,
                      fontWeight: 'bold',
                      color: `${color.text}`,
                    }}>
                    Tenant Name
                  </Text>
                </View>
                <View style={{marginRight: '1%'}}>
                  <Text
                    style={{
                      fontSize: 17,
                      fontWeight: 'bold',
                      color: `${color.text}`,
                    }}>
                    Paid
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginTop: '3%',
                }}>
                <View
                  style={{
                    borderLeftWidth: 1,
                    padding: '1%',
                    justifyContent: 'center',
                    width: '44%',
                  }}>
                  <Text>{element.user_name}</Text>
                </View>

                <View
                  style={{
                    borderLeftWidth: 1,
                    padding: '1%',
                    width: '17%',
                  }}>
                  <AntDesign name="check" size={35} color="green" />
                  {/*
                <Entypo name="cross" size={30} /> */}
                </View>
              </View>
              <Divider style={{marginTop: '5%'}} />
            </View>
          );
        });
      }
    }
  };
  render() {
    console.log(this.state.list, 'deeeatadtadata');
    return (
      <Container>
        <Content style={{marginLeft: '5%', marginRight: '5%'}}>
          {/* //---------------------------------------------------------------------------------------MAP */}
          <View>
            {this.state.isloading ? (
              <Spinner
                type="Circle"
                color={color.header3_color}
                style={{alignSelf: 'center', marginTop: '75%'}}
              />
            ) : (
              <View>{this.map_utilityPAid_list()}</View>
            )}
          </View>
          {/* //================== */}
          <Portal>
            <Dialog visible={this.state.visible} onDismiss={this.hideDialog}>
              <Dialog.Title>Utility</Dialog.Title>

              <Dialog.Content>
                <View>
                  {this.state.loader == true ? (
                    <Spinner
                      type="Circle"
                      color={color.header3_color}
                      style={{alignSelf: 'center'}}
                    />
                  ) : (
                    this.map_tendent_list()
                  )}
                </View>
              </Dialog.Content>
              <Dialog.Actions>
                {/* <Button
                  style={{
                    width: '20%',
                    marginRight: '3%',
                    height: '90%',
                    backgroundColor: '#4c7e80',
                  }}
                  onPress={() => this.add_task()}>
                  <Text style={{color: 'white', marginLeft: '25%'}}>Add</Text>
                </Button> */}
                <Button
                  style={{
                    width: '20%',
                    height: '90%',
                    backgroundColor: `${color.newbtn}`,
                  }}
                  onPress={this.hideDialog}>
                  <Text style={{color: 'white', marginLeft: '15%'}}>
                    Cancel
                  </Text>
                </Button>
              </Dialog.Actions>
            </Dialog>
          </Portal>
        </Content>
      </Container>
    );
  }
}
