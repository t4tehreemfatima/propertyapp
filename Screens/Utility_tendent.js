import MyHeader from '../Constant/MyHeader';
import React from 'react';
import {
  Container,
  Content,
  Title,
  Tab,
  Tabs,
  Thumbnail,
  Right,
  Button,
} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import * as color from '../Constant/Colors';
import {View, Text} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import {Divider} from 'react-native-elements';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {Card} from 'react-native-paper';
import {Paragraph, Dialog, Portal} from 'react-native-paper';
import {TextInput} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import {ActivityIndicator} from 'react-native';
import {Image} from 'react-native';

export default class Utility_tendent extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      data: '',
      isloading: true,
      visible: false,
      selectpaiddata: '',
      loader: true,
      property: '',
      selectedproperty: '',
      utility_data: '',
      selectedUtility: '',
      paid: '',
      total_amount: '',
      utility_loader: false,
    };
  }

  async utility_onchange(value) {
    this.setState({selectedproperty: value});
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response3 = await this.api.picker_utility_select(
      value,
      // '6',
      parse.token,
    );
    console.log(
      response3,
      'responsw --------------------------+++++----------/////',
    );
    this.setState({
      utility_data: response3,
      selectedUtility: response3.data[0].id,
      selectedproperty: value,
      loader: false,
    });
    // console.log(
    //   value,
    //   'idididid-----------------------------------------?????i',
    // );
  }
  async selectpaid_list() {
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      console.log(parse.token, 'token t\\', parse.id);
      const response = await this.api.utilitypaid_select(parse.token);
      const response2 = await this.api.tenant_home_list(parse.id, parse.token);

      if (response2.data[0] != null) {
        this.setState({
          selectedproperty: response2.data[0].property_id,
        });
      } else {
        this.setState({isloading: false});
      }

      //3rd api
      const response3 = await this.api.picker_utility_select(
        this.state.selectedproperty,
        // '64',
        parse.token,
      );
      console.log(
        response3,
        '--------22-------3 hai ya bhaee-----------------------------',
      );
      if (response3.data[0] != null) {
        this.setState({
          selectedUtility: response3.data[0].id,
        });
      } else {
        this.setState({isloading: false});
      }

      this.setState({
        selectpaiddata: response,
        property: response2,
        utility_data: response3,
        loader: false,
      });

      // console.log(this.state.utility_data, '-----3 ka----------->>>>respseeee');
    } catch (err) {
      console.log('Error: ', err);
    }
  }

  async add_utility_btn() {
    this.setState({utility_loader: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.utility_add_tendent(
        // '34',
        this.state.selectedUtility,
        this.state.selectedproperty,
        this.state.total_amount,
        this.state.paid,
        parse.id,
        parse.token,
      );
      if (response.data[0].utility_paid == 'added') {
        alert('Added');
        this.selectpaid_list();
        this.hideDialog();
        this.setState({utility_loader: false});
      } else {
        alert(response.data[0].utility_paid);
        this.selectpaid_list();
        this.hideDialog();
        this.setState({utility_loader: false});
      }
      console.log(
        response,
        this.state.selectedUtility,
        this.state.selectedproperty,
        this.state.total_amount,
        this.state.paid,

        '-----kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk',
      );
      // console.log(response, '---------------->>>>respseeee');
    } catch (err) {
      this.setState({utility_loader: false});

      console.log('Error: ', err);
    }
  }
  showDialog = () => this.setState({visible: true});
  hideDialog = () => this.setState({visible: false});
  property_picker_list = () => {
    return this.state.property.data.map((element, i) => {
      // console.log(element, '------ele-----------eoe----');
      return (
        <Picker.Item
          label={element.property_name}
          value={element.property_id}
        />
      );
    });
  };
  utility_picker_list = () => {
    return this.state.utility_data.data.map((element, i) => {
      console.log(element, '---------UTILITY-----li----------');
      return <Picker.Item label={element.utility_name} value={element.id} />;
    });
  };

  select_paid_list = () => {
    if (this.state.selectpaiddata.data[0] == null) {
      return (
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: '55%',
          }}>
          <Image source={require('../assets/u.png')} />
          <Text style={{color: 'grey'}}>No Utility Paid!</Text>
        </View>
      );
    } else {
      return this.state.selectpaiddata.data.map((element, i) => {
        // console.log(element, 'all list elelmet--------------<<<<');
        return (
          <View
            style={{
              padding: '3%',
              elevation: 1,
              borderWidth: 0.1,
              marginTop: '5%',
              marginBottom: '1%',
            }}>
            <View>
              <View style={{flexDirection: 'row'}}>
                <Text>Utility: </Text>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: 'bold',
                  }}>
                  {element.utility_name}
                </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text>Total Amount: </Text>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: 'bold',
                  }}>
                  {element.total_amount}
                </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text>Paid: </Text>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: 'bold',
                  }}>
                  {element.paid_by_user}
                </Text>
              </View>
            </View>
          </View>
        );
      });
    }
  };
  componentDidMount() {
    const {navigation} = this.props;
    this.focusListener = navigation.addListener('focus', async () => {
      // console.log('cosolllllll');
      this.selectpaid_list();
    });
    this.selectpaid_list();
  }

  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }

  render() {
    console.log(
      this.state.selectedUtility,

      'se--------le+++++++ctb projecy d',
    );
    return (
      <Container>
        <Content style={{marginLeft: '5%', marginRight: '5%'}}>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              marginTop: '3%',
            }}>
            <Ionicons
              onPress={() => this.showDialog()}
              name="add-circle-sharp"
              size={25}
              color="green"
            />
            <Text onPress={() => this.showDialog()} style={{fontSize: 20}}>
              Pay Utility
            </Text>
          </View>
          <View>
            {this.state.loader ? (
              <Spinner
                type="Circle"
                color={color.header3_color}
                style={{alignSelf: 'center'}}
              />
            ) : (
              this.select_paid_list()
            )}
          </View>

          {/* //---------------------------------------------------------------------------------------MAP */}

          {/* //================== */}
          <Portal>
            <Dialog visible={this.state.visible} onDismiss={this.hideDialog}>
              <Dialog.Title>Utility</Dialog.Title>

              <Dialog.Content>
                <Text style={{fontWeight: 'bold'}}>Property Name:</Text>
                <View style={{borderBottomWidth: 1}}>
                  <Picker
                    mode="dialog"
                    selectedValue={this.state.selectedproperty}
                    style={{
                      height: 50,
                      width: '102%',

                      marginBottom: '-2.5%',
                      right: '1.5%',
                    }}
                    itemStyle={{
                      backgroundColor: 'red',
                      borderBottomWidth: 55,
                    }}
                    onValueChange={(itemValue, itemIndex) =>
                      // .onChange_api(itemValue)

                      this.utility_onchange(itemValue)
                    }>
                    {this.state.loader ? (
                      <ActivityIndicator />
                    ) : (
                      this.property_picker_list()
                    )}
                  </Picker>
                </View>
                <Text style={{fontWeight: 'bold', marginTop: '3%'}}>
                  Utility Name:
                </Text>
                <View style={{borderBottomWidth: 1}}>
                  <Picker
                    mode="dialog"
                    selectedValue={this.state.selectedUtility}
                    style={{
                      height: 50,
                      width: '102%',

                      marginBottom: '-2.5%',
                      right: '1.5%',
                    }}
                    itemStyle={{
                      backgroundColor: 'red',
                      borderBottomWidth: 55,
                    }}
                    onValueChange={(itemValue, itemIndex) =>
                      // .onChange_api(itemValue)
                      this.setState({selectedUtility: itemValue})
                    }>
                    {this.state.loader ? (
                      <ActivityIndicator />
                    ) : (
                      this.utility_picker_list()
                    )}
                  </Picker>
                </View>
                <View>
                  <Text style={{fontWeight: 'bold', marginTop: '5%'}}>
                    Total Amount:
                  </Text>
                  <TextInput
                    keyboardType="numeric"
                    onChangeText={(text) => this.setState({total_amount: text})}
                    style={{borderBottomWidth: 1}}
                    placeholder="Enter Your Amount"
                  />
                  <Text style={{fontWeight: 'bold', marginTop: '5%'}}>
                    Paid Amount:
                  </Text>
                  <TextInput
                    keyboardType="numeric"
                    onChangeText={(text) => this.setState({paid: text})}
                    style={{borderBottomWidth: 1}}
                    placeholder="Enter Your Paid Amount"
                  />
                </View>
              </Dialog.Content>
              <Dialog.Actions>
                <Button
                  onPress={() => this.add_utility_btn()}
                  style={{
                    width: '20%',
                    marginRight: '3%',
                    height: '90%',
                    backgroundColor: `${color.theme_Color}`,
                  }}>
                  {this.state.utility_loader ? (
                    <ActivityIndicator />
                  ) : (
                    <Text style={{color: 'white', marginLeft: '25%'}}>Add</Text>
                  )}
                </Button>
                <Button
                  style={{
                    width: '20%',
                    height: '90%',
                    backgroundColor: `${color.newbtn}`,
                  }}
                  onPress={this.hideDialog}>
                  <Text style={{color: 'white', marginLeft: '15%'}}>
                    Cancel
                  </Text>
                </Button>
              </Dialog.Actions>
            </Dialog>
          </Portal>
        </Content>
      </Container>
    );
  }
}
