import {Button, Container, Content, Input, Title, Toast} from 'native-base';
import MyHeader from '../Constant/MyHeader';
import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import API from '../Constant/API';
import * as color from '../Constant/Colors';
import {View, Text, Image} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {ActivityIndicator} from 'react-native-paper';
import {TouchableOpacity, BackHandler} from 'react-native';
const backAction = () => {
  BackHandler.exitApp();
  return true;
};

function HardwareBackBtn() {
  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', backAction);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
    }, []),
  );

  return null;
}

export default class Verification extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      code: '',
      is_resend_loader: false,
      loader: false,
      notify: false,
    };
  }
  async Verify(_bool) {
    this.setState({is_resend_loader: _bool});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.verify(parse.token);
      console.log(response, '-----VERIFICATION ');

      if (response.status) {
        this.setState({notify: true});
        Toast.show({
          text: 'We have sent you a code in you email',
          buttonText: 'Okay',
          duration: 5000,
        });
        this.setState({is_resend_loader: false});
      } else {
        Toast.show({
          text: `${response.data[0].error}`,
          buttonText: 'Okay',
          duration: 5000,
        });
      }
    } catch (error) {
      this.setState({is_resend_loader: false});
      console.log(error, 'ERR');
    }
  }
  componentDidMount() {
    this.Verify(false);
  }
  async code_send() {
    this.setState({loader: true});
    try {
      const UsreData = await AsyncStorage.getItem('user_data');
      const parse = JSON.parse(UsreData);
      const response = await this.api.code(this.state.code, parse.token);
      console.log(response, '-----VERIFICAnsdnsjndTION ');
      this.setState({loader: false});
      if (response.status) {
        if (this.props.route.params.sub_val == 0) {
          this.props.navigation.navigate('Subscriptionscr', {
            from: 'verification',
          });
        } else {
          this.props.navigation.navigate('MainStack');
        }
      } else {
        this.setState({loader: false});

        alert('try again');
      }
    } catch (error) {
      console.log(error, 'ERR');
    }
  }

  render() {
    console.log(this.state.code);
    return (
      <Container>
        <MyHeader
          headername="5"
          name="Verification"
          //   rightIconPress={() => alert('ACCOUNT PRESS')}
          //   rightIconName="settings"
          //   onPressIcon={() => this.props.navigation.goBack()}
        />
        <HardwareBackBtn />

        <Content
          style={{
            alignSelf: 'center',
            marginTop: '40%',
          }}>
          <View style={{alignItems: 'center'}}>
            <Image
              style={{height: 130, width: '45%'}}
              source={require('../assets/we.png')}
            />
          </View>
          <Text
            style={{fontWeight: 'bold', marginBottom: '2%', marginTop: '4%'}}>
            Enter Code:
          </Text>

          <View
            style={{
              borderWidth: 0.6,
              width: 280,
              borderRadius: 3,
              borderColor: 'grey',
            }}>
            <Input
              placeholder="Code...."
              onChangeText={(txt) => this.setState({code: txt})}
              keyboardType="numeric"
            />
          </View>
          <TouchableOpacity onPress={() => this.Verify(true)}>
            {this.state.notify == true ? (
              <Text style={{color: 'grey'}}>
                We have sent you a code in you email
              </Text>
            ) : (
              <View></View>
            )}
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                alignSelf: 'flex-end',
                marginTop: 5,
              }}>
              {this.state.is_resend_loader ? (
                <ActivityIndicator color="black" />
              ) : (
                <MaterialCommunityIcons name="send-outline" size={20} />
              )}
              <Text style={{marginLeft: 5}}>Resend code</Text>
            </View>
          </TouchableOpacity>
          <Button
            onPress={() => this.code_send()}
            style={{
              backgroundColor: `${color.newbtn}`,
              alignSelf: 'center',
              width: 100,
              marginTop: '5%',
              justifyContent: 'center',
            }}>
            {this.state.loader ? (
              <ActivityIndicator color="white" />
            ) : (
              <Text style={{color: 'white'}}>VERIFY</Text>
            )}
          </Button>
        </Content>
      </Container>
    );
  }
}
