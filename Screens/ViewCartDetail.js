import {Container, Content, Title, Tab, Tabs, Thumbnail} from 'native-base';
import MyHeader from '../Constant/MyHeader';
import React from 'react';
import {Button, Avatar} from 'react-native-paper';
import Share from 'react-native-share';
import {Platform} from 'react-native';
import * as color from '../Constant/Colors';
import {SliderBox} from 'react-native-image-slider-box';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {View, Text, Image} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Spinner from 'react-native-spinkit';
//import {ESLint} from 'eslint';
export default class ViewCartDetails extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      id: this.props.route.params.key,
      responseData: '',
      isLoading: true,
      images: [],
      loader: true,
      responseData2: '',
      user_type: '',
      res_loader: false,
    };
  }
  imagesFuntion = (images) => {
    let tmpimages = [];
    images.forEach((element) => {
      // console.log(`${color.link}${element.path}`, 'imagess');
      tmpimages.push(`${color.link}${element.path}`);
      this.setState({images: tmpimages});
    });
  };
  async property_details(id) {
    console.log(id, 'id---');
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response = await this.api.detailofProperty(id, parse.token);

    this.imagesFuntion(response.property_data[0].images);
    this.setState({
      responseData: response,
      user_type: parse.user_type,
      isLoading: false,
    });
    console.log(
      parse.id,
      parse.token,
      response,
      'ccccccchghgghg55555c_________cccccccccccc',
    );
    this.setState({loader: false});

    // console.log(this.state.responseData, 'data by id pass');
  }
  //----------------------------------------------Apply for property
  async apply_for_property() {
    this.setState({res_loader: true});
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response = await this.api.applyProperty(
      parse.id,
      this.state.responseData.property_data[0].id,
      parse.token,
    );
    if (response.data.error) {
      alert(response.data.error);
    } else {
      alert('Applied');
      this.props.navigation.navigate('Home');
    }
  }
  componentDidMount() {
    const {navigation} = this.props;
    this.setState({loader: true, isLoading: true});
    this.focusListener = navigation.addListener('focus', () => {
      console.log('cosolllllll');
      console.log(
        this.state.loader,
        this.state.loader,
        '**********************loader',
      );
      console.log(this.props.route.params.key, 'id refreshh');
      this.property_details(this.props.route.params.key);
    });
    this.focusListenerr = navigation.addListener('blur', () => {
      console.log('un cosolllllll');
      this.setState({loader: true, isLoading: true});
    });
    this.property_details(this.props.route.params.key);
  }
  componentWillUnmount() {
    // Remove the event listener
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
    }
  }
  //--------------------------------------------feature Map funtion
  featureMap() {
    return this.state.responseData.property_data[0].features.map(
      (element, i) => {
        return (
          <View key={i}>
            <View
              style={{
                flexDirection: 'row',
                marginTop: '3%',
                marginLeft: '5%',
                alignItems: 'center',
              }}>
              <Ionicons
                name="alert-circle-sharp"
                size={10}
                color={color.icon_color}
              />
              <Text style={{fontSize: 16}}> {element.feature}</Text>
            </View>
          </View>
        );
      },
    );
  }
  sharelink() {
    const options = {
      message: '',
      url: `eddd`,
    };
    Share.open(options)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        err && console.log(err);
      });
  }
  render() {
    if (this.state.isLoading) {
      console.log(this.state.loader, 'my loader');
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: '1%',
          }}>
          <Spinner type="FadingCircle" color={color.header3_color} />
        </View>
      );
    } else {
      if (this.state.loader) {
        // console.log('if-------------------------------------------0');
        return (
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: '35%',
            }}>
            <Spinner type="Circle" color={color.header3_color} />
          </View>
        );
      } else {
        // console.log('else---------------------------------------');
        return (
          <Container>
            <Content>
              {this.state.responseData.property_data[0].images.length == 0 ? (
                <View>
                  <Image
                    style={{
                      resizeMode: 'contain',
                      height: 200,
                      width: 200,
                      alignSelf: 'center',
                    }}
                    source={require('../assets/myhome.jpg')}
                  />
                </View>
              ) : (
                <View>
                  <SliderBox
                    images={this.state.images}
                    autoplay
                    circleLoop
                    resizeMethod={'resize'}
                    resizeMode={'cover'}
                    // ImageComponentStyle={{width: '90%'}}
                    sliderBoxHeight={200}
                  />
                </View>
              )}
              <View
                style={{
                  backgroundColor: `${color.theme_Color}`,
                  padding: '5%',
                }}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: `${color.text}`,
                    fontSize: 19,
                  }}>
                  {this.state.responseData.property_data[0].name}
                </Text>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Text
                    style={{
                      fontSize: 17,
                      fontWeight: 'bold',
                      marginRight: '2%',
                    }}>
                    Zip Code:
                  </Text>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      color: `${color.text}`,
                      fontSize: 17,
                    }}>
                    {
                      // prettier-ignore
                      this.state.responseData.property_data[0].zip_code
                    }
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginLeft: '0%',
                  }}>
                  <Text
                    style={{
                      marginRight: '3%',
                      fontWeight: 'bold',
                      fontSize: 17,
                    }}>
                    Address:
                  </Text>
                  <Text style={{fontSize: 16, marginLeft: -5}}>
                    {this.state.responseData.property_data[0].street} ,
                    {this.state.responseData.property_data[0].city}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginLeft: '0%',
                  }}>
                  <Text
                    style={{
                      marginRight: '3%',
                      fontWeight: 'bold',
                      fontSize: 17,
                    }}>
                    State:
                  </Text>
                  <Text style={{fontSize: 16, marginLeft: -5}}>
                    {this.state.responseData.property_data[0].state}
                  </Text>
                </View>

                <Text style={{fontSize: 16}}>
                  Year of build:
                  {this.state.responseData.property_data[0].year_build}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginTop: '5%',
                    width: '80%',
                    marginLeft: '9%',
                    marginRight: '9%',
                  }}>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Ionicons
                      name="ios-bed-outline"
                      size={22}
                      color={color.icon_color}
                    />
                    <Text style={{fontWeight: 'bold'}}>
                      {this.state.responseData.property_data[0].bed_rooms}
                    </Text>
                  </View>
                  {/* 2nd */}
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <FontAwesome
                      name="bathtub"
                      size={20}
                      color={color.icon_color}
                    />
                    <Text style={{fontWeight: 'bold'}}>
                      {' '}
                      {this.state.responseData.property_data[0].bath_rooms}
                    </Text>
                  </View>
                  {/* 3rd */}
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <MaterialCommunityIcons
                      name="toilet"
                      size={22}
                      color={color.icon_color}
                    />
                    <Text style={{fontWeight: 'bold'}}>
                      {this.state.responseData.property_data[0].toilets}
                    </Text>
                  </View>
                  <View style={{flexDirection: 'column', alignItems: 'center'}}>
                    <Text style={{fontWeight: 'bold', fontSize: 23}}>
                      {this.state.responseData.property_data[0].rent}
                    </Text>
                    <Text style={{marginTop: '-5%'}}>
                      {this.state.responseData.property_data[0].rent_days} Days
                    </Text>
                  </View>
                </View>
              </View>
              {/* <View
                style={{
                  alignItems: 'center',
                  marginTop: '5%',
                  borderBottomWidth: 0.5,
                }}>
                <Ionicons
                  onPress={() => this.sharelink()}
                  name="ios-share-social"
                  size={25}
                  color={color.icon_color}
                />
                <Text>Share</Text>
              </View> */}
              <View>
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 17,
                    marginLeft: '2%',
                    marginRight: '2%',
                    marginTop: '5%',
                  }}>
                  FEATURES
                </Text>
              </View>
              <View>{this.featureMap()}</View>

              <View
                style={{marginLeft: '2%', marginRight: '2%', marginTop: '3%'}}>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>
                  DESCRIPTION
                </Text>
                <View
                  style={{
                    marginLeft: '2%',
                    marginRight: '2%',
                  }}>
                  <Text>
                    {this.state.responseData.property_data[0].description}
                  </Text>
                </View>
              </View>

              {/* ---------------------------------------------------------------------------------------- */}

              {/* ..0000000000000000000------------------------------------------------------------------------ */}
              <View
                style={{
                  backgroundColor: `${color.backgroundColor}`,
                  padding: '5%',
                  marginLeft: '5%',
                  marginRight: '5%',
                  marginBottom: '5%',
                  marginTop: '2%',
                }}>
                <Text
                  style={{
                    marginLeft: '2%',
                    fontSize: 18,
                    fontWeight: 'bold',
                    marginTop: '-5%',
                    marginBottom: '3%',
                  }}>
                  Landlord Details
                </Text>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <View style={{width: '30%'}}>
                    <Avatar.Image
                      size={70}
                      source={{
                        uri: `${color.link}${this.state.responseData.property_data[0].user_image}`,
                      }}
                    />
                  </View>

                  <View>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 17,
                        marginBottom: '-2%',
                      }}>
                      {this.state.responseData.property_data[0].user_name}
                    </Text>

                    <Text style={{fontSize: 14, textAlign: 'center'}}>
                      {/* <MaterialCommunityIcons name="email-outline" /> */}
                      {/* <Text style={{color: 'blue'}}>Email: </Text> */}

                      {this.state.responseData.property_data[0].user_email}
                    </Text>
                    {this.state.user_type == '1' ? (
                      <View></View>
                    ) : this.props.route.params.status == '' ? (
                      <Button
                        mode="contained"
                        style={{
                          backgroundColor: `${color.header3_color}`,
                          marginLeft: '2%',
                          marginTop: '3%',
                        }}
                        onPress={() => this.apply_for_property()}>
                        <Text style={{fontSize: 10}}>Apply for Property</Text>
                      </Button>
                    ) : (
                      <View></View>
                    )}
                  </View>
                </View>
              </View>
            </Content>
          </Container>
        );
      }
    }
  }
}
//.....
//.....
///.....
///.......
//...........
//......................
///..........................
//..................................
////.........
//tabs component--------------------------------------responsData state-----------------------------------------------
// "rent": 50000,
// "year_build": "2021-05-29",
// "user_name": "Svbj",
// "user_email": "Xgjj",
// "user_mobile": "",
// "city": "sakhar",
// "state": "sindh",
// "street": "001",
// "zip_code": 56788,
