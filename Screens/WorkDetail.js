import {Container, Content, Title} from 'native-base';
import MyHeader from '../Constant/MyHeader';
import {Picker} from '@react-native-picker/picker';
import React from 'react';
import * as color from '../Constant/Colors';
import DocumentPicker from 'react-native-document-picker';
import {Divider} from 'react-native-paper';
import {TextInput, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {View, Text} from 'react-native';
import {Button} from 'react-native-paper';

export default class WorkDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: 'Landlord',
    };
  }
  async uploadFuntion() {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
      });
      console.log(
        res.uri,
        res.type, // mime type
        res.name,
        res.size,
      );
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }
  render() {
    return (
      <Container>
        <MyHeader
          headername="3"
          name="Work Details"
          rightName="Save"
          onPressIcon={() => this.props.navigation.goBack()}
          rightSavePress={() => alert('Save')}
        />

        <Content>
          <View style={{marginLeft: '5%', marginRight: '5%', marginTop: '6%'}}>
            <Text
              style={{fontSize: 16, fontWeight: 'bold', color: 'cadetblue'}}>
              Work History
            </Text>

            <Text style={{fontWeight: 'bold', marginTop: '5%'}}>
              Current Work Status
            </Text>
            <View
              style={{
                borderBottomWidth: 1,
                borderColor: 'cadetblue',
                marginTop: '-1%',
              }}>
              <Picker
                mode="dropdown"
                selectedValue={this.state.user}
                style={{
                  height: 50,
                  width: '102%',
                  color: 'grey',
                  marginBottom: '-2.5%',
                  right: '1.5%',
                }}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({user: itemValue})
                }>
                <Picker.Item label="Landlord" value="Landlord" />
                <Picker.Item label="Tenants" value="Tenants" />
              </Picker>
            </View>
            {/* 2nd */}
            <Text style={{fontWeight: 'bold', fontSize: 15, marginTop: '5%'}}>
              Income
            </Text>
            <TextInput
              style={{
                marginTop: '-3%',
                borderColor: 'cadetblue',
                borderBottomWidth: 1,
              }}
              keyboardType="numeric"
              placeholder="Enter Income"
            />
            {/* 3th */}
            <Text style={{fontWeight: 'bold', marginTop: '5%'}}>
              Pay Frequency
            </Text>
            <View
              style={{
                borderBottomWidth: 1,
                borderColor: 'cadetblue',
                marginTop: '-1%',
              }}>
              <Picker
                mode="dropdown"
                selectedValue={this.state.user}
                style={{
                  height: 50,
                  width: '102%',
                  color: 'grey',
                  marginBottom: '-2.5%',
                  right: '1.5%',
                }}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({user: itemValue})
                }>
                <Picker.Item label="Select Pay Freq" value="Select Pay Freq" />
                <Picker.Item label="Tenants" value="Tenants" />
              </Picker>
            </View>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 16,
                color: 'cadetblue',
                marginTop: '5%',
              }}>
              Current Employer Detail
            </Text>
            <Text
              style={{
                color: `${color.header3_color}`,
                fontWeight: 'bold',
                marginTop: '4%',
              }}>
              Work start Month and Year
            </Text>
            <Text style={{fontSize: 18, marginTop: '2%'}}>March 2021</Text>
            <Divider />
            {/* 2ndd part++++++++++++++++++ */}
            <View style={{marginTop: '5%'}}>
              <Text style={{fontWeight: 'bold', fontSize: 15}}>
                Employer Address
              </Text>
              <TextInput
                style={{
                  marginTop: '-3%',
                  borderColor: 'cadetblue',
                  borderBottomWidth: 1,
                }}
                keyboardType="email-address"
                placeholder="Enter employer address"
              />
              <Text style={{fontWeight: 'bold', fontSize: 15, marginTop: '5%'}}>
                Company Name
              </Text>
              <TextInput
                style={{
                  marginTop: '-3%',
                  borderColor: 'cadetblue',
                  borderBottomWidth: 1,
                }}
                placeholder="Enter comapany name"
              />
              <Text style={{fontWeight: 'bold', fontSize: 15, marginTop: '5%'}}>
                Company Website
              </Text>
              <TextInput
                style={{
                  marginTop: '-3%',
                  borderColor: 'cadetblue',
                  borderBottomWidth: 1,
                }}
                keyboardType="email-address"
                placeholder="Enter company website"
              />

              <Text style={{fontWeight: 'bold', fontSize: 15, marginTop: '5%'}}>
                Reference Name
              </Text>
              <TextInput
                style={{
                  marginTop: '-3%',
                  borderColor: 'cadetblue',
                  borderBottomWidth: 1,
                }}
                placeholder="Enter Reference name"
              />
              <Text style={{fontWeight: 'bold', fontSize: 15, marginTop: '5%'}}>
                Reference Phone Number
              </Text>
              <TextInput
                style={{
                  marginTop: '-3%',
                  borderColor: 'cadetblue',
                  borderBottomWidth: 1,
                }}
                placeholder="Enter Reference phone number"
              />
              <Text style={{fontWeight: 'bold', fontSize: 15, marginTop: '5%'}}>
                Reference Email Address
              </Text>
              <TextInput
                style={{
                  marginTop: '-3%',
                  borderColor: 'cadetblue',
                  borderBottomWidth: 1,
                }}
                placeholder="Enter Reference Email Address"
              />
            </View>
            <Button
              style={{
                backgroundColor: 'green',
                width: '30%',
                marginTop: '5%',
                marginBottom: '5%',
              }}
              mode="contained"
              onPress={() => this.uploadFuntion()}>
              Upload
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
