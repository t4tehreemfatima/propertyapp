import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  View,
  TextInput,
  StyleSheet,
  Image,
  Text,
  BackHandler,
  TouchableOpacity,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import API from '../Constant/API';
import * as color from '../Constant/Colors';
import ValidationComponent from 'react-native-form-validator';
import { Button } from 'native-base';
import { ActivityIndicator } from 'react-native-paper';
const backAction = () => {
  BackHandler.exitApp();
  return true;
};

function HardwareBackBtn() {
  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', backAction);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
    }, []),
  );

  return null;
}
export default class fopassChange extends Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      pass: '',
      cpass: '',
      loader: false,
    };
  }
  async Verify() {
    this.setState({ loader: true });
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    // console.log(this.state.pass, 'mailll sst');
    const response = await this.api.change_forgotPass(
      this.state.pass,
      this.props.route.params.email,
      parse.token,
    );

    if (response.status == true) {
      alert('Password Changed');
      this.setState({ loader: false });
      this.props.navigation.navigate('SignIn');
    } else {
      alert('Try Again');
    }
    this.setState({ loader: false });
    console.log(response, 'change pass------------->>>>>');
  }
  render() {
    console.log(this.props.route.params, '-----------------------------HHHHHHH');
    return (
      <View style={{ backgroundColor: 'white', height: '100%' }}>
        <HardwareBackBtn />
        <Text
          onPress={() => this.props.navigation.goBack()}
          style={{ fontSize: 18, padding: 12 }}>
          {/* <Icon name="chevron-back-outline" color={'cadetblue'} size={19} /> */}
          Change Password
        </Text>
        {/* <Image
          style={styles.stretch}
          source={{
            uri:
              'https://incubatorllc-cdn-wp.incubatorllc.com/wp-content/uploads/2020/02/online-identity-verification-blockchain.png',
          }}
        /> */}
        <View style={{ padding: '5%', marginTop: '44%' }}>
          <Text style={{ fontWeight: 'bold' }}>Enter New Password:</Text>
          <View style={{ paddingTop: '5%' }}>
            <TextInput
              onChangeText={(txt) => this.setState({ pass: txt })}
              style={{
                height: 40,
                borderColor: 'cadetblue',
                borderRightWidth: 1,
                borderBottomWidth: 1,
              }}
              placeholder="Password"
              keyboardType="email-address"
            // value={value}
            />
          </View>

          <View style={styles.btn}>
            <Button
              onPress={() => this.Verify()}
              style={{
                backgroundColor: 'black',
                width: 160,
                justifyContent: 'center',
                alignSelf: 'center',
              }}>
              {this.state.loader ? (
                <ActivityIndicator color="white" />
              ) : (
                <Text style={{ color: 'white' }}>CHANGE PASSWORD</Text>
              )}
            </Button>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  stretch: {
    width: '95%',
    height: '25%',
    resizeMode: 'stretch',
    marginTop: '13%',
  },
  card: {
    padding: 10,
    right: 2,
    borderColor: 'cadetblue',
    // borderWidth: 0.1,
    width: '90%',
    alignSelf: 'center',
    elevation: 4,
    padding: '10%',
    borderRadius: 30,
    marginTop: '6%',
    backgroundColor: 'white',
    borderWidth: 0.5,
  },
  btn: {
    paddingTop: '15%',
  },
});

{
  /* <Icon name="chevron-back-outline" color={'purple'} size={17} /> */
}
