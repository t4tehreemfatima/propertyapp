import React from 'react';
import API from '../Constant/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-spinkit';
import * as color from '../Constant/Colors';
import {Image, Alert} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MI from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Text,
  Left,
  Right,
  Icon,
  Thumbnail,
} from 'native-base';

import {View} from 'react-native-animatable';
import {ActivityIndicator} from 'react-native-paper';
export default class inbox_tenant extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      proprty_id: this.props.route.params.property_id,
      property_data: {},
      isloading: true,
      data: '',
      myid: '',
      myData: [],
    };
  }
  async tenant_list() {
    // console.log(this.props.route.params.property_id,"spppppp")
    this.setState({isloading: true});
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    console.log(parse.token, 'token ten');
    this.setState({myid: parse.id});
    // const response = await this.api.detailofProperty(
    //   this.props.route.params.property_id,
    //   //   '58',
    //   parse.token,
    //   );
    const response2 = await this.api.messages_user_list(
      this.props.route.params.property_id,
      parse.token,
    );
    console.log(response2, '====================');
    this.getlist(parse.token);

    // console.log(response,response2,"asd;alskd;laskd;laksd;las;ld")
    // alert(JSON.stringify(response2));
    this.setState({
      data: response2,
      // // property_data: response.property_data[0],
      // isloading: false,
    });
    // console.log(this.state.data, '-6------DATATArrTAT');
  }

  async getlist(token) {
    var myHeaders = new Headers();
    myHeaders.append('Accept', 'application/json');
    myHeaders.append('Authorization', `Bearer ${token}`);
    myHeaders.append('Content-Type', 'application/json');
    var raw = JSON.stringify({
      id: this.props.route.params.property_id,
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    fetch('https://tekumatics.com/api/property/select/one', requestOptions)
      .then((response) => response.json())
      .then((result) =>
        this.setState({property_data: result.property_data, isloading: false}),
      )
      .catch((error) => console.log('error', error));
  }
  componentDidMount() {
    const {navigation} = this.props;
    this.focusListener = navigation.addListener('focus', () => {
      this.tenant_list();
      console.log('screen 1');
    });
    // this.tenant_list();
  }

  componentWillUnmount() {
    if (this.focusListener != null && this.focusListener.remove) {
      this.focusListener.remove();
      console.log('screen 2');
    }
  }
  //   componentDidMount() {
  //     this.tenant_list();
  //   }
  async getconvolist(opid) {
    // console.log("alksdlkjlkj form data")
    var formdata = new FormData();
    formdata.append('userid', this.state.myid);

    var requestOptions = {
      method: 'POST',
      body: formdata,
      redirect: 'follow',
    };

    fetch('http://tekumatics.com:3000/Get_convo_list', requestOptions)
      .then((response) => response.json())
      .then((result) => this.navigation(result, opid))
      .catch((error) => console.log('error', error));
  }

  navigation(res, opid) {
    // console.log("stage1")
    this.setState({myData: res});
    res.forEach((element, i) => {
      // console.log(element.user,"ahhhhheeeeee")
      if (element.user) {
        // console.log(element,"aaa")
        if (element.user[0]['id'] == opid) {
          console.log(opid, element, 'adalkmlkmlkmd');
          this.props.navigation.navigate('MessageScreen', {
            type: element.type,
            name:
              element.type == 0
                ? element.user[0].name
                : element.property[0].property_name,
            id: element.chat_id,
            details: element,
          });
        }
        // console.log(element)
      }
    });

    // console.log(temp.user[0].id,'assdddffff')
  }

  async addtoprivate(e) {
    // console.log(this.state.myid,e,'aaaaaaaaaaaaaaaaaaaaaaa')
    try {
      const res = await this.api.Addtoprivate(this.state.myid, e, 0);
      if (res.status == true) {
        Alert.alert('Operation Succesfull', 'User Added to you chat');
        this.getconvolist(e);
        // this.props.navigation.navigate('MessageScreen', {
        //             type:element.type,
        //             name: element.type==0?element.user[0].name:element.property[0].property_name,
        //             id: element.chat_id,
        //             details:element
        //           })
      } else {
        this.getconvolist(e); // this.props.navigation.navigate('MessageScreen', {
        //             type:element.type,
        //             name: element.type==0?element.user[0].name:element.property[0].property_name,
        //             id: element.chat_id,
        //             details:element
        //           })
      }
    } catch (error) {
      console.log(error);
    }
  }

  List = () => {
    if (this.state.data.data.length == 0) {
      return (
        <Text style={{color: 'grey', alignSelf: 'center', marginTop: '30%'}}>
          No Tenant Found!
        </Text>
      );
    } else {
      return this.state.data.data.map((element, i) => {
        // console.log(element, '-----');
        return (
          <List>
            <ListItem>
              <Left>
                {element.image == null ? (
                  <Thumbnail small source={require('../assets/dp.jpg')} />
                ) : (
                  <Thumbnail
                    small
                    source={{
                      uri: `${color.link}${element.image}`,
                    }}
                  />
                )}
                <Text style={{marginLeft: '3%'}}>{element.name}</Text>
              </Left>
              <Right>
                <AntDesign
                  onPress={
                    () => this.addtoprivate(element.id)
                    // this.props.navigation.navigate('MessageScreen', {
                    //   name: element.name,
                    //   id: element.id,
                    // })
                  }
                  name="message1"
                  size={25}
                />
              </Right>
            </ListItem>
          </List>
        );
      });
    }
  };

  render() {
    // console.log(this.state.property_data,'asdlasdkalsl')
    return (
      <Container>
        {this.state.isloading ? (
          <ActivityIndicator color={color.theme_Color} />
        ) : (
          <Content>
            {this.state.property_data[0].images.length == 0 ? (
              <Image
                style={{alignSelf: 'center'}}
                resizeMode="stretch"
                source={require('../assets/home.png')}
              />
            ) : (
              <Image
                style={{
                  alignSelf: 'center',
                  width: 280,
                  height: 200,
                  marginTop: 10,
                }}
                resizeMode="stretch"
                source={{
                  uri: `${color.link}${this.state.property_data[0].images[0].path}`,
                }}
              />
            )}

            <View style={{alignItems: 'center'}}>
              <Text
                style={{
                  fontSize: 24,
                  color: `${color.theme_Color}`,
                  fontWeight: 'bold',
                }}>
                {this.state.property_data[0].name}
              </Text>
              <Text style={{fontSize: 15, color: 'grey'}}>
                {this.state.property_data[0].user_email}
              </Text>
            </View>
            {this.state.data.data.length == 0 ? (
              <Text></Text>
            ) : (
              <Text
                style={{marginTop: '9%', marginLeft: '5%', fontWeight: 'bold'}}>
                Property Tenant:
              </Text>
            )}
            {this.List()}
          </Content>
        )}
      </Container>
    );
  }
}
