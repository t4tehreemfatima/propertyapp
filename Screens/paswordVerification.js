import { Button, Container, Content, Input, Title, Toast } from 'native-base';
import MyHeader from '../Constant/MyHeader';
import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import API from '../Constant/API';
import * as color from '../Constant/Colors';
import { View, Text, Image } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons';
export default class paswordVerification extends React.Component {
  constructor(props) {
    super(props);
    this.api = API();
    this.state = {
      code: '',
      loader: false,
      resend: false,
    };
  }
  async code_send() {
    this.setState({ loader: true });
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);
    const response = await this.api.pass_code(this.state.code, parse.token);
    console.log(response, '-----pass    sdnsjndTION ');
    if (response.status == true) {
      this.props.navigation.navigate('forpassChange', { email: this.props.route.params.email });
      this.setState({ loader: false });
    } else {
      alert('Not Matched');
      this.setState({ loader: false });
    }
  }
  async resend() {
    console.log(this.props.route.params.email, '--');
    this.setState({ resend: true });
    const UsreData = await AsyncStorage.getItem('user_data');
    const parse = JSON.parse(UsreData);

    const response = await this.api.forgt_pass_veri(
      this.props.route.params.email,
      parse.token,
    );

    if (response.status == true) {
      Toast.show({
        text: `A verification code has been sent to your email.`,
        buttonText: 'Okay',
        duration: 4000,
      });
      this.setState({ resend: false });
    } else {
      Toast.show({
        text: `${response.data[0].error}`,
        buttonText: 'Okay',
        duration: 4000,
      });

      this.setState({ resend: false });
    }
    console.log(response, '-----foggot ');
  }
  render() {
    console.log(this.props.route.params.email, '------------PRAMAMMA');
    return (
      <Container>
        <MyHeader
          headername="5"
          name=" Password Verification"
        //   rightIconPress={() => alert('ACCOUNT PRESS')}
        //   rightIconName="settings"
        //   onPressIcon={() => this.props.navigation.goBack()}
        />

        <Content
          style={{
            alignSelf: 'center',
            marginTop: '60%',
            backgroundColor: `${color.backgroundColor}`,
          }}>
          <Text style={{ fontWeight: 'bold', marginBottom: '2%' }}>
            Enter Your Code:
          </Text>
          <View
            style={{
              borderWidth: 0.6,
              width: 280,
              borderRadius: 3,
              borderColor: 'grey',
            }}>
            <Input
              placeholderTextColor="grey"
              placeholder="Enter your verification code"
              onChangeText={(txt) => this.setState({ code: txt })}
            />
          </View>
          <TouchableOpacity
            onPress={() => this.resend()}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              alignSelf: 'center',
              left: 80,
            }}>
            {this.state.resend ? (
              <ActivityIndicator color="black" size={18} />
            ) : (
              <View></View>
            )}
            <Text> Resend code </Text>
            <Ionicons name="send" />
          </TouchableOpacity>
          <Button
            onPress={() => this.code_send()}
            style={{
              backgroundColor: 'black',
              width: 160,
              justifyContent: 'center',
              alignSelf: 'center',
              marginTop: '5%',
            }}>
            {this.state.loader ? (
              <ActivityIndicator color="white" />
            ) : (
              <Text style={{ color: 'white' }}>VERIFY</Text>
            )}
          </Button>

          {/* <Button
            onPress={() => this.code_send()}
            style={{
              backgroundColor: `${color.newbtn}`,
              alignSelf: 'center',
              width: 100,
              marginTop: '5%',
            }}>
            <Text style={{left: '80%', color: 'white'}}>VERIFY</Text>
          </Button>
     */}
        </Content>
      </Container>
    );
  }
}
