import React from 'react';
import SignIn from './Components/Authen/SignIn';
import SignUp from './Components/Authen/SignUp';
import MainStack from './MainStack';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import FlashPg from './Components/Authen/FlashPg';
import MessageScreen from './Screens/MessageScreen';
import Chat from './Screens/Chat';
import Verification from './Screens/Verification';
import ForgotPass from './Components/Authen/ForgotPass';
import paswordVerification from './Screens/paswordVerification';
import forpassChange from './Screens/forpassChange';
import Payment from './Screens/Payment';
import Subscriptionscr from './Screens/Subscriptionsrc';
import MySubs from './Screens/MySubs';
import BottomTab from './Constant/BottomTab';
import CreatePropertyForm from './Screens/CreatePropertyForm';
import Agree from './Screens/Agree';

const Stack = createStackNavigator();
export default class StackNavigator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {


    };
  }
  render() {
    console.log(this.props, 'checkk------------------');
    return (
      <NavigationContainer>
        <Stack.Navigator
          headerMode="none"
          initialRouteName={this.props.check == '' ? 'SignIn' : 'MainStack'}>
          <Stack.Screen name="SignIn" component={SignIn} />
          <Stack.Screen name="SignUp" component={SignUp} />
          <Stack.Screen name="Verification" component={Verification} />
          <Stack.Screen name="Subscriptionscr" component={Subscriptionscr} />
          <Stack.Screen name="MySubs" component={MySubs} />
          <Stack.Screen name="Home" component={BottomTab} />
          <Stack.Screen name="Agree" component={Agree} />

          <Stack.Screen
            name="CreatePropertyForm"
            component={CreatePropertyForm}
          />

          <Stack.Screen name="MessageScreen" component={MessageScreen} />
          <Stack.Screen name="Messages" component={Chat} />
          <Stack.Screen name="ForgotPass" component={ForgotPass} />
          <Stack.Screen name="forpassChange" component={forpassChange} />
          <Stack.Screen
            name="Payment"
            component={Payment}
            navigation={this.props.navigation}
          />

          <Stack.Screen
            name="paswordVerification"
            component={paswordVerification}
          />

          <Stack.Screen name="MainStack" component={MainStack} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
