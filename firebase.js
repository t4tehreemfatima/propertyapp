import firebase from 'firebase';

var config = {
  apiKey: 'AIzaSyCGx_U4B9nSdwkQ93RM1_kXkVMO7JA2TxQ',
  authDomain: 'property-f3b30.firebaseapp.com',
  projectId: 'property-f3b30',
  storageBucket: 'property-f3b30.appspot.com',
};
firebase.initializeApp(config);

export default firebase;
